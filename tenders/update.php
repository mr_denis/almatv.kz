<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>
<?

if($curl = curl_init()) {
	curl_setopt($curl, CURLOPT_URL, 'https://mp.com.kz/frontend/Widgets.action');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, "compID=11726");
	$html = curl_exec($curl);
	curl_close($curl);
}

require_once 'phpQuery/phpQuery.php';
$document = phpQuery::newDocument($html);
$div = $document->find('div.lot');

foreach ($div as $el) 
{
	$pq = pq($el); 
	
	$h2 = $pq->find('h2')->text();		
	$mpStartDate = $pq->find('.mpStartDate b.value')->text();
	$mpStopDate = $pq->find('.mpStopDate b.value')->text();
	$mpType = $pq->find('.mpType b.value')->text();
	$mpCategory = $pq->find('.mpCategory b.value')->text();
	$mpSum = $pq->find('.mpSum b.value')->text();
	$mpDescription = $pq->find('.mpDescription div.value')->text();
	
	$name = $h2;
	$pos = strpos($name, '№');
	$str = substr($name, $pos);
	$pos1 = strpos($str, ' ');
	
	$numberStr = (int)substr($str, $pos1);
	$nameStr = trim(substr($str, strlen($numberStr) + $pos1 + 1));

	$arrTendersItem = array(
		"NUMBER" => intval($numberStr),
		"NAME" => $nameStr,
		"START_DATE" => $mpStartDate,
		"STOP_DATE" => $mpStopDate,
		"TYPE" => $mpType,
		"CATEGORY" => $mpCategory,
		"SUM" => preg_replace('/[^0-9 ,]/', ' ', $mpSum),
		"PREVIEW" => $mpDescription,
	);
	$arNumber[] = intval($numberStr);
	$arResultTenders[] = $arrTendersItem;
	
}

//PR($arNumber);

if(!CModule::IncludeModule("iblock"))
	return;

function addElement($fields)
{
	global $USER;
	$el = new CIBlockElement;
	$PROP = array(
		"NUMBER" => $fields['NUMBER'], //Уникальный
		"TYPE" => $fields['TYPE'],
		"CATEGORY" => $fields['CATEGORY'],
		"SUM" => $fields['SUM'],
	);

	$arLoadProductArray = Array(
		"MODIFIED_BY"    => $USER->GetID(),
		"DATE_ACTIVE_FROM" => $fields['START_DATE'],
		"DATE_ACTIVE_TO" => $fields['STOP_DATE'],
		"IBLOCK_ID"      => 34,
		"PROPERTY_VALUES"=> $PROP,
		"NAME"           => $fields['NAME'],
		"ACTIVE"         => "Y",
		"PREVIEW_TEXT"   => $fields['PREVIEW'],
	);

	if($PRODUCT_ID = $el->Add($arLoadProductArray))
		return "New ID: ".$PRODUCT_ID;
	else
		return  "Error: ".$el->LAST_ERROR;
}

function updateElement($id, $fields)
{
	global $USER;
	$el = new CIBlockElement;
	
	$PROP = array(
		"TYPE" => $fields['TYPE'],
		"CATEGORY" => $fields['CATEGORY'],
		"SUM" => $fields['SUM'],
	);
	
	CIBlockElement::SetPropertyValuesEx($id, 34, $PROP);
	
	$arLoadProductArray = Array(
		"MODIFIED_BY"    => $USER->GetID(),
		"DATE_ACTIVE_FROM" => $fields['START_DATE'],
		"DATE_ACTIVE_TO" => $fields['STOP_DATE'],
		"IBLOCK_ID"      => 34,
		"NAME"           => $fields['NAME'],
		"ACTIVE"         => "Y",
		"PREVIEW_TEXT"   => $fields['PREVIEW'],
	);
	
	$res = $el->Update($id, $arLoadProductArray);
	return $res;
}

$arrFilter = Array("IBLOCK_ID"=>34, "ACTIVE"=>"Y", "PROPERTY_NUMBER" => $arNumber); //по номеру добавляем элемент или обновляем из активных
$res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arrFilter, false, false, array('ID', 'PROPERTY_NUMBER'));
while($ob = $res->GetNext())
{
	$arResultNumber[] = $ob['PROPERTY_NUMBER_VALUE'];
	$arResultId[$ob['PROPERTY_NUMBER_VALUE']] = $ob['ID'];
}

foreach ($arResultTenders as $key => $item)
{
	if (in_array($item['NUMBER'], $arResultNumber))
	{
		updateElement($arResultId[$item['NUMBER']], $item);
	}
	else
	{
		addElement($item);
	}
}
?>