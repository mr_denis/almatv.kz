<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пакеты программ");?>

<script src="<?=SITE_TEMPLATE_PATH?>/js/packages.js"></script>

<?
$idCity = getCity();
?>

<?
$APPLICATION->IncludeComponent(
	"alma:packages", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_ID" => "10",
		"TYPE" => PACKAGE_TYPE_ROOM,
		"CITY_ID" => $idCity,
		"TYPE_CLASS" => "flat",
		"AJAX_CONTENT" => "N",
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>