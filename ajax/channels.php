<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>

<?
$id_package = intval($_REQUEST['package_id']);

function getChannels($id_package, $id_city)
{
	if(!CModule::IncludeModule("iblock"))
		return;

	$arFilter = array(
		"IBLOCK_ID" => IBLOCK_CHANNELS,
		"ACTIVE"=>"Y",
		"PROPERTY_PACKAGES" => $id_package,
		"PROPERTY_CITY" => $id_city,
	);

	$obElement = CIBlockElement::GetByID($id_package);
	if($arElement = $obElement->GetNext())
	   $package_name = $arElement["NAME"];

	$rsSections = CIBlockSection::GetList(array(), $arFilter, true, array("ID", "NAME"), false); //Получили список разделов из инфоблока
	$rsSections->SetUrlTemplates(); //Получили строку URL для каждого из разделов (по формату из настроек инфоблока)
	while($arSection = $rsSections->GetNext())
	{
		if (!$arSection['ELEMENT_CNT'])
			continue;
		$arResult["SECTIONS"][] = $arSection;
	}

	//элементы
	$arrFilter = Array("IBLOCK_ID"=>IBLOCK_CHANNELS, "ACTIVE"=>"Y", "PROPERTY_PACKAGES" => $id_package);
	$res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arrFilter, false, false, array('ID', 'NAME', 'PREVIEW_PICTURE', 'IBLOCK_SECTION_ID'));
	while($ob = $res->GetNext())
	{
		$picture = CFile::ResizeImageGet(
			$ob["PREVIEW_PICTURE"],
			array("width" => 20, "height" =>20),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true,
			false
		);
		
		$ob["PREVIEW_PICTURE"] = $picture['src'];
		
		$groups = CIBlockElement::GetElementGroups($ob['ID'], false, array('ID'));  //выбираем все разделы у элемента
		while($ar_group = $groups->Fetch())
		{
			$ob['IBLOCK_SECTION_ID'] = $ar_group['ID'];
			$arResult['ELEMENTS'][] = $ob;
		}
	}
	//PR($arResult["ELEMENTS"]);
	//формируем общий массив
	foreach ($arResult["SECTIONS"] as $key => $section)
	{
		foreach ($arResult['ELEMENTS'] as $element)
		{
			if ($element['IBLOCK_SECTION_ID'] == $section['ID'])
			{
				$arResult["SECTIONS"][$key]["ELEMENTS"][] = $element;
			}
		}
	}

	$arResult['NAME'] = $package_name;
	
	return $arResult;
}

if (isset($id_package) && ($id_package > 0)) {
	
	if (isset($_REQUEST['city_id']) && (!empty($_REQUEST['city_id'])))
		$id_city = intval($_REQUEST['city_id']);
	else
		$id_city = $_COOKIE['City'];

	
	
	$obCache = new CPHPCache; 
	$time = CACHE_TIME * 60 * 60;
	$cacheId = 'ajax_channels_'.$id_package.'_'.$id_city;
	// если кеш есть и он ещё не истек, то
	if($obCache->InitCache($time, $cacheId, "/")) {
		$resCache = $obCache->GetVars();
		$arResult = $resCache["DATA_СHANNELS"];
	} else {
		// иначе обращаемся к базе
		$arResult = getChannels($id_package, $id_city);
	}

	if($obCache->StartDataCache())
	{
		$obCache->EndDataCache(array(
			"DATA_СHANNELS" => $arResult,
		)); 	
	}
	
	?>
	<div class="popup_channels-title">Все телеканалы пакета "<?=$arResult['NAME']?>"</div>
	<div class="popup_channels-box">
	<?

	foreach ($arResult["SECTIONS"] as $arrSection) {?>
		<?if (count($arrSection['ELEMENTS'])){?>
		<div class="popup_channels-col">
			<div class="popup_channels-item">
				<div class="popup_channels-head"><?=$arrSection['NAME']?></div>
				<ul class="popup_channels-list">
					<?foreach ($arrSection['ELEMENTS'] as $arrElement) {?>
					<li class="popup_channels-line">
						<div class="popup_channels-ico">
							<!--<img class="popup_channels-ico_img" src="<?//=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png">-->
							
							<?if (!empty($arrElement['PREVIEW_PICTURE'])) {?>
							<span class="add_packs_box__hidden_pic">
								<img src="<?=$arrElement['PREVIEW_PICTURE']?>" alt="<?=$arrElement['NAME']?>">
							</span>
							<?}else{?>
							<span class="add_packs_box__hidden_pic add_packs_box__hidden_pic--noimg">
								<span><?=substr($arrElement['NAME'], 0, 1)?></span>
							</span>
							<?}?>

						</div>
						<div class="popup_channels-name"><a href = "/channel/<?=$arrElement['ID']?>/"><?=$arrElement['NAME']?></a></div>
					</li>
					<?}?>
				</ul>
			</div>
		</div>
		<?}?>
	<?}?>
	</div>
<?
} else
{?>
	<div class="popup_channels-title">Пакета не существует</div>
<?}
?>