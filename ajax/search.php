<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<style>
	.cat_drop__no_img {
		margin: 0px 6px 0px 3px;
	}
</style>

<?
CModule::IncludeModule("iblock");

if (isset($_REQUEST['query']) && (!empty($_REQUEST['query'])))
{
    $query = $_REQUEST['query'];

    global $DB;
    $dbRes = $DB->Query("SELECT ID, NAME, PREVIEW_PICTURE FROM b_iblock_element WHERE IBLOCK_ID = 9 and NAME like '%".$query."%'");
    $countChannel = 0;
	while ($row = $dbRes->Fetch())
    {
		//$arrIdChannel[] = $row['ID'];
		$image = CFile::ResizeImageGet(
			$row["PREVIEW_PICTURE"],
			array("width" => 25, "height" =>25),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true,
			false
		);
		$row['PREVIEW_PICTURE'] = $image['src'];
		$countChannel++;
        $arResult['CHANNEL'][] = $row;
    }

	$arFilter = Array("IBLOCK_ID"=>17, "ACTIVE"=>"Y", "NAME" => '%'.$query.'%');  //все пакеты для данного города
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('PROPERTY_DATE_FROM', 'PROPERTY_ICONS', 'NAME', 'ID'));
	while($ob = $res->GetNext()) 
	{
		/*$image = CFile::ResizeImageGet(
			current($ob["PROPERTY_ICONS_VALUE"]),
			array("width" => 70, "height" =>70),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true,
			false
		);
		$ob['IMAGE'] = $image['src'];*/
		$arResult['PROGRAMM'][] = $ob;
		$countProgram++;
	}
	//PR($arResult['PROGRAMM']);
	if ($countChannel) {?>
		<div class="result_count">
			<span class="result_count_text">Найдено телеканалов: <span><?=$countChannel?></span></span>
		</div>
		<?//PR($arResult['TIME']);?>
		<div class="result_channels_box">
			<ul class="result_channels">
				<?foreach ($arResult['CHANNEL'] as $arItem) {?>
				<li class="result_channels__item">
					<?if (!empty($arItem['PREVIEW_PICTURE'])) {?>
					<div class="result_channels__img">
						<img src="<?=$arItem['PREVIEW_PICTURE']?>" alt="<?=$arItem['NAME']?>">
					</div>
					<?} else {?>
						<span class="cat_drop__no_img ng-binding"><?=substr($arItem['NAME'], 0, 1);?></span>
					<?}?>
					<a href="/channel/<?=$arItem['ID']?>/" class="result_channels__link"><?=$arItem['NAME']?></a>
				</li>
				<?}?>
			</ul>
		</div>
	<?}?>
	
	<?if ($countProgram) {?>
		<div class="result_count">
			<span class="result_count_text">Найдено телепередач: <span><?=$countProgram?></span></span>
		</div>
		<div class="result_programms_box">
			<ul class="result_programms">
				<?foreach ($arResult['PROGRAMM'] as $arItem) {?>
				<li class="result_programms__item">
					<?if (!empty($arItem['IMAGE'])) {?>
					<div class="result_programms__img">
						<a href="#"><img src="<?=$arItem['IMAGE']?>" alt="<?=$arItem['NAME']?>"></a>
					</div>
					<?} else {?>
						<span class="cat_drop__no_img ng-binding"><?=substr($arItem['NAME'], 0, 1);?></span>
					<?}?>
					<div class="result_programms__cont">
						<div class="result_programms__cont_head">
							<?$unixTime = strtotime($arItem['PROPERTY_DATE_FROM_VALUE']);?>
							<span class="result_programms__time"><?=date("H:s",$unixTime)?></span>
							<?$ARR_MONTH_RU = array(1 => 'января', 2 => 'февраля', 3 => 'марта', 4 => 'апреля', 5 => 'мая', 6 => 'июня', 7 => 'июля', 8 => 'августа', 9 => 'сентября', 10 => 'октября', 11 => 'ноября', 12 => 'декабря',);?>
							<span class="result_programms__date"><?=date("d", $unixTime).' '.$ARR_MONTH_RU[(int)date("n", $unixTime)]?></span>
						</div>
						<span class = "result_programms__name" data-id = "<?=$arItem['ID']?>"><?=$arItem['NAME']?></span>
						<span class="result_programms__channel"><?$arItem['CHANNEL']?></span>
					</div>
				</li>
				<?}?>
			</ul>
		</div>
	<?}?>
	
	<?if (!$countChannel && !$countProgram) {?>
		Поиск не дал результатов попробуйте изменить поисковый запрос
	<?}
}
