<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>

<?
//if ($_REQUEST['ajax'] = 'yes') 
//{
	$ELEMENT_ID = intval($_REQUEST['questions']);
	$id = intval($_REQUEST['answer']);
	//$key = 3;
	if ((CModule::IncludeModule("iblock")) && ($id >= 0))
	{
		$arSelect = Array("ID", "PROPERTY_VOICES", "PROPERTY_FILE");
		$arFilter = Array("IBLOCK_ID"=>23, 'ID' => $ELEMENT_ID);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		
		//PROPERTY_VOICES_VALUE
		if($ob = $res->GetNext())
			//PR($ob);
		$voices = array();
		foreach ($ob['PROPERTY_FILE_VALUE'] as $key => &$Item)
		{
			$voices[$key] = 0;

			if (isset($ob['PROPERTY_VOICES_VALUE'][$key]))
				if ($id == $key)
					$voices[$key] = $ob['PROPERTY_VOICES_VALUE'][$key] + 1;
			else
				$voices[$key] = $ob['PROPERTY_VOICES_VALUE'][$key];
		
		}
		//PR($voices);

		$PROPERTY_VALUES = array(
			"VOICES" => $voices,
		);
//PR($voices);
		CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, 23, $PROPERTY_VALUES);
		//flush();
		//echo json_encode(array('isError' => false, 'num' => $likes));
		
		//выводим ответ, на который отвечаем
		
		global $Filter;
		$Filter = Array(
			"ID" => $ELEMENT_ID,//фильтр вывод на главной 
		);
		
		$APPLICATION->IncludeComponent(
			"bitrix:news.list", 
			"interviewResult", 
			array(
				"ANSWER" => "Результаты нынешнего опроса",
				"IBLOCK_ID" => "23",
				"NEWS_COUNT" => "1",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "Filter",
				"FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"PROPERTY_CODE" => array(
					0 => "",
					1 => "SECTION_ANSWERS",
					2 => "",
				),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"PREVIEW_TRUNCATE_LEN" => "100",
				"ACTIVE_DATE_FORMAT" => "j F Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "N",
				"DISPLAY_DATE" => "N",
				"DISPLAY_NAME" => "N",
				"DISPLAY_PICTURE" => "N",
				"DISPLAY_PREVIEW_TEXT" => "N",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"PARENT_SECTION" => "",
				"IBLOCK_TYPE" => "interview"
			),
			false
		);
	} 
	else  
	{
		flush();
		echo 'Не удалось добавить голос';
		//echo json_encode(array('isError' => true, 'message' => 'Error'));
	}

?>
