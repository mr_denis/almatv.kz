<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 

//flush();?>
<?//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<?
if(!CModule::IncludeModule("iblock"))
    return;
?>

<?
$idChannel = intval($_REQUEST['id']);

if ($idChannel > 0) {

	$arrFilter = Array("IBLOCK_ID"=>9, "ACTIVE"=>"Y", "ID" => $idChannel);
	$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arrFilter, false, false, array('NAME', 'PREVIEW_PICTURE', 'DETAIL_PAGE_URL'));
	//$i = 0;
	$arrChannels = array();
	if($arrChannel = $res->GetNext())
	{
		if (!empty($arrChannel['PREVIEW_PICTURE']))
		{		
			$image = CFile::ResizeImageGet(
				$arrChannel['PREVIEW_PICTURE'],
				array("width" => 112, "height" =>40),
				BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
				true,
				false
			);
		}
	}
	?>
	<div class="checked_channel_holder clear">
		<div class="checked_channel">
			<a href="<?=$arrChannel['DETAIL_PAGE_URL']?>" class="no-underline">
				<?if (!empty($image['src'])) {?>
					<img src="<?=$image['src']?>" class="checked_channel__logo" alt="<?=$arrChannel['NAME']?>">
				<?}?>
				<h2 class="checked_channel__title" style = "margin-left: 20px;"><?=$arrChannel['NAME']?></h2>
			</a>
		</div>
		
		<?
		$currentWeek = getDateWeek();
		
		$filter = array('UF_CHANNEL' => $idChannel,
			">=UF_DATE_FROM"=> date("d.m.Y H:i:s", mktime(0,0,0,(int)$currentWeek[0]['M'],(int)$currentWeek[0]['D'], $currentWeek[0]['Y'])),
			"<=UF_DATE_FROM"=> date("d.m.Y H:i:s", mktime(23,59,59,(int)$currentWeek[6]['M'],(int)$currentWeek[6]['D'], $currentWeek[6]['Y'])),
		);
		?>
		
		<?
		$APPLICATION->IncludeComponent(
			"alma:hlview", 
			"week", 
			array(
				"COMPONENT_TEMPLATE" => ".default",
				"IBLOCK_ID" => HL_TV_PROGRAMM,
				"FILTER" => $filter,
			),
			false
		);
		?>
	<?
	/*global $Filter;
	$currentWeek = getDateWeek();
	$Filter = Array(
		"PROPERTY_CHANNEL" => $idChannel,
		">=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", mktime(0,0,0,(int)$currentWeek[0]['M'],(int)$currentWeek[0]['D'], $currentWeek[0]['Y'])),
		"<=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", mktime(23,59,59,(int)$currentWeek[6]['M'],(int)$currentWeek[6]['D'], $currentWeek[6]['Y'])),
	);
	//PR($Filter);
	?>
	<?
	$APPLICATION->IncludeComponent(
		"bitrix:news.list", 
		"programmWeek", 
		array(
			"IBLOCK_ID" => "17",
			"NEWS_COUNT" => "2000",
			"SORT_BY1" => "PROPERTY_DATE_FROM",
			"SORT_ORDER1" => "ASC",
			"SORT_BY2" => "SORT",
			"SORT_ORDER2" => "ASC",
			"FILTER_NAME" => "Filter",
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			//"CACHE_TYPE" => "A",
			//"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			"PREVIEW_TRUNCATE_LEN" => "100",
			"ACTIVE_DATE_FORMAT" => "j F Y",
			"SET_TITLE" => "N",
			"SET_BROWSER_TITLE" => "Y",
			"SET_META_KEYWORDS" => "Y",
			"SET_META_DESCRIPTION" => "Y",
			"SET_STATUS_404" => "Y",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
			"ADD_SECTIONS_CHAIN" => "Y",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"PARENT_SECTION_CODE" => "",
			"INCLUDE_SUBSECTIONS" => "Y",
			"DISPLAY_DATE" => "Y",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"PAGER_TEMPLATE" => ".default",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"PAGER_TITLE" => "",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"PARENT_SECTION" => "",
			"IBLOCK_TYPE" => "news",
			"FIELD_CODE" => array(
				0 => "",
				1 => "",
			),
			"PROPERTY_CODE" => array(
				0 => "MAIN",
				1 => "PROPERTY_DATE_FROM",
				2 => "PROPERTY_DATE_TO",
			)
		),
		false
	);*/
} else {?>
	Некорректный ID канала
<?}?>