<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(!CModule::IncludeModule("iblock"))
    return;
?>

<?
if (empty($_REQUEST['date']))
	die;

$unixDate = $_REQUEST['date'];
$d = (int)date('d', $unixDate);
$m = (int)date('m', $unixDate);
$y = date('Y', $unixDate);

$idChannel = intval($_REQUEST['id']);

if ($idChannel) {
	
	/*if (empty($_REQUEST['v']))
	{
		//echo '1';
		global $arrFilter;
		$arrFilter = Array(
			">=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", mktime(0,0,0,$m,$d,$y)),
			"<=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", mktime(23,59,59,$m,$d,$y)),
			//">=PROPERTY_DATE"=> date("Y-m-d", mktime(0,0,0,6,1,2015)),
			//"<=PROPERTY_DATE"=> date("Y-m-d", mktime(23,59,59,12,31,2015)),
			"PROPERTY_CHANNEL" => $idChannel,
		);
		//PR($arrFilter);

		$APPLICATION->IncludeComponent(
			"bitrix:news.list", 
			"channelProgram", 
			array(
				"AJAX" => 'Y',
				"IBLOCK_ID" => "17",
				"NEWS_COUNT" => "200",
				"SORT_BY1" => "PROPERTY_DATE",
				"SORT_ORDER1" => "ASC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "arrFilter",
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "N",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "N",
				"PREVIEW_TRUNCATE_LEN" => "100",
				"ACTIVE_DATE_FORMAT" => "j F Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "Y",
				"SET_META_KEYWORDS" => "Y",
				"SET_META_DESCRIPTION" => "Y",
				"SET_STATUS_404" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
				"ADD_SECTIONS_CHAIN" => "Y",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"PARENT_SECTION" => "",
				"IBLOCK_TYPE" => "news",
				"FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"PROPERTY_CODE" => array(
					0 => "MAIN",
					1 => "",
				)
			),
			false
		);
		
	} 
	else
	{*/
		$filter = array('UF_CHANNEL' => $idChannel,
			">=UF_DATE_FROM"=> date("d.m.Y H:i:s", mktime(0,0,0,$m,$d,$y)),
			"<=UF_DATE_TO"=> date("d.m.Y H:i:s", mktime(23,59,59,$m,$d,$y)),
		);

		$APPLICATION->IncludeComponent(
			"alma:hlview", 
			"", 
			array(
				"COMPONENT_TEMPLATE" => ".default",
				"IBLOCK_ID" => HL_TV_PROGRAMM,
				//"ID_CHANNEL" => $idChannel,
				"FILTER" => $filter,
			),
			false
		);
	//}

}?>