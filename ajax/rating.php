<div class="ususal_box">
	<div class="box box--rating">
		<div class="rating">
			<div class="rating__title_box clear">
				<h4 class="rating__title">Какой канал вы смотрите чаще других?</h4>
				<span class="rating__desc">Проголосуйте за ваш любимый канал и мы купим его со следующей зарплаты!</span>
			</div>
			<ul class="rating_result">
				<li class="rating_result__item">
					<div class="rating_result__item_col" style="height: 90%">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res02.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>90%</span>
							</div>
							<span class="rating_result__name">ТНТ</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col" style="height: 70%">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res01.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>70%</span>
							</div>
							<span class="rating_result__name">СТС</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col" style="height: 50%">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res03.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>50%</span>
							</div>
							<span class="rating_result__name"> Астана</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col" style="height: 30%">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res04.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>30%</span>
							</div>
							<span class="rating_result__name">Discovery</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col" style="height: 20%">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res05.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>20%</span>
							</div>
							<span class="rating_result__name"> Россия HD</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col" style="height: 10%">
						<div class="rating_result__item_head rating_result__item_head--less20"> <!-- Пишем if если высота меньше 20% и докидываем класс rating_result__item_head--less20 -->
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res06.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>10%</span>
							</div>
							<span class="rating_result__name">K1</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col" style="height: 5%">
						<div class="rating_result__item_head rating_result__item_head--less20">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res07.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>5%</span>
							</div>
							<span class="rating_result__name">Первый канал</span>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="ususal_box">
	<div class="box box--rating">
		<div class="rating">
			<div class="rating__title_box clear">
				<h4 class="rating__title">Какой канал вы смотрите чаще других?</h4>
				<span class="rating__desc">Проголосуйте за ваш любимый канал и мы купим его со следующей зарплаты!</span>
			</div>
			<ul class="rating_result rating_result--wide">
				<li class="rating_result__item">
					<div class="rating_result__item_col">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res02.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>90%</span>
							</div>
							<span class="rating_result__name">ТНТ</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res01.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>70%</span>
							</div>
							<span class="rating_result__name">СТС</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res03.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>50%</span>
							</div>
							<span class="rating_result__name"> Астана</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res04.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>30%</span>
							</div>
							<span class="rating_result__name">Discovery</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res05.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>20%</span>
							</div>
							<span class="rating_result__name"> Россия HD</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col">
						<div class="rating_result__item_head rating_result__item_head--less20"> <!-- Пишем if если высота меньше 20% и докидываем класс rating_result__item_head--less20 -->
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res06.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>10%</span>
							</div>
							<span class="rating_result__name">K1</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col">
						<div class="rating_result__item_head rating_result__item_head--less20">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res07.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>5%</span>
							</div>
							<span class="rating_result__name">Первый канал</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res02.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>90%</span>
							</div>
							<span class="rating_result__name">ТНТ</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res01.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>70%</span>
							</div>
							<span class="rating_result__name">СТС</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res03.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>50%</span>
							</div>
							<span class="rating_result__name"> Астана</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res04.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>30%</span>
							</div>
							<span class="rating_result__name">Discovery</span>
						</div>
					</div>
				</li>
				<li class="rating_result__item">
					<div class="rating_result__item_col">
						<div class="rating_result__item_head">
							<div class="rating_result__item_img">
								<img class="rating__list_item_img" src="<?=SITE_TEMPLATE_PATH?>/pics/vote_res05.jpg" alt="image_description">
							</div>
							<div class="rating_result__item_num">
								<span>20%</span>
							</div>
							<span class="rating_result__name"> Россия HD</span>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>