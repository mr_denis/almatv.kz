<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>

<?
if(!CModule::IncludeModule("iblock"))
    return;

$inetId = intval($_REQUEST['id']);

if ($inetId > 0)
{
	$arrFilter = Array("IBLOCK_ID"=>18, "ACTIVE"=>"Y", "ID" => $inetId);
	$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arrFilter, false, false, Array("PROPERTY_V_EXT", "PROPERTY_V_INN", "PROPERTY_TECH", "PROPERTY_PURCHASE", "PROPERTY_RENT", "PROPERTY_ANSWER"));

	if($arrInet = $res->GetNext())
	{
	//PR($arrInet);	
	?>	
		<div class="ajax_tarif">
			<table class="ajax_tarif__table">
				<tbody>
					<?if (!empty($arrInet['PROPERTY_TECH_VALUE'])) {?>
					<tr>
						<td>Технология</td>
						<td><?=$arrInet['PROPERTY_TECH_VALUE']?></td>
					</tr>
					<?}?>
					<?if (!empty($arrInet['PROPERTY_V_EXT_VALUE'])) {?>
					<tr>
						<td>Скорость на внешние ресурсы</td>
						<td><?=$arrInet['~PROPERTY_V_EXT_VALUE']['TEXT']?></td>
					</tr>
					<?}?>
					<?if (!empty($arrInet['PROPERTY_V_INN_VALUE'])) {?>
					<tr>
						<td>Скорость на внутренние ресурсы</td>
						<td><?=$arrInet['PROPERTY_V_INN_VALUE']?></td>
					</tr>
					<?}?>
					<?if (!empty($arrInet['~PROPERTY_PURCHASE_VALUE']['TEXT'])) {?>
					<tr>
						<td>Выкуп оборудования</td>
						<td><?=$arrInet['~PROPERTY_PURCHASE_VALUE']['TEXT']?></td>
					</tr>
					<?}?>
					<?if (!empty($arrInet['~PROPERTY_RENT_VALUE'])) {?>
					<tr>
						<td>Аренда оборудования</td>
						<td><?=$arrInet['~PROPERTY_RENT_VALUE']['TEXT']?></td>
					</tr>
					<?}?>
					<?if (!empty($arrInet['PROPERTY_ANSWER_VALUE'])) {?>
					<tr>
						<td>Ответ. хранение оборудования</td>
						<td><?=$arrInet['PROPERTY_ANSWER_VALUE']?></td>
					</tr>
					<?}?>
				</tbody>
			</table>
		</div>
	<?}
}?>
