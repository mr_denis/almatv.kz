<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 
flush();?>
<?
if(!CModule::IncludeModule("iblock"))
    return;
?>

<?
if (($id = intval($_REQUEST['id'])) > 0)
{
	/*if (empty($_REQUEST['v']))
	{
		$APPLICATION->IncludeComponent(
		"bitrix:news.detail", 
		"programm", 
		array(
			"IBLOCK_ID" => "17",
			"ELEMENT_ID" => $id,
			"CHECK_DATES" => "Y",
			"FIELD_CODE" => array(
				0 => "PREVIEW_PICTURE",
				1 => "DETAIL_PICTURE",
				2 => "",
			),
			"PROPERTY_CODE" => array(
				0 => "DATE_FROM",
				1 => "DIRECTOR",
				2 => "ACTOR",
				3 => "YEAR",
				4 => "PRESENTER",
				5 => "ICONS",
				6 => "DATE_TO",
				7 => "GANRE",
			),
			"IBLOCK_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "N",
			"AJAX_OPTION_HISTORY" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_GROUPS" => "N",
			"SET_TITLE" => "Y",
			"SET_BROWSER_TITLE" => "Y",
			"BROWSER_TITLE" => "-",
			"SET_META_KEYWORDS" => "Y",
			"META_KEYWORDS" => "-",
			"SET_META_DESCRIPTION" => "Y",
			"META_DESCRIPTION" => "-",
			"SET_STATUS_404" => "N",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
			"ADD_SECTIONS_CHAIN" => "Y",
			"ADD_ELEMENT_CHAIN" => "N",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"USE_PERMISSIONS" => "N",
			"DISPLAY_DATE" => "Y",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"USE_SHARE" => "N",
			"PAGER_TEMPLATE" => ".default",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"PAGER_TITLE" => "",
			"PAGER_SHOW_ALL" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"COMPONENT_TEMPLATE" => "programm",
			"IBLOCK_TYPE" => "video",
			"ELEMENT_CODE" => "",
			"DETAIL_URL" => "",
			"SET_CANONICAL_URL" => "N",
			"SET_LAST_MODIFIED" => "N",
			"PAGER_BASE_LINK_ENABLE" => "N",
			"SHOW_404" => "N",
			"MESSAGE_404" => ""
		),
		false
	);	
	}
	else
	{*/
		$APPLICATION->IncludeComponent(
			"alma:hlview", 
			"programm", 
			array(
				"COMPONENT_TEMPLATE" => ".default",
				"IBLOCK_ID" => HL_TV_PROGRAMM,
				"ID_TV" => $id,
			),
			false
		);
	//}
}
?>