<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>

<?
$id = intval($_REQUEST['id']);
$idPrev = intval($_REQUEST['id_prev']);

$APPLICATION->IncludeComponent(
	"alma:package",
	"",
	Array(
		"ID_PACKAGE" => $id,
		"ID_PACKAGE_PARENT" => $idPrev,
	)
);
?>