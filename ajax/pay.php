<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>

<style>
	.check_pay__user_id {
		display: block;
		margin: 0 0 5px;
	}
	.check_pay__user_id > span {
		color: #62a3ed;
		display: inline-block;
		margin: 0 0 0 5px;
	}
</style>

<?
require_once($_SERVER["DOCUMENT_ROOT"].'/include/check_number.php');
?>

<?
$arResult = json_decode($arResult);

session_start();
unset($_SESSION ["session_basket"]);

if (($arResult->result == 'success') && check_bitrix_sessid())
{
	require_once ($_SERVER["DOCUMENT_ROOT"]."/include/cnp/CNPMerchantWebServiceClient.php");
	date_default_timezone_set('Asia/Almaty');

	$goodsItem = new GoodsItem ();
	$total = $_REQUEST ["summ"];
	$goodsItem->amount = $total * 100;
	$goodsItem->currencyCode = 398;
	$goodsItem->merchantsGoodsID = guid ();
	$goodsItem->nameOfGoods = 'Оплата услуги: TV + internet';
	$basket[] = $goodsItem;
	
	$_SESSION ["session_basket"] = serialize ( $basket );
	?>

	<div class="pay_online">
		<h4 class="check_balance__title">Проверка договора</h4>
		<h5 class="check_balance__user_name"><?=$arResult->data->name?></h5>
		<span class="check_pay__user_id">Номер договора: <span><?=$_REQUEST['number']?></span></span>
		<span class="check_pay__user_id">Ваш баланс:  <span><?=$arResult->data->balance?> тг.</span> </span>
		<?php if ($total > 0) { ?>
			<a href="/include/cnp/checkout.php?btn_StartCardPayment=now&name=<?=$arResult->data->name?>&number=<?=$_REQUEST['number']?>">Оплатить</a>
		<?php } ?>
	</div>
<?}
else if ($arResult->result == 'error')
{?>
	<div class="pay_online">
	<?
	//PR($arResult);
	?>
		<h4 class="check_balance__title">Проверка договора</h4>
		<span class="check_pay__user_id">Неверные данные, введите правильный номер договора</span>
	</div>
<?}?>
