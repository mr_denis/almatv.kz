<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('highloadblock');
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity as Entity;

$obHLIB = HL\HighloadBlockTable::getById(HLIB_TV_REMINDER)->Fetch();
$obEntity = HL\HighloadBlockTable::compileEntity($obHLIB);
$obDataClass = $obEntity->getDataClass();

$arResult = array(
    "success" => true
);
$arParams = array(
    "UF_BROADCASTING" => intval($_REQUEST["broadcasting"]),
    "UF_EMAIL" => $_REQUEST["email"],
    "UF_SESSID" => bitrix_sessid_get()
);

$arErrors = array();
if (!check_email($arParams["UF_EMAIL"])) {
    $arErrors[] = "Неправильный e-mail";
}
if ($arParams["UF_BROADCASTING"] <= 0) {
    $arErrors[] = "Неправильный ID трансляции";
}

if (empty($arErrors)) {
    $obQuery = new Entity\Query($obEntity);
    $obQuery->setSelect(array("ID"));
    $obQuery->SetFilter(
    	array(
    		"UF_BROADCASTING" => $arParams["UF_BROADCASTING"],
    		"UF_EMAIL" => $arParams["UF_EMAIL"]
    	)
    );
    $obQuery->SetLimit(1);
    $rsReminds = new CDBResult($obQuery->exec());
    if ($rsReminds->Fetch()) {
        $arErrors[] = "Пользователь уже записан для напоминания";
    }
}

if (empty($arErrors)) {
    $obAddResult = $obDataClass::add($arParams);
    if (!$obAddResult->isSuccess()) {
        $arErrors[] = "Не получилось сохранить элемент: " . $obAddResult->getErrorMessages();
        $arResult["success"] = false;
        $arResult["errors"] = $arErrors;
    }
}

if (!empty($arErrors)) {
    $arResult["success"] = false;
    $arResult["errors"] = $arErrors;
}

echo json_encode($arResult);