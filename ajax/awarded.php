<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>

<?
if(!CModule::IncludeModule("iblock"))
	return;

flush();

$id = intval($_REQUEST['id']);
if ($id > 0)
{
	$arFilter = Array("IBLOCK_ID"=>22, "ACTIVE"=>"Y", "ID" => $id);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('NAME', 'DETAIL_PICTURE', 'DETAIL_TEXT'));
	if($ob = $res->GetNext())
	{
		$image = CFile::ResizeImageGet(
			$ob['DETAIL_PICTURE'],
			array("width" => 906, "height" =>400),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALTa,
			true,
			false
		);
		$ob['DETAIL_PICTURE'] = $image['src'];
	}
?> 
<?if (!empty($ob['DETAIL_PICTURE'])) {?>
<div class="popup_awarded__img">
	<img src="<?=$ob['DETAIL_PICTURE']?>" alt="<?=$ob['NAME']?>">
</div>
<?}?>
<div class="popup_awarded__cont">
	<?if (!empty($ob['NAME'])) {?><h5 class="popup_awarded__title"><?=$ob['NAME']?></h5><?}?>
	<?if (!empty($ob['DETAIL_TEXT'])) {?><span class="popup_awarded__text"><?=$ob['DETAIL_TEXT']?></span><?}?>
</div>
<?}?>