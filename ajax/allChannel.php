<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>

<?
if(!CModule::IncludeModule("iblock"))
    return;

function selectChannels($id)
{
	$arrFilter = Array("IBLOCK_ID"=>9, "ACTIVE"=>"Y");
	if ($id > 0)
		$arrFilter['SECTION_ID'] = $id;
	$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arrFilter, false, false, array('ID', 'NAME', 'SORT', 'PREVIEW_PICTURE'));
	$i = 0;
	$arrChannels = array();
	while($arrChannel = $res->GetNext())
	{
		//PR($arrChannel);
		$picture = CFile::ResizeImageGet(
			$arrChannel["PREVIEW_PICTURE"],
			array("width" => 22, "height" =>20),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true,
			false
		);
		$nameChannel = $arrChannel['NAME'];
		$arrChannel = array (
			"img" => array(
				"src" => $picture['src'],//($picture['src'] != '') ? $picture['src'] : 'no', 
				"alt" => $nameChannel,
			),
			"id" => $arrChannel['ID'],
			"name" => $nameChannel,
			"value" => $i++,
			"order" => $arrChannel['SORT'],
		);
		$arrChannels[] = $arrChannel;
	}
	return json_encode($arrChannels); 
}

function selectCategory()
{
	$arFilter = array("IBLOCK_ID" => 9, "ACTIVE"=>"Y");
	$rsSections = CIBlockSection::GetList(Array("NAME"=>"ASC"), $arFilter, true, array('ID', 'NAME', 'SORT', 'PICTURE', 'DETAIL_PICTURE'), false); //�������� ������ �������� �� ���������
	$arrCategory = array();
	while($arrSection = $rsSections->GetNext())
	{
		$picture = CFile::ResizeImageGet(
			$arrSection["PICTURE"],
			array("width" => 22, "height" =>20),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true,
			false
		);
		$picture_hover = CFile::ResizeImageGet(
			$arrSection["DETAIL_PICTURE"],
			array("width" => 22, "height" =>20),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true,
			false
		);
		$namePackage = $arrSection['NAME'];
		$arrCategoryItem = array (
			"img" => array(
				"src" => $picture['src'], 
				"alt" => $namePackage,
			),
			"img_hover" => array(
				"src" => $picture_hover['src'], 
				"alt" => $namePackage,
			),
			"id" => $arrSection['ID'],
			"name" => $namePackage,
			"value" => $i++,
			"order" => $arrSection['SORT'],
		);
		$arrCategory[] = $arrCategoryItem;
	}
	
	return json_encode($arrCategory);
}
//PR(allChannel());
$typeRequest = $_REQUEST['type'];
flush();
switch ($typeRequest)
{
	case 'channel':
		$id = intval($_REQUEST['categoryId']);
		echo selectChannels($id);
		die();
	case 'category':
		echo selectCategory();
		die();
}
