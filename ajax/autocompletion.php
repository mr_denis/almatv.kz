<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
CModule::IncludeModule("iblock");

if (isset($_REQUEST['query']) && (!empty($_REQUEST['query'])))
{
    $query = $_REQUEST['query'];

    global $DB;
    $dbRes = $DB->Query("SELECT NAME, ID, PREVIEW_PICTURE, IBLOCK_SECTION_ID FROM b_iblock_element WHERE NAME like '%".$query."%' and IBLOCK_ID = 9");
    $aRows = array();
    while ($row = $dbRes->Fetch())
    {
		$image = CFile::ResizeImageGet(
			$row["PREVIEW_PICTURE"],
			array("width" => 20, "height" =>20),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true,
			false
		);
		$row['PREVIEW_PICTURE'] = $image['src'];
		
		/*echo '<pre>';
		print_r($row);
		echo '</pre>';*/
		
        /*$res = CIBlockSection::GetByID($row['IBLOCK_SECTION_ID']);
        if($ar_res = $res->GetNext())
        {
            $row["TYPE"] = $ar_res['NAME'];
        }*/
        $arResult[] = $row;
    }
    flush();
    if (count($arResult) > 0) {?>
		<?foreach ($arResult as $item) {?>
		   <div data-id = "<?=$item['ID']?>" data-section-id = "<?=$item['IBLOCK_SECTION_ID']?>" class="cat_drop__drop_item">
				<span class="cat_drop__drop_pic">
					<?if (!empty($item['PREVIEW_PICTURE'])) {?>
						<img src="<?=$item['PREVIEW_PICTURE']?>" alt="<?=$item['NAME']?>">
						<img class="cat_drop__drop_pic--with_hover" src="<?=$item['PREVIEW_PICTURE']?>" alt="<?=$item['NAME']?>"">
					<?} else {?>
						<span class="cat_drop__no_img ng-binding"><?=substr($item['NAME'], 0, 1);?></span>
					<?}?>
				</span>
				<span class="cat_drop__drop_name"><?=$item['NAME']?></span>
			</div>
		<?}
    }
    else
    {?>
        <div class="cat_drop__drop_item">Пусто</div>
    <?}?>
<?}?>
