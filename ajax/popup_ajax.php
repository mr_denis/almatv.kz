<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>

<?
$id_paskages = explode(',', $_REQUEST['packages_id']);
$id_paskage = intval($_REQUEST['package_id']);
$id_category = intval($_REQUEST['category_id']);
$id_type = intval($_REQUEST['type_id']);  //пакеты в квартиру или в дом

if (($id_paskage <= 0) && ($id_category <= 0))
{
	die();
} 
flush();
	
//выбираем все пакеты для данного города, затем выберем по ним все каналы опред типа
/*$arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "PROPERTY_TYPE" => $id_type);
if ($_COOKIE['City'])
{
	$arFilter['PROPERTY_CITY'] = $_COOKIE['City'];
}
else
{
	$arFilter['PROPERTY_CITY'] = $_REQUEST['city'];
}
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);	
while($ob = $res->GetNext())
{
	$id_paskages[] = $ob['ID'];
}*/

function getPackages($id_paskages, $id_paskage, $id_category)
{
	if(!CModule::IncludeModule("iblock"))
		return;

	$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE"); 
	$arFilter = Array("IBLOCK_ID"=> IBLOCK_CHANNELS, /*"PROPERTY_TYPE" => $id_type,*/"PROPERTY_PACKAGES" => $id_paskages, "SECTION_ID" => $id_category, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelect);
	$arChannels = array();

	while($ob = $res->GetNext())
	{
		$image = CFile::ResizeImageGet(
			$ob['PREVIEW_PICTURE'],
			array("width" => 35, "height" =>35),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALTa,
			true,
			false
		);
		
		$ob['PREVIEW_PICTURE'] = $image;
		$ob['F'] = 0;
		$arrChannelAll[] = $ob;
	}

	$arSelect = Array("ID", "NAME", /*"PREVIEW_PICTURE", */"PROPERTY_PACKAGES", "IBLOCK_SECTION_ID");   //PRICE
	$arFilter = Array("IBLOCK_ID"=> IBLOCK_CHANNELS, "PROPERTY_PACKAGES" => $id_paskage, /*"PROPERTY_TYPE" => $id_type,*/ "SECTION_ID" => $id_category, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");  //фильтр по городу
	$res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelect);
	$arChannels = array();
	$count = 0;
	while($ob = $res->GetNext())
	{
		$count++;
		$arrChannelFilter[] = $ob;
	}

	foreach ($arrChannelAll as &$channel)
	{
		foreach ($arrChannelFilter as $channelF)
		{
			if ($channel['ID'] == $channelF['ID'])
			{
				$channel['F'] = 1;
				break;
			}
		}
	}

	return $arrChannelAll;
}

/*добавим кеширование*/
$obCache = new CPHPCache; 
$time = CACHE_TIME * 60 * 60;
$cacheId = 'popup_ajax.php_'.$_REQUEST['packages_id'].$id_paskage.$id_category;
// если кеш есть и он ещё не истек, то
if($obCache->InitCache($time, $cacheId, "/")) {
	$resCache = $obCache->GetVars();
	$arrChannelAll = $resCache["DATA"];
} else {
	// иначе обращаемся к базе
	$arrChannelAll = getPackages($id_paskages, $id_paskage, $id_category);
}

if($obCache->StartDataCache())
{
	$obCache->EndDataCache(array(
		"DATA" => $arrChannelAll,
	)); 	
}

flush();
?>			

<div class="tarif_popup_holder j-tarif_popup_slider">
<?foreach ($arrChannelAll as $key => $item) {?>
	<?if (($key % 3) == 0) { ?>
		<div class="tarif_popup__slider_item">
	<?}?>
		<?if ($item['F']) {?>
			<div class="tarif_popup__list_item">
				<div class="tarif_popup__cont">
						<?if (!empty($item['PREVIEW_PICTURE']['src'])) {?>
							<div class="tarif_popup__list_img">
								<img src="<?=$item['PREVIEW_PICTURE']['src']?>" alt="<?=$item['NAME']?>" >
							</div>
						<?} else {?>
							<div class="tarif_popup__list_img tarif_popup__list_no_img--nobg">
								<div class="tarif_popup__list_no_img">
									<?//var_dump($item['NAME'][0]);?>
									<span><?=substr($item['NAME'], 0, 1)?></span>
								</div>
							</div>
						<?}?>
					<a href="/channel/<?=$item['ID']?>/" class="tarif_popup__list_title"><?=$item['NAME']?></a>
				</div>
			</div>
		<?} else {?>
			<div class="tarif_popup__list_item tarif_popup__list_item--not_available" data-channel = "<?=$item['ID']?>">
				<div class="tarif_popup__cont">
					
						<?if (!empty($item['PREVIEW_PICTURE']['src'])) {?>
							<div class="tarif_popup__list_img">
								<img src="<?=$item['PREVIEW_PICTURE']['src']?>" alt="<?=$item['NAME']?>" >
							</div>
						<?} else {?>
							<div class="tarif_popup__list_img tarif_popup__list_no_img--nobg">
								<div class="tarif_popup__list_no_img">
									<span><?=substr($item['NAME'], 0, 1)?></span>
								</div>
							</div>
						<?}?>
					
					<a href="/channel/<?=$item['ID']?>/" class="tarif_popup__list_title"><?=$item['NAME']?></a>
				</div>
				<div class="tarif_popup__list_available"></div>
			</div>
		<?}?>
	<?if (($key % 3) == 2) { ?>
	</div>
	<?}?>
<?}?>

<?if ((count($arrChannelAll) % 3) != 0) {?>
	</div>
<?}?>
</div>
<div class="tarif_popup_slider__paging"></div>
<div class="tarif_popup_slider__arrows tarif_popup_slider__left"></div>
<div class="tarif_popup_slider__arrows tarif_popup_slider__right"></div>
