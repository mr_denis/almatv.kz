<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>

<?
$idCity = getCity();

$APPLICATION->IncludeComponent(
	"alma:packages", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_ID" => IBLOCK_PACKAGES,
		"TYPE" => PACKAGE_TYPE_HOME,
		"CITY_ID" => $idCity,
		"TYPE_CLASS" => "home",
		"AJAX_CONTENT" => "Y",
	),
	false
);
?>
