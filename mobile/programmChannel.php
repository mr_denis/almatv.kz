<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
CModule::IncludeModule('iblock');
require($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_admin_before.php');

//$_REQUEST['id'] = 226;
//$_REQUEST['date'] = time();

$idChannel = intval($_REQUEST['id']);

if (empty($idChannel))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Не задан канал',
			),
		)
	);
	die();
}

$unixDate = time();

$d = (int)date('d', $unixDate);
$m = (int)date('m', $unixDate);
$y = date('Y', $unixDate);

define(IBLOCK_PROGRAMM, 17);

$arFilter = array(
	"IBLOCK_ID" => IBLOCK_PROGRAMM,
	"ACTIVE" => "Y",
	"PROPERTY_CHANNEL" => $idChannel,
	">=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", mktime(0,0,0,$m,$d,$y)),
	"<=PROPERTY_DATE_TO"=> date("Y-m-d H:i:s", mktime(23,59,59,$m,$d,$y)),
);

$arFields = array("NAME", "ID", "PREVIEW_TEXT", "PROPERTY_CHANNEL", "PROPERTY_RATING", "PROPERTY_ICONS", "PROPERTY_DIRECTOR",
"PROPERTY_ACTOR", "PROPERTY_YEAR", "PROPERTY_PRESENTER", "PROPERTY_DATE_TO", "PROPERTY_DATE_FROM",
"GANRE");

$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arFields);

$arResult = array();
while($ar_res = $res->GetNext())
{
	$arrTmp = array(
		"id" => $ar_res['ID'],
		"name" => $ar_res['NAME'],
		"channel_id" => $ar_res['PROPERTY_CHANNEL_VALUE'],
		"rating" => $ar_res['PROPERTY_RATING_VALUE'],
		"ar_icons" => $ar_res['PROPERTY_ICONS_VALUE'],
		"director" => $ar_res['PROPERTY_DIRECTOR_VALUE'],
		"actor" => $ar_res['PROPERTY_ACTOR_VALUE'],
		"year" => $ar_res['PROPERTY_YEAR_VALUE'], 
		"presenter" => $ar_res['PROPERTY_PRESENTER_VALUE'],
		"date_from" => strtotime($ar_res['PROPERTY_DATE_FROM_VALUE']),
		"date_to" => strtotime($ar_res['PROPERTY_DATE_TO_VALUE']),
		"ganre" => $ar_res['PROPERTY_GANRE_VALUE'],
		"text" => $ar_res['PREVIEW_TEXT'],
	);
	$arResult[] = $arrTmp;
}
//PR($arResult);
echo json_encode(
	array(
		'data' => $arResult,
	)
);
//PR($arResult);
//die();