<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
$startTimeScript = microtime(true);

function getMobileSections()
{
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');
		
	$arFilter = array(
		"IBLOCK_ID" => IBLOCK_CHANNELS,	
		"ACTIVE" => 'Y',
	);

	$arFields = array("NAME", "ID", "PICTURE", "DETAIL_PICTURE", "UF_PICTURE_SVG");

	$rsSections = CIBlockSection::GetList(array("NAME" => "ASC"), $arFilter, false, $arFields);

	$arResult = array();
	while ($arSection = $rsSections->GetNext())
	{
		/*$picture = CFile::ResizeImageGet(
			$arSection["PICTURE"],
			array("width" => 22, "height" =>20),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true,
			false
		);*/
		/*$picture_hover = CFile::ResizeImageGet(
			$arSection["DETAIL_PICTURE"],
			array("width" => 22, "height" =>20),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true,
			false
		);*/
			
		$arrTmp = array(
			"id" => $arSection['ID'],
			"name" => $arSection['NAME'],
			"picture_svg" => CFile::GetPath($arSection["UF_PICTURE_SVG"]),
			//"picture_hover" => $picture_hover['src'],
		);
		
		$arResult[] = $arrTmp;
	}
	
	return $arResult;
}

if (count($_REQUEST))
{
	$arResult = getMobileCity();
}
else
{
	/*добавим кеширование*/
	$obCache = new CPHPCache; 
	$time = CACHE_TIME * 60 * 60;
	$cacheId = 'mobile_sections';
	
	if (isset($_REQUEST['clear_cache']))
		$obCache->Clean($cacheId);

	// если кеш есть и он ещё не истек, то
	if($obCache->InitCache($time, $cacheId, "/")) {
		$resCache = $obCache->GetVars();
		$arResult = $resCache["DATA"];
	} else {
		// иначе обращаемся к базе
		$arResult = getMobileSections();
	}

	if($obCache->StartDataCache())
	{
		$obCache->EndDataCache(array(
			"DATA" => $arResult,
		)); 	
	}
}

$timeScript = microtime(true) - $startTimeScript;

//PR($arResult);
echo json_encode(
	array(
		'data' => $arResult,
		'time' => $timeScript,
	)
);

?>