<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
CModule::IncludeModule("iblock");

$arrFilter = Array("IBLOCK_ID"=>17, "ACTIVE"=>"Y", "!PROPERTY_STAGESCARE" => false,);
$arrGanre = array();
$res = CIBlockElement::GetList(Array(), $arrFilter, array("PROPERTY_GANRE"), false, array('PROPERTY_GANRE'));
while($ob = $res->GetNext())
{
	$arrGanre[] = $ob['PROPERTY_GANRE_VALUE'];
}
//PR($arrGanre);
echo json_encode(
	array(
		'data' => array('ganre' => $arrGanre),
	)
);