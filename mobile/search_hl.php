<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
function getMobilePrograms($query, $ganre)
{	
	if (!CModule::IncludeModule('highloadblock'))
		return;

	$HL_Infoblock_ID = HL_TV_PROGRAMM;
	$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$arrFilter = array(
		">=UF_DATE_FROM" => date("d.m.Y H:i:s", time() - 3600 * 24 * 8),
		"<=UF_DATE_TO" => date("d.m.Y H:i:s", time() + 3600 * 24 * 8),
	);
	
	if (!empty($query))
	{
		$arrFilter['UF_NAME'] = '%'.$query.'%';
	}
	
	if (!empty($ganre))
	{
		$arrFilter['UF_GANRE'] = explode(',', $ganre);
	}

	$Entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 
	$Query->setSelect(array('*'));
	$Query->setFilter($arrFilter);
	
	//if (!empty($limit))
	$Query->setLimit(50);
	//$Query->setOrder(array('UF_SORT' => 'ASC'));

	$result = $Query->exec();

	$result = new CDBResult($result);

	$arResult = array();

	$arResult['PROGRAMM'] = null;
	$countProgram = 0;
	$arChannelsIdTV = array();
	
	while ($row = $result->Fetch())
	{
		foreach ($row as &$itemFields)
		{
			if ($itemFields instanceof \Bitrix\Main\Type\DateTime)
			{
				$itemFields = $itemFields->toString();
			}
		}

		$arrTmp = array(
			"id" => $row['ID'],
			"ar_icons" => $row['UF_ICONS'],
			"name" => $row['UF_NAME'],
			"channel" => $row['UF_CHANNEL'],
			//"ganre" => $row['UF_GANRE'],
			//"date_from" => strtotime($ob['PROPERTY_DATE_FROM_VALUE']),
			//"date_to" => strtotime($ob['PROPERTY_DATE_TO_VALUE']),
			//"channel_name" => $row['UF_CHANNEL'],
		);
		
		$arResult['PROGRAMM'][] = $arrTmp;
		$arChannelsIdTV[$row['UF_CHANNEL']] = null;
		$countProgram++;
	}
	
	$keysChannelsId = array_keys($arChannelsIdTV);
	unset($arChannelsIdTV);

	if(!CModule::IncludeModule("iblock"))
		return;
	
	if (count($keysChannelsId) > 0)
	{
		$arFilter = Array("IBLOCK_ID"=>IBLOCK_CHANNELS, "ID" => $keysChannelsId);
		$res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, array('nTopCount' => 50), array('NAME', 'ID', 'PREVIEW_PICTURE'));
		
		$arChannels = array();
		while ($row = $res->Fetch())
		{
			$image = CFile::ResizeImageGet(
				$row["PREVIEW_PICTURE"],
				array("width" => 25, "height" =>25),
				BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
				true,
				false
			);
			$row['PREVIEW_PICTURE'] = $image['src'];

			$arrTmp = array(
				"id" => $row['ID'],
				"name" => $row['NAME'],
				"picture" => $row['PREVIEW_PICTURE'],
			);
			
			$arChannels[$arrTmp['id']] = $arrTmp;
		}

		foreach ($arResult['PROGRAMM'] as &$item)
		{
			$item['channel'] = $arChannels[$item['channel']];
		}
	}
	//PR($arChannels);
	
	return array(
		"programm" => $arResult['PROGRAMM'],
		"countProgram" => $countProgram,
		"idChannels" => $keysChannelsId, //отсортировать!
	);
}

function getMobileChannels($query, $idChannels, $sound)
{
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');
	
			
	$arFilter = Array("IBLOCK_ID"=>IBLOCK_CHANNELS, "ACTIVE"=>"Y", "NAME" => '%'.$query.'%', "ID" => $idChannels);

	if (!empty($sound))
	{
		$arFilter['PROPERTY_SOUND'] = explode(',', $sound);
	}
	
	$res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, array('nTopCount' => 50), array('NAME', 'ID', 'PREVIEW_PICTURE'));
    $countChannel = 0;
	$arResult['CHANNEL'] = null;
	while ($row = $res->Fetch())
    {
		//$arrIdChannel[] = $row['ID'];
		$image = CFile::ResizeImageGet(
			$row["PREVIEW_PICTURE"],
			array("width" => 25, "height" =>25),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true,
			false
		);
		$row['PREVIEW_PICTURE'] = $image['src'];
		$countChannel++;

		$arrTmp = array(
			"id" => $row['ID'],
			"name" => $row['NAME'],
			"picture" => $row['PREVIEW_PICTURE'],
		);
		
        $arResult['CHANNEL'][] = $arrTmp;
    }
	
	return array(
		"channels" => $arResult['CHANNEL'],
		"countChannel" => $countChannel,
	);
}
	
if ((!empty($_REQUEST['query'])) || (!empty($_REQUEST['ganre'])))
{
    $query = $_REQUEST['query'];

	$startTimeScript = microtime(true);
	
	if (empty($query))
	{
		echo json_encode(
			array(
				'error' => array(
					'code' => 500,
					'error_message' => 'Пустая строка запроса',
				),
			)
		);
		
		die();
	}
	
	if (strlen($query) < 2)
	{
		echo json_encode(
			array(
				'error' => array(
					'code' => 500,
					'error_message' => 'min 2 символа',
				),
			)
		);
		die();
	}
	
	/*добавим кеширование для программ, жанров может не быть*/
	$obCache = new CPHPCache; 
	$time = CACHE_TIME * 60 * 60;
	
	$ganre = '';
	if (!empty($_REQUEST['ganre']))
	{
		$ganre = $_REQUEST['ganre'];
	}

	$cacheId = 'mobile_search_programm_'.$query.$ganre;
	
	if (isset($_REQUEST['clear_cache']))
		$obCache->Clean($cacheId);

	// если кеш есть и он ещё не истек, то
	if($obCache->InitCache($time, $cacheId, "/")) {
		$resCache = $obCache->GetVars();
		$arResult['PROGRAMM'] = $resCache["DATA_PROGRAMM"];
	} else {
		// иначе обращаемся к базе
		$arResult['PROGRAMM'] = getMobilePrograms($query, $ganre);
	}

	if($obCache->StartDataCache())
	{
		$obCache->EndDataCache(array(
			"DATA_PROGRAMM" => $arResult['PROGRAMM'],
		)); 	
	}

	$sound = '';
	if (!empty($_REQUEST['sound_ch']))
	{
		$sound = $_REQUEST['sound_ch'];
	}

	//PR($arResult['PROGRAMM']['idChannels']);
	/*добавим кеширование для программ, учитываю вывод по программам*/
	$obCache = new CPHPCache; 
	$time = CACHE_TIME * 60 * 60;
	$cacheId = 'mobile_search_channels_'.$query.$arResult['PROGRAMM']['idChannels'].$sound;
	
	if (isset($_REQUEST['clear_cache']))
		$obCache->Clean($cacheId);

	// если кеш есть и он ещё не истек, то
	if($obCache->InitCache($time, $cacheId, "/")) {
		$resCache = $obCache->GetVars();
		$arResult['CHANNELS'] = $resCache["DATA_СHANNELS"];
	} else {
		// иначе обращаемся к базе
		
		if (!empty($query))
			$arResult['CHANNELS'] = getMobileChannels($query, $arResult['PROGRAMM']['idChannels'], $sound);
		
	}

	if($obCache->StartDataCache())
	{
		$obCache->EndDataCache(array(
			"DATA_СHANNELS" => $arResult['CHANNELS'],
		)); 	
	}

	//PR($arResult);
	//die();
	$timeScript = microtime(true) - $startTimeScript;
	
	echo json_encode(
		array(
			'data' => array('programms' => $arResult['PROGRAMM']['programm'], 'channels' => $arResult['CHANNELS']['channels'], 'count_channels' => $arResult['CHANNELS']['countChannel'], 'count_proramms' => $arResult['PROGRAMM']['countProgram']),
			'time' => $timeScript,
		)
	);
}
else
{
	echo json_encode(
		array(
			'error' => array(
				'code' => 500,
				'error_message' => 'Обязательный параметр query или ganre',
			),
		)
	);
		
	die();
}