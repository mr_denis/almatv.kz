<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
$idChannel = intval($_REQUEST['id']);

/*не вычисляем самостоятельно, на мобилках время может быть свое!*/
/*$unixDate = time();
$d = (int)date('d', $unixDate); 
$m = (int)date('m', $unixDate);
$y = date('Y', $unixDate);
PR(mktime(0,0,0,$m,$d,$y) - 3600 * 24);
PR(mktime(23,59,59,$m,$d,$y) + 3600 * 24);*/

$date_from = $_REQUEST['date_from'];
$date_to = $_REQUEST['date_to'];

if ($idChannel <= 0)
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Не выбран город',
			),
		)
	);
	die();
}

if (empty($date_from))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Не задано date_from',
			),
		)
	);
	die();
}

if (empty($date_to))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Не задан date_to',
			),
		)
	);
	die();
}

if ($date_to > (time() + 3600 * 24 * 2))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Ограничение по дате',
			),
		)
	);
	die();
}

if ($date_from < (time() - 3600 * 24 * 2))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Ограничение по дате',
			),
		)
	);
	die();
}

$startTimeScript = microtime(true);

function getFields_($HL_Infoblock_ID, $arrFilter = array(), $arrSelect = array('*'), $limit)
{
	$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 
	$Query->setSelect($arrSelect);
	$Query->setFilter($arrFilter);
	
	if (!empty($limit))
		$Query->setLimit($limit);
	//$Query->setOrder(array('UF_SORT' => 'ASC'));

	//Выполним запрос
	$result = $Query->exec();

	$result = new CDBResult($result);

	$arResult = array();

	while ($row = $result->Fetch())
	{
		foreach ($row as &$itemFields)
		{
			if ($itemFields instanceof \Bitrix\Main\Type\DateTime)
			{
				$itemFields = $itemFields->toString();
			}
		}

		$arTypeStr = array();

		$arrTmp = array(
			"id" => $row['ID'],
			"name" => $row['UF_NAME'],
			"date_from" => strtotime($row['UF_DATE_FROM']),
			"date_to" => strtotime($row['UF_DATE_TO']),
		);
		
		$arResult[] = $arrTmp;
	}
	return $arResult;
}

function getMobileChannel($idChannel)
{		
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');

	$arFilter = array(
		"IBLOCK_ID" => IBLOCK_CHANNELS,	
		"ID" => $idChannel,
	);
	
	$arFields = array("NAME", "ID", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID", "PROPERTY_REGION.NAME", "PROPERTY_SOUND.NAME", "DETAIL_TEXT", "PREVIEW_PICTURE");

	$res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arFields);

	$arResult = array();
	if($ar_res = $res->GetNext())
	{
		$arResult = array(
			"id" => $ar_res['ID'],
			"name" => $ar_res['NAME'],
			"picture" => CFile::GetPath($ar_res['PREVIEW_PICTURE']),
			"region" => $ar_res['PROPERTY_REGION_NAME'],
			"sound" => $ar_res['PROPERTY_SOUND_NAME'],
			"text" => strip_tags($ar_res['DETAIL_TEXT']),
			"section" => $ar_res['IBLOCK_SECTION_ID'],
		);
	}
	else
		return 0;
	
	$arFilter = array('IBLOCK_ID' => IBLOCK_CHANNELS, 'ID' => $arResult['section']);
	$rsSections = CIBlockSection::GetList(array(), $arFilter, false, array('NAME'));
	$arSectionResult = array();
	while ($arSection = $rsSections->Fetch())
	{
		$arResult['section'] = $arSection['NAME'];
	}
	
	return $arResult;
}

//добавим кеширование
$obCache = new CPHPCache; 
$time = CACHE_TIME * 60 * 60;
$cacheId = 'mobile_channel_'.$idChannel;

if (isset($_REQUEST['clear_cache']))
	$obCache->Clean($cacheId);

// если кеш есть и он ещё не истек, то
if($obCache->InitCache($time, $cacheId, "/")) {
	$resCache = $obCache->GetVars();
	$arResult['channel'] = $resCache["DATA"];
} else {
	// иначе обращаемся к базе
	if ($arResult['channel'] = getMobileChannel($idChannel));
}

if($obCache->StartDataCache() && $arResult)
{
	$obCache->EndDataCache(array(
		"DATA" => $arResult['channel'],
	)); 	
}

if (!CModule::IncludeModule('iblock'))
	return;

if (!CModule::IncludeModule('highloadblock'))
	return;
	
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$filter = array('UF_CHANNEL' => $idChannel,
	">=UF_DATE_FROM"=> date("d.m.Y H:i:s", $date_from),
	"<=UF_DATE_FROM"=> date("d.m.Y H:i:s", $date_to),
);

//добавим кеширование
$obCache = new CPHPCache; 
$time = CACHE_TIME * 60 * 60;
$cacheId = 'mobile_channel_tv_'.$idChannel; 

if (isset($_REQUEST['clear_cache']))
	$obCache->Clean($cacheId);

// если кеш есть и он ещё не истек, то
if($obCache->InitCache($time, $cacheId, "/")) {
	$resCache = $obCache->GetVars();
	$arResult['tv'] = $resCache["DATA"];
} else {
	// иначе обращаемся к базе
	$arResult['tv'] = getFields_(HL_TV_PROGRAMM, $filter, array('*'), '');
}

if($obCache->StartDataCache() && $arResult)
{
	$obCache->EndDataCache(array(
		"DATA" => $arResult['tv'],
	)); 	
}

//PR($arResult);

$timeScript = microtime(true) - $startTimeScript;

echo json_encode(
	array(
		'data' => $arResult,
		'time' => $timeScript,
	)
);
?>