<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
function getRequestFields($fields)
{
	$request = array();
	//default fields

	/*$request['name'] = 'testName';
	$request['email'] = 'testEmail';
	$request['phone'] = 'testPhone';
	$request['city'] = '10000';
	$request['address'] = 'testAddres';
	$request['description'] = 'testDescription';*/

	$arError = array();
	$arrayFields = array('name', 'email', 'phone', 'city', 'address', 'description');

	foreach ($arrayFields as $name)
	{
		if (empty($fields[$name]))
			$arError[] = $name;
	}
	
	if (count($arError) > 0)
	{
		return array(
			'error' => true,
			'mess' => $arError,
		);
	}
	else
	{
		return array(
			'error' => false,
			'form_text_56' => $fields['name'],
			'form_text_57' => $fields['email'],
			'form_text_58' => $fields['phone'],
			'form_text_59' => $fields['city'],
			'form_text_60' => $fields['address'],
			'form_text_61' => $fields['description'],
		);
	}
}


function getRepairFields($fields)
{
	$request = array();
	
	// default fields
	/*$request['name'] = 'testName';
	$request['email'] = 'testEmail';
	$request['phone'] = 'testPhone';
	$request['city'] = '10000';
	$request['address'] = 'testAddres';
	$request['description'] = 'testDescription';*/
		
	$arError = array();
	$arrayFields = array('name', 'email', 'phone', 'city', 'address', 'description');


	foreach ($arrayFields as $name)
	{
		if (empty($fields[$name]))
			$arError[] = $name;
	}
	
	if (count($arError) > 0)
	{
		return array(
			'error' => true,
			'mess' => $arError,
		);
	}
	else
	{
		return array(
			'error' => false,
			'form_text_62' => $fields['name'],
			'form_text_63' => $fields['email'],
			'form_text_64' => $fields['phone'],
			'form_text_65' => $fields['city'],
			'form_text_66' => $fields['address'],
			'form_text_67' => $fields['description'],
		);
	}
}

if(!CModule::IncludeModule("iblock"))
	return;
if(!CModule::IncludeModule("form"))
	return;

//key ключ для проверки
//private key 


$privateKey = 'jRxsF5aQGWXCcEeI';

/*на стороне клиента*/
/*echo 'Случайная соль из 4 символов';
$salt = '1111';//randString(4);
PR($salt);
echo 'передаваемый пароль, формируется по формуле: Соль.md5(Соль.Пароль), где Пароль - jRxsF5aQGWXCcEeI никому не передаем!';
$keyServer = $salt.md5($salt.$privateKey);

PR($keyServer);*/

$privateKey = 'jRxsF5aQGWXCcEeI';
//PR($_REQUEST['key']);
if (isset($_REQUEST['key']) && (!empty($_REQUEST['key'])))
{
	$key = $_REQUEST['key'];
	$salt = substr($key, 0, 4);
	//формируем пароль по формуле Соль.md5(Соль.Пароль)
	$keyServer = $salt.md5($salt.$privateKey);

	if ($keyServer != $key)
	{
		echo json_encode(
			array(
				'error' => array(
					'data' => 400,
					'error_message' => 'key not valid',
				),
			)
		);

		die();
	}
}
else
{
	echo json_encode(
		array(
			'error' => array(
				'data' => 500,
				'error_message' => 'key is empty',
			),
		)
	);

	die();
}

$privateKey = 'jRxsF5aQGWXCcEeI';

$type_form = $_REQUEST['type_form'];
$request = $_REQUEST;
//PR($request);

$FORM_ID = -1;
$fieldsDB = array();

switch ($type_form)
{
	case 'request':
		$FORM_ID = 4;
		$fieldsDB = getRequestFields($request);
	//	PR($fieldsDB);
		break;
	case 'repair':
		$FORM_ID = 5;
		$fieldsDB = getRepairFields($request);
		//PR($fieldsDB);
		break;
	default:
		$fieldsDB = array(
			'error' => true,
			'mess' => 'error type_form',
		);
}
// ID веб-формы
//$arResult["arrVALUES"] = $_REQUEST;


if (!$fieldsDB['error'])
{	
	if($RESULT_ID = CFormResult::Add($FORM_ID, $fieldsDB))
	{
		echo json_encode(
			array(
				'data' => array(
					'id' => $RESULT_ID,
				),
			)
		);
		//PR($RESULT_ID);
		die();
	}
	else
	{
		global $strError;
		echo json_encode(
			array(
				'error' => array(
					'data' => 400,
					'error_message' => $strError,
				),
			)
		);
		//PR($strError);
		die();
	}
}
else
{
	echo json_encode(
		array(
			'error' => array(
				'data' => 500,
				'error_message' => $fieldsDB['mess'],
			),
		)
	);

	die();
}

?>