<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
$startTimeScript = microtime(true);

require($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_admin_before.php');

$cityId = intval($_REQUEST['city']);

function getMobileOffice($cityId)
{
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');

	$arFilter = array(
		"IBLOCK_ID" => IBLOCK_OFFICE,
		"PROPERTY_CITY" => $cityId,
		"ACTIVE" => "Y",
	);

	$arFields = array("NAME", "PROPERTY_WEEK_DAYS", "PROPERTY_SATURDAY", "PROPERTY_SUNDAY",
		"PROPERTY_ADDRESS", "DETAIL_TEXT", "PREVIEW_TEXT",
		"ID", "PROPERTY_M_CENTER", "PROPERTY_M_ZOOM", "PROPERTY_M_PLACE_MARK",
	);
	
	$res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arFields);

	$arResult = array();

	$path = $_SERVER['HTTP_HOST'];

	while($ar_res = $res->GetNext())
	{
		$arrTmp = array(
			"name" => $ar_res['NAME'],
			"id" => $ar_res['ID'],
			"week_days" => $ar_res['PROPERTY_WEEK_DAYS_VALUE'],
			"saturday" => $ar_res['PROPERTY_SATURDAY_VALUE'],
			"sunday" => $ar_res['PROPERTY_SUNDAY_VALUE'],
			"address" => $ar_res['PROPERTY_ADDRESS_VALUE'],
			"detail_text" => bzcompress($ar_res['DETAIL_TEXT']),
			"preview_text" => $ar_res['PREVIEW_TEXT'],
			"map" => array(
				"center" => $ar_res['PROPERTY_M_CENTER_VALUE'],
				"zoom" => $ar_res['PROPERTY_M_ZOOM_VALUE'],
				"place_mark" => $ar_res['PROPERTY_M_PLACE_MARK_VALUE'],
			),  
			"phone_quality" => $ar_res['PROPERTY_CITY_PROPERTY_PHONE_QUALITY_VALUE'],
			"phone_dev" => $ar_res['PROPERTY_CITY_PROPERTY_PHONE_DEV_VALUE'],
		);
		$arResult[] = $arrTmp;
	}

	$arFields = array("ID", "PROPERTY_CITY.PROPERTY_PHONE_DEV");
	$resPhone = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arFields);

	$arResultPhone = array();
	while($ar_res = $resPhone->GetNext())
	{
		$arResultPhone[] = $ar_res;
	}

	$arFields = array("ID", "PROPERTY_CITY.PROPERTY_PHONE_QUALITY");
	$resPhone = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arFields);

	while($ar_res = $resPhone->GetNext())
	{
		$arResultPhone[] = $ar_res;
	}

	foreach ($arResult as &$item)
	{
		foreach ($arResultPhone as $phone)
		{
			if ($item['id'] == $phone['ID'])
			{
				if (!empty($phone['PROPERTY_CITY_PROPERTY_PHONE_DEV_VALUE']))
					$item['phone_dev'][] = $phone['PROPERTY_CITY_PROPERTY_PHONE_DEV_VALUE'];
				if (!empty($phone['PROPERTY_CITY_PROPERTY_PHONE_QUALITY_VALUE']))
					$item['phone_quality'][] = $phone['PROPERTY_CITY_PROPERTY_PHONE_QUALITY_VALUE'];
			
			}
		}
	}
	
	return $arResult;
}


/*добавим кеширование*/
$obCache = new CPHPCache; 
$time = CACHE_TIME * 60 * 60;
$cacheId = 'mobile_office_'.$cityId;

if (isset($_REQUEST['clear_cache']))
	$obCache->Clean($cacheId);

// если кеш есть и он ещё не истек, то
if($obCache->InitCache($time, $cacheId, "/")) {
	$resCache = $obCache->GetVars();
	$arResult = $resCache["DATA"];
} else {
	// иначе обращаемся к базе
	$arResult = getMobileOffice($cityId);
}

if($obCache->StartDataCache())
{
	$obCache->EndDataCache(array(
		"DATA" => $arResult,
	)); 	
}

//PR($arResult);
$timeScript = microtime(true) - $startTimeScript;

echo json_encode(
	array(
		'data' => $arResult,
		'time' => $timeScript,
	)
);