<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
$idProgramm = intval($_REQUEST['id']);

if ($idProgramm <= 0)
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Не выбран город',
			),
		)
	);
	die();
}

$arrFullName = array(
	"д/с" => "документальный сериал",
	"д/ф" => "документальный фильм",
	"м/с" => "мультсериал",
	"м/ф" => "мультфильм",
	"т/с" => "телесериал",
	"х/ф" => "художественный фильм",
);

function getTypeProgramm()
{
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');
		
	$arFilter = array(
		"IBLOCK_ID" => IB_PROGRAMS,	
		"ACTIVE" => 'Y',
	);

	$arFields = array("NAME", "ID");

	$rsSections = CIBlockSection::GetList(array("NAME" => "ASC"), $arFilter, false, $arFields);

	$arResult = array();
	while ($arSection = $rsSections->GetNext())
	{
		$arResult[$arSection['ID']] = $arSection['NAME'];
	}
	
	return $arResult;
}

function fullName($name, $arrFullName)
{
	foreach ($arrFullName as $key => $item)
	{
		if ($key == $name)
		{
			return $item;
			break;
		}
	}
	
	return $name;
}

function getFields_($HL_Infoblock_ID, $arrFilter = array(), $arrSelect = array('*'), $limit, $f = false)
{
	$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
	//PR($arrSelect);

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 
	$Query->setSelect($arrSelect);
	$Query->setFilter($arrFilter);

	//PR($arrSelect);
	//PR($arrFilter);
	
	if (!empty($limit))
		$Query->setLimit($limit);
	//$Query->setOrder(array('UF_SORT' => 'ASC'));

	//Выполним запрос
	$result = $Query->exec();

	$result = new CDBResult($result);

	$arResult = array();
	
	global $arrFullName;
	
	$arType = getTypeProgramm();

	//PR($arType);
	while ($row = $result->Fetch())
	{		
		//PR($row);

		/*жанры множественное поле*/
		if (count($row['UF_GANRE']) > 0)
		{
			$arrGanreFilter = $row['UF_GANRE'];
			$arrGanreFilter = array('ID' => $arrGanreFilter);
			$arGanre = getFields(HL_TV_GANRE, $arrGanreFilter, array('*'));
			
			//PR($arGanre);
			$arGanre_ = array();
			foreach ($arGanre as $item)
			{
				$arGanreTmp = array(
					"id" => $item['ID'],
					"name_ru" => $item['UF_NAME_RU'],
					"name_kz" => $item['UF_NAME_EU'],
					"name_eu" => $item['UF_NAME_KZ'],
				);
				
				$arGanre_[$item['ID']] = $arGanreTmp;
			}

			foreach ($row['UF_GANRE'] as &$ganre)
			{
				$ganre = $arGanre_[$ganre];
			}
			
			unset($arGanre);
		}
		
		/*страна*/
		if (count($row['UF_COUNTRY']) > 0)
		{
			$arrCountyFilter = $row['UF_COUNTRY'];
			$arrCountyFilter = array('ID' => $arrCountyFilter);
			$arCounty = getFields(HL_TV_COUNTRY, $arrCountyFilter, array('*'));

			$row['UF_COUNTRY'] = array($row['UF_COUNTRY']);

			$arCounty_ = array();
			foreach ($arCounty as $item)
			{
				$arCountyTmp = array(
					"id" => $item['ID'],
					"name_ru" => $item['UF_NAME_RU'],
					"name_kz" => $item['UF_NAME_EU'],
					"name_eu" => $item['UF_NAME_KZ'],
				);
				
				$arCounty_[$item['ID']] = $arCountyTmp;
			}

			foreach ($row['UF_COUNTRY'] as &$country)
			{
				$country = $arCounty_[$country];
			}

			unset($arCounty);
		}
		
		//PR($row);
		foreach ($row as &$itemFields)
		{
			if ($itemFields instanceof \Bitrix\Main\Type\DateTime)
			{
				$itemFields = $itemFields->toString();
			}
		}

		$arTypeStr = array();
		if (count($row['UF_SECTION']) > 0)
		{
			foreach ($row['UF_SECTION'] as $ItemType)
			{
				$arTypeStr[] = fullName($arType[$ItemType], $arrFullName);
			}
		}
		
		$arTmp = array(
			"id" => $row['ID'],
			"name" => $row['UF_NAME'],
			"channel_id" => $row['UF_CHANNEL'],
			"date_from" => strtotime($row['UF_DATE_FROM']),
			"date_to" => strtotime($row['UF_DATE_TO']),
		);
			
		if (count($arrSelect) == 1)
		{
			$arTmp = array(
				"id" => $row['ID'],
				"name" => $row['UF_NAME'],
				"channel_id" => $row['UF_CHANNEL'],
				"rating" => $row['UF_RATING'],
				"ar_icons" => $row['UF_ICONS'],
				//"director" => $ar_res['PROPERTY_DIRECTOR_VALUE'],
				//"actor" => $ar_res['PROPERTY_ACTOR_VALUE'],
				"year" => $row['UF_YEAR'], 
				//"presenter" => $ar_res['PROPERTY_PRESENTER_VALUE'],
				"date_from" => strtotime($row['UF_DATE_FROM']),
				"date_to" => strtotime($row['UF_DATE_TO']),
				"ganre" => $row['UF_GANRE'],
				"country" => $row['UF_COUNTRY'],
				"text" => $row['UF_TEXT_RU'],
				"type" => $arTypeStr,
			);
		}
		
		$arResult[] = $arTmp;
	}

	if ($f)
		$arResult = current($arResult);

	//PR($arResult);
	return $arResult;
}

if (!CModule::IncludeModule('iblock'))
	return;

if (!CModule::IncludeModule('highloadblock'))
	return;
	
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$filter = array('ID' => $idProgramm);

$start = microtime(true); 

//добавим кеширование
$obCache = new CPHPCache; 
$time = CACHE_TIME * 60 * 60;
$cacheId = 'mobile_programm_tv'.$idProgramm; 
// если кеш есть и он ещё не истек, то
if($obCache->InitCache($time, $cacheId, "/")) {
	$resCache = $obCache->GetVars();
	$arResult['programm'] = $resCache["DATA"];
} else {
	// иначе обращаемся к базе
	$arResult['programm'] = getFields_(HL_TV_PROGRAMM, $filter, array('*'), '', true);
}

if($obCache->StartDataCache() && $arResult)
{
	$obCache->EndDataCache(array(
		"DATA" => $arResult['programm'],
	)); 	
}

$time_ = microtime(true) - $start;

/*ближайщие трансляции не кешируем и группируем по дате*/
if (!empty($arResult['programm']['name']))
{
	$filterTranslation = array('UF_CHANNEL' => $arResult['programm']['channel_id'],
		"UF_NAME" => $arResult['programm']['name'],
		">=UF_DATE_FROM"=> date("d.m.Y H:i:s", time()),
	);
	//PR($filterTranslation);
	$arFields = array("ID", "UF_DATE_FROM", "UF_DATE_TO", "UF_NAME", "UF_CHANNEL");
	$arResult['translation'] = getFields_(HL_TV_PROGRAMM, $filterTranslation, $arFields, '', false);
}

/*group*/
//$arResGroup = array();
//global $USER;

/*foreach ($arResult['translation'] as $arItem)
{
	$key = date("d.m.Y", $arItem['date_from']);
	$arResGroup[$key][] = $arItem;
}

$arResult['translation'] = $arResGroup;
*/

/*нереализованный функционал*/

/*Person*/

if ($arResult['programm']['type'][0] != 'спорт')
{
	$arResult['programm']['persons'][] = array('type' => 'presenter', 'name' => 'Александр Васильев', 'arr_icons' => array('http://xmldata.epgservice.ru:8181/EPGService/hs/xmldata/almatv_extended/perspic/443'));
	$arResult['programm']['persons'][] = array('type' => 'presenter', 'name' => 'Эвелина Хромченко', 'arr_icons' => array('http://xmldata.epgservice.ru:8181/EPGService/hs/xmldata/almatv_extended/perspic/889'));
	$arResult['programm']['persons'][] = array('type' => 'presenter', 'name' => 'Антон Привольнов', 'arr_icons' => array('http://xmldata.epgservice.ru:8181/EPGService/hs/xmldata/almatv_extended/perspic/947', 'http://xmldata.epgservice.ru:8181/EPGService/hs/xmldata/almatv_extended/perspic/949'));
	$arResult['programm']['persons'][] = array('type' => 'director', 'name' => 'Владимир Шевельков', 'arr_icons' => array('http://xmldata.epgservice.ru:8181/EPGService/hs/xmldata/almatv_extended/perspic/4023'));
	$arResult['programm']['persons'][] = array('type' => 'actor', 'name' => 'Ольга Белинская', 'arr_icons' => array());
}
else
{
	/*Sport*/
	$arResult['programm']['championship'] = 'Премьер-лига';
	$arResult['programm']['sport'] = 'Футбол';
	$arResult['programm']['rival_1'] = 'Кристал Пэлас';
	$arResult['programm']['rival_2'] = 'Сандерленд';
	$arResult['programm']['live'] = 1;
}

//PR($arResult);

echo json_encode(
	array(
		'data' => $arResult,
		'time' => $time_,
	)
);


