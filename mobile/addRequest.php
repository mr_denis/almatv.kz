<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
//type_form = repair, tv, inet

/*status, name, email, phone, city, address, description*/
function getRepairFields($request)
{
	$request = array();
	
	// default fields
	$request['status'] = 19; //20
	$request['name'] = 'testName';
	$request['email'] = 'testEmail';
	$request['phone'] = 'testPhone';
	$request['city'] = '10000';
	$request['address'] = 'testAddres';
	$request['description'] = 'testDescription';
		
	$arError = array();
	$arrayFields = array('status', 'name', 'email', 'phone', 'city', 'address', 'description');
	
	foreach ($arrayFields as $name)
	{
		if (empty($request[$name]))
			$arError[] = $name;
	}
	
	if (count($arError) > 0)
	{
		return array(
			'error' => true,
			'mess' => $arError,
		);
	}
	else
	{
		return array(
			'error' => false,
			'form_dropdown_STATUS' => $request['status'],
			'form_text_21' => $request['name'],
			'form_text_22' => $request['email'],
			'form_text_23' => $request['phone'],
			'form_text_24' => $request['city'],
			'form_text_25' => $request['address'],
			'form_textarea_26' => $request['description'],
		);
	}
}

function getTVFields($request)
{
	$request = array();
	//default fields
	$request['status'] = 1; //2
	/*
	<option value="1">физическое лицо</option>
	<option value="2">юридическое лицо</option>
	*/
	$request['name'] = 'testName';
	$request['email'] = 'testEmail';
	$request['phone'] = 'testPhone';
	$request['city'] = '10000';
	$request['address'] = 'testAddres';
	$request['type'] = 'Квартира/Частный дом';
	$request['count'] = '13';
	$request['package'] = '139'; //id пакета
	$request['extra_package'] = 'Доп. пакет[11];Доп. пакет[ID12]';
	$request['combined'] = '36';
	
	/*	
	<option value="13">1</option>
	<option value="14">2</option>
	<option value="15">3</option>
	<option value="16">4</option>
	<option value="17">5</option>
	<option value="18">более 5</option>
	*/
	$arError = array();
	$arrayFields = array('status', 'name', 'email', 'phone', 'city', 'address', 'type', 'count');
	
	foreach ($arrayFields as $name)
	{
		if (empty($request[$name]))
			$arError[] = $name;
	}
	
	if (count($arError) > 0)
	{
		return array(
			'error' => true,
			'mess' => $arError,
		);
	}
	else
	{
		return array(
			'error' => false,
			'form_dropdown_STATUS' => $request['status'],
			'form_text_4' => $request['name'],
			'form_text_5' => $request['email'],
			'form_text_6' => $request['phone'],
			'form_text_7' => $request['city'],
			'form_text_8' => $request['type'],	//Квартира, Частный дом
			'form_text_9' => $request['address'],
			'form_dropdown_COUNT' => $request['count'],	//1, 2, 3, 4, 5, Более 5
			'form_checkbox_COMBINED[]' => $request['combined'],		// value = 36 ДА
			'form_text_11' => $request['package'], //id пакета
			'form_text_12' => $request['extra_package'],  //добавляем string = NAME[ID];NAME1[ID1]
		);
	}
}

function getInetFields($request)
{
	$request = array();
	//default fields
	$request['status'] = 27; //28
	/*
	<option value="27">физическое лицо</option>
	<option value="28">юридическое лицо</option>
	*/
	$request['name'] = 'testName';
	$request['email'] = 'testEmail';
	$request['phone'] = 'testPhone';
	$request['city'] = '10000';
	$request['address'] = 'testAddres';
	$request['type'] = 'Квартира/Частный дом';
	$request['package'] = '139'; //id пакета
	$request['extra_package'] = 'Доп. пакет[11];Доп. пакет[ID12]';
	$request['combined'] = '37';
	$request['tarif'] = '116';
	$request['options'] = '8';
	/*	
	<option value="13">1</option>
	<option value="14">2</option>
	<option value="15">3</option>
	<option value="16">4</option>
	<option value="17">5</option>
	<option value="18">более 5</option>
	*/
	$arError = array();
	$arrayFields = array('status', 'name', 'email', 'phone', 'city', 'address', 'tarif');
	
	foreach ($arrayFields as $name)
	{
		if (empty($request[$name]))
			$arError[] = $name;
	}
	
	if (count($arError) > 0)
	{
		return array(
			'error' => true,
			'mess' => $arError,
		);
	}
	else
	{
		return array(
			'error' => false,
			'form_dropdown_STATUS' => $request['status'],
			'form_text_29' => $request['name'],
			'form_text_30' => $request['email'],
			'form_text_31' => $request['phone'],
			'form_text_32' => $request['city'],
			'form_text_33' => $request['address'],
			'form_checkbox_COMBINED[]' => array($request['combined']),		// value = 37 ДА
			'form_text_34' => $request['tarif'], //id тарифа
			'form_text_35' => $request['options'],//значение
		);
	}
}


if(!CModule::IncludeModule("iblock"))
	return;
if(!CModule::IncludeModule("form"))
	return;

$type_form = $_REQUEST['type_form'];
$request = $_REQUEST;

$FORM_ID = -1;
$fieldsDB = array();

switch ($type_form)
{
	case 'repair':
		$FORM_ID = 1;
		$fieldsDB = getRepairFields($request);
		PR($fieldsDB);
		break;
	case 'tv':
		$FORM_ID = 2;
		$fieldsDB = getTVFields($request);
		PR($fieldsDB);
		break;
	case 'inet':
		$FORM_ID = 3;
		$fieldsDB = getInetFields($request);
		PR($fieldsDB);
		break;
	default:
		$fieldsDB = array(
			'error' => true,
			'mess' => 'Ошибка type_form',
		);
}
// ID веб-формы
//$arResult["arrVALUES"] = $_REQUEST;

if (!$fieldsDB['error'])
{
	PR('добавляем!');
}
else
{
	PR($fieldsDB['mess']);
}
/*
if($RESULT_ID = CFormResult::Add($FORM_ID, $fieldsDB))
{
	PR($RESULT_ID);
}
else
{
    global $strError;
    echo $strError;
}*/
?>