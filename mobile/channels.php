<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
$startTimeScript = microtime(true);

function getMobileChannels($params)
{		
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');

	$arFilter = array(
		"IBLOCK_ID" => IBLOCK_CHANNELS,	
	);
	
	$arFields = array("NAME", "ID", "PREVIEW_PICTURE");

	if (!empty($params['section_id']))
	{
		$arFilter['SECTION_ID'] = explode(',', $params['section_id']);
	}
		
	if (!empty($params['sound_id']))
	{
		$arFilter['PROPERTY_SOUND'] = explode(',', $params['sound_id']);
	}

	$res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arFields);

	$arResult = array();
	while($ar_res = $res->GetNext())
	{
		$arrTmp = array(
			"id" => $ar_res['ID'],
			"name" => $ar_res['NAME'],
			"picture" => CFile::GetPath($ar_res['PREVIEW_PICTURE']),
		);
		
		$arResult[] = $arrTmp;
	}
	return $arResult;
}

if (isset($_REQUEST['clear_cache']))
	$obCache->Clean($cacheId);

if (count($_REQUEST))
{
	$arResult = getMobileChannels($_REQUEST);
}
else
{
	/*добавим кеширование*/
	$obCache = new CPHPCache; 
	$time = CACHE_TIME * 60 * 60;
	$cacheId = 'mobile_channels';
	// если кеш есть и он ещё не истек, то
	if($obCache->InitCache($time, $cacheId, "/")) {
		$resCache = $obCache->GetVars();
		$arResult = $resCache["DATA"];
	} else {
		// иначе обращаемся к базе
		$arResult = getMobileChannels($_REQUEST);
	}

	if($obCache->StartDataCache())
	{
		$obCache->EndDataCache(array(
			"DATA" => $arResult,
		)); 	
	}
}

$timeScript = microtime(true) - $startTimeScript;

echo json_encode(
	array(
		'data' => $arResult,
		'time' => $timeScript,
	)
);
?>