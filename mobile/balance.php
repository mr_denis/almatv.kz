<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
//$_REQUEST['city'] = 42;
//$_REQUEST['number'] = 100504768;
//$_REQUEST['surname'] = strtolower('Черкашина');

function getListService($usluga)
{
	$pos = strpos($usluga, '+');
	$arUsluga = array();
	if ($pos === false) {
		switch ($usluga)
		{
			case 'АТВ':
				$usluga = 'Аналоговое телевидение';
				break;
			case 'ЦТВ':
				$usluga = 'Цифровое телевидение';
				break;	
		}
		$arUsluga[] = $usluga;
	}
	else
	{
		$arrFio = explode('+', $usluga);
		foreach ($arrFio as $item)
		{
			
			switch ($item)
			{
				case 'АТВ':
					$item = 'Аналоговое телевидение';
					break;
				case 'ЦТВ':
					$item = 'Цифровое телевидение';
					break;
			}
			$arUsluga[] = $item;
		}
	}
	return $arUsluga;
}

function getArrFio($strFio)
{
	$abonent = strtolower($strFio);
	$abonent = mb_convert_case($abonent, MB_CASE_TITLE, "UTF-8");
	$arrFio = explode(' ', $abonent);
	$arrFio = array('surname' => $arrFio[0], 'name' => $arrFio[1], 'patronymic' => $arrFio[2]);
	return $arrFio;
}

if (isset($_REQUEST['surname'])) {
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/xml.php");

	$query = "date_req=".$DB->FormatDate($RATE_DAY_SHOW, CLang::GetDateFormat("SHORT", SITE_ID), "D.M.Y");
	$userNumber = (int)$_REQUEST['number']; 
	$userName = $_REQUEST['surname'];  //strtolower()
	$flAll = false;
	
	$cityId = intval($_REQUEST['city']);
	if ($cityId) {
		CModule::IncludeModule('iblock');
		$res = CIBlockElement::GetList(array(),array('IBLOCK_ID' => 8, 'ID' => $cityId), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_CHECKBALANCE_ID', 'PROPERTY_IS_CHECKBALANCE'));
		if($ar_res = $res->GetNext())
		{
			if ($ar_res['PROPERTY_IS_CHECKBALANCE_VALUE'] == 'Y')
			{
				echo json_encode(
					array(
						'error' => array(
							'code' => 400,
							'error_message' => 'В данном городе сервис не доступен',
						),
					)
				);
				die();
			}
			else
			{
				//$userNumber = 100504768;
				//$userName = strtolower('Черкашина');

				$query = QueryGetData("81.88.148.168:6064", 80, "/balance/app?command=check&surname={$userName}&number={$userNumber}&region={$ar_res['PROPERTY_CHECKBALANCE_ID_VALUE']}", $query, $errno, $errstr);
				
				//PR($query);
				//PR($errno);
				//PR($errstr);
				if (empty($query))
				{
					echo json_encode(
						array(
							'error' => array(
								'code' => 400,
								'error_message' => 'Абонент не найден',
							),
						)
					);
					die();
				}
			}
		}
		$flAll = true;
	}
	else
	{
		echo json_encode(
			array(
				'error'  => array(
					'code' => 500,
					'error_message' => 'Не выбран город',
				),
			)
		);
		//PR('Не выбран город');
		die();
	}

	if($errstr)
	{
		echo json_encode(
			array(
				'error'  => array(
					'code' => 400,
					'error_message' => $errstr,
				),
			)
		);
		//PR($errstr);
		die();
	} 
	$query = eregi_replace("<!DOCTYPE[^>]{1,}>", "", $query);
	$query = eregi_replace("<"."\?XML[^>]{1,}\?".">", "", $query);

	$objXML = new CDataXML();
	$objXML->LoadString($query);
	$arData   = $objXML->GetArray();

	$arFields = $arData['response']['#'];
	
	//PR($arData);
	//die();
	
	$code = $arFields['code'][0]['#'];

	switch ($code) {
		case 0:  //абонент не найден
			$error = 'Договор с абонентом расторжен';
			break;
		case 1:  //информация по абоненту
			break;
		case 2:
		case "-1":  //абонент не найден
			$error='Абонент не найден';
			break;
		case "300":  //внутренняя ошибка поставщика
			$error='Внутренняя ошибка поставщика';
			break;
		case "301":  //неизвестный тип запроса
			$error='Неизвестный тип запроса';
			break;
	}

	if ( $code==1 ) {
		
		if ( !$flAll ) {

			$number  = $arFields['number'][0]['#'];
			$abonent = $arFields['name'][0]['#'];
			$balance = $arFields['balance'][0]['#'];
			$status  = $arFields['status'][0]['#'];
			$usluga  = $arFields['service'][0]['#'];
			
			$tarif   = implode('<br>',explode(';',$arFields['tariff'][0]['#']));

			
			
			$data = array(
				"number" => $arFields['number'][0]['#'],
				"abonent" => getArrFio($abonent),
				"balance" => $arFields['balance'][0]['#'],
				"status" => $arFields['status'][0]['#'],
				"usluga" => getListService($usluga),
				//"usluga_base" => $usluga,
				"tarif" => $arFields['tarif'][0]['#'],
				"info1" => $arFields['info1'][0]['#'],
				"info2" => $arFields['info2'][0]['#'],
			);
			
			echo json_encode(array('data' => $data));
			die();
			?>
		<?
		} else {
			
			$usluga = $arFields['usluga'][0]['#'];
			$abonent = $arFields['abonent'][0]['#'];
			
			$data = array(
				"ndoc" => $arFields['ndoc'][0]['#'],
				"abonent" => getArrFio($abonent),
				"tcosns" => $arFields['tcosns'][0]['#'],
				"stat" => $arFields['stat'][0]['#'],
				"usluga" => getListService($usluga),
				//"usluga_base" => $usluga,
				"tposns" => $arFields['tposns'][0]['#'],
				"osnk" => $arFields['osnk'][0]['#'],
				"dtpay" => $arFields['dtpay'][0]['#'],
				"dolg" => $arFields['dolg'][0]['#'],
				"dtfk" => $arFields['dtfk'][0]['#'],
				"summfk" => $arFields['summfk'][0]['#'],
				"doc" => $arFields['doc'][0]['#'],
				"tpdops" => $arFields['tpdops'][0]['#'],
				"date" => $arFields['date'][0]['#'],
				"tp" => $arFields['tp'][0]['#'],
				"balans" => $arFields['balans'][0]['#'],
				"tarif" => $arFields['tarif'][0]['#'],
				"info1" => $arFields['info1'][0]['#'],
				"info2" => $arFields['info2'][0]['#'],
			);

			//PR($data);
			
			echo json_encode(array('data' => $data));
			die();
			?>
		<?}?>
	<? } else {
		echo json_encode(
			array(
				'error' => array(
					'code' => 400,
					'error_message' => $error,
				),
			)
		);
		die();
	} 
}
else
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Нет фамилии',
			),
		)
	);
	
	die();
}
?>