<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>

<?
$cityId = intval($_REQUEST['city']);

if (empty($cityId))
{
	echo json_encode(
		array(
			'error' => array(
				'code' => 500,
				'error_message' => 'Не передан ID города',
			),
		)
	);
	die();
}

$type = intval($_REQUEST['type']);

switch ($type)
{
	case 0:
		$typeID = 17;
		break;
	case 1:
		$typeID = 18;
		break;
	default:
		echo json_encode(
			array(
				'error' => array(
					'code' => 500,
					'error_message' => 'Не передан тип пакетов',
				),
			)
		);
		die();
}

$startTimeScript = microtime(true);

function getMobilePackages($cityId, $typeID)
{
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');
		
	$arFilter = Array("IBLOCK_ID"=>IBLOCK_PACKAGES, "ACTIVE" => "Y", "PROPERTY_CITY" => $cityId, "PROPERTY_EXT" => $typeID);  //общие пакеты
	$res = CIBlockElement::GetList(Array("SORT" => "DESC"), $arFilter, false, false, array("ID", "NAME", "PROPERTY_PRICE", "PROPERTY_NAME_KZ", "PROPERTY_NAME_EU", "PROPERTY_RECOMMEND", "PROPERTY_TYPE"));
	$arResult = array();
	$arrId = array();

	while($ob = $res->GetNext()) 
	{
		$arrId[] = $ob['ID'];

		$arTmp = array(
			"id" => $ob['ID'],
			"name" => $ob['NAME'],
			"price" => $ob['PROPERTY_PRICE_VALUE'],
		);
			
		//15 квартира, 16 дом
		if ($ob['PROPERTY_TYPE_ENUM_ID'] == PACKAGE_TYPE_HOME)
		{
			$arResult['home'][] = $arTmp;
		}
		else if ($ob['PROPERTY_TYPE_ENUM_ID'] == PACKAGE_TYPE_ROOM)
		{
			$arResult['room'][] = $arTmp;
		}
	}

	/*находим число каналов для каждого пакета*/
	if (count($arrId) > 0)
	{	
		$arFilter = Array("IBLOCK_ID"=> IBLOCK_CHANNELS, "PROPERTY_PACKAGES" => $arrId, "ACTIVE" => "Y", "PROPERTY_CITY" => $cityId);  //общие пакеты
		$res = CIBlockElement::GetList(Array(), $arFilter, array("PROPERTY_PACKAGES"), false, array("ID"));

		$arChannelCount = array();
		while($ob = $res->GetNext()) 
		{
			$arChannelCount[$ob['PROPERTY_PACKAGES_VALUE']] = $ob['CNT']; 
		}

		foreach ($arResult['home'] as &$item)
		{
			$item['num'] = $arChannelCount[$item['id']];
		}

		foreach ($arResult['room'] as &$item)
		{
			$item['num'] = $arChannelCount[$item['id']];
		}
	}

	return $arResult;
}

/*добавим кеширование*/
$obCache = new CPHPCache; 
$time = CACHE_TIME * 60 * 60;
$cacheId = 'mobile_packages_'.$cityId.'_'.$typeID;

if (isset($_REQUEST['clear_cache']))
	$obCache->Clean($cacheId);

// если кеш есть и он ещё не истек, то
if($obCache->InitCache($time, $cacheId, "/")) {
	$resCache = $obCache->GetVars();
	$arResult = $resCache["DATA"];
} else {
	// иначе обращаемся к базе
	$arResult = getMobilePackages($cityId, $typeID);
}

if($obCache->StartDataCache())
{
	$obCache->EndDataCache(array(
		"DATA" => $arResult,
	)); 	
}

$timeScript = microtime(true) - $startTimeScript;

//PR($arResult);
echo json_encode(
	array(
		'data' => $arResult,
		'time' => $timeScript,
	)
);