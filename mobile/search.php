<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
function getMobilePrograms($query, $ganre)
{
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');

	$arFilter = Array("IBLOCK_ID"=>IB_PROGRAMS, "ACTIVE"=>"Y", "NAME" => '%'.$query.'%');	
	//time() - 3600 * 24 * 1
	//time() + 3600 * 24 * 7
	
	$arFilter[">=PROPERTY_DATE_FROM"] = date("Y-m-d H:i:s", time() - 3600 * 24 * 8);
	$arFilter["<=PROPERTY_DATE_FROM"] = date("Y-m-d H:i:s", time() + 3600 * 24 * 8);

	if (!empty($ganre))
	{
		$arFilter['PROPERTY_GANRE'] = explode(',', $ganre);
	}

	$res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, array('nTopCount' => 50), array('PROPERTY_DATE_FROM', 'PROPERTY_TO_FROM', 'PROPERTY_GANRE', 'PROPERTY_ICONS', 'NAME', 'ID', 'PROPERTY_CHANNEL', 'PROPERTY_CHANNEL.NAME'));
	
	$arResult['PROGRAMM'] = null;
	$countProgram = 0;
	$arChannelsIdTV = array();
	
	while($ob = $res->GetNext()) 
	{
		$arrTmp = array(
			"id" => $ob['ID'],
			"icons" => $ob['PROPERTY_ICONS_VALUE'],
			"name" => $ob['NAME'],
			//"date_from" => strtotime($ob['PROPERTY_DATE_FROM_VALUE']),
			//"date_to" => strtotime($ob['PROPERTY_DATE_TO_VALUE']),
			"channel" => $ob['PROPERTY_CHANNEL_VALUE'],
			"channel_name" => $ob['PROPERTY_CHANNEL_NAME'],
		);
		
		$arResult['PROGRAMM'][] = $arrTmp;
		$arChannelsIdTV[$ob['PROPERTY_CHANNEL_VALUE']] = null;
		$countProgram++;
		$keysChannelsId = array_keys($arChannelsIdTV);
		unset($arChannelsIdTV);
	}
	
	return array(
		"programm" => $arResult['PROGRAMM'],
		"countProgram" => $countProgram,
		"idChannels" => asort($keysChannelsId),
	);
}

function getMobileChannels($query, $idChannels, $sound)
{
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');
	
			
	$arFilter = Array("IBLOCK_ID"=>IBLOCK_CHANNELS, "ACTIVE"=>"Y", "NAME" => '%'.$query.'%', "ID" => $idChannels);

	if (!empty($sound))
	{
		$arFilter['PROPERTY_SOUND'] = explode(',', $sound);
	}
	
	$res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, array('nTopCount' => 50), array('NAME', 'ID', 'PREVIEW_PICTURE'));
    $countChannel = 0;
	$arResult['CHANNEL'] = null;
	while ($row = $res->Fetch())
    {
		//$arrIdChannel[] = $row['ID'];
		$image = CFile::ResizeImageGet(
			$row["PREVIEW_PICTURE"],
			array("width" => 25, "height" =>25),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true,
			false
		);
		$row['PREVIEW_PICTURE'] = $image['src'];
		$countChannel++;

		$arrTmp = array(
			"id" => $row['ID'],
			"name" => $row['NAME'],
			"preview_picture" => $row['PREVIEW_PICTURE'],
		);
		
        $arResult['CHANNEL'][] = $arrTmp;
    }
	
	return array(
		"channels" => $arResult['CHANNEL'],
		"countChannel" => $countChannel,
	);
}
	
if (isset($_REQUEST['query']) && (!empty($_REQUEST['query'])))
{
    $query = $_REQUEST['query'];

	$startTimeScript = microtime(true);
	
	if (empty($query))
	{
		echo json_encode(
			array(
				'error' => array(
					'code' => 500,
					'error_message' => 'Пустая строка запроса',
				),
			)
		);
		
		die();
	}
	
	if (strlen($query) < 2)
	{
		echo json_encode(
			array(
				'error' => array(
					'code' => 500,
					'error_message' => 'min 2 символа',
				),
			)
		);
		die();
	}
	
	/*добавим кеширование для программ, жанров может не быть*/
	$obCache = new CPHPCache; 
	$time = CACHE_TIME * 60 * 60;
	
	$ganre = '';
	if (!empty($_REQUEST['ganre']))
	{
		$ganre = $_REQUEST['ganre'];
	}
	
	$cacheId = 'mobile_search_programm_'.$query.$ganre;
	// если кеш есть и он ещё не истек, то
	if($obCache->InitCache($time, $cacheId, "/")) {
		$resCache = $obCache->GetVars();
		$arResult['PROGRAMM'] = $resCache["DATA_PROGRAMM"];
	} else {
		// иначе обращаемся к базе
		$arResult['PROGRAMM'] = getMobilePrograms($query, $ganre);
	}

	if($obCache->StartDataCache())
	{
		$obCache->EndDataCache(array(
			"DATA_PROGRAMM" => $arResult['PROGRAMM'],
		)); 	
	}
	//PR($keysChannelsId);
	
	$sound = '';
	if (!empty($_REQUEST['sound_ch']))
	{
		$sound = $_REQUEST['sound_ch'];
	}
	
	/*добавим кеширование для программ, учитываю вывод по программам*/
	$obCache = new CPHPCache; 
	$time = CACHE_TIME * 60 * 60;
	$cacheId = 'mobile_search_channels_'.$query.$arResult['PROGRAMM']['idChannels'].$sound;
	// если кеш есть и он ещё не истек, то
	if($obCache->InitCache($time, $cacheId, "/")) {
		$resCache = $obCache->GetVars();
		$arResult['CHANNELS'] = $resCache["DATA_СHANNELS"];
	} else {
		// иначе обращаемся к базе
		$arResult['CHANNELS'] = getMobileChannels($query, $arResult['PROGRAMM']['idChannels'], $sound);
	}

	if($obCache->StartDataCache())
	{
		$obCache->EndDataCache(array(
			"DATA_СHANNELS" => $arResult['CHANNELS'],
		)); 	
	}

	//PR($arResult);
	//die();
	$timeScript = microtime(true) - $startTimeScript;
	
	echo json_encode(
		array(
			'data' => array('programms' => $arResult['PROGRAMM']['programm'], 'channels' => $arResult['CHANNELS']['channels'], 'count_channels' => $arResult['CHANNELS']['countChannel'], 'count_proramms' => $arResult['PROGRAMM']['countProgram']),
			'time' => $timeScript,
		)
	);
}
else
{
	echo json_encode(
		array(
			'error' => array(
				'code' => 500,
				'error_message' => 'Обязательный параметр query',
			),
		)
	);
		
	die();
}