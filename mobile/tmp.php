<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

function getMobileChannels($params)
{		
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');

	$arFilter = array(
		"IBLOCK_ID" => IBLOCK_CHANNELS,	
	);

	$arFields = array("NAME", "ID", "PREVIEW_PICTURE");

	if (!empty($params['section_id']))
	{
		$arFilter['SECTION_ID'] = explode(',', $params['section_id']);
	}
		
	$res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, $arFields);

	/*if (!empty($params['sound_id']))
	{
		$arFilter['PROPERTY_SOUND'] = explode(',', $params['sound_id']);
	}*/

	$arResult = array();
	while($ar_res = $res->GetNext())
	{
		$arResult[] = $ar_res['ID'];
	}
	
	return $arResult;
}

function getMobileGanreId($arChannels/*$query, $ganre*/)
{	
	if (!CModule::IncludeModule('highloadblock'))
		return;

	$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById(HL_TV_PROGRAMM)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$arrFilter = array('UF_CHANNEL' => $arChannels,/*'UF_NAME' => '%'.$query.'%',*/
		">=UF_DATE_FROM" => date("d.m.Y H:i:s", time() - 3600 * 24 * 8),
		"<=UF_DATE_TO" => date("d.m.Y H:i:s", time() + 3600 * 24 * 8),
	);
	
	/*if (!empty($ganre))
	{
		$arrFilter['UF_GANRE'] = explode(',', $ganre);
	}*/

	$Entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 
	
	$rsData = $entity_data_class::getList([
        'select'  => array('CNT', 'UF_GANRE'),
        'runtime' => [
            new \Bitrix\Main\Entity\ExpressionField('CNT', 'COUNT(*)')
        ],
        'filter'  => $arrFilter,
        'group'   => ['UF_GANRE'],  /*частичная группировка по множественному полю*/
    ]);
	/*$Query->setSelect(array('UF_GANRE'));
	$Query->setFilter($arrFilter);
	
	//if (!empty($limit))
	$Query->setLimit(50);
	//$Query->setOrder(array('UF_SORT' => 'ASC'));

	//Выполним запрос
	$result = $Query->exec();*/

	//$result = new CDBResult($result);

	//$arResult = array();

	/*$arResult['PROGRAMM'] = null;
	$countProgram = 0;
	$arChannelsIdTV = array();*/
	
	$arResultGanre = array();
	
	while ($row = $rsData->Fetch())
	{
		foreach ($row['UF_GANRE'] as $ganre)
		{
			$arResultGanre[$ganre] = null;
		}
	}

	return array_keys($arResultGanre);
}

function getMobileGanre($arrId)
{
	if(!CModule::IncludeModule("iblock"))
		return;

	if (!CModule::IncludeModule('highloadblock'))
		return;

	$hlblock = HL\HighloadBlockTable::getById(HL_TV_GANRE)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 


	$Query->setFilter(array("ID" => $arrId));
	$Query->setSelect(array('*'));

	$result = $Query->exec();

	$result = new CDBResult($result);

	$arResult = array();
	
	while ($row = $result->Fetch())
	{
		$itemGanre = array(
			'id' => $row['ID'],
            'name_ru' => $row['UF_NAME_RU'],
            'name_eu' => $row['UF_NAME_EU'],
            'name_kz' => $row['UF_NAME_KZ'],
		);
		
		$arResult[$row['ID']][] = $itemGanre;
	}

	return $arResult;
}

$params = $_REQUEST;
$arChannels = getMobileChannels($params);

$startTimeScript = microtime(true);
$arResultGanre = getMobileGanreId($arChannels);

$arResultGanre = getMobileGanre($arResultGanre);

$timeScript = microtime(true) - $startTimeScript;

PR($arResultGanre);

echo json_encode(
	array(
		'data' => $arResultGanre,
		'time' => $timeScript,
	)
);
?>