<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
CModule::IncludeModule('iblock');
//require($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_admin_before.php');

function getTypeProgramm()
{
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');
		
	$arFilter = array(
		"IBLOCK_ID" => IB_PROGRAMS,	
		"ACTIVE" => 'Y',
	);

	$arFields = array("NAME", "ID");

	$rsSections = CIBlockSection::GetList(array("NAME" => "ASC"), $arFilter, false, $arFields);

	$arResult = array();
	while ($arSection = $rsSections->GetNext())
	{
		$arResult[$arSection['ID']] = $arSection['NAME'];
	}
	
	return $arResult;
}

function getFields_($HL_Infoblock_ID, $arrFilter = array(), $arrSelect = array('*'), $limit)
{
	$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 
	$Query->setSelect($arrSelect);
	$Query->setFilter($arrFilter);
	
	if (!empty($limit))
		$Query->setLimit($limit);
	//$Query->setOrder(array('UF_SORT' => 'ASC'));

	//Выполним запрос
	$result = $Query->exec();

	$result = new CDBResult($result);

	$arResult = array();
	
	$arType = getTypeProgramm();
	//PR($arType);
	while ($row = $result->Fetch())
	{
		foreach ($row as &$itemFields)
		{
			if ($itemFields instanceof \Bitrix\Main\Type\DateTime)
			{
				$itemFields = $itemFields->toString();
			}
		}

		$arTypeStr = array();
		foreach ($row['UF_SECTION'] as $ItemType)
		{
			$arTypeStr[] = $arType[$ItemType];
		}

		//$arResult[$row['ID']] = $row;
		$arrTmp = array(
			"id" => $row['ID'],
			"name" => $row['UF_NAME'],
			"channel_id" => $row['UF_CHANNEL'],
			"rating" => $row['UF_RATING'],
			"ar_icons" => $row['UF_ICONS'],
			//"director" => $ar_res['PROPERTY_DIRECTOR_VALUE'],
			//"actor" => $ar_res['PROPERTY_ACTOR_VALUE'],
			"year" => $row['UF_YEAR'], 
			//"presenter" => $ar_res['PROPERTY_PRESENTER_VALUE'],
			"date_from" => strtotime($row['UF_DATE_FROM']),
			"date_to" => strtotime($row['UF_DATE_TO']),
			"ganre" => $row['UF_GANRE'],
			"text" => $row['UF_TEXT_RU'],
			"type" => $arTypeStr,
		);
		
		$arResult[] = $arrTmp;
	}
	//PR($arResult);
	return $arResult;
}

if (isset($_REQUEST['id']))
{
	$idChannel = explode(',',$_REQUEST['id']);
}
else
	$idChannel = '';

$idSection = intval($_REQUEST['section_id']);

//PR($idSection);
/*if (empty($idChannel))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Не задан канал',
			),
		)
	);
	die();
}*/

$date_from = $_REQUEST['date_from'];
$date_to = $_REQUEST['date_to'];

if (empty($date_to))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Не задан date_to',
			),
		)
	);
	die();
}

if (empty($date_from))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Не задано date_from',
			),
		)
	);
	die();
}

if ($date_to > (time() + 3600 * 24 * 14))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Ограничение по дате',
			),
		)
	);
	die();
}

if ($date_from < (time() - 3600 * 24 * 14))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Ограничение по дате',
			),
		)
	);
	die();
}

if (!CModule::IncludeModule('highloadblock'))
	return;
	
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$filter = array(/*'UF_CHANNEL' => $idChannel,*/
	">=UF_DATE_FROM"=> date("d.m.Y H:i:s", $date_from),
	"<=UF_DATE_TO"=> date("d.m.Y H:i:s", $date_to),
);

$start = microtime(true); 

if (!empty($idChannel))
{
	//по 1 каналу
	$filter["UF_CHANNEL"] = $idChannel;
}
else
{
	/*выбираем опред. каналы и добавляем их в фильтр*/
	$f = $f1 = false;
	if ($idSection > 0)
	{
		$arrFilter = array(
			"IBLOCK_ID" => IBLOCK_CHANNELS,	
			"SECTION_ID" => $idSection,
		);
		
		$f = true;
	}
	
	if (!empty($_REQUEST['sound_id']))
	{
		$arrFilter['PROPERTY_SOUND'] = explode(',', $_REQUEST['sound_id']);
		
		$f = true;
	}
	
	$arChannels = array(0);

	if ($f || $f1) 
	{			
		$arFields = array("ID");
		
		$res = CIBlockElement::GetList(array("ID" => "ASC"), $arrFilter, false, false, $arFields);

		while($ar_res = $res->GetNext())
		{
			$arChannels[] = $ar_res['ID'];
		}
		
		$filter['UF_CHANNEL'] = $arChannels;
	}
}
/*добавим фильтр по жанрам*/
if (!empty($_REQUEST['ganre']))
{
	$filter['UF_GANRE'] = explode(',', $_REQUEST['ganre']);
}
	
if (count($arChannels) == 1)
{
	echo json_encode(
		array(
			'data' => array(),
		)
	);
	die();
}

//PR($arFilter);
//die();

/*
$arFields = array("NAME", "ID", "PREVIEW_TEXT", "PROPERTY_CHANNEL", "PROPERTY_RATING", "PROPERTY_ICONS", "PROPERTY_DIRECTOR",
"PROPERTY_ACTOR", "PROPERTY_YEAR", "PROPERTY_PRESENTER", "PROPERTY_DATE_TO", "PROPERTY_DATE_FROM",
"GANRE");

$res = CIBlockElement::GetList(array('PROPERTY_DATE_FROM' => 'ASC'), $arFilter, false, false, $arFields);

$arResult = array();
while($ar_res = $res->GetNext())
{
	$arrTmp = array(
		"id" => $ar_res['ID'],
		"name" => $ar_res['NAME'],
		"channel_id" => $ar_res['PROPERTY_CHANNEL_VALUE'],
		"rating" => $ar_res['PROPERTY_RATING_VALUE'],
		"ar_icons" => $ar_res['PROPERTY_ICONS_VALUE'],
		"director" => $ar_res['PROPERTY_DIRECTOR_VALUE'],
		"actor" => $ar_res['PROPERTY_ACTOR_VALUE'],
		"year" => $ar_res['PROPERTY_YEAR_VALUE'], 
		"presenter" => $ar_res['PROPERTY_PRESENTER_VALUE'],
		"date_from" => strtotime($ar_res['PROPERTY_DATE_FROM_VALUE']),
		"date_to" => strtotime($ar_res['PROPERTY_DATE_TO_VALUE']),
		"ganre" => $ar_res['PROPERTY_GANRE_VALUE'],
		"text" => $ar_res['PREVIEW_TEXT'],
	);
	$arResult[] = $ar_res;//$arrTmp;
}*/

//PR($arResult);

//UF_GANRE
$arResult = getFields_(HL_TV_PROGRAMM, $filter, array('*'), '');

//PR($arResult);
//die();

$time = microtime(true) - $start;

echo json_encode(
	array(
		'data' => $arResult,
		'time' => $time,
	)
);


