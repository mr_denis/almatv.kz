<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
$startTimeScript = microtime(true);

function getMobileCity()
{
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');
	
	$res = CIBlockElement::GetList(array(),array('IBLOCK_ID' => IBLOCK_CITY, "ACTIVE" => "Y",/*'ID' => $cityId*/), false, false, array("NAME", "ID", "PROPERTY_LATITUDE", "PROPERTY_LONGITUDE", "PROPERTY_SCALE"));
	$arResult = array();
	while($ar_res = $res->GetNext())
	{
		$arrTmp = array(
			"id" => $ar_res['ID'],
			"name" => $ar_res['NAME'],
			"latitude" => $ar_res['PROPERTY_LATITUDE_VALUE'],
			"longitude" => $ar_res['PROPERTY_LONGITUDE_VALUE'],
			"scale" => $ar_res['PROPERTY_SCALE_VALUE'],
		);
		
		$arResult[] = $arrTmp;
	}

	return $arResult;
}

if (isset($_REQUEST['clear_cache']))
	$obCache->Clean($cacheId);

if (count($_REQUEST))
{
	$arResult = getMobileCity();
}
else
{
	/*добавим кеширование*/
	$obCache = new CPHPCache; 
	$time = CACHE_TIME * 60 * 60;
	$cacheId = 'mobile_city';
	// если кеш есть и он ещё не истек, то
	if($obCache->InitCache($time, $cacheId, "/")) {
		$resCache = $obCache->GetVars();
		$arResult = $resCache["DATA"];
	} else {
		// иначе обращаемся к базе
		$arResult = getMobileCity();
	}

	if($obCache->StartDataCache())
	{
		$obCache->EndDataCache(array(
			"DATA" => $arResult,
		)); 	
	}
}

$timeScript = microtime(true) - $startTimeScript;

if (count($arResult))
{
	echo json_encode(
		array(
			'data' => $arResult,
			'time' => $timeScript,
		)
	);
}
else
{
	echo json_encode(
		array(
			'error' => array(
				'code' => 400,
				'error_message' => 'Ошибка БД',
			),
		)
	);
}