<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
CModule::IncludeModule('iblock');
//require($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_admin_before.php');

if (isset($_REQUEST['id']))
{
	$idChannel = explode(',',$_REQUEST['id']);
}
else
	$idChannel = '';

$idSection = intval($_REQUEST['section_id']);

//PR($idSection);
/*if (empty($idChannel))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Не задан канал',
			),
		)
	);
	die();
}*/

$date_from = $_REQUEST['date_from'];
$date_to = $_REQUEST['date_to'];

if (empty($date_from))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Не задано date_from',
			),
		)
	);
	die();
}

if (empty($date_to))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Не задан date_to',
			),
		)
	);
	die();
}

if ($date_from < (time() - 3600 * 24 * 14))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Ограничение по дате',
			),
		)
	);
	die();
}

if ($date_to > (time() + 3600 * 24 * 14))
{
	echo json_encode(
		array(
			'error'  => array(
				'code' => 500,
				'error_message' => 'Ограничение по дате',
			),
		)
	);
	die();
}

define(IBLOCK_PROGRAMM, 17);

$arFilter = array(
	"IBLOCK_ID" => IBLOCK_PROGRAMM,	
	"ACTIVE" => "Y",
	">=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", $_REQUEST['date_from']),
	"<=PROPERTY_DATE_TO"=> date("Y-m-d H:i:s", $_REQUEST['date_to']),
);

//PR(count($idChannel));
//die();

$start = microtime(true); 

if (!empty($idChannel))
{
	//
	$arFilter["PROPERTY_CHANNEL"] = $idChannel;
}
else
{
	$f = $f1 = false;
	if ($idSection > 0)
	{
		$arrFilter = array(
			"IBLOCK_ID" => IBLOCK_CHANNELS,	
			"SECTION_ID" => $idSection,
		);
		
		$f = true;
	}
	
	if (!empty($_REQUEST['sound_id']))
	{
		$arrFilter['PROPERTY_SOUND'] = explode(',', $_REQUEST['sound_id']);
		
		$f = true;
	}
	
	$arChannels = array(0);

	if ($f || $f1) 
	{			
		$arFields = array("ID");
		
		$res = CIBlockElement::GetList(array("ID" => "ASC"), $arrFilter, false, false, $arFields);

		while($ar_res = $res->GetNext())
		{
			$arChannels[] = $ar_res['ID'];
		}
		
		$arFilter['PROPERTY_CHANNEL'] = $arChannels;
	}
}

//PR($arFilter);

if (count($arChannels) == 1)
{
	echo json_encode(
		array(
			'data' => array(),
		)
	);
	die();
}

//PR($arFilter);
//die();

$arFields = array("NAME", "ID", "PREVIEW_TEXT", "PROPERTY_CHANNEL", "PROPERTY_RATING", "PROPERTY_ICONS", "PROPERTY_DIRECTOR",
"PROPERTY_ACTOR", "PROPERTY_YEAR", "PROPERTY_PRESENTER", "PROPERTY_DATE_TO", "PROPERTY_DATE_FROM",
"GANRE");

$res = CIBlockElement::GetList(array('PROPERTY_DATE_FROM' => 'ASC'), $arFilter, false, false, $arFields);

$arResult = array();
while($ar_res = $res->GetNext())
{
	$arrTmp = array(
		"id" => $ar_res['ID'],
		"name" => $ar_res['NAME'],
		"channel_id" => $ar_res['PROPERTY_CHANNEL_VALUE'],
		"rating" => $ar_res['PROPERTY_RATING_VALUE'],
		"ar_icons" => $ar_res['PROPERTY_ICONS_VALUE'],
		"director" => $ar_res['PROPERTY_DIRECTOR_VALUE'],
		"actor" => $ar_res['PROPERTY_ACTOR_VALUE'],
		"year" => $ar_res['PROPERTY_YEAR_VALUE'], 
		"presenter" => $ar_res['PROPERTY_PRESENTER_VALUE'],
		"date_from" => strtotime($ar_res['PROPERTY_DATE_FROM_VALUE']),
		"date_to" => strtotime($ar_res['PROPERTY_DATE_TO_VALUE']),
		"ganre" => $ar_res['PROPERTY_GANRE_VALUE'],
		"text" => $ar_res['PREVIEW_TEXT'],
	);
	$arResult[] = $arrTmp;
}

$time = microtime(true) - $start;

//PR($arResult);

echo json_encode(
	array(
		'data' => $arResult,
		'time' => $time,
	)
);
//PR($arResult);
//die();