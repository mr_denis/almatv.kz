<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
require($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_admin_before.php');

$startTimeScript = microtime(true);

$cityId = intval($_REQUEST['city']);
$type = htmlspecialchars($_REQUEST['type']);

if (empty($cityId))
{
	echo json_encode(
		array(
			'error' => array(
				'code' => 500,
				'error_message' => 'Не передан ID города',
			),
		)
	);
	die();
}

if (empty($type))
{
	echo json_encode(
		array(
			'error' => array(
				'code' => 500,
				'error_message' => 'Не передан Раздел',
			),
		)
	);
	die();
}

$sectionId = 19;
switch ($type)
{
	case 'news':
		$sectionId = 19;
		break;
	case 'actions':
		$sectionId = 124;
		break;
	case 'info':
		$sectionId = 95;
		break;
	default:
		echo json_encode(
			array(
				'error' => array(
					'code' => 500,
					'error_message' => 'Не верный тип',
				),
			)
		);
		die();
}


function getMobileNews($cityId, $section_id)
{
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');

	$arFilter = array(
		"IBLOCK_ID" => IB_NEWS,
		"PROPERTY_CITY" => $cityId,
		"ACTIVE" => "Y",
		"SECTION_ID" => $section_id,
	);

	$arFields = array("ID", "NAME", "ACTIVE_FROM", "PREVIEW_TEXT", "DETAIL_TEXT", "PREVIEW_PICTURE", "DETAIL_PICTURE"/*, "PROPERTY_CITY_TO_NEWS"*/, "PROPERTY_MOBILE_TEXT", "IBLOCK_SECTION_ID");

	$res = CIBlockElement::GetList(array("ACTIVE_FROM" => "DESC"), $arFilter, false, false/*array("nTopCount" => 2)*/, $arFields);

	$arResult = array();
	//PR($_SERVER['HTTP_HOST']);
	$path = $_SERVER['HTTP_HOST'];

	while($ar_res = $res->GetNext())
	{
		if (!empty($ar_res['DETAIL_PICTURE']))
			$detailPicture = $path.CFile::GetPath($ar_res["DETAIL_PICTURE"]);
		if (!empty($ar_res['PREVIEW_PICTURE']))
			$previewPicture = $path.CFile::GetPath($ar_res["PREVIEW_PICTURE"]);

		$arrTmp = array(
			"id" => $ar_res['ID'],
			"name" => $ar_res['NAME'],
			//"aa" => $ar_res['ACTIVE_FROM'],
			"action_from" => strtotime($ar_res['ACTIVE_FROM']),
			//"previewText" => $ar_res['PREVIEW_TEXT'],
			"text" => $ar_res['PROPERTY_MOBILE_TEXT_VALUE']['TEXT'],
			/*"preview_text" => $ar_res['PREVIEW_TEXT'], //gzencode
			"detail_text" => $ar_res['DETAIL_TEXT'],*/
			"detail_picture" => $detailPicture,
			"preview_picture" => $previewPicture,
			"section_id" => $ar_res['IBLOCK_SECTION_ID'],
		);
		$arResult[] = $arrTmp;
	}
	
	return $arResult;
}

/*добавим кеширование*/
$obCache = new CPHPCache; 
$time = CACHE_TIME * 60 * 60;
$cacheId = 'mobile_news_'.$cityId.$type;

if (isset($_REQUEST['clear_cache']))
	$obCache->Clean($cacheId);

// если кеш есть и он ещё не истек, то
if($obCache->InitCache($time, $cacheId, "/")) {
	$resCache = $obCache->GetVars();
	$arResult = $resCache["DATA"];
} else {
	// иначе обращаемся к базе
	$arResult = getMobileNews($cityId, $sectionId);
}

if($obCache->StartDataCache())
{
	$obCache->EndDataCache(array(
		"DATA" => $arResult,
	)); 	
}

$timeScript = microtime(true) - $startTimeScript;

//flush();

//PR($arResult);

echo json_encode(
	array(
		'data' => $arResult,
		'time' => $timeScript,
	)
);
