<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
$startTimeScript = microtime(true);

function getMobileSound()
{
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');

	$res = CIBlockElement::GetList(array(),array('IBLOCK_ID' => IBLOCK_SOUND, "ACTIVE" => "Y"), false, false, array("NAME", "ID", "PROPERTY_LATITUDE", "PROPERTY_LONGITUDE", "PROPERTY_SCALE"));
	$arResult = array();
	while($ar_res = $res->GetNext())
	{
		//PR($ar_res);
		$arrTmp = array(
			"id" => $ar_res['ID'],
			"name" => $ar_res['NAME'],
			"preview_picture" => '',
			"detail_picture" => '',
		);
		
		$arResult[] = $arrTmp;
	}
	
	return $arResult;
}

if (count($_REQUEST))
{
	$arResult = getMobileSound();
}
else
{
	/*добавим кеширование*/
	$obCache = new CPHPCache; 
	$time = CACHE_TIME * 60 * 60;
	$cacheId = 'mobile_sound';
	
	if (isset($_REQUEST['clear_cache']))
		$obCache->Clean($cacheId);

	// если кеш есть и он ещё не истек, то
	if($obCache->InitCache($time, $cacheId, "/")) {
		$resCache = $obCache->GetVars();
		$arResult = $resCache["DATA"];
	} else {
		// иначе обращаемся к базе
		$arResult = getMobileSound();
	}

	if($obCache->StartDataCache())
	{
		$obCache->EndDataCache(array(
			"DATA" => $arResult,
		)); 	
	}
}

$timeScript = microtime(true) - $startTimeScript;

//PR($arResult);
if (count($arResult))
{
	echo json_encode(
		array(
			'data' => $arResult,
			'time' => $timeScript,
		)
	);
}
else
{
	echo json_encode(
		array(
			'error' => array(
				'code' => 400,
				'error_message' => 'Ошибка БД',
			),
		)
	);
}