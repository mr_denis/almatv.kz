<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>

<?
$cityId = intval($_REQUEST['city']);

$startTimeScript = microtime(true);

if (empty($cityId))
{
	echo json_encode(
		array(
			'error' => array(
				'code' => 500,
				'error_message' => 'Не передан ID города',
			),
		)
	);
	die();
}

function getMobileInet($cityId)
{
	if (!CModule::IncludeModule('iblock'))
		return new Exception('Не установлен модуль инфоблоки');

	$arFilter = Array("IBLOCK_ID"=>IB_INET, "ACTIVE" => "Y", "PROPERTY_CITY" => $cityId);  //общие пакеты
	$res = CIBlockElement::GetList(Array("SORT" => "DESC"), $arFilter, false, false, array("ID", "NAME", "PROPERTY_PRICE", "PROPERTY_NAME_KZ", "PROPERTY_NAME_EU", "PROPERTY_V"));
	$arResult = array();

	while($ob = $res->GetNext()) 
	{
		$arTmp = array(
			"id" => $ob['ID'],
			"name" => $ob['NAME'],
			"price" => $ob['PROPERTY_PRICE_VALUE'],
			"speed" => $ob['PROPERTY_V_VALUE'],
		);

		$arResult[] = $arTmp;
	}
	
	return $arResult;
}

/*добавим кеширование*/
$obCache = new CPHPCache; 
$time = CACHE_TIME * 60 * 60;
$cacheId = 'mobile_inet_'.$cityId;

if (isset($_REQUEST['clear_cache']))
	$obCache->Clean($cacheId);

// если кеш есть и он ещё не истек, то
if($obCache->InitCache($time, $cacheId, "/")) {
	$resCache = $obCache->GetVars();
	$arResult = $resCache["DATA"];
} else {
	// иначе обращаемся к базе
	$arResult = getMobileInet($cityId);
}

if($obCache->StartDataCache())
{
	$obCache->EndDataCache(array(
		"DATA" => $arResult,
	)); 	
}

$timeScript = microtime(true) - $startTimeScript;

//PR($arResult);
echo json_encode(
	array(
		'data' => $arResult,
		'time' => $timeScript,
	)
);