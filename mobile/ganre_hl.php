<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
$startTimeScript = microtime(true);
$sort = $_REQUEST['sort'];

switch ($sort)
{
	case 'ru':
		$arOrder = array('UF_NAME_RU' => 'ASC');
		break;
	case 'eu':
		$arOrder = array('UF_NAME_EU' => 'ASC');
		break;
	case 'kz':
		$arOrder = array('UF_NAME_KZ' => 'ASC');
		break;
	default:
		$arOrder = array('UF_NAME_RU' => 'ASC');
}

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

function getGanre($id, $arOrder)
{
	if(!CModule::IncludeModule("iblock"))
		return;

	if (!CModule::IncludeModule('highloadblock'))
		return;

	$hlblock = HL\HighloadBlockTable::getById($id)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 

	//$Query->setFilter(array(/*"UF_NAME_RU" => $name_ru*/));
	$Query->setSelect(array('*'));
	$Query->setOrder($arOrder);
	
	$result = $Query->exec();

	$result = new CDBResult($result);

	$arResult = array();
	
	while ($row = $result->Fetch())
	{
		$itemGanre = array(
			'id' => $row['ID'],
            'name_ru' => $row['UF_NAME_RU'],
            'name_eu' => $row['UF_NAME_EU'],
            'name_kz' => $row['UF_NAME_KZ'],
		);
		
		$arResult[] = $itemGanre;
	}
	
	return $arResult;
}


/*добавим кеширование*/
$obCache = new CPHPCache; 
$time = CACHE_TIME * 60 * 60;
$cacheId = 'mobile_ganre'.$sort;

if (isset($_REQUEST['clear_cache']))
	$obCache->Clean($cacheId);

// если кеш есть и он ещё не истек, то
if($obCache->InitCache($time, $cacheId, "/")) {
	$resCache = $obCache->GetVars();
	$arResult = $resCache["DATA"];
} else {
	$arResult = getGanre(HL_TV_GANRE, $arOrder);
}

if($obCache->StartDataCache())
{
	$obCache->EndDataCache(array(
		"DATA" => $arResult,
	)); 	
}

//PR($arResult);

$timeScript = microtime(true) - $startTimeScript;

echo json_encode(
	array(
		'data' => $arResult,
		'time' => $timeScript,
	)
);


?>