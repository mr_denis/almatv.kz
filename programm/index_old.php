<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("ТВ программа");
?>

<script src="<?=SITE_TEMPLATE_PATH?>/js/angular.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/app.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/tv.js"></script>

<style>
	.cat_drop__list--search
	{
		max-height: 301px;
	}
	.cat_drop__list_inner {
		border-top: 1px solid #b2c6cd;
		border-bottom: 1px solid #b2c6cd;
		border-left: 1px solid #b2c6cd;
	}
</style>

<h1>ТВ программа</h1>
<!-- Angular code -->
<div class="filter_holder" ng-app="almatv" ng-controller="programmFilter">
	<form action="#">
		<fieldset>
			<div class="filter_holder__inner">
				<div class="filter_holder__item filter_holder__item--hover">
					<span class="filter_holder__title">Категория</span>
					<div class="cat_drop">
						<div class="cat_drop__main">
							<input type="hidden" name="category" ng-model="categoryActive.value">
							<span class="cat_drop__drop_pic"><img ng-src="{{categoryActive.img.src}}" alt="{{categoryActive.img.alt}}"></span>
							<span class="cat_drop__drop_name">{{(categoryActive.name || 'Выберите категорию')}}</span>
						</div>
						<div class="cat_drop__list cat_drop__list--category">
							<div 
								ng-repeat="category in categoryList | orderBy:'order' track by $index" 
								ng-click="changeCat($index)"
								class="cat_drop__drop_item">
								<span class="cat_drop__drop_pic cat_drop__drop_pic--hover">
									<img ng-src="{{category.img.src}}" alt="{{category.img.alt}}">
									<img class="cat_drop__drop_pic--with_hover" src="{{category.img_hover.src}}" alt="{{category.img_hover.alt}}">
								</span>
								<span class="cat_drop__drop_name">{{category.name}}</span>
							</div>
						</div>
					</div>
				</div>
				<div class="filter_holder__item filter_holder__item--hover test" ng-class="{'test' : !categoryActive.img.src }">
					<span class="filter_holder__title">Телеканал</span>
					<div class="cat_drop">
						<div class="cat_drop__main">
							<input type="hidden" name="channel" ng-model="channelActive.value">
							<span class="cat_drop__drop_pic">
								<span class="cat_drop__no_img" 
								ng-show="(channelActive.img.src == null && channelActive.name.length > 0)"
								>{{channelActive.name[0]}}</span>
								<img ng-src="{{channelActive.img.src}}" ng-show="channelActive.img.src" alt="{{channelActive.img.alt}}">
							</span>
							<span class="cat_drop__drop_name">{{channelActive.name}}</span>
						</div>
						<div class="cat_drop__list">
							<div class="cat_drop__search">
								<div class="cat_drop__field_holder">
									<input type="text" placeholder="Поиск канала" ng-model="search.name" class="cat_drop__field">
								</div>
							</div>
							<div class="cat_drop__list_inner cat_drop__list_inner--channel">
								<div class="cat_drop__drop_item" 
								ng-repeat="channel in channelList | filter: search " <?//| orderBy:'order' track by $index?>
								ng-click="changeChannel($index)">
									<span class="cat_drop__drop_pic">
										<span class="cat_drop__no_img" ng-hide="channel.img.src">{{channel.name[0]}}</span>
										<img ng-src="{{channel.img.src}}" ng-show="channel.img.src" alt="{{channel.img.alt}}">
										<img class="cat_drop__drop_pic--with_hover" alt="{{channel.img.alt}}"  ng-show="channel.img.src"ng-src="{{channel.img.src}}">
									</span> <?//ng-hide="{{channel.img.src}} == 'no'" ?>
									<span class="cat_drop__drop_name">{{channel.name}}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="filter_holder__item">
					<div class="quick_search">
						<input type="text" class="quick_search__field" placeholder="Быстрый поиск канала">
						<div class="quick_search__result">
							<div class="cat_drop__list cat_drop__list--search">
								<div class="cat_drop__list_inner"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<div class="after_filter__info">
	Пожалуйста, выберите категорию и желаемый телеканал из списка или воспользуйтесь быстрым поиском.
</div>
<!-- Angular code -->

<div id = "tv-programm">
<?/* Оставим мозможно нужно будет подгрузать последнее просмотренное расписание!
global $Filter;
$Filter = Array(
	"PROPERTY_CHANNEL" => 223,
	">=PROPERTY_DATE"=> date("Y-m-d", mktime(0,0,0,3,1,2015)),
	"<=PROPERTY_DATE"=> date("Y-m-d", mktime(23,59,59,9,31,2015)),
);
?>
<?
$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"programmWeek", 
	array(
		"IBLOCK_ID" => "17",
		"NEWS_COUNT" => "200",
		"SORT_BY1" => "PROPERTY_DATE",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "Filter",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "100",
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PARENT_SECTION" => "",
		"IBLOCK_TYPE" => "news",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "MAIN",
			1 => "",
		)
	),
	false
);
*/?>
</div>
<div class="popup__loader" style="text-align:center; display:none;opacity: 0.6;background-color: rgb(0, 0, 0);position: fixed;left: 0;top: 0;right: 0;bottom: 0;z-index: 1000;">
	<img style="margin-top:25%" src="<?=SITE_TEMPLATE_PATH?>/images/loader.gif">
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>