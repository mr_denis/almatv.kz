<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заявка на ремонт");
?><script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.min.js"></script> <script src="<?=SITE_TEMPLATE_PATH?>/js/messages_ru.js"></script> <script>
	$(document).ready(function() {
		$("#fix_order").validate({
			rules: {
				form_text_21: "required",
				form_text_22 : {
					required: true,
					form_text_22: true
				},
				form_text_23: {
					required: true,
					minlength: 8,
				},
				form_text_25: {
					required: true
				},
				form_textarea_26: {
					required: true
				}
			}
		});
		//$('.j-user_data__field--phone').mask('+7-000-000-0000');
		$(".j-user_data__field--phone").inputmask("+7 9{0,10}");
		
		$('#requestCity').change(function()
		{
			$('input[name=form_text_24]').val($(this).val());
		});
	});
</script>

<script src="<?=SITE_TEMPLATE_PATH?>/js/inputmask.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.inputmask.js" type="text/javascript"></script>

<h1>Заявка на ремонт</h1>
<p>
 <span style="color: #364347; background-color: #ffffff;">Форма предназначена для существующих абонентов, если Вы желаете подключиться - просим пройти по данной </span><a href="/request/">ссылке </a>
</p>
<p>
	<br>
</p>
<?$APPLICATION->IncludeComponent(
	"alma:form.result.new",
	"remont",
	Array(
		"WEB_FORM_ID" => "1",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"USE_EXTENDED_ERRORS" => "N",
		"SEF_MODE" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"LIST_URL" => "",
		"EDIT_URL" => "",
		"SUCCESS_URL" => "",
		"CHAIN_ITEM_TEXT" => "",
		"CHAIN_ITEM_LINK" => "",
		"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",)
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>