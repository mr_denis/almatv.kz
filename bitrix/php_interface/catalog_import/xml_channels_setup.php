<?
//<title>CommerceML MySql Fast</title>
IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/catalog/import_setup_templ.php');

$arSetupErrors = array();

//********************  ACTIONS  **************************************//
if (($ACTION == 'IMPORT_EDIT' || $ACTION == 'IMPORT_COPY') && $STEP == 1)
{
    if (isset($arOldSetupVars['URL_FILE_XML']))
        $URL_FILE_XML = $arOldSetupVars['URL_FILE_XML'];
}
if ($STEP > 1)
{
    $DATA_FILE_NAME = "";

    if (strlen($URL_FILE_XML) > 0 && file_exists($_SERVER["DOCUMENT_ROOT"].$URL_FILE_XML) && is_file($_SERVER["DOCUMENT_ROOT"].$URL_FILE_XML))
        $DATA_FILE_NAME = $_SERVER["DOCUMENT_ROOT"].$URL_FILE_XML;

    if (strlen($DATA_FILE_NAME) <= 0)
    {
        $arSetupErrors[] = GetMessage("CICML_ERROR_NO_DATAFILE");
    }

    if (!empty($arSetupErrors))
    {
        $STEP = 1;
    }
}
//********************  END ACTIONS  **********************************//

$aMenu = array(
    array(
        "TEXT"=>GetMessage("CATI_ADM_RETURN_TO_LIST"),
        "TITLE"=>GetMessage("CATI_ADM_RETURN_TO_LIST_TITLE"),
        "LINK"=>"/bitrix/admin/cat_import_setup.php?lang=".LANGUAGE_ID,
        "ICON"=>"btn_list",
    )
);

$context = new CAdminContextMenu($aMenu);

$context->Show();

if (!empty($arSetupErrors))
    ShowError(implode('<br />', $arSetupErrors));
?>
<form method="POST" action="<? echo $APPLICATION->GetCurPage(); ?>" ENCTYPE="multipart/form-data" name="dataload">
    <?
    $aTabs = array(
        array("DIV" => "edit1", "TAB" => GetMessage("CAT_ADM_CML1_IMP_TAB1"), "ICON" => "store", "TITLE" => GetMessage("CAT_ADM_CML1_IMP_TAB1_TITLE")),
        array("DIV" => "edit2", "TAB" => GetMessage("CAT_ADM_CML1_IMP_TAB2"), "ICON" => "store", "TITLE" => GetMessage("CAT_ADM_CML1_IMP_TAB2_TITLE")),
    );

    $tabControl = new CAdminTabControl("tabControl", $aTabs, false, true);

    $tabControl->Begin();

    $tabControl->BeginNextTab();

    if ($STEP == 1)
    {
        ?><tr class="heading">
        <td colspan="2"><? echo GetMessage("CICML_DATA_IMPORT"); ?></td>
        </tr>
        <tr>
            <td valign="top" width="40%"><? echo GetMessage("CICML_F_DATAFILE2");?></td>
            <td valign="top" width="60%">
                <input type="text" name="URL_FILE_XML" size="40" value="<?= htmlspecialcharsbx($URL_FILE_XML) ?>">
                <input type="button" value="<? echo GetMessage("CML_S_SELECT"); ?>" onclick="cmlBtnSelectClick()">
                <?
                CAdminFileDialog::ShowScript(
                    array(
                        "event" => "cmlBtnSelectClick",
                        "arResultDest" => array("FORM_NAME" => "dataload", "FORM_ELEMENT_NAME" => "URL_FILE_XML"),
                        "arPath" => array("PATH" => "/upload/catalog", "SITE" => SITE_ID),
                        "select" => 'F',// F - file only, D - folder only, DF - files & dirs
                        "operation" => 'O',// O - open, S - save
                        "showUploadTab" => true,
                        "showAddToMenuTab" => false,
                        "fileFilter" => 'xml',
                        "allowAllFiles" => true,
                        "SaveConfig" => true
                    )
                );
                ?></td>
        </tr><?
    }
    $tabControl->EndTab();

    $tabControl->BeginNextTab();

    if ($STEP == 2)
    {
        $FINITE = true;
    }

    $tabControl->EndTab();

    $tabControl->Buttons();

    ?>

    <? echo bitrix_sessid_post(); ?>
    <?
    if ($ACTION == 'IMPORT_EDIT' || $ACTION == 'IMPORT_COPY')
    {
        ?><input type="hidden" name="PROFILE_ID" value="<? echo intval($PROFILE_ID); ?>"><?
    }

    if ($STEP < 2)
    {
        ?><input type="hidden" name="STEP" value="<? echo intval($STEP) + 1;?>">
        <input type="hidden" name="lang" value="<? echo LANGUAGE_ID; ?>">
        <input type="hidden" name="ACT_FILE" value="<? echo htmlspecialcharsbx($_REQUEST["ACT_FILE"]); ?>">
        <input type="hidden" name="ACTION" value="<? echo htmlspecialcharsbx($ACTION); ?>">
        <input type="hidden" name="SETUP_FIELDS_LIST" value="URL_FILE_XML,IBLOCK_TYPE_ID,keepExistingProperties,keepExistingData,clearTempTables,deleteComments,cmlDebug,cmlMemoryDebug,activateFileData,USE_TRANSLIT,ADD_TRANSLIT">
        <input type="submit" value="<? echo (($ACTION=="IMPORT")?GetMessage("CICML_NEXT_STEP_F"):GetMessage("CICML_SAVE"))." &gt;&gt;" ?>" name="submit_btn"><?
    }

    $tabControl->End();

    ?></form>
<script type="text/javascript">
    <?if ($STEP < 2):?>
    tabControl.SelectTab("edit1");
    tabControl.DisableTab("edit2");
    <?elseif ($STEP == 2):?>
    tabControl.SelectTab("edit2");
    tabControl.DisableTab("edit1");
    <?endif;?>
</script>