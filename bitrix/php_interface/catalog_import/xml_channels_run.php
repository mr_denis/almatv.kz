<?
//<title>XML импорт каналов</title>
set_time_limit(0);
ignore_user_abort(true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/dump.php");

CModule::IncludeModule("iblock");
$CIBE = new CIBlockElement;
$CIBS = new CIBlockSection;

if (strval($DATA_FILE_NAME) == "" || !file_exists($DATA_FILE_NAME)) {
    die("Unexisting file \"$DATA_FILE_NAME\"");
}

$arErrors = array();

$XML = simplexml_load_file($DATA_FILE_NAME);

$arChannelsNamesByID = array();
foreach($XML->channel as $obChannel) {
    $obChannelAttributes = $obChannel->attributes();
    $arChannelsNamesByID[strval($obChannelAttributes->id)] = strval($obChannel->{"display-name"});
}
$arChannels = array();
$rsChannels = CIBlockElement::GetList(
    array(),
    array("NAME" => array_values($arChannelsNamesByID)),
    false,
    false,
    array("ID", "NAME")
);
while ($arChannel = $rsChannels->GetNext()) {
    $arChannels[array_search($arChannel["NAME"], $arChannelsNamesByID)] = $arChannel["ID"];
}


$arPrograms = array();
$i = 1;
$arProgramsSections = array();
foreach($XML->programme as $obProgram) {
    $arLocalErrors = array();
    $obProgramAttributes = $obProgram->attributes();
    $channelID = strval($obProgramAttributes->channel);

    if (!isset($obProgramAttributes->channel) || $obProgramAttributes->channel == "") {
        $arLocalErrors[] = "Ошибка в передаче #$i: отсутствует id канала.";
    }
    if (!isset($obProgramAttributes->start) || $obProgramAttributes->start == "") {
        $arLocalErrors[] = "Ошибка в передаче #$i: отсутствует время начала трансляции.";
    }
    elseif (strtotime($obProgramAttributes->start) <= 0) {
        $arLocalErrors[] = "Ошибка в передаче #$i: время начала трансляции имеет неверный формат (" . $obProgramAttributes["start"] . ").";
    }
    if (!isset($obProgram->title) || $obProgram->title == "") {
        $arLocalErrors[] = "Ошибка в передаче #$i: отсутствует название.";
    }
    if (!isset($arChannels[$channelID])) {
        $arLocalErrors[] = "Ошибка в передаче #$i: отсутствует канал с названием " . $arChannelsNamesByID[$channelID] . ".";
    }

    if (empty($arLocalErrors)) {

        $arCategories = array();
        foreach ($obProgram->category as $category) {
            $arCategories[] = strval($category);
        }
        $arProgramsSections = array_merge($arProgramsSections, $arCategories);

        $arFiles = array();
        foreach($obProgram->icon as $icon) {
            $arIconAttributes = $icon->attributes();
            if (isset($arIconAttributes->src) && $arIconAttributes->src != "" && $arIconAttributes->src != "empty") {
                if ($arFile = CFile::MakeFileArray(strval($arIconAttributes->src))) {
                    if ($arFile["type"] != "unknown" && isset($arFile["tmp_name"]) && $arFile["tmp_name"] != "") {
                        $arFiles[] = $arFile;
                    }
                }
            }
        }


        if (!isset($arProgramsByChannels[$channelID]))
            $arProgramsByChannels[$channelID] = array();
        $arPrograms[] = array(
            "IBLOCK_ID" => IB_PROGRAMS,
            "DATE_ACTIVE_FROM" => date("d.m.Y H:i:s", strtotime($obProgramAttributes->start)),
            "DATE_ACTIVE_TO" => date("d.m.Y H:i:s", strtotime($obProgramAttributes->start)),
            "NAME" => strval($obProgram->title),
            "PREVIEW_TEXT" => strval($obProgram->desc),
            "PROPERTY_VALUES" => array(
                "CHANNEL" => $arChannels[$channelID],
                "DATE" => strval($obProgram->date),
                "RATING" => strval($obProgram->rating->value),
                "ICONS" => $arFiles
            ),
            "CATEGORIES" => $arCategories,
        );
    }
    else {
        $arErrors = array_merge($arErrors, $arLocalErrors);
    }
    $i++;
}

$arProgramsSections = array_unique($arProgramsSections);
$arExistingProgramsSections = array();
$rsProgramsSections = CIBlockSection::GetList(
    array(),
    array(
        "IBLOCK_ID" => IB_PROGRAMS,
        "NAME" => $arProgramsSections
    ),
    false,
    array(
        "ID",
        "NAME"
    )
);
while ($arProgramsSection = $rsProgramsSections->GetNext()) {
    $arExistingProgramsSections[$arProgramsSection["NAME"]] = $arProgramsSection["ID"];
}

$arUnexistingProgramsSections = array_diff($arProgramsSections, array_keys($arExistingProgramsSections));
foreach($arUnexistingProgramsSections as $section) {
    $id = $CIBS->add(array(
        "IBLOCK_ID" => IB_PROGRAMS,
        "NAME" => $section
    ));
    if ($id) {
        $arExistingProgramsSections[$section] = $id;
    }
}

$successCnt = 0;
foreach ($arPrograms as &$arProgram) {
    $arCategories = $arProgram["CATEGORIES"];
    $arNewCategories = array();
    foreach($arProgram["CATEGORIES"] as $category) {
        $arNewCategories[] = $arExistingProgramsSections[$category];
    }
    $arProgram["IBLOCK_SECTION"] = $arNewCategories;
    unset($arNewCategories);
    $id = $CIBE->Add($arProgram);
    if (!$id) {
        $arErrors["Ошибка: не получилось сохранить передачу \"" . $arProgram["NAME"] . "\" на " . $arProgram["DATE_ACTIVE_FROM"]];
    }
    else {
        $successCnt++;
    }
}
unset($arProgram);

$strImportErrorMessage = implode("\n", $arErrors);
$strImportOKMessage = "Импорт закончен. добавлено " . $successCnt . " позиций.";