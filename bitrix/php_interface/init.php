<?
require_once(dirname(__FILE__) . "/dump.php");
require_once(dirname(__FILE__) . "/define.php");

function remoteFileExists($url)
{
	$Headers = @get_headers($url);
	// проверяем ли ответ от сервера с кодом 200 - ОК
	if(strpos('200', $Headers[0])) {
		return 1;
	} 
	return 0;
}


function getWordEnding ($num, $forms) {
    $num = $num % 100;
    if ($num <= 14 && $num >= 11)
        return $forms[2];
    $num = $num % 10;
    if ($num >= 5)
        return $forms[2];
    if ($num >= 2)
        return $forms[1];
    return $forms[0];
}

function getCityList()
{
	if(!CModule::IncludeModule("iblock"))
		return;
	$arFilter = Array("IBLOCK_ID"=>IBLOCK_CITY, "ACTIVE" => "Y");
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, array('ID', 'NAME'));
	while($ob = $res->GetNext())
	{
		$arrItem = array(
			"ID" => $ob['ID'],
			"NAME" => $ob['NAME'],
		);
		$arrCity[] = $arrItem;
	}
	return $arrCity;
}

AddEventHandler('form', 'onAfterResultAdd', Array("ClassWebForm","onAfterResultAddHandler"));

function getDateWeek()
{
	$now = date('w', time()); 
	if ($now == 0) 
		$now += 7;
	for ($i = 1; $i <= 7; $i++) 
	{
		$date =  strtotime(($i-$now)." day");
		$arrDateItem = array(
			"UNIX" => $date,
			"D" => date('d',$date),
			"M" => date('m',$date),
			"Y" => date('Y',$date),
			"N" => date('N',$date),
		);
		$arrDate[] = $arrDateItem;
	}
	return $arrDate;
}
	
function isInetCity($idCity)
{
	if(!CModule::IncludeModule("iblock"))
		return;
	//$arSelect = Array("PROPERTY_COMBO");
	$arFilter = Array("IBLOCK_ID"=>18, "PROPERTY_CITY" => $idCity, "!PROPERTY_COMBO_VALUE" => "Y" ,"ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	if($ob = $res->GetNext())
		return true;
	return false;
}

function isBlockPayment()
{
	global $USER;
	if ($USER->IsAdmin()) {
		global $DB;
		$dbRes = $DB->Query("SELECT ID FROM b_iblock_element WHERE IBLOCK_ID = 8 AND ACTIVE = 'Y' AND ID IN(
						SELECT IBLOCK_ELEMENT_ID FROM b_iblock_element_property WHERE IBLOCK_PROPERTY_ID = 108)");
		$arrDate = array();
		while ($row = $dbRes->Fetch())
		{
			$arrDate[] = $row['ID'];
		}
		
		return in_array($_COOKIE['City'], $arrDate);
	}
	return false;
}


function DeclensionNameChannel($count, $ru = "ru")
{
	$channelName = 'канал';
	switch ($count % 10)
	{
		case 2:
		case 3:
		case 4:
			$channelName .= 'a';
			break;
		case 0:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
			$channelName .= 'ов';

	}
	return $channelName;
}

function DeclensionNamePackages($count, $ru = "ru")
{
	$packageName = 'пакет';
	switch ($count % 10)
	{
		case 2:
		case 3:
		case 4:
			$packageName .= 'a';
			break;
		case 0:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
			$packageName .= 'ов';

	}
	return $packageName;
}

function DeclensionNameSound($count, $ru = "ru")
{
	$packageSound = 'дорож';
	switch ($count % 10)
	{
		case 0:
			$packageSound .= 'кa';
			break;
		case 1:
			$packageSound .= 'кa';
			break;
		case 2:
		case 3:
		case 4:
			$packageSound .= 'ки';
			break;

		case 0:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
			$packageSound .= 'ек';

	}
	return $packageSound;
}

function city($idCity)
{
	$arSelect = Array("ID", "NAME", "PROPERTY_REQUEST", "PROPERTY_REQUEST_R", "PROPERTY_EMAIL");
	$arFilter = Array("IBLOCK_ID"=>8, "ID" => $idCity, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	$cityName = '';
	if($ob = $res->GetNext())
	{
		return $ob;
	}
}

class ClassWebForm
{
	function onAfterResultAddHandler($WEB_FORM_ID, $RESULT_ID)
	{
		if ($WEB_FORM_ID == 2) /*заявка на подключение*/
		{
			//пересохраним город
			$arAnswer = CFormResult::GetDataByID($RESULT_ID, array(), $arResult,$arAnswer2);
			$idCity = $arAnswer['CITY'][0]['USER_TEXT'];
			
			$arrCity = city($idCity);
			
			if (!empty($arrCity['NAME']))
			{
				$arVALUE = array();
				$FIELD_SID = "CITY";
				$ANSWER_ID = 7;
				$arVALUE[$ANSWER_ID] = $arrCity['NAME'];
				CFormResult::SetField($RESULT_ID, $FIELD_SID, $arVALUE);
			}
			
			$idPackages = $arAnswer['PACKAGES'][0]['USER_TEXT'];

			if ($idPackages > 0) {
				$arFilter = Array("IBLOCK_ID"=>10, "ID" => $idPackages, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("NAME"));
				if($ob = $res->GetNext())
				
				$ob['NAME'] .= '['.$idPackages.']';	
				$arVALUE = array();
				$FIELD_SID = "PACKAGES";
				$ANSWER_ID = 11;
				$arVALUE[$ANSWER_ID] = $ob['NAME'];
				CFormResult::SetField($RESULT_ID, $FIELD_SID, $arVALUE);
			}
			
			$combined = 'Нет';
			if ($arAnswer['COMBINED'][0]['ANSWER_ID'] == 36)
			{
				$combined = 'Да';
			}
			
			$arSend = array(
				"STATUS" => $arAnswer['STATUS'][0]['ANSWER_TEXT'],
				"NAME" => $arAnswer['NAME'][0]['USER_TEXT'],
				"EMAIL" => $arAnswer['EMAIL'][0]['USER_TEXT'],
				"PHONE" => $arAnswer['PHONE'][0]['USER_TEXT'], 
				"CITY" => $arrCity['NAME'],
				"TYPE" => $arAnswer['TYPE'][0]['USER_TEXT'],
				"ADDRESS" => $arAnswer['ADDRESS'][0]['USER_TEXT'],
				"PACKAGES" => $ob['NAME'],  //айди изменяем на название пакета
				"PACKAGES_EXP" => $arAnswer['PACKAGES_EXP'][0]['USER_TEXT'],
				"COUNT" => $arAnswer['COUNT'][0]['ANSWER_TEXT'], 
				"COMBINED" => $combined, 
				"EMAIL_R" => $arrCity['PROPERTY_EMAIL_VALUE'],
				"FILE" => $_SERVER['SERVER_NAME'].CFile::GetPath($arAnswer['FILE'][0]['USER_FILE_ID']),
			);
			
			CEvent::Send('FORM_FILLING_SIMPLE_FORM_2','s1',$arSend);
			//CFormResult::Mail($RESULT_ID);
		}
		else if ($WEB_FORM_ID == 1) /*заявка на ремонт*/
		{
			$arAnswer = CFormResult::GetDataByID($RESULT_ID, array(), $arResult,$arAnswer2);
			$idCity = $arAnswer['CITY'][0]['USER_TEXT'];

			//$file = $_SERVER["DOCUMENT_ROOT"].'/log.log';
			//file_put_contents($file, serialize($arAnswer));
			
			$arrCity = city($idCity);
			
			if (!empty($arrCity['NAME']))
			{
				$arVALUE = array();
				$FIELD_SID = "CITY";
				$ANSWER_ID = 24;
				$arVALUE[$ANSWER_ID] = $arrCity['NAME'];
				CFormResult::SetField($RESULT_ID, $FIELD_SID, $arVALUE);
			}
			
			$arSend = array(
				"STATUS" => $arAnswer['STATUS'][0]['ANSWER_TEXT'],
				"NAME" => $arAnswer['NAME'][0]['USER_TEXT'],
				"EMAIL" => $arAnswer['EMAIL'][0]['USER_TEXT'],
				"PHONE" => $arAnswer['PHONE'][0]['USER_TEXT'], 
				"CITY" => $arrCity['NAME'],
				"ADDRESS" => $arAnswer['ADDRESS'][0]['USER_TEXT'],
				"DESCRIPTION" => $arAnswer['DESCRIPTION'][0]['USER_TEXT'], 
				"EMAIL_R" => $arrCity['PROPERTY_EMAIL_VALUE'],
				"FILE" => $_SERVER['SERVER_NAME'].CFile::GetPath($arAnswer['FILE'][0]['USER_FILE_ID']),
			);
			
			CEvent::Send('FORM_FILLING_SIMPLE_FORM_1','s1',$arSend);
			//CFormResult::Mail($RESULT_ID);
		}
		else if ($WEB_FORM_ID == 3) /*заявка интернет*/
		{
			$arAnswer = CFormResult::GetDataByID($RESULT_ID, array(), $arResult,$arAnswer2);
			$idCity = $arAnswer['CITY'][0]['USER_TEXT'];

			$arrCity = city($idCity);
			
			if (!empty($arrCity['NAME']))
			{
				$arVALUE = array();
				$FIELD_SID = "CITY";
				$ANSWER_ID = 32;
				$arVALUE[$ANSWER_ID] = $arrCity['NAME'];
				CFormResult::SetField($RESULT_ID, $FIELD_SID, $arVALUE);
			}

			$idTarif = $arAnswer['TARIFF'][0]['USER_TEXT'];
			if ($idTarif > 0) {
				$arFilter = Array("IBLOCK_ID"=>18, "ID" => $idTarif, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("NAME"));
				if($ob = $res->GetNext())
				$ob['NAME'] .= '['.$idTarif.']';
				$arVALUE = array();
				$FIELD_SID = "TARIFF";
				$ANSWER_ID = 34;
				
				$arVALUE[$ANSWER_ID] = $ob['NAME'];
				CFormResult::SetField($RESULT_ID, $FIELD_SID, $arVALUE);
			}

			$combined = 'Нет';
			if ($arAnswer['COMBINED'][0]['ANSWER_ID'] == 37)
			{
				$combined = 'Да';
			}
			
			$arSend = array(
				"STATUS" => $arAnswer['STATUS'][0]['ANSWER_TEXT'],
				"NAME" => $arAnswer['NAME'][0]['USER_TEXT'],
				"EMAIL" => $arAnswer['EMAIL'][0]['USER_TEXT'],
				"PHONE" => $arAnswer['PHONE'][0]['USER_TEXT'], 
				"CITY" => $arrCity['NAME'],
				"ADDRESS" => $arAnswer['ADDRESS'][0]['USER_TEXT'],
				"TARIFF" => $ob['NAME'], //айди изменяем на название тарифа
				"OPTIONS" => $arAnswer['OPTIONS'][0]['USER_TEXT'], 
				"COMBINED" => $combined, 
				"EMAIL_R" => $arrCity['PROPERTY_EMAIL_VALUE'],
				"FILE" => $_SERVER['SERVER_NAME'].CFile::GetPath($arAnswer['FILE'][0]['USER_FILE_ID']),
			);
			
			CEvent::Send('FORM_FILLING_SIMPLE_FORM_3','s1',$arSend);
			//CFormResult::Mail($RESULT_ID);
		}
		else if ($WEB_FORM_ID == 4) /* заявка на подключение mobile */
		{
			$arAnswer = CFormResult::GetDataByID($RESULT_ID, array(), $arResult,$arAnswer2);
			$idCity = $arAnswer['CITY'][0]['USER_TEXT'];
			$arrCity = city($idCity);
			
			if (!empty($arrCity['NAME']))
			{
				$arVALUE = array();
				$FIELD_SID = "CITY";
				$ANSWER_ID = 59;
				$arVALUE[$ANSWER_ID] = $arrCity['NAME'];
				CFormResult::SetField($RESULT_ID, $FIELD_SID, $arVALUE);
			}
			
			$arSend = array(
				"NAME" => $arAnswer['NAME'][0]['USER_TEXT'],
				"EMAIL" => $arAnswer['EMAIL'][0]['USER_TEXT'],
				"PHONE" => $arAnswer['PHONE'][0]['USER_TEXT'],
				"CITY" => $arrCity['NAME'],
				"ADDRESS" => $arAnswer['ADDRESS'][0]['USER_TEXT'],
				"DESCRIPTION" => $arAnswer['DESCRIPTION'][0]['USER_TEXT'], 
				"EMAIL_R" => $arrCity['PROPERTY_EMAIL_VALUE'],
			);
			//CEvent::Send('MOBILE_REQUEST','s1',$arSend);
		}
		else if ($WEB_FORM_ID == 5) /* заявка на ремонт mobile */
		{
			$arAnswer = CFormResult::GetDataByID($RESULT_ID, array(), $arResult,$arAnswer2);
			$idCity = $arAnswer['CITY'][0]['USER_TEXT'];
			$arrCity = city($idCity);
			
			$arSend = array(
				"NAME" => $arAnswer['NAME'][0]['USER_TEXT'],
				"EMAIL" => $arAnswer['EMAIL'][0]['USER_TEXT'],
				"PHONE" => $arAnswer['PHONE'][0]['USER_TEXT'],
				"CITY" => $arrCity['NAME'],
				"ADDRESS" => $arAnswer['ADDRESS'][0]['USER_TEXT'],
				"DESCRIPTION" => $arAnswer['DESCRIPTION'][0]['USER_TEXT'],
				"EMAIL_R" => $arrCity['PROPERTY_EMAIL_VALUE'],
			);
			
			if (!empty($arrCity['NAME']))
			{
				$arVALUE = array();
				$FIELD_SID = "CITY";
				$ANSWER_ID = 65;
				$arVALUE[$ANSWER_ID] = $arrCity['NAME'];
				CFormResult::SetField($RESULT_ID, $FIELD_SID, $arVALUE);
			}
			//CEvent::Send('MOBILE_REPAIR','s1',$arSend);
		}
	}
}

function getExtension($filename) {
	return substr(strrchr($fileName, '.'), 1);
}

function cityId()
{
	$cityId = 42;//алматы
	if (!empty($_COOKIE['City']))
	{
		$cityId = $_COOKIE['City'];
	}
	return $cityId;
}

function cityOffice()
{
	$id = cityId();
	$arrCityId[] = $id;
	CModule::IncludeModule('iblock');
	$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
	$arFilter = Array("IBLOCK_ID"=>14, "PROPERTY_CITY.ID" => $arrCityId, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC",), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNext())
	{
		$arr[] = $ob['ID'];
	}
	return $arr;
}

function ckopStr($text,$char1,$char2)
{
    $text = strip_tags($text); // Удаляем теги
     
    $text = mb_convert_encoding($text, "windows-1251", "UTF-8"); // Меняем кодировку на windows-1251 
    $countChars = strlen($text); // Считаем сколько символов в целой строке
     
        if($char2 == '-') {$text = substr($text,$char1);} // варинат функции substr с двумя параметрами
        else{$text = substr($text,$char1,$char2);}// варинат функции substr с тремя параметрами
         
        $text = mb_convert_encoding($text, "UTF-8", "windows-1251");// Меняем кодировку обратно на UTF-8 
        $word = explode(' ',$text);  // разбиваем текст на слова и помещаем в массив
        $countWords = count($word) - 1;  // считаем сколько слов у нас есть
         
        if($char1 < $countChars and $char1 != '0') // Если строка обрезалась с начала текста
        {
            $start= '...'; // Троеточие в начале
            $startWord = 1; 
        }
        else {$startWord = 0;} 
         
         
        if($char2 < $countChars and $char2 != '-')
        {
            $finish= '...';// Троеточие в конце
            $finishWord = $countWords - 1;
        }
        else {$finishWord = $countWords;}
         
        // StartWord указывает с какого слова нужно начинать текст (если строка резалась, то со второго, если нет, то с первого)
        // finishWord указывает с какого слова нужно закончить текст (если строка резалась, то с предпоследнего, если нет, то с последнего)
         
        for($startWord;$startWord <= $finishWord; $startWord++) // Перебираем массив со словами,и создаем строку
        {
            $obr_text .= $word[$startWord].' ';
        }
        $text = trim($obr_text); // Удаляем пробелы в конце и в начале строки
         
        return $start.$text.$finish;   
}

function getCity($id = 0)
{
	if (!empty($id))
		return $id;
	if ($_COOKIE['City'])
	{
		return $_COOKIE['City'];
	}

	return 42;
}	
			
/*Работа с хайлоад*/
function getFields($HL_Infoblock_ID, $arrFilter = array(), $arrSelect = array('*'), $limit)
{
	$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 
	//$Query->setOrder(array('UF_DATE_FROM' => 'ASC'));
	$Query->setSelect($arrSelect);
	$Query->setFilter($arrFilter);
	
	if (!empty($limit))
		$Query->setLimit($limit);
	//$Query->setOrder(array('UF_SORT' => 'ASC'));

	//Выполним запрос
	$result = $Query->exec();

	$result = new CDBResult($result);

	$arResult = array();
	
	while ($row = $result->Fetch())
	{
		foreach ($row as &$itemFields)
		{
			if ($itemFields instanceof \Bitrix\Main\Type\DateTime)
			{
				$itemFields = $itemFields->toString();
			}
		}

		$arResult[$row['ID']] = $row;
	}
	
	return $arResult;
}
			
/*
	Выводит информацию об объекте в удобочитаемом виде.(Для отладки)
	$o - объект
	Необязательные параметры:
	[$stack] - true: показывать стек вызовов,По умолч - false
	[$option] - true: var_dump, false: print_r,По умолч - false
*/
function PR($o, $option = false, $stack = false)
{
    $bt =  debug_backtrace();
    $bt = $bt[0];
    $dRoot = $_SERVER["DOCUMENT_ROOT"];
    $dRoot = str_replace("/","\\",$dRoot);
    $bt["file"] = str_replace($dRoot,"",$bt["file"]);
    $dRoot = str_replace("\\","/",$dRoot);
    $bt["file"] = str_replace($dRoot,"",$bt["file"]);
    ?>
    <div style='font-size:9pt; color:#000; background:#fff; border:1px dashed #000;'>
        <div style='padding:3px 5px; background:#99CCFF; font-weight:bold;'>File: <?=$bt["file"]?> [<?=$bt["line"]?>]</div>
        <?if ($option): ?>
            <pre style='padding:10px;'><? var_dump(!$stack ? $o : debug_backtrace()); ?></pre>
        <?else:?>
            <pre style='padding:10px;'><? print_r(!$stack ? $o : debug_backtrace()); ?></pre>
        <?endif;?>
    </div>
<?
}
?>