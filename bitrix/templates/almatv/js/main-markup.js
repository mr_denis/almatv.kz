$(function() {

	$('input, textarea').placeholder();

	$(document).bind( 'mousewheel', function ($event) { 
	    var delta = 0;
	    if (!event) event = window.event; // Событие IE.
	    // Установим кроссбраузерную delta
	    if (event.wheelDelta) { 
	        // IE, Opera, safari, chrome - кратность дельта равна 120
	        delta = event.wheelDelta/120;
	    } else if (event.detail) { 
	        // Mozilla, кратность дельта равна 3
	        delta = -event.detail/3;
	    }
	    if (delta) {
	        // Отменим текущее событие - событие поумолчанию (скролинг окна).
	        if (event.preventDefault) {
	            event.preventDefault();
	        }
	        event.returnValue = false; // для IE
	    }

		var nt = $(window).scrollTop()-(delta * 200); 
		event.preventDefault(); 
		event.stopPropagation(); 
		$('html, body').stop().animate( { 
		 scrollTop : nt 
		} , 600, 'easeOutQuint');  
	} );

	$('.is_scrolled').bind('mousewheel', function($event) { // Добавлять этот класс для блоков в которых используется скролл, так как плавный скрол на всей странице блокирует это событие
		event.stopPropagation(); 
	});

	$(window).load(function(){
		$('.cat_drop__list--category,.cat_drop__list_inner--channel, .add_packs_box__hidden_scroll, .chat__dialog_scroll, .popup_channels-box, .event__scroll').mCustomScrollbar();
		$('#j-main_slider').carouFredSel({
			responsive: true,
			items: {
				visible: 1
			},
			scroll: {
				duration: 800,
				timeoutDuration: 4000,
				fx: 'crossfade'
			},
			circular: true,
			infinite: true,
			pauseOnHover: true,
			auto: false,
			pagination: '#j-main_slider__paging',
			prev:{
				button: "#j-main_slider_left"
			},
			next:{
				button: "#j-main_slider_right"
			}
		});
		$('#j-telecast__slider').carouFredSel({
			items: {
				visible: 1,
				width: 654,
				height: 360
			},
			scroll: {
				duration: 800,
				timeoutDuration: 4000,
				fx: 'crossfade'
			},
			circular: true,
			infinite: true,
			pauseOnHover: true,
			auto: false,
			pagination: '#j-telecast__slider_paging',
			prev:{
				button: "#j-telecast__slider_left"
			},
			next:{
				button: "#j-telecast__slider_right"
			}
		});

		$('#j-inet_pluses_slider').carouFredSel({
			responsive: true,
			scroll: 1,
			items: {
				visible: {
					min: 1,
					max: 5
				},
				width: 300,
				duration: 200,
				timeoutDuration: 4000,
				fx: 'scroll',
			},
			circular: true,
			infinite: true,
			pauseOnHover: true,
			auto: false,
			prev:{
				button: "#j-inet_pluses_slider_left"
			},
			next:{
				button: "#j-inet_pluses_slider_right"
			}
		});


		$('.pick_channel__label').on('click',function(event){
			event.stopPropagation();
		});


		
		$(".container").on('click', ".tarif_row__cell:not('.tarif_row__cell--first, .tarif_row__cell--last, .highlight')", function(event){
			var myThis = this;
			$.ajax({
				url: 'popup_ajax.php',
				beforeSend: function(jqXHR, settings){
					$(myThis).parents('.tarif_row').find('.tarif_popup__loader').addClass('active');
				},
				success: function(response){

					$(myThis).parents('.tarif_row').find('.tarif_popup__loader').removeClass('active');
					$(myThis).parents('.tarif_row').find('.tarif_popup_scroll').html(response);
					$(myThis).parents('.tarif_row').find('.j-tarif_popup_slider').carouFredSel({
						responsive: true,
						height: 315,
						items: {
							visible: {
								min: 4,
								max: 6
							},
							width: 250,
							duration: 200,
							timeoutDuration: 4000,
							fx: 'scroll',
						},
						circular: true,
						infinite: true,
						pauseOnHover: true,
						auto: false,
						pagination: '.tarif_popup_slider__paging',
						prev:{
							button: ".tarif_popup_slider__left"
						},
						next:{
							button: ".tarif_popup_slider__right"
						}
					});
					
				}
			});


			if ($(this).hasClass('highlight')){
				$(this).removeClass('highlight');
				$('.tarif_popup').removeClass('active');
				$('.tarif_popup_scroll').removeClass('active');
				$(this).parents('.tarif_row').find('.tarif_popup').removeClass('active').find('.tarif_popup_scroll').removeClass('active');
			}else{
				$('.tarif_row__cell').removeClass('highlight');
				$('.tarif_popup').removeClass('active');
				$('.tarif_popup_scroll').removeClass('active');
				$(this).parents('.tarif_row').find('.tarif_popup').addClass('active').find('.tarif_popup_scroll');
				var that = this;
				setTimeout(function(){
					$(that).parents('.tarif_row').find('.tarif_popup_scroll').addClass('active');
				},500);
				$(this).addClass('highlight');
			};
			event.stopPropagation();
		});
		$('.tarif_popup__close').on('click', function(){
			$(this).parents('.tarif_popup').removeClass('active');
			$('.tarif_row__cell').removeClass('highlight');
			$('.tarif_popup_scroll').removeClass('active');
		});


	});

	$('.main').on('change', '.pick_channel__checkbox', function(){
		if ($(this).hasClass('checked')){
			$('.'+($(this).data('channel'))).addClass('active');
		}else{
			$('.'+($(this).data('channel'))).removeClass('active');
		};
		$(this).trigger('refresh');
		/*pickChannelSelect();*/
	});


	$('select, input[type="checkbox"], input[type="radio"]').styler({
		selectSearch: true
	});

	$('#j-popup_city').arcticmodal({
		closeOnOverlayClick: false
	});

	$('.tv_accordion__broadcast').on('click', function(){
		$('#j-popup_programm').arcticmodal();
	});
	$('.today_programm__name').on('click', function(){
		$('#j-popup_programm').arcticmodal();
	});
	$('.result_programms__name').on('click', function(){
		$('#j-popup_programm').arcticmodal();
	});
	$('.pack_main_info__show_all').on('click', function(){
		$('#j-popup_channels').arcticmodal();
	});
	$('.tarif_row__list_all_channels').on('click', function(){
		$('#j-popup_channels').arcticmodal();
	});

	$('#j-popup_info').arcticmodal();

	$('.j-special_menu__link--balance').on('click', function(event){
		event.preventDefault();
		$('#j-popup_balance').arcticmodal();
	});
	$('.close_popup').on('click', function(){
		$.arcticmodal('close');
	});


	$('.tabs_links--one_news').jwTabs($('.tabs_content--one_news'));
	$('.tabs_links--business').jwTabs($('.tabs_content--business'));
	$('.tabs_links--press').jwTabs($('.tabs_content--press'));
	$('.tabs_links--podkl').jwTabs($('.tabs_content--podkl'));
	$('.tabs_links--flat').jwTabs($('.tabs_content--flat'));
	$('.tabs_links--home').jwTabs($('.tabs_content--home'));
	$('.j-tarif_cat__trigger--flat_home').jwTabs($('.flat_home_tabs'));


	$('.j-tarif_cat__tabs_head').jwTabs($('.tarif_cat__tabs_cont'), $('.j-add_eq__tabs_head'), $('.add_eq__tabs_cont'));
	$('.j-add_eq__tabs_head').jwTabs($('.add_eq__tabs_cont'), $('.j-tarif_cat__tabs_head'), $('.tarif_cat__tabs_cont'));
	$('#j-single_programm_tabs_head').jwTabs($('#j-single_programm_tabs_body'));
	$('#j-business_tabs_head').jwTabs($('#j-business_tabs_cont'));

	$('.add_packs_box__count').on('click', function(event){
		event.stopPropagation();
		var el = $(this);
		if (el.hasClass('active')){
			el.removeClass('active');
		}else{
			$('.add_packs_box__count').removeClass('active');
			el.addClass('active');
		};
	});

	$('.get_pack__add').on('click', function(event){
		event.stopPropagation();
		var el = $(this);
		if (el.hasClass('active')){
			el.removeClass('active');
		}else{
			$('.get_pack__add').removeClass('active');
			el.addClass('active');
		};
	});

	$('.menu_trigger').on('click',  function(event){
		event.stopPropagation();
		sidemenuTrigger ();
		$('.chat').removeClass('active');
		$('.chat__trigger').removeClass('active');
	});

	$('.sidemenu').on('click',function(event){
		event.stopPropagation();
	});

	$('.chat__trigger').on('click', function(){
		$('.chat').toggleClass('active');
		$(this).toggleClass('active');
	});

	$('.telecast__accordion_trigger').on('click', function(){
		$(this).toggleClass('active');
		$(this).prev('.telecast__accordion').toggleClass('active');
		if ($(this).hasClass('active')){
			$(this).text('Скрыть описание');
		}else{
			$(this).text('Показать описание полностью');
		};
	});

	$('.telecast__remind_box').on('click', function(){
		$('.telecast__remind_form').removeClass('active');
		$(this).toggleClass('active');
		$(this).children('.telecast__remind_form').toggleClass('active');
	});

	$(document).on('click',function(){
		if ($('.menu_trigger').hasClass('active')) {
			sidemenuTrigger ();
		};
		$('.cat_drop__list').removeClass('active');
		$('.cat_drop__main').removeClass('active');
		$('.add_packs_box__count').removeClass('active');
		$('.get_pack__add').removeClass('active');
		$('.chat').removeClass('active');
		$('.chat__trigger').removeClass('active');
		$('.head_search_trigger').removeClass('active');
		$('.head_search').removeClass('active');
		$('.menu_trigger').removeClass('menu_trigger--search_open');
		$('.chat').removeClass('chat--search_open');
		$('.menu_trigger').removeClass('menu_trigger--location_open');
		$('.chat').removeClass('chat--location_open');
		$('.telecast__remind_box').removeClass('active');
		$('.telecast__remind_form').removeClass('active');
		$('.head_location').removeClass('active');
		$('.header_location_trigger').removeClass('active');
		$('.tarif_popup').removeClass('active');
		$('.tarif_row__cell').removeClass('highlight');
		$('.tarif_popup__list_item--not_available').removeClass('active');
		$('.tarif_popup_scroll').removeClass('active');

	});

	$('.head_search_trigger').on('click', function(){
		$('.head_search').toggleClass('active');
		$(this).toggleClass('active');
		$('.menu_trigger').removeClass('menu_trigger--location_open');
		$('.chat').removeClass('chat--location_open');
		$('.menu_trigger').toggleClass('menu_trigger--search_open');
		$('.chat').toggleClass('chat--search_open');
		$('.head_location').removeClass('active');
		$('.header_location_trigger').removeClass('active');
	});

	$('.header_location_trigger, .head_location__close').on('click', function(){
		$('.head_location').toggleClass('active');
		$(this).toggleClass('active');
		$('.menu_trigger').removeClass('menu_trigger--search_open');
		$('.chat').removeClass('chat--search_open');
		$('.menu_trigger').toggleClass('menu_trigger--location_open');
		$('.chat').toggleClass('chat--location_open');
		$('.head_search').removeClass('active');
		$('.head_search_trigger').removeClass('active');
	});
	$('.checked_city__another_link').on('click', function(){
		$('.header_location_trigger').click();
	});
	$('.jq-radio.location_city').on('change', function(){
		if ($(this).hasClass('checked')){
			$('.head_location__radio').removeClass('active');
			$(this).parent('.head_location__radio').addClass('active');
		};
	});

	$('.head_location__link').on('click', function(){
		$('.head_location__link').removeClass('active');
		$(this).addClass('active');
	});


	$('.cat_drop__drop_item').on('click', function(){
		$(this).parents('.cat_drop__list').removeClass('active');
		$(this).parents('.cat_drop__list').siblings('.cat_drop__main').removeClass('active');
	});
	$('.cat_drop__main').on('click', function(event){
		$('.cat_drop__main').removeClass('active');
		$('.cat_drop__list').removeClass('active');
		$(this).next('.cat_drop__list').toggleClass('active');
		$(this).toggleClass('active');
		event.stopPropagation;
	});
	$('.cat_drop, .chat, .head_search_trigger, .head_search, .telecast__remind_form, .telecast__remind_box, .header_location_trigger, .checked_city__another_link, .head_location, .tarif_popup, .add_packs_box__hidden').on('click',function(event){
		event.stopPropagation();
	});
	
	$('.tv_accordion__trigger').on('click', function(){
		$(this).siblings('.tv_accordion__slide').slideToggle();
		$(this).toggleClass('active');
	});

	$('.j-accordion__trigger').on('click', function(){
		$(this)
			.toggleClass('active')
			.siblings('.j-accordion__slide')
			.slideToggle()
			.parent('.vacancy')
			.siblings('.vacancy')
			.find('.j-accordion__slide:visible')
			.slideUp()
			.siblings('.j-accordion__trigger')
			.removeClass('active');
	});

	$('.main').on('change', '.upload_input', function(){
		var files = $(this)[0].files;
		for (var i = 0; i < files.length; i++) {
			$(this).parent().siblings('.upload_list').append('<div class="file_item"><div class="file_item__close"></div><span class="file_item__name">' + files[i].name + '</span></div>');
		};
		$(this).parents('.upload_holder').addClass('active');
		$('.upload_box').append('<div class="upload_holder"><div class="upload_list"></div><div class="upload_button"><input type="file"class="upload_input" name="files[]"></div></div>');
	});
	$('.main').on('click', '.file_item__close', function(){
		$(this).parents('.upload_holder').fadeOut('500', function() {
			$(this).parents('.upload_holder').remove();
		});
	});


	$('.j-accordion__slide.active').show();

	$('.tarif__name').on('click', function(){
		$(this).parents('.tarif__table').siblings('.tarif__hidden_info').slideToggle();
		$(this).toggleClass('active');
	});
	$('.tarif__hidden_info.active').show();

	$('.turn_all__item--up').on('click', function(){
		$('.tv_accordion__slide').slideUp().removeClass('active');
		$('.tv_accordion__trigger').removeClass('active');
	});
	$('.turn_all__item--down').on('click', function(){
		$('.tv_accordion__slide').slideDown().addClass('active');
		$('.tv_accordion__trigger').addClass('active');
	});
	$('.tv_accordion__slide.active').show();

	$('.get_pack__item').on('click', function(){
		$('.get_pack__item').removeClass('active');
		$(this).addClass('active');
		$('.get_pack_result').val($(this).data('pack'));
	});


	if (Modernizr.touch) {
		/* cache dom references */ 
		var $body = jQuery('body'); 

		/* bind events */
		$(document)
		.on('focus', 'input', function(e) {
		    $body.addClass('fixfixed');
		})
		.on('blur', 'input', function(e) {
		    $body.removeClass('fixfixed');
		});
	};

	$('.j-get_pack--home').addClass('get_pack--hidden');
	$('.j-user_data__radio_input').on('change', function(){
		if ($(this).val() == 'home'){
			$('.get_pack').removeClass('get_pack--hidden');
			$('.j-get_pack--flat').addClass('get_pack--hidden');
		};
		if ($(this).val() == 'flat'){
			$('.get_pack').removeClass('get_pack--hidden');
			$('.j-get_pack--home').addClass('get_pack--hidden');
		};
	});

	$('.events_nav__item_text').linkScroll(300, 30);

	$('.events_nav__plank-prev').on('click',function(){
		var prev = $('.events_nav__item.active').prev('li').children('a');
		if(prev.length){
			$('.events_nav__item.active').removeClass('active').prev('li').children('a').click();
		};
	});
	$('.events_nav__plank-next').on('click',function(){
		var next = $('.events_nav__item.active').next('li').children('a');
		if (next.length) {
			$('.events_nav__item.active').removeClass('active').next('li').children('a').click();
		};
	});

});

function sidemenuTrigger () {
	$('.sidemenu').toggleClass('active');
	$('.menu_trigger').toggleClass('active');
	$('.wrapper').addClass('transition').toggleClass('opened');
	setTimeout(function(){
		$('.wrapper').removeClass('transition');
	},500);
};

function positionTrigger () {
	var btn = $('.menu_trigger').outerWidth();
	var posLeft = parseInt($('.container').position().left);
	var marginLeft = parseInt($('.container').css('margin-left'));
	var plt = (posLeft > marginLeft) ? posLeft : marginLeft;
	plt = (plt - btn)/2;

	plt = (plt>0) ? plt : 20;
	$('.menu_trigger').css({left:plt});
};

(function($){
	$.fn.jwTabs = function(tabs, syncElemHead, syncElemBody){
		var backActive = $(this).children('li.active').index() + 1;
		var start = 1;
		if (backActive) {start = backActive;};
		$(this).children('li').eq(start-1).addClass('active');
		tabs.children('li').hide(0);
		tabs.children('li').eq(start-1).show(0);
		$(this).children('li').on('click',function(event){
			$(this).siblings('li').removeClass('active').end().addClass('active');
			tabs.children('li').fadeOut(0).eq($(this).index()).fadeToggle(0);

			if (syncElemHead){
				syncElemHead.children('li').eq($(this).index()).siblings('li').removeClass('active').end().addClass('active');
				syncElemBody.children('li').fadeOut(0).eq($(this).index()).fadeToggle(0);
			};
		});
	};



})(jQuery);

var click = false;
function plankMenu(t){
	var  liTop = $('.events_nav__item.active').position().top;
	$('.events_nav__plank').stop().animate({opacity:1, top : liTop}, t, 'linear');
};

function pickChannelSelect(){
	var str = "";
	var checkInput = $('.pick_channel__checkbox:checked');
	if(checkInput.length){
		for (var length = checkInput.length, i = 0; i < length; i+=1) {
			str += $(checkInput[i]).parents('.pick_channel__label').find('.pick_channel__label_cat').text() + ', ';
		};
	}else{
		str = "Выберите категории";
	};
	$('.pick_channel__main').html(str);
};

$.fn.linkScroll = function(speed, offset){
	var that = this;
	$(this).on('click', function(event){
		click = true;
		var elTop = $($(this).attr('href')).offset().top;
		$($(this).attr('href')).parents('.bg_section__item').addClass('animated');
		var param = (parseInt(Math.abs($(window).scrollTop() - elTop)/1000) + 1);
		$(this).parent('li').addClass('active').siblings('li').removeClass('active');
		plankMenu(speed * param);
		$('html, body').animate({scrollTop : elTop - offset}, speed * param);
		setTimeout(function(){
			click = false;
		},speed * param);
		event.preventDefault();
	});
	$(window).on('scroll', function(){
		if (!click) {
			$(that).each(function(){
				if (($(window).scrollTop()+ offset + 5) > $($(this).attr('href')).offset().top &&
					($(window).scrollTop()+ offset + 5) < $($(this).attr('href')).offset().top + 100) {
					$(this).parent('li').addClass('active').siblings('li').removeClass('active');
					plankMenu();
					$($(this).attr('href')).parents('.bg_section__item').addClass('animated');
				};
			});
		};
	});
};