$(document).on('click', '.tarif_cat__trigger_item:not(.not_load)', function(event)
{
	event.preventDefault();
	$.ajax({
		type: 'POST',
		url: '/ajax/packages_page.php',
		success: function(ob)
		{
			$('#home').html(ob);
			$('.tabs_links--home').jwTabs($('.tabs_content--home'));
			
			$('.add_packs_box__hidden_scroll').mCustomScrollbar();
		}
	});
	
	$(this).addClass('not_load');
});


$(document).on('click', '.tabs_links__item:not(.not_load)', function(event)
{
	event.preventDefault();

	var $id = $(this).data('id'), 
		$idPrev = $(this).data('prev'),
		_this = this,
		classType,
		$select = $(this).parent(),
		index = $select.children().index(this); 

	if ($select.hasClass('tabs_links--flat'))
		classType = '.tabs_content--flat';
	else
		classType = '.tabs_content--home';
	//console.log(index);
	
	$.ajax({
		type: 'POST',
		data: ({id: $id, id_prev: $idPrev}),
		url: '/ajax/diffChannels.php',
		success: function(ob)
		{
			$(_this).addClass('not_load');
			//console.log($('.tabs_content__item'));
			$(classType).find('.tabs_content__item').eq(index).html(ob);
			
			$('.add_packs_box__hidden_scroll').mCustomScrollbar();
		}
	});
	
	$(this).addClass('.not_load');
});

