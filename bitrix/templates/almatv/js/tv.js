$(document).on('mouseenter', '.tarif_popup__list_item--not_available', function()
{
	$content_packaged = $(this).find('.tarif_popup__list_available').text();
	if ($content_packaged == '')
	{
		$type_id = $('.tarif_cat__trigger_holder').find('.tarif_cat__trigger_item.active').data('id');
		$packages = $('.tarif_row__table .highlight').data('packages');
		$package_ = $('.tarif_row__table .highlight').data('package');
		
		$arr = $packages.split(',');
		var a = [];
		$.each($arr, function( i, val ) {
			if (val != $package_)
			{
				a.push(val);
			}
		});

		$(this).addClass('ajax_content');
		$_this = this;
		$.ajax({
			type: 'POST',
			url: '/ajax/packages.php',
			data: ({channel_id: $($_this).data('channel'), package_id: $package_, type: $type_id, packages_id: JSON.stringify(a)}),
			success: function(ob)
			{
				$($_this).find('.tarif_popup__list_available').html(ob);
			}
		});
	}
});

$(document).on('click', '.tarif_row__list_all_channels, .channel_button', function(event)
{
	$('.content_channels-modal').html(
	'<div class="popup__loader" style="text-align:center; opacity: 0.6; z-index: 1000;">' +
		'<img src="/bitrix/templates/almatv/images/loader.gif">' +
	'</div>');
	
	event.preventDefault();
	$('#j-popup_channels').arcticmodal();
	$.ajax({
		type: 'POST',
		url: '/ajax/channels.php', //pachage.php
		data: ({package_id: $(this).data('id')}),
		success: function(ob)
		{
			$('.content_channels-modal').html(ob);
			$('.popup_channels-box').mCustomScrollbar(); 
		}
	});
});

$(document).on('click', '.tv_accordion__broadcast, .today_programm__name, .result_programms__name', function(event)
{
	event.preventDefault();
	$id = $(this).data('id');
	$v = $(this).data('v');
	$.ajax({ 
		url: '/ajax/programm.php',
		data: ({id: $(this).data('id'), v: $v}),
		beforeSend: function(jqXHR, settings){
			$('.popup__loader').show();
			/*$('.popup__loader').arcticmodal();*/
			//$(myThis).parents('.tarif_row').siblings('.tarif_row').find('.tarif_popup').slideUp(300);
		},
		success: function(response){

			$('.popup__loader').hide();
			/*$().arcticmodal('close');*/
			
			$('.telecast').html(response);
			
			$('#j-popup_programm').arcticmodal();

			$('#j-telecast__slider').carouFredSel({
				items: {
					visible: 1,
					width: 654,
					height: 360
				},
				scroll: {
					duration: 800,
					timeoutDuration: 4000,
					fx: 'crossfade',
					pauseOnHover: true
				},
				circular: true,
				infinite: true,
				pauseOnHover: true,
				auto: true,
				pagination: '#j-telecast__slider_paging',
				prev:{
					button: "#j-telecast__slider_left"
				},
				next:{
					button: "#j-telecast__slider_right"
				}
			});
		}
	});	
});

$(document).on('click', '.tv_accordion__trigger', function(event)
{
	$(this).siblings('.tv_accordion__slide').slideToggle();
	$(this).toggleClass('active');
});

$(document).on('click', '.turn_all__item--up', function(event){
	$('.tv_accordion__slide').slideUp().removeClass('active');
	$('.tv_accordion__trigger').removeClass('active');
});

$(document).on('click', '.turn_all__item--down', function(event) {
	$('.tv_accordion__slide').slideDown().addClass('active');
	$('.tv_accordion__trigger').addClass('active');
});

//$('.cat_drop__drop_item').on('click', function(){
/*$(document).on('click', '.cat_drop__drop_item', function(event) {
	
});*/

/*$(document).on('click', '.cat_drop__main', function(event) {
	$('.cat_drop__main').removeClass('active');
	$('.cat_drop__list').removeClass('active');
	$(this).next('.cat_drop__list').toggleClass('active');
	$(this).toggleClass('active');
	event.stopPropagation;
});*/

/*$(document).on('click', '.tv_accordion__trigger', function(event) {
	$(this).siblings('.tv_accordion__slide').slideToggle();
	$(this).toggleClass('active');
});*/
$(function()
{
	function showDemo(){
		$('#j-popup_demo').arcticmodal();
		function setNavi( $c, $i ) {
			var current = $c.triggerHandler( 'currentPosition' );
			$('#pagenumber span').text( current+1 );
		}
		$('.popup_demo-slider').carouFredSel({
			responsive: true,
			height: 546,
			items: {
				visible: 1
			},
			scroll: {
				duration: 800,
				timeoutDuration: 4000,
				fx: '',
				onBefore: function( data ) {
					setNavi( $(this), data.items.visible );
				},
				onCreate: function( data ) {
					setNavi( $(this), data.items );
				}
			},
			circular: false,
			infinite: true,
			pauseOnHover: true,
			auto: false,
			pagination: '',
			prev:{
				button: ".popup_demo-slider_prev"
			},
			next:{
				button: ".popup_demo-slider_next"
			}
		});
	};

	/* проверять город */
	$(document).on('click', '.close_popup_demo', function(event) {
		setCookie('showDemo', 1, { expires: 24 * 60 * 60 * 30 * 1000, path: '/' });
	});

	if (!getCookie('showDemo') && getCookie('City')) /*если город не выбран не показывать окно*/
	{
		showDemo();
	}
	
	$( ".quick_search__field" ).keyup(function() {
        //$this = this;
		$query = $(this).val();
        $.ajax({
            type: 'POST',
            url: '/ajax/autocompletion.php',
            data: ({query: $query}),
            success: function(ob)
            {
				$('.quick_search__result').find('.cat_drop__list_inner').html(ob);
				$('.cat_drop__list--search').mCustomScrollbar();
				$('.cat_drop__list--search').addClass('active').fadeIn();
            }
        });
    });
	
	/*$(".cat_drop__drop_item").click(function() {
		$('.cat_drop__list').removeClass('active');
		$('.cat_drop__list').fadeOut();
	});*/
});

$(document).on('focus', '.quick_search__field', function(event) {
	$('.quick_search__result').fadeIn();
});

$(function()
{
	$('.quick_search__field').focus(function()
	{
		$(this).val('');
	});
});

$(document).on('click', '.cat_drop__drop_item', function(event) {
	//$(this).parents('.cat_drop__list').removeClass('active');
	//$(this).parents('.cat_drop__list').siblings('.cat_drop__main').removeClass('active');
	$('.quick_search__field').val('');
	$search = $('.quick_search__field');
	$this = $(this);
	$.ajax({
		type: 'POST',
		url: '/ajax/weekProgrammChannel.php',
		data: ({id: $(this).data('id')}),
		success: function(ob)
		{
			$search.val($this.find('.cat_drop__drop_name').text());
			$('#tv-programm').html(ob);
			$('.after_filter__info').html('');
			$('.quick_search__result').fadeOut();
		}
	});
});

/*validation*/
function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
}
