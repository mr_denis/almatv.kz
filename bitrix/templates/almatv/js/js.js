$(document).on('click', '.channel_button', function(event)
{
	$('.content').html('');
	event.preventDefault();
	$('#j-popup_channels').arcticmodal();
	$.ajax({
		type: 'POST',
		url: '/ajax/channels.php',
		data: ({package_id: $(this).data('id')}),
		success: function(ob)
		{
			$('.content_channels-modal').html(ob);
			$('.popup_channels-box').mCustomScrollbar();  
		}
	});
});

$(document).on('click', '.dev, .main_cats__link--deal', function(event)
{
	$('#j-popup_dev').arcticmodal();
	return false;
});

$(function()
{
	$(".year_awarded").click(function()
	{
		if (!$(this).hasClass('no-active'))
		{
			$.ajax({ 
				url: '/ajax/awarded.php',
				data: ({id: $(this).data('id')}),
				success: function(response){
					$('.popup_awarded__img, .popup_awarded__cont').remove();
					$('#j-popup_awarded').append(response);
					$('#j-popup_awarded').arcticmodal();
				}
			});
		}
		return false;
	});
});

/*$(document).on('click', '.dev, .punkt2', function(event)
{
	$('#j-popup_dev').arcticmodal();
	return false;
});*/

$(document).on('click', 'li.side_nav__item', function(event)
{
	event.preventDefault();
	
	if ($(this).hasClass("active"))
	{
		return false;
	}
	
	$.ajax({
		type: 'POST',
		url: '/ajax/office.php',
		data: ({id: $(this).data('id')}),
		success: function(ob)
		{
			$('.contact_content').html(ob);
		}
	});
		
	$('li.side_nav__item').removeClass('active');
	$(this).addClass('active');
});

$(function()
{
	$('#a-tenders').click(function(e)
	{
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: '/ajax/tenders.php',
			//data: ({number: $number, surname: $surname}),
			success: function(ob)
			{
				$('.content-tenders').html(ob);
			} 
		});
	});
	
	
	$('.check-balance').submit(function()
	{
		$(this).find("input").each(function( index ) {
			if ($(this).val() == '')
			{
				$(this).addClass('error');
			}
			else
			{
				$(this).removeClass('error');
			}
		});
		
		$number = $(this).find('input[name=number]').val();
		$surname = $(this).find('input[name=surname]').val();

		if (($number != '') && ($surname != ''))
		{
			$('.check_balance__cont').remove();
			$('#j-check_balance').append('<div class="check_balance__cont"><h4 class="check_balance__title">Проверка баланса</h4><div class="check_balance_eror"><span>Поиск абонента</span><img class="check_balance__loader" alt="image_description" src="/bitrix/templates/almatv/images/712.GIF"></div></div>');
			$('#j-check_balance').arcticmodal();
			$.ajax({
				type: 'POST',
				url: '/include/check_balance.php',
				data: ({number: $number, surname: $surname}),
				success: function(ob)
				{
					$('.check_balance__cont').remove();
					$('#j-check_balance').append(ob);
				} 
			});
		} 
			
		return false;
	});
	
	/*$('.pay_online_pr').submit(function()
	{
		$number = $(this).find('input[name=number]').val();
		$summ = $(this).find('input[name=summ]').val();
		
		$.ajax({
			type: 'POST',
			url: '/include/check_number.php',
			data: ({number: $number, summ: $summ}),
			success: function(ob)
			{
				console.log(ob);
				alert(ob);
				//$('.check_balance__cont').remove();
				//$('#j-check_balance').append(ob);
			} 
		});
			
		return false;
	});*/
	
	$('.pay_online_button').on('click', function(){
		
		$form = $('#j-pay_online_pr-bull');
		$num = $form.find('input[name=number]').val();
		$summ = $form.find('input[name=summ]').val();
	
		$.ajax({ 
			url: '/ajax/pay.php',
			data: ({number: $num, summ: $summ, sessid: BX.bitrix_sessid()}),
			success: function(response){
				console.log(response);
				//$('.check_balance__cont').remove();
				//$('#j-check_balance').append(response);
				//$('#j-check_balance').arcticmodal();
				$('#j-popup_pay').html(response);
				$('#j-popup_pay').arcticmodal();
			}
		});	
		return false;
	});
	
	$('.head_location__radio, .head_location__link').click(function()
	{
		$city = $(this).data('name');
		$id = $(this).data('id');
		setCookie('CityName', $city, { expires: 24 * 60 * 60 * 30, path: '/' });
		setCookie('City', $id, { expires: 24 * 60 * 60 * 30, path: '/'});

		$('.header_location_trigger').html($city);
		$('.checked_city__name').html($city);
		//console.log($id);
		/*$.ajax({
			type: 'POST',
			url: '/ajax/contacts.php',
			data: ({id: $id}),
			success: function(ob)
			{
				$('.contact_common').html(ob);
			}
		});*/
	});
	
	/*$('.head_location__radio').click(function()
	{
		$city = $(this).data('name');
		$id = $(this).data('id');
		setCookie('CityName', $city, { expires: 24 * 60 * 60 * 30, path: '/' });
		setCookie('City', $id, { expires: 24 * 60 * 60 * 30, path: '/'});
	}*/
});