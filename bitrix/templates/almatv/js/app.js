var app = angular.module('almatv', []);
//allChannel.php?type=channel - получаем в json формате каналы
//allChannel.php?type=category - получаем в json формате категории
app.controller('programmFilter',function ($scope, $http) {
	/*загружаем категории*/
	$http.get('/ajax/allChannel.php?type=category').
    success(function(data, status, headers, config) {
		$scope.categoryList = data;
    }).
    error(function(data, status, headers, config) {
		console.log(data);
    });

	$scope.changeCat = function (index){
		$('.quick_search__field').val('');
		//фильтруем каналы по категориям
		$scope.categoryActive = $scope.categoryList[index];
		idCategory = $scope.categoryList[index]['id'];
		$http.get('/ajax/allChannel.php?type=channel&categoryId=' + idCategory).
		success(function(data, status, headers, config) {
			$scope.channelList = data;
			$scope.channelActive = $scope.channelList[0];	
			$('.cat_drop__list').removeClass('active');
			$('.cat_drop__list').siblings('.cat_drop__main').removeClass('active');
			$.ajax({
				type: 'POST',
				url: '/ajax/weekProgrammChannel.php',
				data: ({id: $scope.channelActive.id}),
				success: function(ob)
				{
					$('#tv-programm').html(ob);
                                        $('.after_filter__info').html('');
					$('img[ng-src=""]').css('display', 'none');
				}
			});
		
		}).
		error(function(data, status, headers, config) {
			console.log(data);
		});
	};
	$scope.changeChannel = function (index){
		$('.quick_search__field').val('');
		$scope.channelActive = $scope.channelList[index];
		nameChannel = $scope.channelList[index]['name'];
		$('.checked_channel__title').text(nameChannel);
		$('.cat_drop__list').removeClass('active');
		$('.cat_drop__list').siblings('.cat_drop__main').removeClass('active');
		/*подгружаем программу канала*/
		$.ajax({
			type: 'POST',
			url: '/ajax/weekProgrammChannel.php',
			data: ({id: $scope.channelActive.id}),
			success: function(ob)
			{
				$('.cat_drop__list').removeClass('active');
				$('#tv-programm').html(ob);
                                $('.after_filter__info').html('');
				$('img[ng-src=""]').css('display', 'none');
			}
		});
	};

	function obId($arr, $id)
	{
		var $key;
		angular.forEach($arr, function(value, key){
			if (value.id == $id)
			{
				$key = key;
			}
		});
		return $key;
	}
	
	$(document).on('click','.cat_drop__drop_item', function()
	{
		$id = $(this).data('id');
		$categoryId = $(this).data('section-id');
		$scope.categoryActive = $scope.categoryList[obId($scope.categoryList, $categoryId)];
		$scope.$apply(function() {
			
			$http.get('/ajax/allChannel.php?type=channel&categoryId=' + $categoryId).
			success(function(data, status, headers, config) {
				$scope.channelList = data;
				$scope.channelActive = $scope.channelList[obId($scope.channelList, $id)];	
			}).
			error(function(data, status, headers, config) {
				console.log(data);
			});
		});
	});
});

