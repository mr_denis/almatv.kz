$(document).on('click', '.telecast__accordion_trigger', function() {
	$(this).toggleClass('active');
	if ($(this).hasClass('active')){
		$(this).text('Скрыть описание');
		$(this).prev('.telecast__accordion').addClass('active')
			.find(".text-fade").hide();
	} else {
		$(this).text('Показать описание полностью');
		$(this).prev('.telecast__accordion').removeClass('active')
			.find(".text-fade").show();
	};
});

$(function() {
	$('.full_banner-item').on('click',function(){
		$(this).removeClass('active');
	});
	//$('.full_banner-btn').on('mousemove',function(){
	//	var fullBox = $(this).parents('.full_banner-item');
	//	var parentOffset = fullBox.offset();
	//	var relX = event.pageX - parentOffset.left;
	//	var relY = event.pageY - parentOffset.top;
	//	fullBox.find('.full_banner-bg').css({
	//		'bottom' : fullBox.outerHeight() - relY,
	//		'left' : relX
	//	});
	//});
	$('.full_banner-btn').on('click',function(event){
		var fullBox = $(this).parents('.full_banner-item');
		fullBox.addClass('active');
		event.stopPropagation();
	});
	$('input, textarea').placeholder();
	$('body').on('click','.telecast__actors_user',function(){
		var that = this;
		var delay = 300;
		$(that).fadeOut();
		setTimeout(function(){
			$(that).parent('.telecast__actors_list__show').siblings('.telecast__actors_list__hide').slideDown();
		},delay);
		$(that).parent('.telecast__actors_list__show').siblings('.telecast__actors_list__hide').find('.telecast__actors_item').each(function(){
			var item = this;
			delay = delay + 150;
			setTimeout(function(){
				$(item).animate({opacity:1},300);
			},delay);
		});
	});
	$('body').on('click','.j-telecast__actors_item',function(){
		var that = this;
		if($(that).hasClass('active')){
			$(that).removeClass('active');
			$(that).parents('.telecast__actors_list_row').find('.telecast__actors_popup').slideUp();
		}else{
			$(that).parents('.telecast__li').find('.telecast__actors_item').removeClass('active');
			$(that).parents('.telecast__actors_list_row').siblings('.telecast__actors_list_row').find('.telecast__actors_popup').slideUp();
			$(that).parents('.telecast__actors_list_row').find('.telecast__actors_popup').slideDown();
			$('.telecast__actors_popup_sliderlist').carouFredSel({
				//synchronise : '.telecast__actors_popup_sliderthumb',
				pauseOnHover: true,
				auto: false,
				prev:{
					button: ".telecast__actors_popup_slider_prev"
				},
				next:{
					button: ".telecast__actors_popup_slider_next"
				},
				scroll: {
					onAfter: function(data){
						$('.telecast__actors_popup_sliderthumb .telecast__actors_popup_slideritemthumb[data-slide='+ data.items.visible.attr('data-slide') +']').addClass('selected').siblings('.telecast__actors_popup_slideritemthumb').removeClass('selected');
					}
				}
			});
			var slWidth = $('.telecast__actors_popup_sliderthumb .telecast__actors_popup_slideritemthumb').length * 100;
			$('.telecast__actors_popup_sliderthumb').carouFredSel({
				auto: false,
				align: 'center',
				width: (slWidth > 500 ? 500 : slWidth),
				items: {
					width:100,
				}
			});
			$('.telecast__actors_popup_sliderthumb .telecast__actors_popup_slideritemthumb').on('click',function(){
				$('.telecast__actors_popup_sliderlist').trigger( 'slideTo', [ $('.telecast__actors_popup_sliderlist .telecast__actors_popup_slideritem[data-slide='+ $(this).attr('data-slide') +']') ] );
				//$('.telecast__actors_popup_sliderthumb .telecast__actors_popup_slideritemthumb').removeClass('selected');
				//$(this).addClass('selected');
			});
			$(that).addClass('active');
		};
	});
	/*$('#title-search-input').on('keyup',function(){
		$(this).parents('.head_search').find('.head_search_res').slideDown();
	});
	$('#title-search-input').on('blur',function(){
		$(this).parents('.head_search').find('.head_search_res').slideUp();
	});*/

	var options =  { 
		'translation': {A: {pattern: /[А-Я а-яёЁ]/}},
	  onKeyPress: function(cep, event, currentField, options){
	    cep = cep.slice(0, 1).toUpperCase() + cep.slice(1);
	    $(currentField).val(cep);
	  }
	};
	
	//$(".j-user_data__field--phone").inputmask("+7 9{0,10}",{ "clearIncomplete": true, "greedy": false});

	$('.user_data__field--valid').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', options);
	
	var mobileHover = function () {
		$('*').on('touchstart', function () {
		    $(this).trigger('hover');
		}).on('touchend', function () {
		    $(this).trigger('hover');
		});
	};
 
	$(window).scroll(function(){
	    if ($(this).scrollTop() > 800) {
	        $('.scrollup').fadeIn();
	    } else {
	        $('.scrollup').fadeOut();
	    }
	}); 

	$('.scrollup').click(function(){
	    $("html, body").animate({ scrollTop: 0 }, 600);
	    return false;
	});
	$('.scrollup').hide(0);

	mobileHover();

	//$.srSmoothscroll({ // Плавный скрол на всей странице
	//	step: 150,
	//	speed: 600,
	//	ease: 'easeOutQuint'
	//});

	/*$.arcticmodal('setDefault', {
		beforeOpen: function(){
			$.srSmoothscroll.destroy(); // Отключаем плавный скрол при появлении попапа, чтобы не было прокрутки
		},
		afterClose: function(){
			$.srSmoothscroll({ // Реинициализируем плавную прокрутку
				step: 150,
				speed: 600,
				ease: 'easeOutQuint'
			});
		}
	});*/

	jQuery.validator.addMethod("lettersonly", function(value, element) { // Дополнительное правило на валидацию. Только буквы
		return this.optional(element) || /^[a-z, а-я]+$/i.test(value);
	}, "Пожалуйста, вводите только буквы."); 

	$('.head_search__field').on('focus',function(){
		//$('.head_search__box__list').slideDown(300);
		$('#j-drop_search_results').slideDown(300);
	});
	$('.head_search__field').on('blur',function(){
		//$('.head_search__box__list').slideUp(300);
		$('#j-drop_search_results').slideUp(300);
	});

	$(window).load(function(){
		$('.cat_drop__list--category,.cat_drop__list_inner--channel, .add_packs_box__hidden_scroll, .chat__dialog_scroll, .popup_channels-box, .event__scroll, .head_search__box__list').mCustomScrollbar();
		$('#j-main_slider').carouFredSel({
			responsive: true,
			items: {
				visible: 1
			},
			scroll: {
				duration: 800,
				timeoutDuration: 4000,
				fx: 'crossfade',
				pauseOnHover: true
			},
			circular: true,
			infinite: true,
			pauseOnHover: true,
			auto: true,
			pagination: '#j-main_slider__paging',
			prev:{
				button: "#j-main_slider_left"
			},
			next:{
				button: "#j-main_slider_right"
			}
		});
		
		$('#j-telecast__slider').carouFredSel({
			items: {
				visible: 1,
				width: 654,
				height: 360
			},
			scroll: {
				duration: 800,
				timeoutDuration: 4000,
				fx: 'crossfade',
				pauseOnHover: true
			},
			circular: true,
			infinite: true,
			pauseOnHover: true,
			auto: true,
			pagination: '#j-telecast__slider_paging',
			prev:{
				button: "#j-telecast__slider_left"
			},
			next:{
				button: "#j-telecast__slider_right"
			}
		});

		$('#j-inet_pluses_slider').carouFredSel({
			responsive: true,
			scroll: 1,
			items: {
				visible: {
					min: 1,
					max: 5
				},
				width: 300,
				duration: 200,
				timeoutDuration: 4000,
				fx: 'scroll',
			},
			circular: true,
			infinite: true,
			pauseOnHover: true,
			auto: false,
			prev:{
				button: "#j-inet_pluses_slider_left"
			},
			next:{
				button: "#j-inet_pluses_slider_right"
			}
		});


		$('.pick_channel__label').on('click',function(event){
			event.stopPropagation();
		});

		$('.tarif_row__cell--isset').hover(function() {
			var myThis = $(this);
			$(this).prevAll('.tarif_row__cell').addClass('blue_highlight');
			$(this).parents('.tarif_row').find('.tarif_row__item').addClass('blue_highlight');
			$(this).parents('.tarif_row').prevAll('.tarif_row').each(function(index, el) {
				$(el).find('.tarif_row__cell').eq(myThis.index()).addClass('blue_highlight');
			});
		}, function() {
			$(this).prevAll('.tarif_row__cell').removeClass('blue_highlight');
			$(this).parents('.tarif_row').find('.tarif_row__item').removeClass('blue_highlight')
			$(this).parents('.tarif_row_holder--internettv').find('.blue_highlight').removeClass('blue_highlight');
		}); 
		
		$(".container").on('click', ".j-tarif_row__cell_tarif:not('.tarif_row__cell--first, .tarif_row__cell--last, .highlight')", function(event){
			$inetId = parseInt($(this).parent().data('id-inet'));
			if ($inetId > 0) {
				var myThis = this;
				$.ajax({
					url: '/ajax/popup_ajax_tarif.php',
					data: ({id: $inetId }),
					beforeSend: function(jqXHR, settings){
						$(myThis).parents('.tarif_row').find('.tarif_popup__loader').addClass('active');
					},
					success: function(response){

						$(myThis).parents('.tarif_row').find('.tarif_popup__loader').removeClass('active');
						$(myThis).parents('.tarif_row').find('.tarif_popup_scroll').html(response);
						$(myThis).parents('.tarif_row').siblings('.tarif_row').find('.tarif_popup').slideUp(300);
						$(myThis).parents('.tarif_row').find('.tarif_popup').slideDown(300);
					}
				});

				if ($(this).hasClass('highlight')){
					$(this).removeClass('highlight');
					$('.tarif_popup').removeClass('active');
					$('.tarif_popup_scroll').removeClass('active');
					$(this).parents('.tarif_row').find('.tarif_popup').removeClass('active').find('.tarif_popup_scroll').removeClass('active');
				}else{
					$('.tarif_row__cell').removeClass('highlight');
					$('.tarif_popup').removeClass('active');
					$('.tarif_popup_scroll').removeClass('active');
					$(this).parents('.tarif_row').find('.tarif_popup').addClass('active').find('.tarif_popup_scroll');
					var that = this;
					setTimeout(function(){
						$(that).parents('.tarif_row').find('.tarif_popup_scroll').addClass('active');
					},500);
					$(this).addClass('highlight');
				};
				event.stopPropagation();
				

				$('.tarif_popup__close').on('click', function(){
					$(this).parents('.tarif_popup').removeClass('active');
					$('.j-tarif_row__cell_tarif').removeClass('highlight');
					$('.tarif_popup_scroll').removeClass('active');
				});
			}
		});

		var p_id;
		var c_id;
		$(".container").on('click', ".j-tarif_row__cell_channels:not('.tarif_row__cell--first, .tarif_row__cell--last, .null, .highlight')", function(event){
			var myThis = this;
			$category_id = $(this).data('category');
			$package_id = $(this).data('package');
			$packages_id = $(this).data('packages');
			$type_id = $('.tarif_cat__trigger_holder').find('.tarif_cat__trigger_item.active').data('id');
			/*console.log($type_id);*/
			if (($category_id != c_id) || ($package_id != p_id))
			{
				$.ajax({ 
					url: '/ajax/popup_ajax.php',
					data: ({package_id: $package_id, category_id: $category_id, type_id: $type_id, packages_id: $packages_id}),
					beforeSend: function(jqXHR, settings){
						$(myThis).parents('.tarif_row').find('.tarif_popup__loader').addClass('active');
						$(myThis).parents('.tarif_row').siblings('.tarif_row').find('.tarif_popup').slideUp(300);
						$(myThis).parents('.tarif_row').find('.tarif_popup').slideDown(300);
					},
					success: function(response){
						c_id = $(myThis).data('category');
						p_id = $(myThis).data('package');
					
						$(myThis).parents('.tarif_row').find('.tarif_popup__loader').removeClass('active');
						$(myThis).parents('.tarif_row').find('.tarif_popup_scroll').html(response);
						$(myThis).parents('.tarif_row').find('.j-tarif_popup_slider').carouFredSel({
							responsive: true,
							items: {
								visible: {
									min: 4,
									max: 6
								},
								width: 250,
								duration: 200,
								timeoutDuration: 4000,
								fx: 'scroll',
							},
							circular: false,
							infinite: true,
							pauseOnHover: true,
							auto: false,
							pagination: '.tarif_popup_slider__paging',
							prev:{
								button: ".tarif_popup_slider__left"
							},
							next:{
								button: ".tarif_popup_slider__right"
							}
						});
						
					}
				});
			}

			if ($(this).hasClass('highlight')){
				$(this).removeClass('highlight');
				$('.tarif_popup').removeClass('active');
				$('.tarif_popup_scroll').removeClass('active');
				$(this).parents('.tarif_row').find('.tarif_popup').removeClass('active').find('.tarif_popup_scroll').removeClass('active');
			}else{
				$('.tarif_row__cell').removeClass('highlight');
				$('.tarif_popup_scroll').removeClass('active');
				$(this).parents('.tarif_row').find('.tarif_popup').addClass('active').find('.tarif_popup_scroll');
				var that = this;
				$(that).parents('.tarif_row').siblings('.tarif_row').find('.tarif_popup').slideUp(300);
				$(that).parents('.tarif_row').find('.tarif_popup').slideDown(300);
				setTimeout(function(){
					$(that).parents('.tarif_row').find('.tarif_popup_scroll').addClass('active');
				},500);
				$(this).addClass('highlight');
			};
			event.stopPropagation();
		});
		$('.tarif_popup__close').on('click', function(){
			$(this).parents('.tarif_popup').removeClass('active');
			$('.tarif_row__cell').removeClass('highlight');
			$('.j-tarif_row__cell_tarif').removeClass('highlight');
			$('.tarif_popup_scroll').removeClass('active');
		});
	});

	$('.main').on('change', '.pick_channel__checkbox', function(){
		if ($(this).hasClass('checked')){
			$('.'+($(this).data('channel'))).addClass('active');
		}else{
			$('.'+($(this).data('channel'))).removeClass('active');
		};
		$(this).trigger('refresh');
		/*pickChannelSelect();*/
	});


	$('select, input[type="checkbox"], input[type="radio"]').styler({
		selectSearch: true
	});
	$('.custom-file').styler();
	$('#j-popup_city').arcticmodal({
		closeOnOverlayClick: false
	});

	/*$('.tv_accordion__broadcast, .today_programm__name').on('click', function(){
		
		$id = $(this).data('id');
		$.ajax({ 
			url: '/ajax/programm.php',
			data: ({id: $(this).data('id')}), //3103
			success: function(response){
				//$('.popup_awarded__img, .popup_awarded__cont').remove();
				//$('#j-popup_awarded').append(response);
				$('#j-popup_awarded').html(response);

				$('#j-telecast__slider').carouFredSel({
					items: {
						visible: 1,
						width: 654,
						height: 360
					},
					scroll: {
						duration: 800,
						timeoutDuration: 4000,
						fx: 'crossfade'
					},
					circular: true,
					infinite: true,
					pauseOnHover: true,
					auto: false,
					pagination: '#j-telecast__slider_paging',
					prev:{
						button: "#j-telecast__slider_left"
					},
					next:{
						button: "#j-telecast__slider_right"
					}
				});
				
				$('#j-popup_awarded').arcticmodal();
				
			}
		});
		
		$('#j-popup_programm').arcticmodal();
		
	});*/
	
	/*$('.today_programm__name').on('click', function(){
		$('#j-popup_programm').arcticmodal();
	});*/
	var roteteClass = "rotete-start";
	$('.rotete-elem').on('click',function(){
		$('.inet_item__rotete').removeClass(roteteClass).addClass($(this).data('rotete'));
		roteteClass = $(this).data('rotete');
	});
	$('.result_programms__name').on('click', function(){
		$('#j-popup_programm').arcticmodal();
	});
	$('.pack_main_info__show_all').on('click', function(){
		$('#j-popup_channels').arcticmodal();
	});
	$('.tarif_row__list_all_channels').on('click', function(){
		$('#j-popup_channels').arcticmodal();
	});
	$('.tarif_row__list_all_channels, .get_pack__add').each(function(index, el){
		$(el).append('<div class="tarif_row__list_all_channels_tip">Посмотреть все каналы</div>');
	});

	$('#j-popup_info').arcticmodal();

	$('.j-special_menu__link--balance').on('click', function(event){
		event.preventDefault();
		$('#j-popup_balance').arcticmodal(); 
	});
	$('.close_popup').on('click', function(){
		$.arcticmodal('close');
	});


	$('.tabs_links--one_news').jwTabs($('.tabs_content--one_news'));
	$('.tabs_links--business').jwTabs($('.tabs_content--business'));
	$('.tabs_links--press').jwTabs($('.tabs_content--press'));
	$('.tabs_links--podkl').jwTabs($('.tabs_content--podkl'));
	$('.tabs_links--flat').jwTabs($('.tabs_content--flat'));
	$('.tabs_links--home').jwTabs($('.tabs_content--home'));
	$('.j-tarif_cat__trigger--flat_home').jwTabs($('.flat_home_tabs'));


	$('.j-tarif_cat__tabs_head').jwTabs($('.tarif_cat__tabs_cont'), $('.j-add_eq__tabs_head'), $('.add_eq__tabs_cont'));
	$('.j-add_eq__tabs_head').jwTabs($('.add_eq__tabs_cont'), $('.j-tarif_cat__tabs_head'), $('.tarif_cat__tabs_cont'));
	$('#j-single_programm_tabs_head').jwTabs($('#j-single_programm_tabs_body'));
	$('#j-business_tabs_head').jwTabs($('#j-business_tabs_cont'));

	$('.wrapper').on('click', '.j-tarif_cat__tabs_head li, .j-add_eq__tabs_head li',function(){
		$('.j-add_eq__tabs_img img').eq($(this).index()).fadeIn(600).siblings('img').fadeOut(600);
		$('.content-pack').removeClass('active').eq($(this).index()).addClass('active');
	});


	/*$('.add_packs_box__count').on('click', function(event){
		event.stopPropagation();
		var el = $(this);
		if (el.hasClass('active')){
			el.removeClass('active');
		}else{
			$('.add_packs_box__count').removeClass('active');
			el.addClass('active');
		};
	});*/

	/*$('.get_pack__add:not(.channel_button)').on('click', function(event){
		event.stopPropagation();
		var el = $(this);
		if (el.hasClass('active')){
			el.removeClass('active');
		}else{
			$('.get_pack__add').removeClass('active');
			el.addClass('active');
		};
	});*/

	$('.menu_trigger').on('click',  function(event){
		event.stopPropagation();
		sidemenuTrigger ();
		$('.chat').removeClass('active');
		$('.chat__trigger').removeClass('active');
	});

	$('.sidemenu').on('click',function(event){
		event.stopPropagation();
	});

	$('.chat__trigger').on('click', function(){
		$('.chat').toggleClass('active');
		$(this).toggleClass('active');
	});
	
	$(document).on('click',function(){
		if ($('.menu_trigger').hasClass('active')) {
			sidemenuTrigger ();
		};
		$('.cat_drop__list').removeClass('active');
		$('.cat_drop__main').removeClass('active');
		/*$('.add_packs_box__count').removeClass('active');*/
		/*$('.get_pack__add').removeClass('active');*/
		$('.chat').removeClass('active');
		$('.chat__trigger').removeClass('active');
		$('.head_search_trigger').removeClass('active');
		$('.head_search').removeClass('active');
		//$('.head_search__box__list').hide();
		$('#j-drop_search_results').hide();
		$('.menu_trigger').removeClass('menu_trigger--search_open');
		$('.chat').removeClass('chat--search_open');
		$('.menu_trigger').removeClass('menu_trigger--location_open');
		$('.chat').removeClass('chat--location_open');
		$('.head_location').removeClass('active');
		$('.header_location_trigger').removeClass('active');
		$('.tarif_popup').removeClass('active');
		$('.tarif_row__cell').removeClass('highlight');
		$('.tarif_popup_scroll').removeClass('active');
		$('.tarif_popup').removeClass('active');
		$('.tarif_row').find('.tarif_popup').slideUp(300);


		$('.tarif_popup__list_item--not_available').removeClass('active');
		$('.tarif_popup_scroll').removeClass('active');
		$('.j-check-balance--1280 span.error').fadeOut();
		$('.pay_online__row span.error').fadeOut(300);

	});

	$('.cat_drop, .chat, .head_search, .head_search_trigger, .header_location_trigger, .checked_city__another_link, .head_location, .tarif_popup, .j-check-balance--1280, #j-check-balance, #j-check-balance_mini').on('click',function(event){
		event.stopPropagation(); //
	});

	$('.head_search_trigger').on('click', function(){
		$('.head_search').toggleClass('active');
		$(this).toggleClass('active');
		$('.menu_trigger').removeClass('menu_trigger--location_open');
		$('.chat').removeClass('chat--location_open');
		$('.menu_trigger').toggleClass('menu_trigger--search_open');
		$('.chat').toggleClass('chat--search_open');
		$('.head_location').removeClass('active');
		$('.header_location_trigger').removeClass('active');
		$('#title-search-input').focus();
	});


	$('.header_location_trigger, .head_location__close').on('click', function(){
		$('.head_location').toggleClass('active');
		$(this).toggleClass('active');
		$('.menu_trigger').removeClass('menu_trigger--search_open');
		$('.chat').removeClass('chat--search_open');
		$('.menu_trigger').toggleClass('menu_trigger--location_open');
		$('.chat').toggleClass('chat--location_open');
		$('.head_search').removeClass('active');
		$('.head_search_trigger').removeClass('active');
	});
	$('.checked_city__another_link').on('click', function(){
		$('.header_location_trigger').click();
	});
	$('.head_location__radio').on('click', function(){
		$('.head_location__radio').removeClass('active');
		$(this).addClass('active');
	});

	$('.head_location__link').on('click', function(){
		$('.head_location__link').removeClass('active');
		$(this).addClass('active');
	});


	$('body').on('click', '.cat_drop__drop_item', function(){
		$(this).parents('.cat_drop__list').removeClass('active');
		$(this).parents('.cat_drop__list').siblings('.cat_drop__main').removeClass('active');
	});
	$('.cat_drop__main').on('click', function(event){
		if (!$(this).hasClass('active')) {
			$('.cat_drop__main').removeClass('active');
			$('.cat_drop__list').removeClass('active');
			$(this).next('.cat_drop__list').addClass('active');
			$(this).addClass('active');
		}else{
			$('.cat_drop__main').removeClass('active');
			$('.cat_drop__list').removeClass('active');
		};
		event.stopPropagation;
	});
	var hideDrop = null;
	// $('body').on('click', '.filter_holder__item--hover .cat_drop', function(){
	// 	console.info(2345);
	// 	$('.filter_holder__item--hover .cat_drop__main').removeClass('active');
	// 	$('.filter_holder__item--hover .cat_drop__list').removeClass('active');
	// 	$(this).children('.cat_drop__main').next('.cat_drop__list').addClass('active');
	// 	$(this).children('.cat_drop__main').addClass('active');
	// });
	$('.filter_holder__item--hover .cat_drop').hover(function(event){
		clearInterval(hideDrop);
		// $('.filter_holder__item--hover .cat_drop__main').removeClass('active');
		// $('.filter_holder__item--hover .cat_drop__list').removeClass('active');
		// $(this).children('.cat_drop__main').next('.cat_drop__list').addClass('active');
		// $(this).children('.cat_drop__main').addClass('active');
		// event.stopPropagation;
	},function(){
		clearInterval(hideDrop);
		hideDrop = setTimeout(function(){
			$('.filter_holder__item--hover .cat_drop__main').removeClass('active');
			$('.filter_holder__item--hover .cat_drop__list').removeClass('active');
		},2000);
	});
	
	$('.tv_accordion__trigger').on('click', function(){
		$(this).siblings('.tv_accordion__slide').slideToggle();
		$(this).toggleClass('active');
	});

	$('.j-accordion__trigger').on('click', function(){
		$(this)
			.toggleClass('active')
			.siblings('.j-accordion__slide')
			.slideToggle()
			.parent('.vacancy')
			.siblings('.vacancy')
			.find('.j-accordion__slide:visible')
			.slideUp()
			.siblings('.j-accordion__trigger')
			.removeClass('active');
	});

	$('.main').on('change', '.upload_input', function(){
		var files = $(this)[0].files;
		for (var i = 0; i < files.length; i++) {
			$(this).parent().siblings('.upload_list').append('<div class="file_item"><div class="file_item__close"></div><span class="file_item__name">' + files[i].name + '</span></div>');
		};
		$(this).parents('.upload_holder').addClass('active');
		$('.upload_box').append('<div class="upload_holder"><div class="upload_list"></div><div class="upload_button"><input type="file"class="upload_input" name="files[]"></div></div>');
	});
	$('.main').on('click', '.file_item__close', function(){
		$(this).parents('.upload_holder').fadeOut('500', function() {
			$(this).parents('.upload_holder').remove();
		});
	});


	$('.j-accordion__slide.active').show();

	$('.tarif__name').on('click', function(){
		$(this).parents('.tarif__table').siblings('.tarif__hidden_info').slideToggle();
		$(this).toggleClass('active');
	});
	$('.tarif__hidden_info.active').show();

	$('.turn_all__item--up').on('click', function(){
		$('.tv_accordion__slide').slideUp().removeClass('active');
		$('.tv_accordion__trigger').removeClass('active');
	});
	$('.turn_all__item--down').on('click', function(){
		$('.tv_accordion__slide').slideDown().addClass('active');
		$('.tv_accordion__trigger').addClass('active');
	});
	$('.tv_accordion__slide.active').show();

	$('.get_pack__item').on('click', function(){
		$('.get_pack__item').removeClass('active');
		$(this).addClass('active');
		$('.get_pack_result').val($(this).data('pack'));
	});


	if (Modernizr.touch) {
		/* cache dom references */ 
		var $body = jQuery('body'); 

		/* bind events */
		$(document)
		.on('focus', 'input', function(e) {
		    $body.addClass('fixfixed');
		})
		.on('blur', 'input', function(e) {
		    $body.removeClass('fixfixed');
		});
	};

	$('.j-get_pack--home').addClass('get_pack--hidden');
	$('.j-user_data__radio_input').on('change', function(){
		if ($(this).val() == 'home'){
			$('.get_pack').removeClass('get_pack--hidden');
			$('.j-get_pack--flat').addClass('get_pack--hidden');
		};
		if ($(this).val() == 'flat'){
			$('.get_pack').removeClass('get_pack--hidden');
			$('.j-get_pack--home').addClass('get_pack--hidden');
		};
	});

	$('.events_nav__item_text').linkScroll(300, 30);

	$('.events_nav__plank-prev').on('click',function(){
		var prev = $('.events_nav__item.active').prev('li').children('a');
		if(prev.length){
			$('.events_nav__item.active').removeClass('active').prev('li').children('a').click();
		};
	});
	$('.events_nav__plank-next').on('click',function(){
		var next = $('.events_nav__item.active').next('li').children('a');
		if (next.length) {
			$('.events_nav__item.active').removeClass('active').next('li').children('a').click();
		};
	});
});

function sidemenuTrigger () {
	$('.sidemenu').toggleClass('active');
	$('.menu_trigger').toggleClass('active');
	$('.wrapper').addClass('transition').toggleClass('opened');
	setTimeout(function(){
		$('.wrapper').removeClass('transition');
	},500);
};

function positionTrigger () {
	var btn = $('.menu_trigger').outerWidth();
	var posLeft = parseInt($('.container').position().left);
	var marginLeft = parseInt($('.container').css('margin-left'));
	var plt = (posLeft > marginLeft) ? posLeft : marginLeft;
	plt = (plt - btn)/2;

	plt = (plt>0) ? plt : 20;
	$('.menu_trigger').css({left:plt});
};

(function($){
	$.fn.jwTabs = function(tabs, syncElemHead, syncElemBody){
		var backActive = $(this).children('li.active').index() + 1;
		var start = 1;
		if (backActive) {start = backActive;};
		$(this).children('li').eq(start-1).addClass('active');
		tabs.children('li').hide(0);
		tabs.children('li').eq(start-1).show(0);
		$(this).children('li').on('click',function(event){
			var myThis = $(this);
			$(this).siblings('li').removeClass('active').end().addClass('active');
			if (syncElemHead){
				syncElemHead.children('li').eq($(this).index()).siblings('li').removeClass('active').end().addClass('active');
			};
			var popupIsVisible = $('.tarif_popup.active').length;
			var delay = 0;
			if (popupIsVisible){
				delay = 550;
			};
			setTimeout(function(){
				tabs.children('li').fadeOut(0).eq(myThis.index()).fadeToggle(0);
				if (syncElemHead){
					syncElemBody.children('li').fadeOut(0).eq(myThis.index()).fadeToggle(0);
				};
			}, delay);
		});
	};
})(jQuery);

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Letters only please"); 


var click = false;
function plankMenu(t){
	var  liTop = $('.events_nav__item.active').position().top;
	$('.events_nav__plank').stop().animate({opacity:1, top : liTop}, t, 'linear');
};

function pickChannelSelect(){
	var str = "";
	var checkInput = $('.pick_channel__checkbox:checked');
	if(checkInput.length){
		for (var length = checkInput.length, i = 0; i < length; i+=1) {
			str += $(checkInput[i]).parents('.pick_channel__label').find('.pick_channel__label_cat').text() + ', ';
		};
	}else{
		str = "Выберите категории";
	};
	$('.pick_channel__main').html(str);
};

$.fn.linkScroll = function(speed, offset){
	var that = this;
	$(this).on('click', function(event){
		click = true;
		var elTop = $($(this).attr('href')).offset().top;
		$($(this).attr('href')).parents('.bg_section__item').addClass('animated');
		var param = (parseInt(Math.abs($(window).scrollTop() - elTop)/1000) + 1);
		$(this).parent('li').addClass('active').siblings('li').removeClass('active');
		plankMenu(speed * param);
		$('html, body').animate({scrollTop : elTop - offset}, speed * param);
		setTimeout(function(){
			click = false;
		},speed * param);
		event.preventDefault();
	});
	$(window).on('scroll', function(){
		if (!click) {
			$(that).each(function(){
				if (($(window).scrollTop()+ offset + 5) > $($(this).attr('href')).offset().top &&
					($(window).scrollTop()+ offset + 5) < $($(this).attr('href')).offset().top + 100) {
					$(this).parent('li').addClass('active').siblings('li').removeClass('active');
					plankMenu();
					$($(this).attr('href')).parents('.bg_section__item').addClass('animated');
				};
			});
		};
	});
};