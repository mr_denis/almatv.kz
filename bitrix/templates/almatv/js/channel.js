$(document).on('click', '.tarif_cat__trigger_item:not(.full)', function(event)
{
	$datef = $(this).data('datef');
	$('.single_programm_head__date').html($datef);
	
	$('.single_programm_tabs_body li.single_programm_tabs_body__item').hide();
	$num = $(this).data('num');
	$id = $('#j-single_programm_tabs_head').data('id');
	$date = $(this).data('date');
	
	//$(".tarif_cat__trigger_item").removeClass('full');
	$(this).addClass('full');
	//console.log($num);
	event.preventDefault();
	$v = $(this).data('v');
	
	$.ajax({
		type: 'POST',
		url: '/ajax/channelProgram.php',
		data: ({id: $id, date: $date, v: $v}),
		success: function(ob)
		{
			$('.single_programm_tabs_body li.single_programm_tabs_body__item').eq($num).html(ob).fadeIn(1000);
			//$('body,html').animate({scrollTop: 450}, 500);
		}
	});
});
