$(function()
{
	$( ".main_search__btn").click(function(event) {
        
		event.preventDefault();
		//$this = this;
		$query = $('.main_search__field').val();
        $.ajax({
            type: 'POST',
            url: '/ajax/search.php',
            data: ({query: $query}),
            success: function(ob)
            {
				$('#result_content').html(ob);
				//$('.cat_drop__list--search').fadeIn();
            }
        });
    });
});