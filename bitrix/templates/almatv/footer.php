<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
	<?if (($page != '/company/index.php') && ($page != '/internet_tv/index.php')) {?>
		</div>
	</div>
	<?}?>
		<footer class="footer">
			<div class="top_footer">
				<div class="container">
					<?/*<a href="/" class="logo logo--footer"></a>*/?>
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include", 
						".default", 
						array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_TEMPLATE_PATH."/include/social_top.php",
							"EDIT_TEMPLATE" => ""
						),
						false
					);?>
					<!-- template MenuCity -->
					<?$APPLICATION->ShowViewContent('phoneFooter1280');?>		
				</div>
			</div>
			<div class="bottom_footer">
				<div class="container">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include", 
						".default", 
						array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_TEMPLATE_PATH."/include/social.php",
							"EDIT_TEMPLATE" => ""
						),
						false
					);?>
					
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include", 
						".default", 
						array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_TEMPLATE_PATH."/include/footer_menu.php",
							"EDIT_TEMPLATE" => ""
						),
						false
					);?>

	<!-- template MenuCity -->
	<?$APPLICATION->ShowViewContent('phoneFooter');?>	
				</div>
			</div>
		</footer>
	</div>
	<div class="menu_trigger"></div>
	<div class="sidemenu">
		<h6 class="sidemenu__title">Навигация</h6>
		<?$APPLICATION->IncludeComponent("bitrix:menu", "special_menu_left", Array(
			"CITY" => getCity(),
			"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
			"MAX_LEVEL" => "1",	// Уровень вложенности меню
			"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
			"DELAY" => "N",	// Откладывать выполнение шаблона меню
			"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
			"MENU_CACHE_TYPE" => "A",	// Тип кеширования
			"MENU_CACHE_TIME" => "360000",	// Время кеширования (сек.)
			"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
			"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
			"MENU_THEME" => "site"
			),
			false
		);?>
	</div>
	

<!-- {literal}

	<script type='text/javascript'>
		window['l'+'iv'+'e'+'Tex'] = true,
		window['l'+'ive'+'TexI'+'D'] = 102365,
		window['liveTe'+'x_'+'objec'+'t'] = true;
		(function() {
			var t = document['cre'+'ateEl'+'ement']('script');
			t.type ='text/javascript';
			t.async = true;
			t.src = '//cs15.l'+'iv'+'etex'+'.'+'ru'+'/'+'j'+'s/cl'+'ie'+'nt.'+'js';
			var c = document['getEl'+'ement'+'sByTagName']('script')[0];
			if ( c ) c['p'+'ar'+'entN'+'o'+'d'+'e']['ins'+'er'+'tBef'+'o'+'re'](t, c);
			else document['d'+'ocument'+'Ele'+'me'+'nt']['fir'+'s'+'tCh'+'i'+'ld']['appen'+'dC'+'hild'](t);
		})();
	</script>

 {/literal} -->


	<!-- {literal} -->
	<script type='text/javascript'>

	window['l'+'iv'+'e'+'Tex'] = true,
	window['l'+'ive'+'TexI'+'D'] = 83585,
	window['liveTe'+'x_'+'objec'+'t'] = true;
	(function() {
		var t = document['cre'+'ateEl'+'ement']('script');
		t.type ='text/javascript';
		t.async = true;
		t.src = '//cs15.l'+'iv'+'etex'+'.'+'ru'+'/'+'j'+'s/cl'+'ie'+'nt.'+'js';
		var c = document['getEl'+'ement'+'sByTagName']('script')[0];
		if ( c ) c['p'+'ar'+'entN'+'o'+'d'+'e']['ins'+'er'+'tBef'+'o'+'re'](t, c);
		else document['d'+'ocument'+'Ele'+'me'+'nt']['fir'+'s'+'tCh'+'i'+'ld']['appen'+'dC'+'hild'](t);
	})();
	</script>
	
	<!-- RedHelper -->
<?/*<script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async" 
	src="https://web.redhelper.ru/service/main.js?c=mrdenis73">
</script>*/?> 
<!--/Redhelper -->
	<!-- {/literal} -->
 
	<!--<div class="chat">
		<div class="chat__box">
			<div class="chat__admin">
				<div class="chat__admin_ava">
					<img src="<?//=SITE_TEMPLATE_PATH?>/pics/chat_admin.jpg" alt="image_description">
				</div>
				<div class="chat__admin_info">
					<span class="chat__admin_name">Наталья Верховская</span>
					<span class="chat__admin_pos">Специалист отдела технической поддержки</span>
				</div>
			</div>
			<form action="#">
				<fieldset>
					<div class="chat__dialog_scroll">
						<div class="chat__dialog_holder">
							<div class="chat__dialog">
								<div class="chat__dialog_ava">
									<img src="<?//=SITE_TEMPLATE_PATH?>/pics/chat_admin.jpg" alt="image_description">
								</div>
								<div class="chat__dialog_msg">
									<span>Здравствуйте! Я готова ответить на все ваши вопросы! введите сообщение в поле ниже.</span>
								</div>
								<div class="chat__dialog_time">14:53</div>
							</div>
							<div class="chat__dialog">
								<div class="chat__dialog_ava">
									<img src="<?//=SITE_TEMPLATE_PATH?>/pics/chat_user.jpg" alt="image_description">
								</div>
								<div class="chat__dialog_msg">
									<span>Здравствуйте, наталья! не могли бы вы подсказать, как настроить роутер Motorolla? Компьютер его видит, авторизация проходит, но доступ в интернет не появляется.</span>
								</div>
								<div class="chat__dialog_time">14:55</div>
							</div>
							<div class="chat__dialog">
								<div class="chat__dialog_ava">
									<img src="<?//=SITE_TEMPLATE_PATH?>/pics/chat_admin.jpg" alt="image_description">
								</div>
								<div class="chat__dialog_msg">
									<span>Даже не знаю. Извините, я в роутерах не разбираюсь. Попробуйте выключить и включить компьютер!</span>
								</div>
								<div class="chat__dialog_time">14:57</div>
							</div>
						</div>
					</div>
					<div class="chat__dialog_field_box">
						<input type="text" class="chat__dialog_field" placeholder="Введите сообщение...">
						<input type="submit" value="" class="chat__dialog_submit">
					</div>
				</fieldset>
			</form>
		</div>
		<div class="chat__trigger"></div>
	</div>-->

	<?if (empty($_COOKIE['City'])) {?>
	
	<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"city", 
	array(
		"CITY_ID" => $cityId, //для правильной работы кеша по городам
		"IBLOCK_TYPE" => "city",
		"IBLOCK_ID" => "8",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "NAME",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PARENT_SECTION" => ""
	),
	false
);?>

	<?}?>	
	<div class="hidden_popups">
		<a id="prev-demo" href="#"></a>
		<div class="popup popup_balance" id="j-popup_balance">
			<div class="close_popup"></div>
			<div class="pay_online">
				<h2 class="popup_balance__title">Проверить баланс</h2>
				<script type="text/javascript">
					$(function() {
						$("#j-check-balance_footer").validate({
							rules: {
								number: {
									required: true,
								},
								surname: {
									required: true,
									minlength: 1,
									maxlength: 30
								}
							},
							errorPlacement: function(error, element) {
								error.insertAfter(element);
							}
						});
						
						$('.pay_online__field--mask').mask('0000000000');

						var options =  { 
							'translation': {A: {pattern: /[А-Я а-яёЁ]/}},
						  onKeyPress: function(cep, event, currentField, options){
							cep = cep.slice(0, 1).toUpperCase() + cep.slice(1);
							$(currentField).val(cep);
						  }
						};

						$('.pay_online__field--surname').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', options);
					});
				</script>
				<form id = "j-check-balance_footer" class = "check-balance" action="#">
					<fieldset>
						<div class="popup_balance__row">
							<input name="number" type="tel"  pattern="\d*" class="pay_online__field pay_online__field--mask" minlength="1" maxlength="10">
							<label class="pay_online__label">Номер договора</label>
						</div>
						<div class="popup_balance__row">
							<input name = "surname" type="text" class="pay_online__field" class="pay_online__field pay_online__field--surname" minlength="1" maxlength="30" autocorrect="off" autocapitalize="words">
							<label class="pay_online__label">Ваша фамилия</label>
						</div>
						<button class="button--big pay_online__btn">Проверить</button>
					</fieldset>
				</form>
			</div>
		</div>
		
		<!-- page tv-->
		<?if ($isTV) {?>
		<div class="popup popup_demo" id="j-popup_demo">
			<div class="close_popup_demo"></div>
			<div class="popup_demo-slider_prev"><span class="slider_buttons-arrow"><</span> Назад</div>
			<div class="popup_demo-slider_next">Вперед <span class="slider_buttons-arrow">></span></div>
			<div id="pagenumber"><span>1</span> из 2</div>
			<div class="popup_demo-slider">
				<div>
					<div class="popup_demo-head">Тип подключения</div>
					<div class="popup_demo-info">Вы можете просмотреть наполнение каждого из тарифных планов после выбора желаемого типа подключения.</div>
					<img src="https://dl.dropboxusercontent.com/u/15331997/%D0%90%D0%BB%D0%BC%D0%B0%D1%82%D0%B2/40/tutor_1_1.gif">
				</div>
				<div>
					<div class="popup_demo-head">Категории и телеканалы</div>
					<div class="popup_demo-info">При желании узнать наполнение тарифного плана в каждой из категорий, нажмите на соответствующую ячейку.</div>
					<img src="https://dl.dropboxusercontent.com/u/15331997/%D0%90%D0%BB%D0%BC%D0%B0%D1%82%D0%B2/40/tutor_2_2.gif">
				</div>
			</div>
		</div>
		<?}?>
		<div class="popup popup_channels" id="j-popup_channels">
			<div class = "close_popup"></div>
			<div class = "content_channels-modal">
				<div class="popup__loader" style="text-align:center; opacity: 0.6; z-index: 1000;">
					<img src="<?=SITE_TEMPLATE_PATH?>/images/loader.gif">
				</div>
			</div>
		</div>
		
		<div class="popup popup_awarded" id="j-popup_awarded">
			<div class="close_popup"></div>
		</div>
		
		<div class="popup popup_import" id="j-popup_import">
			<div class="close_popup"></div>
		</div>
		
		<div class="popup popup_pay" id="j-popup_pay">
			<div class="close_popup"></div>
		</div>
		
		<div class="popup popup_dev" id="j-popup_dev">
			<div class="close_popup"></div>
			<div class="popup_dev__cont">
				<div class="popup_dev__img">
					<div class="popup_dev__anim_box">
						<div class="popup_dev__anim_cog_01"></div>
						<div class="popup_dev__anim_cog_02"></div>
						<div class="popup_dev__anim_cog_03"></div>
						<div class="popup_dev__anim_cog_04"></div>
						<div class="popup_dev__anim_cog_05"></div>
						<div class="popup_dev__anim_cog_mini_01"></div>
						<div class="popup_dev__anim_cog_mini_02"></div>
					</div>
				</div>
				<h5 class="popup_dev__title">Компонент находится в разработке</h5>
				<span class="popup_dev__text">Мы активно ведем работы над сайтом и скоро весь <br> функционал будет доступен!<br>Обновления веб-сайта <a href="http://almatv.kz">almatv.kz</a> публикуются <a target = "_blank" href="https://medium.com/@almatv">здесь</a>.</span>
			</div>
		</div>
		
		<div class="popup check_balance" id="j-check_balance">
			<div class="close_popup"></div>
		</div>

		<?//if ($isTV) {?>
		<div class="popup popup_programm" id="j-popup_programm">
			<div class="close_popup"></div>
			<div class="telecast"></div>
		</div>
		<?//}?>
	</div>

	<a href="#" class="scrollup" style="display: inline;">Scroll</a>
	
<?/*	<!-- RedHelper -->
<script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async" src="https://web.redhelper.ru/service/main.js?c=mrdenis73p"></script>
<!-- /RedHelper -->*/?>
	
	<!-- RedHelper -->
<!--<script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async" 
	src="https://web.redhelper.ru/service/main.js?c=denis73">
</script> -->

<style>
	/*#rh-topPanel
	{
		display: none;
	}*/
</style> 
<?/*
<script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async" 
	src="//web.redhelper.ru/service/main.js?c=denis73" data-settings="bitrix/templates/almatv/redhelper/custom.js">
</script>
*/?>
<!--/Redhelper -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	 
	  ga('create', 'UA-68480564-1', 'auto');
	  ga('send', 'pageview');
	 
	</script>
	
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter23779249 = new Ya.Metrika({
						id:23779249,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						webvisor:true
					});
				} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = "https://mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/23779249" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
</body>
</html>