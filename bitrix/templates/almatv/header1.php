<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE HTML>
<html>
<head>
<?$APPLICATION->ShowHead();?>
<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css">
<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/media.css">
<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.mCustomScrollbar.css">
<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.formstyler.css">
<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.arcticmodal-0.3.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.1.min.js"><\/script>')</script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mCustomScrollbar.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mousewheel-3.0.6.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.carouFredSel-6.2.1.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.formstyler.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.arcticmodal-0.3.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/modernizr-touch.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
<!--[if lt IE 9]>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/html5shiv.js"></script>
<![endif]-->
<title><?$APPLICATION->ShowTitle()?></title>

</head>

<body>
<div id="panel"><?$APPLICATION->ShowPanel()?></div>
	<div class="wrapper">
		<div class="head_search">
			<form action="#">
				<fieldset>
					<div class="head_search__box">
						<input type="text" placeholder="Поиск по сайту" class="head_search__field">
						<button type="submit" class="head_search__submit"></button>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="head_location">
			<div class="head_location__box">
				<?$APPLICATION->IncludeComponent("bitrix:news.list", "MenuCity", Array(
	"IBLOCK_TYPE" => "news",	// Тип информационного блока (используется только для проверки)
		"IBLOCK_ID" => "8",	// Код информационного блока
		"NEWS_COUNT" => "50",	// Количество новостей на странице
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"FILTER_NAME" => "",	// Фильтр
		"FIELD_CODE" => array(	// Поля
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
			2 => "",
			3 => "",
		),
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
		"SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
		"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
		"SET_STATUS_404" => "Y",	// Устанавливать статус 404, если не найдены элемент или раздел
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
		"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
		"PAGER_TITLE" => "",	// Название категорий
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"PARENT_SECTION" => "",	// ID раздела
	),
	false
);?>
				<div class="head_location__close">Закрыть</div>
			</div>
		</div>
		<header class="header">
			<div class="container">
				<div class="header_location_trigger_box">
					<span class="header_location_trigger"><?if (!empty($_COOKIE['CityName'])) { echo $_COOKIE['CityName']; } else { echo 'Алматы'; }?></span>
				</div>
				<div class="centerd_header_box">
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu", 
						"top", 
						array(
							"ROOT_MENU_TYPE" => "top",
							"MAX_LEVEL" => "1",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MENU_THEME" => "site"
						),
						false
					);?>
					<div class="head_search_trigger"></div>
				</div>
				<ul class="lang_switcher">
					<li class="lang_switcher__item"><a class="lang_switcher__link" href="#">kz</a></li>
					<li class="lang_switcher__item"><a class="lang_switcher__link active" href="#">ru</a></li>
					<li class="lang_switcher__item"><a class="lang_switcher__link" href="#">en</a></li>
				</ul>
			</div>
		</header>
		
		<div class="main container" role="main">
			<div class="menu_trigger_holder">
				
			</div>
			<div class="secondary_menu_holder">
				<div class="secondary_menu_box clear">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include", 
						".default", 
						array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_TEMPLATE_PATH."/include/logo_header.php",
							"EDIT_TEMPLATE" => ""
						),
						false
					);?>
					<ul class="main_cats clear">
						<li class="main_cats__item"><a href="#" class="main_cats__link main_cats__link--tv">Телевидение</a></li>
						<li class="main_cats__item"><a href="#" class="main_cats__link main_cats__link--inet">Интернет</a></li>
						<li class="main_cats__item"><a href="#" class="main_cats__link main_cats__link--deal">Для бизнеса</a></li>
					</ul>
					<div class="secondary_additional clear">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include", 
							".default", 
							array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_TEMPLATE_PATH."/include/support.php",
								"EDIT_TEMPLATE" => ""
							),
							false
						);?>
						<a href="#" class="personal_cab_link">Личный кабинет</a>
					</div>
				</div>