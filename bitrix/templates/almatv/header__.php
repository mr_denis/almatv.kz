<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Главная</title>
	<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css">
	<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/media.css">
	<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.mCustomScrollbar.css">
	<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.formstyler.css">
	<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.arcticmodal-0.3.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.11.1.min.js"><\/script>')</script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mCustomScrollbar.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mousewheel-3.0.6.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.carouFredSel-6.2.1.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.formstyler.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.placeholder.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.arcticmodal-0.3.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/modernizr-touch.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
	<!--[if lt IE 9]>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/html5shiv.js"></script>
	<![endif]-->
</head>

<body>
	<div class="wrapper">
		<div class="head_search">
			<form action="#">
				<fieldset>
					<div class="head_search__box">
						<input type="text" placeholder="Поиск по сайту" class="head_search__field">
						<button type="submit" class="head_search__submit"></button>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="head_location">
			<div class="head_location__box">
				<form action="#">
					<fieldset>
						<span class="head_location__title">Выберите свой город</span>
						<ul class="head_location__list">
							<li class="head_location__list_item">
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Аксу</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Актау</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Актобе</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Алматы</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Астана</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Атырау</label>
							</li>
							<li class="head_location__list_item">
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Зыряновск</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Караганда</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Кокшетау</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Костанай</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Павлодар</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Семей</label>
							</li>
							<li class="head_location__list_item">
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Талдыкорган</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Тараз</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Уральск</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Усть-Каменогорск</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Шымкент</label>
								<label class="head_location__radio"><input type="radio" name="location_city" class="location_city">Экибастуз</label>
							</li>
						</ul>
					</fieldset>
				</form>
				<div class="head_location__close">Закрыть</div>
			</div>
		</div>
		<header class="header">
			<div class="container">
				<div class="header_location_trigger_box">
					<span class="header_location_trigger">Алматы</span>
				</div>
				<div class="centerd_header_box">
					<ul class="main_nav">
						<li class="main_nav__item"><a class="main_nav__link" href="#">Вакансии</a></li>
						<li class="main_nav__item"><a class="main_nav__link" href="#">О компании</a></li>
						<li class="main_nav__item"><a class="main_nav__link" href="zayavka_podkl.html">Как подключиться</a></li>
						<li class="main_nav__item"><a class="main_nav__link" href="tv_programm.html">ТВ программа</a></li>
						<li class="main_nav__item main_nav__item--hidden_drop">
							<a class="main_nav__link" href="#">Ещё</a>
							<ul class="main_nav__inner_ul">
								<li class="main_nav__inner_item"><a class="main_nav__inner_link" href="#">Заявка на ремонт</a></li>
								<li class="main_nav__inner_item"><a class="main_nav__inner_link" href="#">Вопросы и ответы</a></li>
							</ul>
						</li>
						<li class="main_nav__item main_nav__item--hidden"><a class="main_nav__link" href="zayavka_remont.html">Заявка на ремонт</a></li>
						<li class="main_nav__item main_nav__item--hidden"><a class="main_nav__link" href="#">Вопросы и ответы</a></li>
					</ul>
					<div class="head_search_trigger"></div>
				</div>
				<ul class="lang_switcher">
					<li class="lang_switcher__item"><a class="lang_switcher__link" href="#">kz</a></li>
					<li class="lang_switcher__item"><a class="lang_switcher__link active" href="#">ru</a></li>
					<li class="lang_switcher__item"><a class="lang_switcher__link" href="#">en</a></li>
				</ul>
			</div>
		</header>
		<div class="main container" role="main">
			<div class="menu_trigger_holder">
				
			</div>
			<div class="secondary_menu_holder">
				<div class="secondary_menu_box clear">
					<a href="home.html" class="logo"></a>
					<ul class="main_cats clear">
						<li class="main_cats__item">
							<a href="#" class="main_cats__link main_cats__link--tv">Телевидение</a>
						</li>
						<li class="main_cats__item">
							<a href="#" class="main_cats__link main_cats__link--inet">Интернет</a>
							<ul class="main_cats__drop">
								<li class="main_cats__drop_item"><a href="#" class="main_cats__drop_link">Интернет</a></li>
								<li class="main_cats__drop_item"><a href="#" class="main_cats__drop_link">Интернет+ТВ</a></li>
								<li class="main_cats__drop_item"><a href="#" class="main_cats__drop_link">Статический IP</a></li>
								<li class="main_cats__drop_item"><a href="#" class="main_cats__drop_link">Условия подключения</a></li>
							</ul>
						</li>
						<li class="main_cats__item">
							<a href="#" class="main_cats__link main_cats__link--deal">Для бизнеса</a>
						</li>
					</ul>
					<div class="secondary_additional clear">
						<div class="support_phone">
							<span class="support_phone__tel"><span>8 (727)</span> 3000 501</span>
							<span class="support_phone__link">Служба поддержки</span>
						</div>
						<a href="#" class="personal_cab_link">Личный кабинет</a>
					</div>
				</div>
				<div class="slider_and_menu_holder clear">
					<ul class="special_menu">
						<li class="special_menu__item">
							<a href="zayavka_podkl.html" class="special_menu__link special_menu__link--connect">Заявка на подключение</a>
						</li>
						<li class="special_menu__item">
							<a href="zayavka_remont.html" class="special_menu__link special_menu__link--fix">Заявка на ремонт</a>
						</li>
						<li class="special_menu__item">
							<a href="tv_programm.html" class="special_menu__link special_menu__link--tv_prog">Телевизионная программа</a>
						</li>
						<li class="special_menu__item">
							<a href="#" class="fancybox special_menu__link special_menu__link--balance j-special_menu__link--balance">Проверить баланс</a>
						</li>
						<li class="special_menu__item">
							<a href="instruct_drivers.html" class="special_menu__link special_menu__link--instuction">Инструкции и драйверы</a>
						</li>
					</ul>
					<div class="main_slider_holder">
						<div class="main_slider" id="j-main_slider">
							<div class="main_slider__item">
								<img class="main_slider__img" src="pics/slider_pic_01.jpg" alt="image_description">
								<div class="main_slider__text">
									<h5 class="main_slider__title">Бесплатное подключение </h5>
									<span class="main_slider__desc">цифрового телевидения в Алматы</span>
									<div class="timer">
										<div class="timer__item">
											<div class="timer__num">
												<span>15</span>
											</div>
											<span class="timer__deep">дней</span>
										</div>
										<div class="timer__item">
											<div class="timer__num">
												<span>6</span>
											</div>
											<span class="timer__deep">часов</span>
										</div>
										<div class="timer__item">
											<div class="timer__num">
												<span>23</span>
											</div>
											<span class="timer__deep">минуты</span>
										</div>
									</div>
								</div>
							</div>
							<div class="main_slider__item">
								<img class="main_slider__img" src="pics/slider_pic_01.jpg" alt="image_description">
								<div class="main_slider__text">
									<h5 class="main_slider__title">Бесплатное подключение </h5>
									<span class="main_slider__desc">цифрового телевидения в Алматы</span>
									<div class="timer">
										<div class="timer__item">
											<div class="timer__num">
												<span>15</span>
											</div>
											<span class="timer__deep">дней</span>
										</div>
										<div class="timer__item">
											<div class="timer__num">
												<span>6</span>
											</div>
											<span class="timer__deep">часов</span>
										</div>
										<div class="timer__item">
											<div class="timer__num">
												<span>23</span>
											</div>
											<span class="timer__deep">минуты</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="main_slider_left" id="j-main_slider_left"></div>
						<div class="main_slider_right" id="j-main_slider_right"></div>
						<div class="main_slider__paging" id="j-main_slider__paging"></div>
					</div>
					<div class="box box--dotted box--pay_top">
						<div class="pay_online">
							<h2 class="pay_online__title">Оплачивайте услуги Алма-ТВ онлайн</h2>
							<form action="#">
								<fieldset>
									<div class="pay_online__row clear">
										<div class="pay_online__col_left">
											<input type="text" class="pay_online__field">
											<label class="pay_online__label">Номер договора</label>
										</div>
										<div class="pay_online__col_right">
											<input type="text" class="pay_online__field">
											<label class="pay_online__label">Сумма к пополнению</label>
										</div>
									</div>
									<div class="pay_online__row pay_online__row--m0 clear">
										<div class="pay_online__col_left">
											<button class="pay_online__btn">Оплатить</button>
										</div>
										<div class="pay_online__col_right">
											<ul class="pay_online__way">
												<li class="pay_online__way_item"><a href="#"><img src="images/visa.png" height="23" width="55" alt="image_description"></a></li>
												<li class="pay_online__way_item"><a href="#"><img src="images/master_card.png" height="23" width="66" alt="image_description"></a></li>
												<li class="pay_online__way_item pay_online__way_item--kassa"><a href="#"><img src="images/pay_kassa24.png" height="21" width="27" alt="image_description"></a></li>
												<li class="pay_online__way_item"><a href="#"><img src="images/pay_qiwi.png" height="27" width="27" alt="image_description"></a></li>
											</ul>
										</div>
									</div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
				<div class="ususal_box ususal_box--hide1280">
					<div class="box box--dotted box--pay">
						<div class="pay_online">
							<h2 class="pay_online__title">Оплачивайте услуги Алма-ТВ онлайн</h2>
							<form action="#">
								<fieldset>
									<div class="pay_online__row clear">
										<div class="pay_online__col_left">
											<input type="text" class="pay_online__field">
											<label class="pay_online__label">Номер договора</label>
										</div>
										<div class="pay_online__col_right">
											<input type="text" class="pay_online__field">
											<label class="pay_online__label">Сумма к пополнению</label>
										</div>
									</div>
									<div class="pay_online__row pay_online__row--m0 clear">
										<div class="pay_online__col_left">
											<button>Оплатить</button>
										</div>
										<div class="pay_online__col_right">
											<ul class="pay_online__way">
												<li class="pay_online__way_item"><a href="#"><img src="images/visa.png" height="23" width="55" alt="image_description"></a></li>
												<li class="pay_online__way_item"><a href="#"><img src="images/master_card.png" height="23" width="66" alt="image_description"></a></li>
												<li class="pay_online__way_item"><a href="#"><img src="images/pay_kassa24.png" height="21" width="27" alt="image_description"></a></li>
												<li class="pay_online__way_item"><a href="#"><img src="images/pay_qiwi.png" height="27" width="27" alt="image_description"></a></li>
											</ul>
										</div>
									</div>
								</fieldset>
							</form>
						</div>
					</div>
					<div class="banner_box">
						<a href="#">
							<img src="images/ntv_banner.jpg" height="241" width="307" alt="image_description">
						</a>
					</div>
				</div>
				<div class="ususal_box">
					<div class="box">
						<div class="why_we clear">
							<h2 class="why_we__title">Почему выбирают нас?</h2>
							<div class="why_we__list">
								<div class="why_we__list_item why_we__list_item--time">
									<span class="why_we__list_link">Менее 30 минут  на подключение</span>
								</div>
								<div class="why_we__list_item why_we__list_item--age why_we__list_item--show1028">
									<span class="why_we__list_link">20 лет успешной работы в 18 городах Казахстана</span>
								</div>
								<div class="why_we__list_item why_we__list_item--package">
									<span class="why_we__list_link">Пакеты рейтинговых каналов HDTV и 3D</span>
								</div>
								<div class="why_we__list_item why_we__list_item--100 why_we__list_item--show1600">
									<span class="why_we__list_link">100 каналов для вас и ваших друзей</span>
								</div>
								<div class="why_we__list_item why_we__list_item--awarded">
									<span class="why_we__list_link">АЛМА ТВ — выбор года 2006 — 2014 г.</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="ususal_box">
					<div class="box box--package_tv">
						<div class="package_intro">
							<h3 class="package_intro__title package_intro__title--tv">Пакеты телевидения</h3>
							<table class="package_intro__tab">
								<tr>
									<td>
										<h5 class="package_intro__pack_name package_intro__pack_name--yel package_intro__pack_name--recomm">Браво</h5>
									</td>
									<td>
										<span class="package_intro__pack_info"><span>78</span> каналов</span>
									</td>
									<td>
										<span class="package_intro__pack_info"><span>1 250</span> тг./мес.</span>
									</td>
								</tr>
								<tr>
									<td>
										<h5 class="package_intro__pack_name package_intro__pack_name--orange_l">Стандарт</h5>
									</td>
									<td>
										<span class="package_intro__pack_info"><span>101</span> канал</span>
									</td>
									<td>
										<span class="package_intro__pack_info"><span>1 800</span> тг./мес.</span>
									</td>
								</tr>
								<tr>
									<td>
										<h5 class="package_intro__pack_name package_intro__pack_name--orange">Престиж</h5>
									</td>
									<td>
										<span class="package_intro__pack_info"><span>121</span> канал</span>
									</td>
									<td>
										<span class="package_intro__pack_info"><span>2 650</span> тг./мес.</span>
									</td>
								</tr>
							</table>
							<div class="clear">
								<a href="#" class="package_intro__all package_intro__all--green">Посмотреть все пакеты</a>
								<a href="#" class="button package_intro__btn">Подключиться</a>
							</div>
						</div>
					</div>
					<div class="box box--package_int">
						<div class="package_intro">
							<h3 class="package_intro__title package_intro__title--int">Пакеты интернета</h3>
							<table class="package_intro__tab">
								<tr>
									<td>
										<h5 class="package_intro__pack_name package_intro__pack_name--blue package_intro__pack_name--recomm">Браво</h5>
									</td>
									<td>
										<span class="package_intro__pack_info"><span>10</span> Мбит/с</span>
									</td>
									<td>
										<span class="package_intro__pack_info"><span>1 250</span> тг./мес.</span>
									</td>
								</tr>
								<tr>
									<td>
										<h5 class="package_intro__pack_name package_intro__pack_name--blue_m">Стандарт</h5>
									</td>
									<td>
										<span class="package_intro__pack_info"><span>30</span> Мбит/с</span>
									</td>
									<td>
										<span class="package_intro__pack_info"><span>1 800</span> тг./мес.</span>
									</td>
								</tr>
								<tr>
									<td>
										<h5 class="package_intro__pack_name package_intro__pack_name--blue_h">Престиж</h5>
									</td>
									<td>
										<span class="package_intro__pack_info"><span>100</span> Мбит/с</span>
									</td>
									<td>
										<span class="package_intro__pack_info"><span>2 650</span> тг./мес.</span>
									</td>
								</tr>
							</table>
							<div class="clear">
								<a href="#" class="package_intro__all package_intro__all--purple">Посмотреть все пакеты</a>
								<a href="#" class="button package_intro__btn">Подключиться</a>
							</div>
						</div>
					</div>
					<div class="banner_box banner_box--slim">
						<a href="#">
							<img src="images/ntv_banner_slim.jpg" height="393" width="204" alt="image_description">
						</a>
					</div>
					<div class="banner_box banner_box--double">
						<a href="#" class="inner_banner">
							<img src="images/ntv_banner_1600.jpg" height="210" width="353" alt="image_description">
						</a>
						<a href="#">
							<img src="images/skidka_banner.jpg" height="210" width="353" alt="image_description">
						</a>
					</div>
				</div>
				<div class="ususal_box">
					<div class="box box--no_pad box--no_border">
						<div class="news">
							<h3 class="news__title">Новости Алма ТВ</h3>
							<div class="news__list_holder">
								<ul class="news__list">
									<li class="news__list_item">
										<h5 class="news__list_item_title"><a class="news__title_link" href="one_news.html">Скидки на пакетные решения Интернет + ТВ</a></h5>
										<span class="news__list_item_date">15 января 2015</span>
										<span class="news__list_item_link">Алма ТВ предлагает вам скидки в новом году. На все пакетные цена! </span>
									</li>
									<li class="news__list_item">
										<h5 class="news__list_item_title"><a class="news__title_link" href="one_news.html">Социальная программа для пенсионеров и ветеранов ВОВ</a></h5>
										<span class="news__list_item_date">15 января 2015</span>
										<span class="news__list_item_link">Мы предлагаем льготные предложения по подключению интернета для пенсионеров!</span>
									</li>
									<li class="news__list_item">
										<h5 class="news__list_item_title"><a class="news__title_link" href="one_news.html">Уезжаете в отпуск? Заморозьте свой баланс!</a></h5>
										<span class="news__list_item_date">15 января 2015</span>
										<span class="news__list_item_link">Вы можете заморозить свой счёт, если уезжаете в отпуск время отпуска средства не будут списываться!</span>
									</li>
									<li class="news__list_item news__list_item--show1280">
										<h5 class="news__list_item_title"><a class="news__title_link" href="one_news.html">Расширена услуга родительского контрол</a></h5>
										<span class="news__list_item_date">15 января 2015</span>
										<span class="news__list_item_link">Теперь вы легко можете регулировать контент, который ваши дети в интернете! Узнайте как!</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="ususal_box">
					<div class="box box--rating">
						<div class="rating">
							<div class="rating__title_box clear">
								<h4 class="rating__title">Какой канал вы смотрите чаще других?</h4>
								<span class="rating__desc">Проголосуйте за ваш любимый канал и мы купим его со следующей зарплаты!</span>
							</div>
							<div class="rating__list_holder">
								<ul class="rating__list">
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/tnt_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">ТНТ</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/sts_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">СТС</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/ort_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">Первый канал</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/discovery_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">Discovery</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/k_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">K1</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/e_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">Россия HD</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/astana_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">Астана</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="ususal_box">
					<div class="box box--rating">
						<div class="rating">
							<div class="rating__title_box clear">
								<h4 class="rating__title">Какой канал вы смотрите чаще других?</h4>
								<span class="rating__desc">Проголосуйте за ваш любимый канал и мы купим его со следующей зарплаты!</span>
							</div>
							<div class="rating__list_holder">
								<ul class="rating__list rating__list--wide">
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/tnt_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">ТНТ</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/sts_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">СТС</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/ort_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">Первый канал</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/discovery_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">Discovery</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/k_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">K1</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/e_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">Россия HD</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/astana_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">Астана</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/tnt_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">ТНТ</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/sts_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">СТС</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/ort_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">Первый канал</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/discovery_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">Discovery</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/k_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">K1</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/e_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">Россия HD</span>
									</li>
									<li class="rating__list_item">
										<a class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="pics/astana_pic.jpg" alt="image_description"></a>
										<span class="rating__list_title">Астана</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="ususal_box">
					<div class="box box--rating">
						<div class="rating">
							<div class="rating__title_box clear">
								<h4 class="rating__title">Какой канал вы смотрите чаще других?</h4>
								<span class="rating__desc">Проголосуйте за ваш любимый канал и мы купим его со следующей зарплаты!</span>
							</div>
							<ul class="rating_result">
								<li class="rating_result__item">
									<div class="rating_result__item_col" style="height: 90%">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res02.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>90%</span>
											</div>
											<span class="rating_result__name">ТНТ</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col" style="height: 70%">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res01.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>70%</span>
											</div>
											<span class="rating_result__name">СТС</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col" style="height: 50%">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res03.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>50%</span>
											</div>
											<span class="rating_result__name"> Астана</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col" style="height: 30%">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res04.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>30%</span>
											</div>
											<span class="rating_result__name">Discovery</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col" style="height: 20%">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res05.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>20%</span>
											</div>
											<span class="rating_result__name"> Россия HD</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col" style="height: 10%">
										<div class="rating_result__item_head rating_result__item_head--less20"> <!-- Пишем if если высота меньше 20% и докидываем класс rating_result__item_head--less20 -->
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res06.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>10%</span>
											</div>
											<span class="rating_result__name">K1</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col" style="height: 5%">
										<div class="rating_result__item_head rating_result__item_head--less20">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res07.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>5%</span>
											</div>
											<span class="rating_result__name">Первый канал</span>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="ususal_box">
					<div class="box box--rating">
						<div class="rating">
							<div class="rating__title_box clear">
								<h4 class="rating__title">Какой канал вы смотрите чаще других?</h4>
								<span class="rating__desc">Проголосуйте за ваш любимый канал и мы купим его со следующей зарплаты!</span>
							</div>
							<ul class="rating_result rating_result--wide">
								<li class="rating_result__item">
									<div class="rating_result__item_col">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res02.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>90%</span>
											</div>
											<span class="rating_result__name">ТНТ</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res01.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>70%</span>
											</div>
											<span class="rating_result__name">СТС</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res03.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>50%</span>
											</div>
											<span class="rating_result__name"> Астана</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res04.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>30%</span>
											</div>
											<span class="rating_result__name">Discovery</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res05.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>20%</span>
											</div>
											<span class="rating_result__name"> Россия HD</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col">
										<div class="rating_result__item_head rating_result__item_head--less20"> <!-- Пишем if если высота меньше 20% и докидываем класс rating_result__item_head--less20 -->
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res06.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>10%</span>
											</div>
											<span class="rating_result__name">K1</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col">
										<div class="rating_result__item_head rating_result__item_head--less20">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res07.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>5%</span>
											</div>
											<span class="rating_result__name">Первый канал</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res02.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>90%</span>
											</div>
											<span class="rating_result__name">ТНТ</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res01.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>70%</span>
											</div>
											<span class="rating_result__name">СТС</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res03.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>50%</span>
											</div>
											<span class="rating_result__name"> Астана</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res04.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>30%</span>
											</div>
											<span class="rating_result__name">Discovery</span>
										</div>
									</div>
								</li>
								<li class="rating_result__item">
									<div class="rating_result__item_col">
										<div class="rating_result__item_head">
											<div class="rating_result__item_img">
												<img class="rating__list_item_img" src="pics/vote_res05.jpg" alt="image_description">
											</div>
											<div class="rating_result__item_num">
												<span>20%</span>
											</div>
											<span class="rating_result__name"> Россия HD</span>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="top_footer">
				<div class="container">
					<ul class="social">
						<li class="social__item"><a href="#" class="social__link social__link--fb"></a></li>
						<li class="social__item"><a href="#" class="social__link social__link--tw"></a></li>
						<li class="social__item"><a href="#" class="social__link social__link--vk"></a></li>
						<li class="social__item"><a href="#" class="social__link social__link--mailru"></a></li>
						<li class="social__item"><a href="#" class="social__link social__link--some_net"></a></li>
						<li class="social__item"><a href="#" class="social__link social__link--inst"></a></li>
					</ul>
					<div class="footer_tels">
						<div class="footer_tels__left">
							<span class="footer_tels__num">8 800 080 51 51</span>
							<span class="footer_tels__link">Служба качества</span>
						</div>
						<div class="footer_tels__right">
							<span class="footer_tels__num"><span>8 (727)</span> 3000 501</span>
							<span class="footer_tels__link">Служба поддержки</span>
						</div>
					</div>
				</div>
			</div>
			<div class="bottom_footer">
				<div class="container">
					<div class="big_social_box">
						<ul class="big_social">
							<li class="big_social__item"><a href="#" class="big_social__link big_social__link--fb"></a></li>
							<li class="big_social__item"><a href="#" class="big_social__link big_social__link--tw"></a></li>
							<li class="big_social__item"><a href="#" class="big_social__link big_social__link--vk"></a></li>
							<li class="big_social__item"><a href="#" class="big_social__link big_social__link--mailru"></a></li>
							<li class="big_social__item"><a href="#" class="big_social__link big_social__link--somesoc"></a></li>
							<li class="big_social__item"><a href="#" class="big_social__link big_social__link--inst"></a></li>
						</ul>
					</div>
					<ul class="common_footer_nav">
						<li class="common_footer_nav__item">
							<h6 class="common_footer_nav__title"><a class="common_footer_nav__title_link" href="#">Компания</a></h6>
							<ul class="footer_nav">
								<li class="footer_nav__item"><a href="tenders.html" class="footer_nav__link">Закупки</a></li>
								<li class="footer_nav__item"><a href="press_center.html" class="footer_nav__link">Пресс-центр</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Вакансии</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Контакты</a></li>
							</ul>
						</li>
						<li class="common_footer_nav__item">
							<h6 class="common_footer_nav__title"><a class="common_footer_nav__title_link" href="#">Телевидение</a></h6>
							<ul class="footer_nav">
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Цифровое</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Цифровое в частный дом</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">HDTV</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Аналоговое</a></li>
							</ul>
						</li>
						<li class="common_footer_nav__item">
							<h6 class="common_footer_nav__title"><a class="common_footer_nav__title_link" href="#">Поддержка</a></h6>
							<ul class="footer_nav">
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Вопросы по услугам</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Задать вопрос</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Драйверы</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Инструкции</a></li>
							</ul>
						</li>
						<li class="common_footer_nav__item">
							<h6 class="common_footer_nav__title"><a class="common_footer_nav__title_link" href="#">Клиентам</a></h6>
							<ul class="footer_nav">
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Подключение</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Способы оплаты</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Акции и бонусы</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">ТВ программа</a></li>
							</ul>
						</li>
						<li class="common_footer_nav__item">
							<h6 class="common_footer_nav__title"><a class="common_footer_nav__title_link" href="#">Интернет</a></h6>
							<ul class="footer_nav">
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Тариф GIG</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Тариф ALMANET</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Статические IP-адреса</a></li>
								<li class="footer_nav__item"><a href="#" class="footer_nav__link">Условия</a></li>
							</ul>
						</li>
					</ul>
					<div class="footer_tels footer_tels--onwider">
						<div class="footer_tels__left footer_tels__left--no_float">
							<span class="footer_tels__num">8 800 080 51 51</span>
							<span class="footer_tels__link">Служба качества</span>
						</div>
						<div class="footer_tels__right footer_tels__right--no_float">
							<span class="footer_tels__num"><span>8 (727)</span> 3000 501</span>
							<span class="footer_tels__link">Служба поддержки</span>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<div class="menu_trigger"></div>
	<div class="sidemenu">
		<h6 class="sidemenu__title">Навигация</h6>
		<ul class="special_menu special_menu--side_position">
			<li class="special_menu__item">
				<a href="#" class="special_menu__link special_menu__link--connect">Заявка на подключение</a>
			</li>
			<li class="special_menu__item">
				<a href="#" class="special_menu__link special_menu__link--fix">Заявка на ремонт</a>
			</li>
			<li class="special_menu__item">
				<a href="#" class="special_menu__link special_menu__link--tv_prog">Телевизионная программа</a>
			</li>
			<li class="special_menu__item">
				<a href="#" class="special_menu__link special_menu__link--balance j-special_menu__link--balance">Проверить баланс</a>
			</li>
			<li class="special_menu__item">
				<a href="#" class="special_menu__link special_menu__link--instuction">Инструкции и драйверы</a>
			</li>
		</ul>
	</div>
	<div class="chat">
		<div class="chat__box">
			<div class="chat__admin">
				<div class="chat__admin_ava">
					<img src="pics/chat_admin.jpg" alt="image_description">
				</div>
				<div class="chat__admin_info">
					<span class="chat__admin_name">Наталья Верховская</span>
					<span class="chat__admin_pos">Специалист отдела технической поддержки</span>
				</div>
			</div>
			<form action="#">
				<fieldset>
					<div class="chat__dialog_scroll">
						<div class="chat__dialog_holder">
							<div class="chat__dialog">
								<div class="chat__dialog_ava">
									<img src="pics/chat_admin.jpg" alt="image_description">
								</div>
								<div class="chat__dialog_msg">
									<span>Здравствуйте! Я готова ответить на все ваши вопросы! введите сообщение в поле ниже.</span>
								</div>
								<div class="chat__dialog_time">14:53</div>
							</div>
							<div class="chat__dialog">
								<div class="chat__dialog_ava">
									<img src="pics/chat_user.jpg" alt="image_description">
								</div>
								<div class="chat__dialog_msg">
									<span>Здравствуйте, наталья! не могли бы вы подсказать, как настроить роутер Motorolla? Компьютер его видит, авторизация проходит, но доступ в интернет не появляется.</span>
								</div>
								<div class="chat__dialog_time">14:55</div>
							</div>
							<div class="chat__dialog">
								<div class="chat__dialog_ava">
									<img src="pics/chat_admin.jpg" alt="image_description">
								</div>
								<div class="chat__dialog_msg">
									<span>Даже не знаю. Извините, я в роутерах не разбираюсь. Попробуйте выключить и включить компьютер!</span>
								</div>
								<div class="chat__dialog_time">14:57</div>
							</div>
						</div>
					</div>
					<div class="chat__dialog_field_box">
						<input type="text" class="chat__dialog_field" placeholder="Введите сообщение...">
						<input type="submit" value="" class="chat__dialog_submit">
					</div>
				</fieldset>
			</form>
		</div>
		<div class="chat__trigger"></div>
	</div>
	<div class="hidden_popups">
		<div class="popup popup_balance" id="j-popup_balance">
			<div class="close_popup"></div>
			<div class="pay_online">
				<h2 class="popup_balance__title">Проверить баланс</h2>
				<form action="#">
					<fieldset>
						<div class="popup_balance__row">
							<input type="text" class="pay_online__field">
							<label class="pay_online__label">Номер договора</label>
						</div>
						<div class="popup_balance__row">
							<input type="text" class="pay_online__field">
							<label class="pay_online__label">Ваша фамилия</label>
						</div>
						<button class="button--big">Проверить</button>
					</fieldset>
				</form>
			</div>
		</div>
		<div class="popup popup_programm" id="j-popup_programm">
			<div class="close_popup"></div>
			<div class="telecast">
				<div class="telecast__slider_box">
					<div class="telecast__time_box">
						<span class="telecast__date">11 Февраля</span>
						<span class="telecast__date telecast__date--time">02:25</span>
					</div>
					<div class="telecast__slider" id="j-telecast__slider">
						<div class="telecast__slider_item">
							<img src="pics/ironman.jpg" alt="image_description">
						</div>
						<div class="telecast__slider_item">
							<img src="pics/ironman.jpg" alt="image_description">
						</div>
					</div>
					<div class="telecast__slider_paging" id="j-telecast__slider_paging"></div>
					<div class="telecast__slider_right" id="j-telecast__slider_right"></div>
					<div class="telecast__slider_left" id="j-telecast__slider_left"></div>
				</div>
				<div class="telecast__content">
					<div class="telecast__li">
						<div class="telecast__title_holder">
							<h4 class="telecast__title">Железный человек 3 <span class="telecast__title_eng">Iron Man 3</span></h4>
							<span class="telecast__title_date">2013</span>
						</div>
						<ul class="telecast__label">
							<!-- <li class="telecast__label_item">
								<a href="#" class="telecast__label_item_link telecast__label_item_link--blue">Боевик</a>
							</li>
							<li class="telecast__label_item">
								<a href="#" class="telecast__label_item_link telecast__label_item_link--purple">Драма</a>
							</li>
							<li class="telecast__label_item">
								<a href="#" class="telecast__label_item_link telecast__label_item_link--yellow">Комедия</a>
							</li> -->
							<li class="telecast__label_item">
								<a href="#" class="telecast__label_item_link telecast__label_item_link--red">Художественный фильм</a>
							</li>
						</ul>
					</div>
					<div class="telecast__li">
						<h5 class="telecast__title_mini">В ролях</h5>
						<span>Роберт Дауни мл., Гвинет Пэлтроу, Дон Чидл, Гай Пирс, Бен Кингсли, Джеймс Бэдж Дэйл, Ребекка Холл  и др.</span>
					</div>
					<div class="telecast__li">
						<div class="telecast__accordion">
							<span>«Какой смысл умирать в палате под стоны и хрип безнадежных больных. Не лучше ли устроить пир на эти двадцать семь тысяч и, приняв яд, переселиться в другой мир под звуки струн, окруженным хмельными красавицами и лихими друзьями?» (Михаил Булгаков «Мастер и Маргарита») <br> «Какой смысл умирать в палате под стоны и хрип безнадежных больных. Не лучше ли устроить пир на эти двадцать семь тысяч и, приняв яд, переселиться в другой мир под звуки струн, окруженным хмельными красавицами и лихими друзьями?» (Михаил Булгаков «Мастер и Маргарита»)</span>
						</div>
						<span class="telecast__accordion_trigger">Показать описание полностью</span>
					</div>
					<div class="telecast__li">
						<h5 class="telecast__title_mini">Ближайшие трансляции</h5>
						<table class="telecast__nearest">
							<thead>
								<tr>
									<td>Дата</td>
									<td>Время</td>
									<td>Название канала</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>11 февраля</td>
									<td>
										<span class="telecast__nearest_time">02:35</span>
									</td>
									<td>
										<div class="clear">
											<div class="telecast__nearest_channel">
												<span class="telecast__nearest_channel_img"><img src="pics/dom_kino.png" height="20" width="19" alt="image_description"></span>
												<span class="telecast__nearest_channel_name">Дом Кино</span>
											</div>
											<div class="telecast__remind_box">
												<div class="telecast__remind_form">
													<form action="#">
														<fieldset>
															<div class="telecast__row">
																<input type="text" class="telecast__field">
																<label class="telecast__label">Ваша эл. почта</label>
															</div>
															<div class="telecast__row telecast__row--lm">
																<input type="text" class="telecast__field">
																<label class="telecast__label">Ваш номер телефона</label>
															</div>
															<button class="button--telecast">Напомнить о начале</button>
														</fieldset>
													</form>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>23 февраля</td>
									<td>
										<span class="telecast__nearest_time">02:35</span>
									</td>
									<td>
										<div class="clear">
											<div class="telecast__nearest_channel">
												<span class="telecast__nearest_channel_img"><img src="pics/ntv_plus.png" height="7" width="20" alt="image_description"></span>
												<span class="telecast__nearest_channel_name">НТВ-ПЛЮС 3D</span>
											</div>
											<div class="telecast__remind_box">
												<div class="telecast__remind_form">
													<form action="#">
														<fieldset>
															<div class="telecast__row">
																<input type="text" class="telecast__field">
																<label class="telecast__label">Ваша эл. почта</label>
															</div>
															<div class="telecast__row telecast__row--lm">
																<input type="text" class="telecast__field">
																<label class="telecast__label">Ваш номер телефона</label>
															</div>
															<button class="button--telecast">Напомнить о начале</button>
														</fieldset>
													</form>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>26 февраля</td>
									<td>
										<span class="telecast__nearest_time">02:35</span>
									</td>
									<td>
										<div class="clear">
											<div class="telecast__nearest_channel">
												<span class="telecast__nearest_channel_img"><img src="pics/perets.png" height="20" width="7" alt="image_description"></span>
												<span class="telecast__nearest_channel_name">Перец +2</span>
											</div>
											<div class="telecast__remind_box">
												<div class="telecast__remind_form">
													<form action="#">
														<fieldset>
															<div class="telecast__row">
																<input type="text" class="telecast__field">
																<label class="telecast__label">Ваша эл. почта</label>
															</div>
															<div class="telecast__row telecast__row--lm">
																<input type="text" class="telecast__field">
																<label class="telecast__label">Ваш номер телефона</label>
															</div>
															<button class="button--telecast">Напомнить о начале</button>
														</fieldset>
													</form>
												</div>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>