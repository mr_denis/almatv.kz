$(function() {

	visibleIt.programItemsVisible();
	$(window).load(function(){
		$('.cat_drop__list--category,.cat_drop__list_inner--channel, .add_packs_box__hidden_scroll, .chat__dialog_scroll').mCustomScrollbar();
		$('#j-main_slider').carouFredSel({
			responsive: true,
			items: {
				visible: 1
			},
			scroll: {
				duration: 800,
				timeoutDuration: 4000,
				fx: 'crossfade'
			},
			pauseOnHover: true,
			auto: false,
			pagination: '#j-main_slider__paging',
			prev:{
				button: "#j-main_slider_left"
			},
			next:{
				button: "#j-main_slider_right"
			}
		});
		$('#j-telecast__slider').carouFredSel({
			items: {
				visible: 1,
				width: 654,
				height: 360
			},
			scroll: {
				duration: 800,
				timeoutDuration: 4000,
				fx: 'crossfade'
			},
			pauseOnHover: true,
			auto: false,
			pagination: '#j-telecast__slider_paging',
			prev:{
				button: "#j-telecast__slider_left"
			},
			next:{
				button: "#j-telecast__slider_right"
			}
		});
		$('#j-tarif_slider_flat').carouFredSel({
			responsive: true,
			scroll: 1,
			height: 'variable',
			items: {
				visible: visibleIt.visibleItems,
				width: 192,
				duration: 200,
				timeoutDuration: 4000,
				fx: 'scroll',
				height: 'variable'
			},
			circular: false,
			infinite: false,
			pauseOnHover: true,
			auto: false,
			prev:{
				button: "#j-tarif__slider_left"
			},
			next:{
				button: "#j-tarif__slider_right"
			}
		});
		/*The same slider for home packages*/
		$('#j-tarif_slider_home').carouFredSel({
			responsive: true,
			scroll: 1,
			height: 'variable',
			items: {
				visible: {
					min: visibleIt.visibleItems,
					max: visibleIt.visibleItems
				},
				width: 192,
				duration: 200,
				timeoutDuration: 4000,
				fx: 'scroll',
				height: 'variable'
			},
			circular: false,
			infinite: false,
			pauseOnHover: true,
			auto: false,
			prev:{
				button: "#j-tarif__slider_left_home"
			},
			next:{
				button: "#j-tarif__slider_right_home"
			}
		});
		$('.tarif_cat__trigger_item').on('click', function(){
			$('#j-tarif_slider_home').trigger('updateSizes'); 
			$('#j-tarif_slider_flat').trigger('updateSizes');
		});

		$('.main').on('click', '.tarif_slider__list_all_channels', function(){
			// < Закрытие разъезжания слайдера
			closePopupChannel();
			// Закрытие разъезжания слайдера >
			// < Разъезжание слайдера
			var popup = $('#j-tarif_popup');
			var popupHeight = popup.outerHeight() - 25;
			var indexRow = $(this).parents('li.tarif_slider__list_li').index();
			$(this).parents('li.tarif_slider__list_li').addClass('active highlight').animate({height: '+='+popupHeight},0);
			$(this).parents('.tarif_slider__item').siblings('.tarif_slider__item').each(function(){
				$(this).find('li').eq(indexRow).addClass('active').animate({height: '+='+popupHeight},0);
			});
			$('.tarif_cat').each(function(){
				$(this).find('li').eq(indexRow).addClass('active').animate({height: '+='+popupHeight},0);
			});
			$('#j-tarif_slider_flat').trigger("updateSizes");
			$('#j-tarif_slider_home').trigger("updateSizes");

			// Разъезжание слайдера >
			var thisTop = $(this).offset().top + 32;
			popup.addClass('active').css({
				top: thisTop
			});
			$('.tarif_slider__num').removeClass('active');
			$(this).addClass('active');
			var body = $("html, body");
			body.animate({scrollTop:thisTop-150}, 700);
			event.stopPropagation();

		});

		$('.main').on('click', '.j-tarif_slider__list_li:not(.tarif_slider__list_li--empty)', function(event){
			var popup = $('#j-tarif_popup');
			// < Разъезжание слайдера
			// < Закрытие разъезжания слайдера
			closePopupChannel();
			// Закрытие разъезжания слайдера >

			var popupHeight = popup.outerHeight() - 6;
			var indexRow = $(this).index();
			console.info(indexRow);
			$(this).addClass('active highlight').animate({height: '+='+popupHeight},0);
			$(this).parents('.tarif_slider__item').siblings('.tarif_slider__item').each(function(){
				$(this).find('li').eq(indexRow).addClass('active').animate({height: '+='+popupHeight},0);
			});
			$('.tarif_cat').each(function(){
				$(this).find('li').eq(indexRow).addClass('active').animate({height: '+='+popupHeight},0);
			});
			$('#j-tarif_slider_flat').trigger("updateSizes");
			$('#j-tarif_slider_home').trigger("updateSizes");

			// Разъезжание слайдера >

			var thisTop = $(this).offset().top + 45;
			popup.addClass('active').css({
				top: thisTop
			});
			$('.tarif_slider__num').removeClass('active');
			$(this).addClass('active');
			var body = $("html, body");
			body.animate({scrollTop:thisTop-150}, 700);
			event.stopPropagation();
		});
		$('#j-tarif_popup__close').on('click', function(){
			$('#j-tarif_popup').removeClass('active');
			$('.tarif_slider__num').removeClass('active');
			// < Закрытие разъезжания слайдера
			closePopupChannel();
			// Закрытие разъезжания слайдера >

		});


	});

	$('.main').on('change', '.pick_channel__checkbox', function(){
		if ($(this).hasClass('checked')){
			$('.'+($(this).data('channel'))).addClass('active');
		}else{
			$('.'+($(this).data('channel'))).removeClass('active');
		};
		$(this).trigger('refresh');
	});

	
	$(window).resize(function(){
		positionTrigger();
		visibleIt.programItemsVisible();
	});
	positionTrigger();

	$('select, input[type="checkbox"], input[type="radio"]').styler({
		selectSearch: true
	});

	$('.tv_accordion__broadcast').on('click', function(){
		$('#j-popup_programm').arcticmodal();
	});
	$('.today_programm__name').on('click', function(){
		$('#j-popup_programm').arcticmodal();
	});

	$('.j-special_menu__link--balance').on('click', function(event){
		event.preventDefault();
		$('#j-popup_balance').arcticmodal();
	});
	$('.close_popup').on('click', function(){
		$.arcticmodal('close');
	});

	$('.tabs_links').jwTabs($('.tabs_content'));
	$('.j-tarif_cat__tabs_head').jwTabs($('.tarif_cat__tabs_cont'), $('.j-add_eq__tabs_head'), $('.add_eq__tabs_cont'));
	$('.j-add_eq__tabs_head').jwTabs($('.add_eq__tabs_cont'), $('.j-tarif_cat__tabs_head'), $('.tarif_cat__tabs_cont'));
	$('#j-single_programm_tabs_head').jwTabs($('#j-single_programm_tabs_body'));

	$('.add_packs_box__count').on('click', function(event){
		event.stopPropagation();
		var el = $(this);
		if (el.hasClass('active')){
			el.removeClass('active');
		}else{
			$('.add_packs_box__count').removeClass('active');
			el.addClass('active');
		};
	});

	$('.get_pack__add').on('click', function(event){
		event.stopPropagation();
		var el = $(this);
		if (el.hasClass('active')){
			el.removeClass('active');
		}else{
			$('.get_pack__add').removeClass('active');
			el.addClass('active');
		};
	});

	$('.menu_trigger').on('click',  function(event){
		event.stopPropagation();
		sidemenuTrigger ();
		$('.chat').removeClass('active');
		$('.chat__trigger').removeClass('active');
	});

	$('.sidemenu').on('click',function(event){
		event.stopPropagation();
	});

	$('.chat__trigger').on('click', function(){
		$('.chat').toggleClass('active');
		$(this).toggleClass('active');
	});

	$('.telecast__accordion_trigger').on('click', function(){
		$(this).toggleClass('active');
		$(this).prev('.telecast__accordion').toggleClass('active');
		if ($(this).hasClass('active')){
			$(this).text('Скрыть описание');
		}else{
			$(this).text('Показать описание полностью');
		};
	});

	$('.telecast__remind_box').on('click', function(){
		$('.telecast__remind_form').removeClass('active');
		$(this).toggleClass('active');
		$(this).children('.telecast__remind_form').toggleClass('active');
	});

	$(document).on('click',function(){
		if ($('.menu_trigger').hasClass('active')) {
			sidemenuTrigger ();
		};
		$('.cat_drop__list').removeClass('active');
		$('.cat_drop__main').removeClass('active');
		$('.add_packs_box__count').removeClass('active');
		$('.get_pack__add').removeClass('active');
		$('.chat').removeClass('active');
		$('.chat__trigger').removeClass('active');
		$('.head_search_trigger').removeClass('active');
		$('.head_search').removeClass('active');
		$('.menu_trigger').removeClass('menu_trigger--search_open');
		$('.chat').removeClass('chat--search_open');
		$('.menu_trigger').removeClass('menu_trigger--location_open');
		$('.chat').removeClass('chat--location_open');
		$('.telecast__remind_box').removeClass('active');
		$('.telecast__remind_form').removeClass('active');
		$('.head_location').removeClass('active');
		$('.header_location_trigger').removeClass('active');
		$('#j-tarif_popup').removeClass('active');
		$('.tarif_slider__num').removeClass('active');
		$('.tarif_popup__list_item--not_available').removeClass('active');

		// < Закрытие разъезжания слайдера
		closePopupChannel();
		// Закрытие разъезжания слайдера >
	});

	$('.head_search_trigger').on('click', function(){
		$('.head_search').toggleClass('active');
		$(this).toggleClass('active');
		$('.menu_trigger').removeClass('menu_trigger--location_open');
		$('.chat').removeClass('chat--location_open');
		$('.menu_trigger').toggleClass('menu_trigger--search_open');
		$('.chat').toggleClass('chat--search_open');
		$('.head_location').removeClass('active');
		$('.header_location_trigger').removeClass('active');
	});

	$('.header_location_trigger, .head_location__close').on('click', function(){
		$('.head_location').toggleClass('active');
		$(this).toggleClass('active');
		$('.menu_trigger').removeClass('menu_trigger--search_open');
		$('.chat').removeClass('chat--search_open');
		$('.menu_trigger').toggleClass('menu_trigger--location_open');
		$('.chat').toggleClass('chat--location_open');
		$('.head_search').removeClass('active');
		$('.head_search_trigger').removeClass('active');
	});
	$('.jq-radio.location_city').on('change', function(){
		if ($(this).hasClass('checked')){
			$('.head_location__radio').removeClass('active');
			$(this).parent('.head_location__radio').addClass('active');
		};
	});


	$('.cat_drop__drop_item').on('click', function(){
		$(this).parents('.cat_drop__list').removeClass('active');
		$(this).parents('.cat_drop__list').siblings('.cat_drop__main').removeClass('active');
	});
	$('.cat_drop__main').on('click', function(event){
		$('.cat_drop__main').removeClass('active');
		$('.cat_drop__list').removeClass('active');
		$(this).next('.cat_drop__list').toggleClass('active');
		$(this).toggleClass('active');
		event.stopPropagation;
	});
	$('.cat_drop, .chat, .head_search_trigger, .head_search, .telecast__remind_form, .telecast__remind_box, .header_location_trigger, .head_location, #j-tarif_popup').on('click',function(event){
		event.stopPropagation();
	});
	
	$('.tv_accordion__trigger').on('click', function(){
		$(this).siblings('.tv_accordion__slide').slideToggle();
		$(this).toggleClass('active');
	});

	$('.j-accordion__trigger').on('click', function(){
		$(this).siblings('.j-accordion__slide').slideToggle();
		$(this).toggleClass('active');
	});
	$('.j-accordion__slide.active').show();

	$('.tarif__name').on('click', function(){
		$(this).parents('.tarif__table').siblings('.tarif__hidden_info').slideToggle();
		$(this).toggleClass('active');
	});
	$('.tarif__hidden_info.active').show();

	$('.turn_all__item--up').on('click', function(){
		$('.tv_accordion__slide').slideUp().removeClass('active');
		$('.tv_accordion__trigger').removeClass('active');
	});
	$('.turn_all__item--down').on('click', function(){
		$('.tv_accordion__slide').slideDown().addClass('active');
		$('.tv_accordion__trigger').addClass('active');
	});
	$('.tv_accordion__slide.active').show();

	$('.get_pack__item').on('click', function(){
		$('.get_pack__item').removeClass('active');
		$(this).addClass('active');
		$('.get_pack_result').val($(this).data('pack'));
	});

	if (Modernizr.touch) {
		/* cache dom references */ 
		var $body = jQuery('body'); 

		/* bind events */
		$(document)
		.on('focus', 'input', function(e) {
		    $body.addClass('fixfixed');
		})
		.on('blur', 'input', function(e) {
		    $body.removeClass('fixfixed');
		});
	};

	$('.j-get_pack--home').addClass('get_pack--hidden');
	$('.j-user_data__radio_input').on('change', function(){
		if ($(this).val() == 'home'){
			$('.get_pack').removeClass('get_pack--hidden');
			$('.j-get_pack--flat').addClass('get_pack--hidden');
		};
		if ($(this).val() == 'flat'){
			$('.get_pack').removeClass('get_pack--hidden');
			$('.j-get_pack--home').addClass('get_pack--hidden');
		};
	});


});

function sidemenuTrigger () {
	$('.sidemenu').toggleClass('active');
	$('.menu_trigger').toggleClass('active');
	$('.wrapper').addClass('transition').toggleClass('opened');
	setTimeout(function(){
		$('.wrapper').removeClass('transition');
	},500);
};

function positionTrigger () {
	var btn = $('.menu_trigger').outerWidth();
	var posLeft = parseInt($('.container').position().left);
	var marginLeft = parseInt($('.container').css('margin-left'));
	var plt = (posLeft > marginLeft) ? posLeft : marginLeft;
	plt = (plt - btn)/2;

	plt = (plt>0) ? plt : 20;
	$('.menu_trigger').css({left:plt});
};

var visibleIt = {
	visibleItems : 3,
	visibleNew : 5,
	programItemsVisible : function(){
		var windowWidth = $(window).width();
		if (windowWidth > 1600) {
			this.visibleNew = 5;
		}else{
			this.visibleNew = 3;
		};
		if (this.visibleItems !== this.visibleNew) {
			this.visibleItems = this.visibleNew;
			$('#j-tarif_slider_flat').trigger('configuration', [
				'items', {
					visible : this.visibleItems
				}
			], true);
			$('#j-tarif_slider_home').trigger('configuration', [
				'items', {
					visible : this.visibleItems
				}
			], true);
		};
	}
};

(function($){
	$.fn.jwTabs = function(tabs, syncElemHead, syncElemBody){
		var backActive = $(this).children('li.active').index() + 1;
		var start = 1;
		if (backActive) {start = backActive;};
		$(this).children('li').eq(start-1).addClass('active');
		tabs.children('li').hide(0);
		tabs.children('li').eq(start-1).show(0);
		$(this).children('li').on('click',function(event){
			$(this).siblings('li').removeClass('active').end().addClass('active');
			tabs.children('li').fadeOut(0).eq($(this).index()).fadeToggle(200);

			if (syncElemHead){
				syncElemHead.children('li').eq($(this).index()).siblings('li').removeClass('active').end().addClass('active');
				syncElemBody.children('li').fadeOut(0).eq($(this).index()).fadeToggle(200);
			};
		});
	};
})(jQuery);

function closePopupChannel(){
	var heightRow = 50;
	$('.tarif_slider__item').each(function(){
		if($(this).find('li.active').index() === 0){
			heightRow = 140;
		};
		$(this).find('li.active').removeClass('active highlight').animate({height: heightRow },0);
	});
	$('.tarif_cat').each(function(){
		$(this).find('li.active').removeClass('active').animate({height: heightRow },0);
	});
	$('#j-tarif_slider_flat').trigger("updateSizes");
	$('#j-tarif_slider_home').trigger("updateSizes");
};