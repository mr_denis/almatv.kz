var app = angular.module('almatv', []);

app.controller('programmFilter',function ($scope) {
	$scope.categoryList = [];
	$scope.init = function(cat, channel){
		$scope.categoryList = cat;
		$scope.channelList = channel;
		$scope.categoryActive = $scope.categoryList[0];
		$scope.channelActive = $scope.channelList[0];
	};
	$scope.changeCat = function (index){
		$scope.categoryActive = $scope.categoryList[index];
	};
	$scope.changeChannel = function (index){
		$scope.channelActive = $scope.channelList[index];
	};
});