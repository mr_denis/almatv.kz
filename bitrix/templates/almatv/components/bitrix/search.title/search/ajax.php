<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script>
	$('.head_search__box__list').mCustomScrollbar();
	
	$('.head_search__field').on('focus',function(){
		$('.head_search__box__list').slideDown(300);
	});
	$('.head_search__field').on('blur',function(){
		$('.head_search__box__list').slideUp(300);
	});

</script>

<div class="head_search__box__list">
	<div class="head_search__box__item">
		<a href="#" class="head_search__box__link">Discovery World</a>
		необъяснимых исчезновений, тайны археологических находок, которые тысячелетиями были недоступны человеческому взору, разгадает каждый зритель Discovery World.
	</div>
	<div class="head_search__box__item">
		<a href="#" class="head_search__box__link">Discovery World Discovery World</a>
	</div>
	<div class="head_search__box__item">
		<a href="#" class="head_search__box__link">Discovery World</a>
	</div>
	<div class="head_search__box__item">
		<a href="#" class="head_search__box__link">Discovery World Discovery World</a>
		необъяснимых исчезновений, тайны археологических находок, которые тысячелетиями были недоступны человеческому взору, разгадает каждый зритель Discovery World.
	</div>
	<div class="head_search__box__item">
		<a href="#" class="head_search__box__link">Discovery World</a>
	</div>
	<div class="head_search__box__item">
		<a href="#" class="head_search__box__link">Discovery World</a>
	</div>
	<div class="head_search__box__item">
		<a href="#" class="head_search__box__link">Discovery World Discovery World</a>
	</div>
	<div class="head_search__box__item">
		<a href="#" class="head_search__box__link">Discovery World</a>
	</div>
	<div class="head_search__box__item">
		<a href="#" class="head_search__box__link">Discovery World</a>
	</div>
	<div class="head_search__box__item">
		<a href="#" class="head_search__box__link">Discovery World Discovery World</a>
	</div>
	<div class="head_search__box__item">
		<a href="#" class="head_search__box__link">Discovery World</a>
	</div>
	<div class="head_search__box__item">
		<a href="#" class="head_search__box__link">Discovery World</a>
	</div>
	
	
	<?if(!empty($arResult["CATEGORIES"])):?>
		<table class="title-search-result">
			<?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
				<tr>
					<th class="title-search-separator">&nbsp;</th>
					<td class="title-search-separator">&nbsp;</td>
				</tr>
				<?foreach($arCategory["ITEMS"] as $i => $arItem):?>
				<tr>
					<?if($i == 0):?>
						<th>&nbsp;<?echo $arCategory["TITLE"]?></th>
					<?else:?>
						<th>&nbsp;</th>
					<?endif?>

					<?if($category_id === "all"):?>
						<td class="title-search-all"><a href="<?echo $arItem["URL"]?>"><?echo $arItem["NAME"]?></td>
					<?elseif(isset($arItem["ICON"])):?>
						<td class="title-search-item"><a href="<?echo $arItem["URL"]?>"><img src="<?echo $arItem["ICON"]?>"><?echo $arItem["NAME"]?></td>
					<?else:?>
						<td class="title-search-more"><a href="<?echo $arItem["URL"]?>"><?echo $arItem["NAME"]?></td>
					<?endif;?>
				</tr>
				<?endforeach;?>
			<?endforeach;?>
			<tr>
				<th class="title-search-separator">&nbsp;</th>
				<td class="title-search-separator">&nbsp;</td>
			</tr>
		</table><div class="title-search-fader"></div>
	<?endif;?>
	
</div>
