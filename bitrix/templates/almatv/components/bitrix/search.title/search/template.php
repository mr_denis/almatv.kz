<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>
<?
$INPUT_ID = trim($arParams["~INPUT_ID"]);
if(strlen($INPUT_ID) <= 0)
	$INPUT_ID = "title-search-input";
$INPUT_ID = CUtil::JSEscape($INPUT_ID);

$CONTAINER_ID = trim($arParams["~CONTAINER_ID"]);
if(strlen($CONTAINER_ID) <= 0)
	$CONTAINER_ID = "title-search";
$CONTAINER_ID = CUtil::JSEscape($CONTAINER_ID);
?>

<?if($arParams["SHOW_INPUT"] !== "N") {?>
<div class="head_search">
	<form action="/search/">
		<fieldset>
			<div class="head_search__box">
				<input type="text" placeholder="Поиск по сайту" autocorrect="off" autocapitalize="words" id="<?echo $INPUT_ID?>" name = "q" class="head_search__field" autocomplete="off" />
				<button type="submit" class="head_search__submit" name="s"></button>
			</div>
			<div id="search_result"> </div>
		</fieldset>
	</form>
</div>
<?}?>

<script>
	BX.ready(function(){
		new JCTitleSearch({
			'AJAX_PAGE' : '<?echo CUtil::JSEscape(POST_FORM_ACTION_URI)?>',
			//'CONTAINER_ID': '<?echo $CONTAINER_ID?>',
			'INPUT_ID': '<?echo $INPUT_ID?>',
			'MIN_QUERY_LEN': 2
		});
	});
</script>