<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

flush();
?>
<?
$prop = $arResult['PROPERTIES'];
//PR($prop);
?>


<?
$isParamsGoogle = false;
if (!empty($prop['M_CENTER']['VALUE']) && !empty($prop['M_ZOOM']['VALUE']) && !empty($prop['M_PLACE_MARK']['VALUE']))
{
	$isParamsGoogle = true;
}

?>

	
<?if ($isParamsGoogle) {?>
<script type="text/javascript">
	  var mapOptions = {
		zoom: <?=$prop['M_ZOOM']['VALUE']?>,
		center: new google.maps.LatLng(<?=$prop['M_CENTER']['VALUE']?>)
	  }
	  var map = new google.maps.Map(document.getElementById('map-canvas'),
									mapOptions);

	  var image = '<?=SITE_TEMPLATE_PATH?>/images/map_label.png';
	  var myLatLng = new google.maps.LatLng(<?=$prop['M_PLACE_MARK']['VALUE']?>);
	  var beachMarker = new google.maps.Marker({
		  position: myLatLng,
		  map: map,
		  icon: image
	  });
</script>
<?}?>

<div class="contact_info">
	<h2 class="contact_info__office"><?=$arResult['NAME']?></h2>
	<span class="contact_info__office_desc"><?=$arResult['PREVIEW_TEXT']?></span>
</div>
<?if(!empty($prop['CALL_CENTER']['VALUE']) || !empty($prop['DEPARTMENT_OF_QUALITY']['VALUE'])){?>
	<div class="contact_info">
		<?if(!empty($prop['CALL_CENTER']['VALUE'])){?>
		<div class="contact_info__col">
			<h4 class="contact_info__mini_title">Call центр</h4>
			<span class="contact_info__phone">
				<a href="tel:<?=$prop['CALL_CENTER']['VALUE']?>"> <?=$prop['CALL_CENTER']['VALUE']?> </a></span> 
				<span class="contact_info__time_desc"><?=$prop['CALL_CENTER_MODE']['VALUE']?> </span>
		</div>
		<?}?>
		<?if(!empty($prop['DEPARTMENT_OF_QUALITY']['VALUE'])){?>
		<div class="contact_info__col contact_info__col--no_margin">
			<h4 class="contact_info__mini_title">Отдел управления качеством услуг</h4>
			<span class="contact_info__phone"><a href="tel:<?=$prop['DEPARTMENT_OF_QUALITY']['VALUE']?>"><?=$prop['DEPARTMENT_OF_QUALITY']['VALUE']?></a></span> 
			<span class="contact_info__time_desc"><?=$prop['DEPARTMENT_OF_QUALITY_MODE']['VALUE']?></span>
		</div>
		<?}?>
		<br>
	</div>
<?}?>
<h4 class="contact_info__mini_title">Режим работы офиса</h4>
<ul class="opening_times">
	<li class="opening_times__item">
		<span class="opening_times__day">пн</span>
		<span class="opening_times__desc"><?=$prop['WEEK_DAYS']['VALUE']?></span>
	</li>
	<li class="opening_times__item">
		<span class="opening_times__day">вт</span>
	</li>
	<li class="opening_times__item">
		<span class="opening_times__day">ср</span>
	</li>
	<li class="opening_times__item">
		<span class="opening_times__day">чт</span>
	</li>
	<li class="opening_times__item">
		<span class="opening_times__day">пт</span>
	</li>
	<li class="opening_times__item opening_times__item--half">
		<span class="opening_times__day">сб</span>
		<span class="opening_times__desc"><?=$prop['SATURDAY']['VALUE']?></span>
	</li>
	<li class="opening_times__item opening_times__item--weekend">
		<span class="opening_times__day">вс</span>
		<span class="opening_times__desc"><?=$prop['SUNDAY']['VALUE']?></span>
	</li>
</ul>
<?if ($isParamsGoogle) {?>
	<div class="map_holder">
		<div id="map-canvas"></div>
	</div>
<?}?>