<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//if(!isset($arParams["CACHE_TIME"]))
//	$arParams["CACHE_TIME"] = 36000000;

/*if($this->StartResultCache())
{
	
}*/

$picture = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"]["ID"],
	array("width" => 85, "height" =>85),
	BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
	true,
	false
);

$arResult["DETAIL_PICTURE"] = $picture;
/*находим все пакеты в городе*/
$arFilter = array('IBLOCK_ID' => IBLOCK_CHANNELS, 'ID' => $arResult['IBLOCK_SECTION_ID'], 'ACTIVE' => 'Y');
$rsSections = CIBlockSection::GetList(array(), $arFilter, false, array("NAME", "UF_*", "PICTURE"));
if ($arSection = $rsSections->Fetch())
{
	$image = CFile::ResizeImageGet(
		$arSection['PICTURE'],
		array("width" => 40, "height" =>40),
		BX_RESIZE_IMAGE_EXACT_ALL,
		true,
		false
	);

	$arResult['SECTION'] = $arSection;
	$arResult['SECTION']['PICTURE'] = $image['src']; 
}

 
$packages = $arResult['PROPERTIES']['PACKAGES']['VALUE'];

if (!empty($packages))
{
	$arFilter = Array("IBLOCK_ID"=>IBLOCK_PACKAGES, "ACTIVE"=>"Y", "PROPERTY_TYPE" => $type);  //пакеты в квартиру

	if ($_COOKIE['City'])
	{
		$arFilter['PROPERTY_CITY'] = $_COOKIE['City'];
	}
	else
	{
		$arFilter['PROPERTY_CITY'] = $_REQUEST['city'];
	}

	//$arFilter['PROPERTY_EXT'] = 17; 
	//$arFilter["PROPERTY_TYPE"] = PACKAGE_TYPE_ROOM;
	
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, array('ID', 'NAME', 'PROPERTY_TYPE'));
	while($ob = $res->GetNext())
	{
		$arRes['PACKAGE'][] = $ob;
	}

	foreach ($arRes['PACKAGE'] as $item)
	{
		if (in_array($item['ID'], $packages))
		{
			//у $item находим пробел
			$pos  = strripos($item['NAME'], " ");
			if ($pos !== false)
			{
				$item['NAME'] = substr($item['NAME'], $pos);
			}

			if ($item['PROPERTY_TYPE_ENUM_ID'] == PACKAGE_TYPE_ROOM)
			{
				$arResult['PACKAGE'][PACKAGE_TYPE_ROOM][] = $item;
			}
			elseif ($item['PROPERTY_TYPE_ENUM_ID'] == PACKAGE_TYPE_HOME)
			{
				$arResult['PACKAGE'][PACKAGE_TYPE_HOME][] = $item;
			}
			
		}
	}
}

//звуковые дорожки
if (!empty($arResult['PROPERTIES']['SOUND']['VALUE']))
{
	$arrFilter = Array("IBLOCK_ID"=>28, "ACTIVE"=>"Y", "ID" => $arResult['PROPERTIES']['SOUND']['VALUE']);  //пакеты в квартиру
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arrFilter, false, false, array('ID', 'NAME'));
	while($obSound = $res->GetNext())
	{
		$arResult['SOUND'][] = $obSound['NAME'];
	}
}

//$arResult['SOUND'] = implode(', ', $arResult['SOUND']);

if (!empty($arResult['PROPERTIES']['REGION']['VALUE']))
{
	$arrFilter = Array("IBLOCK_ID"=>27, "ACTIVE"=>"Y", "ID" => $arResult['PROPERTIES']['REGION']['VALUE']);  //пакеты в квартиру
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arrFilter, false, false, array('ID', 'NAME', "PREVIEW_PICTURE"));
	while($obRegion = $res->GetNext())
	{
		$picture = CFile::ResizeImageGet(
			$obRegion["PREVIEW_PICTURE"],
			array("width" => 30, "height" =>20),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true,
			false
		);
		$obRegion['PREVIEW_PICTURE'] = $picture['src'];
		$arResult['REGION'] = $obRegion;
	}
}

//$arResult["DETAIL_PICTURE"] = $picture['src'];
?>