<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<script src="<?=SITE_TEMPLATE_PATH?>/js/channel.js"></script>

<style>
	.single_channel_desc
	{
		margin: 0 0 18px;
	}
	
	li.active
	{
		cursor: default;
	}
</style>
<h1 class="channel_head__font">
<span class="single_channel_title_img">
<?
if (!empty($arResult["DETAIL_PICTURE"]['src'])) {?>
	<img src="<?=$arResult["DETAIL_PICTURE"]['src']?>" alt="<?=$arResult['NAME']?>">
<?/*} else {?>
	<img src="<?=$arResult["PREVIEW_PICTURE"]['SRC']?>" alt="<?=$arResult['NAME']?>">
<?*/}?>
</span><?=$arResult['NAME']?></h1>
<ul class="ch_main_info">
	<?if (!empty($arResult['REGION'])) {?>
	<li class="ch_main_info__item">
		<span class="ch_main_info__title">Страна</span>
		<span class="ch_main_info__counrty">
			<span><img src="<?=$arResult['REGION']['PREVIEW_PICTURE']?>" alt="<?=$arResult['REGION']['NAME']?>"></span>
			<?=$arResult['REGION']['NAME']?>
		</span>
	</li>
	<?}?>
	
	<?if (!empty($arResult['SECTION'])) {?>
	<li class="ch_main_info__item">
		<span class="ch_main_info__title">Категория канала</span>
		<a style = "text-decoration: none" class="ch_main_info__cat_link"><span><span class="ch_pic">
			<?if (!empty($arResult['SECTION']['PICTURE'])) {?>
				<img src="<?=$arResult['SECTION']['PICTURE']?>" alt="<?=$arResult['SECTION']['NAME']?>">		
			<?}?>
			</span><?=$arResult['SECTION']['NAME']?></span>
		</a>
	</li>
	<?}?>
	
	<?//SOUND?>
	<?
	$countSound = count($arResult['SOUND']);?>
	<?if ($countSound > 0) {?>
	<li class="ch_main_info__item">
		<span class="ch_main_info__title">Звуковая дорожка</span>
		<?
		$separator = ', ';
		$count = 2;
		for ($i = 0; $i < $count; $i++) {
			if ($i == ($count - 1) || ($countSound == 1))
				$separator = '';
			$arItem = $arResult['SOUND'][$i];
			?>
			<span><?=$arItem.$separator?></span>
		<?}?>
		<?if ($countSound > 2) {?>
		<br/><span class="add_packs_box__count add_packs_box__count--tarif_add">
			Ещё <?=($countSound - 2)?> <?=DeclensionNameSound($countSound - 2)?>
			<span class="add_packs_box__hidden add_packs_box__hidden--left_pos">
			<div class="add_packs_box__hidden_scroll">
				<?for ($i = 2; $i < count($arResult['SOUND']); $i++) {
					$arItem = $arResult['SOUND'][$i];
					echo $arItem.'<br />';
				}?>
			</div>
			</span>
		</span>
		<?}?>
	</li>
	<?}?>
	<?$countPackage = count($arResult['PACKAGE']);?>
	<?if ($countPackage > 0) {?>
	<li class="ch_main_info__item">
		<span class="ch_main_info__title">В какие пакеты входит</span>
		
		<?if (count($arResult['PACKAGE'][PACKAGE_TYPE_ROOM]) > 0) {?>
		<div class="ch_main_info__item__chlist">
			<div class="ch_main_info__item__chpic">
				<img src="/bitrix/templates/almatv/images/ch-list-tv.png">
			</div>
			
			<div class="ch_main_info__item__chitems">
				<?foreach ($arResult['PACKAGE'][PACKAGE_TYPE_ROOM] as $key => $arItem) {
					
					switch ($key % 4)
					{
						case 0:
							$classColor = 'ch_color_tv_30';
							break;
						case 1:
							$classColor = 'ch_color_tv_70';
							break;
						case 2:
							$classColor = 'ch_color_tv_100';
							break;
						case 3:
							$classColor = 'ch_color_tv_MAX';
							break;
					}?>
					<span class="ch_main_info__item__chitem <?=$classColor?>"><?=$arItem['NAME']?></span>
				<?}?>
			</div>
		</div>
		<?}?>
		
		<?if (count($arResult['PACKAGE'][PACKAGE_TYPE_HOME]) > 0) {?>
		<div class="ch_main_info__item__chlist">
			<div class="ch_main_info__item__chpic">
				<img src="/bitrix/templates/almatv/images/ch-list-ant.png">
			</div>
			<div class="ch_main_info__item__chitems">
			
				<?foreach ($arResult['PACKAGE'][PACKAGE_TYPE_HOME] as $key => $arItem) {
					
					switch ($key % 3)
					{
						case 0:
							$classColor = 'ch_color_ant_30';
							break;
						case 1:
							$classColor = 'ch_color_ant_70';
							break;
						case 2:
							$classColor = 'ch_color_ant_100';
							break;
					}?>
					<span class="ch_main_info__item__chitem <?=$classColor?>"><?=$arItem['NAME']?></span>
				<?}?>
			</div>
		</div>
		<?}?>
		
		<?
		//$separator = ', ';
		$count = 2;
		?>
		<?for ($i = 0; $i < $count; $i++) {
			//if ($i == ($count - 1)|| ($countPackage == 1))
				//$separator = '';
			$arItem = $arResult['PACKAGE'][$i];
			switch ($i % 3)
			{
				case 0:
					$class = "";
					break;
				case 1:
					$class = "ch_main_info__pack--orange";
					break;
				case 2:
					$class = "ch_main_info__pack--orange_h";
					break;
			}
			?>
			<span data-id = "<?=$arItem['ID']?>" class="ch_main_info__pack <?=$class?>"><?=$arItem['NAME']?> </span> 
		<?}?>
	<?if ($countPackage > 2) {?>
		<br/><span class="add_packs_box__count add_packs_box__count--tarif_add">
			Ещё <?=($countPackage - 2)?> <?=DeclensionNamePackages($countPackage - 2)?>
			<span class="add_packs_box__hidden add_packs_box__hidden--left_pos">
			<div class="add_packs_box__hidden_scroll">
				<?for ($i = 2; $i < count($arResult['PACKAGE']); $i++) {?>
				<?
				$arItem = $arResult['PACKAGE'][$i];
				switch ($i % 3)
				{
					case 0:
						$class = "";
						break;
					case 1:
						$class = "ch_main_info__pack--orange";
						break;
					case 2:
						$class = "ch_main_info__pack--orange_h";
						break;
				}
				?>
				<span data-s = "<?=$i?>" data-id = "<?=$arItem['ID']?>" class="ch_main_info__pack <?=$class?>"><?=$arItem['NAME']?> </span><br />
				<?}?>
			</div>
			</span>
		</span>
	<?}?>
	</li>
	<?}?>
</ul>
<?if (!empty($arResult["DETAIL_TEXT"])) {?>
<span class="single_channel_desc">
	<?=$arResult["DETAIL_TEXT"];?>
</span>
<?}?>
<?/*	
<div id="hypercomments_widget"></div>
<script type="text/javascript">
_hcwp = window._hcwp || [];
_hcwp.push({widget:"Stream", widget_id: 61559});
(function() {
if("HC_LOAD_INIT" in window)return;
HC_LOAD_INIT = true;
var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true;
hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://w.hypercomments.com/widget/hc/61559/"+lang+"/widget.js";
var s = document.getElementsByTagName("script")[0];
s.parentNode.insertBefore(hcc, s.nextSibling);
})();
</script>
<span class="tarif_cat__trigger_title">Поделитесь своим мнением о данном телеканале!</span>
<br />
<br />*/?>
