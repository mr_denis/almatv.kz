<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?
$prop = $arResult['PROPERTIES'];
//PR($prop);
//PR($arResult['PROPERTIES']);?>

<div class="contact_content">
	
	

<?
$isParamsGoogle = false;
if (!empty($prop['M_CENTER']['VALUE']) && !empty($prop['M_ZOOM']['VALUE']) && !empty($prop['M_PLACE_MARK']['VALUE']))
{
	$isParamsGoogle = true;
}

?>

<?if ($isParamsGoogle) {?>
<script type="text/javascript">
	function initialize() {
	  var mapOptions = {
		zoom: <?=$prop['M_ZOOM']['VALUE']?>,
		center: new google.maps.LatLng(<?=$prop['M_CENTER']['VALUE']?>)
	  }
	  var map = new google.maps.Map(document.getElementById('map-canvas'),
									mapOptions);

	  var image = '<?=SITE_TEMPLATE_PATH?>/images/map_label.png';
	  var myLatLng = new google.maps.LatLng(<?=$prop['M_PLACE_MARK']['VALUE']?>);
	  var beachMarker = new google.maps.Marker({
		  position: myLatLng,
		  map: map,
		  icon: image
	  });
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?}?>			
				<div class="contact_info">
					<h2 class="contact_info__office"><?=$arResult['NAME']?></h2>
					<span class="contact_info__office_desc"><?=$arResult['PREVIEW_TEXT']?></span>
				</div>
				<?if(!empty($prop['CALL_CENTER']['VALUE']) || !empty($prop['DEPARTMENT_OF_QUALITY']['VALUE'])){?>
				<div class="contact_info">
					<?if(!empty($prop['CALL_CENTER']['VALUE'])){?>
					<div class="contact_info__col">
						<h4 class="contact_info__mini_title">Call центр</h4>
						<span class="contact_info__phone">
							<a href="tel:<?=$prop['CALL_CENTER']['VALUE']?>"> <?=$prop['CALL_CENTER']['VALUE']?> </a></span> 
							<span class="contact_info__time_desc"><?=$prop['CALL_CENTER_MODE']['VALUE']?> </span>
					</div>
					<?}?>
					<?if(!empty($prop['DEPARTMENT_OF_QUALITY']['VALUE'])){?>
					<div class="contact_info__col contact_info__col--no_margin">
						<h4 class="contact_info__mini_title">Отдел управления качеством услуг</h4>
						<span class="contact_info__phone"><a href="tel:<?=$prop['DEPARTMENT_OF_QUALITY']['VALUE']?>"><?=$prop['DEPARTMENT_OF_QUALITY']['VALUE']?></a></span> 
						<span class="contact_info__time_desc"><?=$prop['DEPARTMENT_OF_QUALITY_MODE']['VALUE']?></span>
					</div>
					<?}?>
					<br>
				</div>
				<?}?>
				<h4 class="contact_info__mini_title">Режим работы офиса</h4>
				<ul class="opening_times">
					<li class="opening_times__item">
						<span class="opening_times__day">пн</span>
						<span class="opening_times__desc"><?=$prop['WEEK_DAYS']['VALUE']?></span>
					</li>
					<li class="opening_times__item">
						<span class="opening_times__day">вт</span>
					</li>
					<li class="opening_times__item">
						<span class="opening_times__day">ср</span>
					</li>
					<li class="opening_times__item">
						<span class="opening_times__day">чт</span>
					</li>
					<li class="opening_times__item">
						<span class="opening_times__day">пт</span>
					</li>
					<li class="opening_times__item opening_times__item--half">
						<span class="opening_times__day">сб</span>
						<span class="opening_times__desc"><?=$prop['SATURDAY']['VALUE']?></span>
					</li>
					<li class="opening_times__item opening_times__item--weekend">
						<span class="opening_times__day">вс</span>
						<span class="opening_times__desc"><?=$prop['SUNDAY']['VALUE']?></span>
					</li>
				</ul>
				<?if ($isParamsGoogle) {?>
				<div class="map_holder">
					<div id="map-canvas"></div>
				</div>
				<?}?>
			</div>
					<!-- офисы по городу-->
					
			<?
			$arr_id = cityOffice();
			global $arrFilter;
			$arrFilter = Array(
				"ID" => $arr_id,
			);?>

			<?$APPLICATION->IncludeComponent("bitrix:news.list", "office", Array(
				"SELECTED_OFFICE" => $arParams["ELEMENT_ID"],
				"IBLOCK_ID" => "14",	// Код информационного блока
				"NEWS_COUNT" => "20",	// Количество новостей на странице
				"USE_SEARCH" => "N",
				"USE_RSS" => "Y",
				"NUM_NEWS" => "20",
				"NUM_DAYS" => "30",
				"YANDEX" => "N",
				"USE_RATING" => "N",
				"USE_CATEGORIES" => "N",
				"USE_FILTER" => "N",
				"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
				"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
				"SORT_BY2" => "ACTIVE_FROM",	// Поле для второй сортировки новостей
				"SORT_ORDER2" => "DESC",	// Направление для второй сортировки новостей
				"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
				"SEF_MODE" => "Y",
				"SEF_FOLDER" => "/company/contacts/",
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"AJAX_OPTION_SHADOW" => "N",
				"AJAX_OPTION_JUMP" => "Y",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
				"AJAX_OPTION_HISTORY" => "Y",	// Включить эмуляцию навигации браузера
				"CACHE_TYPE" => "A",	// Тип кеширования
				"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
				"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
				"DISPLAY_PANEL" => "N",
				"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
				"SET_STATUS_404" => "Y",	// Устанавливать статус 404, если не найдены элемент или раздел
				"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
				"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
				"USE_PERMISSIONS" => "N",
				"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
				"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
				"LIST_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"LIST_PROPERTY_CODE" => array(
					0 => "",
					1 => "",
				),
				"HIDE_LINK_WHEN_NO_DETAIL" => "Y",	// Скрывать ссылку, если нет детального описания
				"DISPLAY_NAME" => "Y",	// Выводить название элемента
				"META_KEYWORDS" => "-",
				"META_DESCRIPTION" => "-",
				"BROWSER_TITLE" => "-",
				"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
				"DETAIL_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"DETAIL_PROPERTY_CODE" => array(
					0 => "",
					1 => "",
				),
				"DETAIL_DISPLAY_TOP_PAGER" => "N",
				"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
				"DETAIL_PAGER_TITLE" => "Страница",
				"DETAIL_PAGER_TEMPLATE" => "arrows",
				"DETAIL_PAGER_SHOW_ALL" => "N",
				"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
				"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
				"PAGER_TITLE" => "Контакты",	// Название категорий
				"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
				"PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
				"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",	// Время кеширования страниц для обратной навигации
				"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
				"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
				"IBLOCK_TYPE" => "vacancies",	// Тип информационного блока (используется только для проверки)
				"FILTER_NAME" => "arrFilter",	// Фильтр
				"FIELD_CODE" => array(	// Поля
					0 => "",
					1 => "",
				),
				"PROPERTY_CODE" => array(	// Свойства
					0 => "",
					1 => "",
				),
				"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
				"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
				"SET_BROWSER_TITLE" => "Y",	// Устанавливать заголовок окна браузера
				"SET_META_KEYWORDS" => "Y",	// Устанавливать ключевые слова страницы
				"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
				"PARENT_SECTION" => "",	// ID раздела
				"PARENT_SECTION_CODE" => "",	// Код раздела
				"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
				"DISPLAY_DATE" => "Y",	// Выводить дату элемента
				"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
				"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
			),
			false
		);?>
