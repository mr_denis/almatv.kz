<?//if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$prop = &$arResult['DISPLAY_PROPERTIES'];

if (count($prop["ICONS"]['VALUE']) > 0)
{
	//PR($arResult['DETAIL_PICTURE']);
	global $USER;
	if ($USER->IsAdmin()) 
	{
		//PR($arResult['PROPERTIES']['CHANNEL']['VALUE']);
		if ($arResult['PROPERTIES']['CHANNEL']['VALUE'])
		{
			$arFilter = Array(
				"IBLOCK_ID"=> IBLOCK_CHANNELS, 
				"ID" => $arResult['PROPERTIES']['CHANNEL']['VALUE'],
				"ACTIVE"=>"Y",
			);
			
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, Array("DETAIL_PICTURE"));	
			if($ar_fields = $res->GetNext())
			{
				//PR($ar_fields['DETAIL_PICTURE']);
				$image = CFile::ResizeImageGet(
					$ar_fields['DETAIL_PICTURE'],
					array("width" => 40, "height" => 30),
					BX_RESIZE_IMAGE_PROPORTIONAL,
					true,
					false
				);
			}
			
			$arResult['IMAGE'] = $image['src'];
			//$arResult['PROPERTIES']['CHANNEL']['VALUE']
		}
	}
	
	foreach ($prop["ICONS"]['VALUE'] as $key => &$arItem)
	{
		//проверим на удаленное существование, файлы могут быть недоступны
		if (remoteFileExists($arItem))
		{	
			$image = CFile::ResizeImageGet(
				$arItem,
				array("width" => 654, "height" =>360),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true,
				false
			);
			$arItem = $image['src'];
		}
	}
}
//PR($prop["ICONS"]);
$db_old_groups = CIBlockElement::GetElementGroups($arResult['ID'], true, array('NAME'));

$arSection = array();
while($ar_group = $db_old_groups->Fetch())
    $arResult["_SECTION_"][] = $ar_group;

?>