<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?
$prop = $arResult['DISPLAY_PROPERTIES'];
$ARR_MONTH_RU = array(1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля', 5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа', 9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря',);
$ARR_WEEK_RU = array(1 => 'ПН', 2 => 'ВТ', 3 => 'СР', 4 => 'ЧТ', 5 => 'ПТ', 6 => 'СБ', 7 => 'ВС');
$dateUnix = strtotime($prop['DATE_FROM']['VALUE']);
$dateUnixTo = strtotime($prop['DATE_TO']['VALUE']);

$dataD = date("j", $dateUnix);
$dataM = date("n", $dateUnix);

$dataHi = date("H:i", $dateUnix);
$dataMin = date("i", $dateUnix);
$dataWeek = (int)date("N", $dateUnix);
$dataHiTo = date("H:i", $dateUnixTo);
$dataMinTo = date("i", $dateUnixTo);

$f = (count($prop['ICONS']['VALUE']) > 0);
?>

<div class="telecast__slider_box <?if (!$f) {
	$i = rand(1,4);
	echo 'telecast__slider_box--noimg telecast__slider_box--noimg-'.$i;}?>">
	<div class="telecast__time_box">
		<span class="telecast__date telecast__date--channel">
			<?if (!empty($arResult['IMAGE'])) {?>
				<img src="<?=$arResult['IMAGE']?>" alt="<?=$arResult['NAME']?>">
			<?}?>
		</span>
		<span class="telecast__date">
			<span class="telecast__date--day"><?=$ARR_WEEK_RU[$dataWeek]?></span>
			<?=$dataD.' '.$ARR_MONTH_RU[$dataM]?>
		</span>
		<span class="telecast__date telecast__date--time"><?=$dataHi?> - <?=$dataHiTo?></span>
	</div>
	<?if ($f) {?>
		<div class="telecast__slider" id="j-telecast__slider" style = "background-color: aliceblue;">
			<?foreach ($prop['ICONS']['VALUE'] as $item) {?>
			<div class="telecast__slider_item">
				<img src="<?=$item?>" alt="image_description">
			</div>
			<?}?>
			<?/*<div class="telecast__slider_item">
				<img src="<?=SITE_TEMPLATE_PATH?>/pics/ironman.jpg" alt="image_description">
			</div>*/?>
		</div>
		<div class="telecast__slider_paging" id="j-telecast__slider_paging"></div>
		<div class="telecast__slider_right" id="j-telecast__slider_right">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="17.53px" height="33.09px" viewBox="0 0 17.53 33.09" enable-background="new 0 0 17.53 33.09" xml:space="preserve">
			<g><g><path class="slider_arrow" fill="#FFFFFF" d="M15.128,16.545L0.279,1.696c-0.391-0.391-0.391-1.023,0-1.414s1.023-0.391,1.414,0L17.25,15.838 c0.391,0.39,0.391,1.023,0,1.414L1.693,32.809c-0.391,0.391-1.023,0.391-1.414,0s-0.391-1.023,0-1.414L15.128,16.545z"/></g></g>
		</svg>
		</div>
		<div class="telecast__slider_left" id="j-telecast__slider_left">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="17.53px" height="33.09px" viewBox="0 0 17.53 33.09" enable-background="new 0 0 17.53 33.09" xml:space="preserve">
			<g><g><path class="slider_arrow" fill="#FFFFFF" d="M2.401,16.545L17.25,1.696c0.391-0.391,0.391-1.023,0-1.414s-1.023-0.391-1.414,0L0.28,15.838 c-0.391,0.39-0.391,1.023,0,1.414l15.556,15.556c0.391,0.391,1.023,0.391,1.414,0s0.391-1.023,0-1.414L2.401,16.545z"/></g></g>
		</svg>
		</div>
	<?}?>
</div>
<div class="telecast__content">
	<div class="telecast__li">
		<div class="telecast__title_holder">
			<h4 class="telecast__title"><?=$arResult['NAME']?> <span class="telecast__age">12+</span><?/*<span class="telecast__title_eng">Iron Man 3</span></h4>*/?>
			<?if (!empty($prop['YEAR']['VALUE'])) {?>
				<span class="telecast__title_date"><?=$prop['YEAR']['VALUE']?></span>
			<?}?>
		</div>
		<?if (count($prop['GANRE']['VALUE']) > 0) {?>
		<ul class="telecast__label">
			<?//foreach ($arResult["_SECTION_"] as $key => $category) {?
					/*function fullName($name)
					{
						$arrFullName = array(
							"д/c" => "документальный сериал",
							"д/ф" => "документальный фильм",
							"м/c" => "мультсериал",
							"м/ф" => "мультфильм",
							"т/с" => "телесериал",
							"х/ф" => "художественный фильм",
						);
						foreach ($arrFullName as $key => $item)
						{
							if ($key == $name)
							{
								return $item;
								break;
							}
						}
						
						return $name;
					}*/
					
				foreach ($prop['GANRE']['VALUE'] as $key => $ganre)
				{
					switch ($key % 4)
					{
						case 0:
							$class = 'telecast__label_item_link--blue';
							break;
						case 1:
							$class = 'telecast__label_item_link--purple';
							break;
						case 2:
							$class = 'telecast__label_item_link--yellow';
							break;
						case 3:
							$class = 'telecast__label_item_link--red';
							break;
					}
				{?>
					<li class="telecast__label_item">
						<a class="telecast__label_item_link <?=$class?>"><?=$ganre?><?//=fullName($category['NAME'])?></a>
					</li>
				<?}?>					
			<?}?>
		</ul>
		<?}?>
	</div>
	<?if (!empty($prop['ACTOR']['VALUE'])) {?>
	<div class="telecast__li">
			<?//PR($prop['ACTOR']['VALUE']);?>
			<?$strActor = implode(', ', $prop['ACTOR']['VALUE']);?>
			<h5 class="telecast__title_mini">В ролях</h5>
			<span><?=$strActor?></span>
	</div>
	<?}?>
	
	<?if (!empty($arResult['PREVIEW_TEXT'])) {?>
	<div class="telecast__li">
		<?if (strlen($arResult['PREVIEW_TEXT']) > 450):?>
			<div class="telecast__accordion">
				<span><?=$arResult['PREVIEW_TEXT']?></span>
				<div class="text-fade"></div>
			</div>
			<span class="telecast__accordion_trigger">Показать описание полностью</span>
		<?else:?>
			<div class="telecast__accordion">
				<span><?=$arResult['PREVIEW_TEXT']?></span>
			</div>
		<?endif?>
	</div>
	<?}?>
	<!-- Ближайшие трансляции filtr по идентичному названию -->
	<?	
	//global $USER;
	//if ($USER->IsAdmin())
	//{
		global $Filter;
		$Filter = Array(
			//">=PROPERTY_DATE_FROM" => date("Y-m-d H:i:s", time()),
			//"NAME" => '%'.$arResult['NAME'].'%',
			//"CHANNEL_PROPERTY" => $arResult['ID'],
		); 
		//PR($Filter);
		$APPLICATION->IncludeComponent(
			"bitrix:news.list", 
			"translation", 
			array(
				"IBLOCK_ID" => "17",
				"NEWS_COUNT" => "20",
				"SORT_BY1" => "PROPERTY_DATE_FROM",
				"SORT_ORDER1" => "ASC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "Filter",
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "N",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"PREVIEW_TRUNCATE_LEN" => "100",
				"ACTIVE_DATE_FORMAT" => "j F Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"PARENT_SECTION" => "",
				"FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"PROPERTY_CODE" => array(
					0 => "PROPERTY_DATE_FROM",
					1 => "PROPERTY_CHANNEL",
				)
			),
			false
		);
	//}
	?>
</div>