<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
   die();

if ($_REQUEST['ID'] == 595) {
  // HACK: add scripts manually
  global $APPLICATION;
  $APPLICATION->SetAdditionalCSS('/bitrix/components/alma/contest/css/style.css');
  $APPLICATION->AddHeadScript('/bitrix/components/alma/contest/js/jquery.cookie.min.js');
  $APPLICATION->AddHeadScript('/bitrix/components/alma/contest/js/jquery.numeric.min.js');
  $APPLICATION->AddHeadScript('/bitrix/components/alma/contest/js/func.js');
}
?>