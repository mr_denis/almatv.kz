<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?//PR($arResult['PROPERTIES']['HEADER']['VALUE']);?>
<?
$classPositionHeader == '';
if ($arResult['PROPERTIES']['HEADER']['VALUE'] == 'Y')
{
	$classPositionHeader = 'single_post__info_center';
}

?>
<ul class="tabs_content tabs_content--one_news">
	<li class="tabs_content__item">
		<div class="single_post single_post--dotted">
			<div class="single_post__img">
				<img src="<?=$arResult['arImage']['src']?>" alt="<?=$arResult['NAME']?>">
				<div class="single_post__center">
					<div class="single_post__info  <?=$classPositionHeader?>">
						<h2 class="single_post__title_img related_news__title_detal"><?=$arResult['NAME']?></h2>
						<span class="single_post__date related_news__date_detal"><?=$arResult['DISPLAY_ACTIVE_FROM']?></span>
					</div>
				</div>
			</div>
			<div class="single_post__center single_post__center--text">
				<?=$arResult['DETAIL_TEXT'];?>
			</div>
		</div>

		<?
		global $arrFilter;
		$cityId = CITY_ID_ALMATA;
		if (!empty($_COOKIE['City']))
		{
			$cityId = $_COOKIE['City'];
		}
		$arrFilter = array(
			"!ID" => $arResult['ID'],
			"PROPERTY_CITY_TO_NEWS.ID"=>$cityId,
		);
		?>
		
		<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"similarNews", 
	array(
		"IBLOCK_ID" => "1",
		"NEWS_COUNT" => "4",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(
			0 => "DETAIL_TEXT",
			1 => "DETAIL_PICTURE",
			2 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PARENT_SECTION" => $arResult['IBLOCK_SECTION_ID'],
		"IBLOCK_TYPE" => "news"
	),
	false
);?>
	</li>
</ul>

<?/*
<div id="hypercomments_widget"></div>
<script type="text/javascript">
_hcwp = window._hcwp || [];
_hcwp.push({widget:"Stream", widget_id: 61559});
(function() {
if("HC_LOAD_INIT" in window)return;
HC_LOAD_INIT = true;
var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true;
hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://w.hypercomments.com/widget/hc/61559/"+lang+"/widget.js";
var s = document.getElementsByTagName("script")[0];
s.parentNode.insertBefore(hcc, s.nextSibling);
})();
</script> */?>