<?
$id = $arResult["DETAIL_PICTURE"]['ID'];

$arResult['arImage'] = CFile::ResizeImageGet(
	$id,
	array("width" => 1450, "height" =>500),
	BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
	true,
	false
);

if ($_REQUEST['ID'] == 595) {
  ob_start();
  $APPLICATION->IncludeComponent(
    'alma:contest',
    '.default',
    array(),
    false
  );
  $almaContestForm = ob_get_contents();
  ob_end_clean();

  $arResult['DETAIL_TEXT'] = str_replace('#ALMA_CONTEST_FORM#', $almaContestForm, $arResult['DETAIL_TEXT']);
}

?>