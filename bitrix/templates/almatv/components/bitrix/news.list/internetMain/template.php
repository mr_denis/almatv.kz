<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if (count($arResult["ITEMS"])) {?>
<div class="box box--package_int" style="padding-left: 25px; padding-right: 25px;">
	<div class="package_intro">
		<a class = "decoration-none" href = "/tariffs/"> <h3 class="package_intro__title package_intro__title--int">Интернет в квартиру</h3> </a>
		<table class="package_intro__tab">
		<?foreach($arResult["ITEMS"] as $key => $arItem):?>
			<?
			$prop = $arItem['PROPERTIES'];
			$recommend = $prop['RECOMMEND'];

			$class_reccomend = '';
			if ($recommend['VALUE_ENUM_ID'] == 13)
			{
				$class_reccomend = 'package_intro__pack_name--recomm';
			}

			switch ($key)
			{
				case 0:
					$style = '--blue';
					break;
				case 1:
					$style = '--blue_m';
					break;
				case 2:
					$style = '--blue_h';
					break;
			}
			
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<tr id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<td>
					<a style = "text-decoration: none" class="channel_button--inet" href = "/tariffs/<?=$arItem['ID']?>/">
						<h5 class="package_intro__pack_name package_intro__pack_name<?=$style?> <?=$class_reccomend?>"><?=$prop['NAME_RU']['VALUE']?>
							<div class="tarif_row__list_all_channels_tip">Посмотреть наполнение</div>
						</h5>
					</a>
				</td>
				<td>
					<span class="package_intro__pack_info"><span><?=$prop['V']['VALUE']?></span> Мбит/с </span>
				</td>
				<td>
					<span class="package_intro__pack_info"><span><?=$prop["PRICE"]["VALUE"]//$arItem["PRICE_VALUE"]?></span> тг./мес.</span>
				</td>
			</tr>
		<?endforeach;?>
		</table>
		<div class="clear">
			<?/*<a href="/tariffs/" class="package_intro__all package_intro__all--green">Посмотреть все пакеты</a>
				<a href="/request/?type=inet" class="button package_intro__btn">Подключиться</a>*/?>
		</div>
	</div>
</div>
<?} else {
	global $arrFilter;
	$arrFilter = Array(
		"PROPERTY_MAIN_VALUE" => 'Y',//фильтр вывод на главной
		"PROPERTY_TYPE" => 16,
	);

	if ($_COOKIE['City'])
	{
		$arrFilter['PROPERTY_CITY'] = $arParams['CITY_ID'];
	}
	else
	{
		$arrFilter['PROPERTY_CITY'] = $_REQUEST['city'];
	}

	$APPLICATION->IncludeComponent(
		"bitrix:news.list", 
		"tvMainHome", 
		array(
			"CITY_ID" => $arParams['CITY_ID'], //для правильной работы кеша по городам
			"IBLOCK_ID" => "10",
			"NEWS_COUNT" => "4",
			"SORT_BY2" => "NAME",
			"SORT_ORDER2" => "ASC",
			"SORT_BY1" => "SORT",
			"SORT_ORDER1" => "ASC",
			"FILTER_NAME" => "arrFilter",
			"FIELD_CODE" => array(
				0 => "",
				1 => "",
			),
			"PROPERTY_CODE" => array(
				0 => "MAIN",
				1 => "",
			),
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			"PREVIEW_TRUNCATE_LEN" => "100",
			"ACTIVE_DATE_FORMAT" => "j F Y",
			"SET_TITLE" => "N",
			"SET_BROWSER_TITLE" => "Y",
			"SET_META_KEYWORDS" => "Y",
			"SET_META_DESCRIPTION" => "Y",
			"SET_STATUS_404" => "Y",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
			"ADD_SECTIONS_CHAIN" => "Y",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"PARENT_SECTION_CODE" => "",
			"INCLUDE_SUBSECTIONS" => "Y",
			"DISPLAY_DATE" => "Y",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"PAGER_TEMPLATE" => ".default",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"PAGER_TITLE" => "",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"PARENT_SECTION" => "",
			"IBLOCK_TYPE" => "internet"
		),
		false
	);
}?>
