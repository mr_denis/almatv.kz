<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if (count($arResult["ITEMS"]) > 0) {?>
<div class="vacancy_holder">
<?
	//$count = count($arResult["ITEMS"]) -1;
	foreach($arResult["ITEMS"] as $arItem) {?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<?
	$class = '';
	if ($arItem['ID'] == $_REQUEST['ELEMENT_ID'])
		$class = 'active';
	?>
	<div class="vacancy" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<span class="j-accordion__trigger vacancy__trigger <?=$class?>"><?=$arItem['NAME']?></span>
		<div class="j-accordion__slide vacancy__content cms_content <?=$class?>">
			<h2 class="vacancy__title"><?=$arItem['NAME']?></h2>
			<?=$arItem['DETAIL_TEXT']?>
		</div>
	</div>
	<?}?>
</div>
<?}?>