<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?if (count($arResult["ITEMS"]) > 0):?>
	<?if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'):?>
		<script><?=file_get_contents(__DIR__ . "/script.js");?></script>
	<?endif?>
	<div class="telecast__li">
		<h5 class="telecast__title_mini">Ближайшие трансляции</h5>
		<table class="telecast__nearest">
			<thead>
				<tr>
					<td>Дата</td>
					<td>Время</td>
					<td>Напоминание о трансляции</td>
				</tr>
			</thead>
			<tbody>
				<?foreach($arResult["ITEMS"] as $arItem) {
					$prop = $arItem['PROPERTIES'];
					//PR($prop['DATE']['VALUE']);
					$d = date('d', strtotime($prop['DATE_FROM']['VALUE']));
					$m = date('m', strtotime($prop['DATE_FROM']['VALUE']));
					$Hi = date('H:i', strtotime($prop['DATE_FROM']['VALUE']));
					$ARR_MONTH_RU = array(1 => 'января', 2 => 'февраля', 3 => 'марта', 4 => 'апреля', 5 => 'мая', 6 => 'июня', 7 => 'июля', 8 => 'августа', 9 => 'сентября', 10 => 'октября', 11 => 'ноября', 12 => 'декабря',);
					
					//PR($prop['CHANNEL']['VALUE']);
					?>
				<tr>
					<td><?=$d.' '.$ARR_MONTH_RU[(int)$m]?></td>
					<td>
						<span class="telecast__nearest_time"><?=$Hi?></span>
					</td>
					<td>
						<div class="clear">
							<div class="telecast__nearest_channel">
								<?if ($arItem["REMIND"] == "Y"):?>
									Вы записаны
								<?else:?>
									<div class="telecast__remind_box">
										<div class = "telecast__remind_form_cap"></div>
									</div>
									<div class="telecast__remind_form">
										<form action="" class="translation-remind">
											<input type="hidden" name="broadcasting" value="<?=$arItem['ID']?>">
											<fieldset>
												<div class="telecast__row">
													<input type="text" class="telecast__field" name="email">
													<label class="telecast__label">Ваша эл. почта</label>
												</div>
												<button type="submit">Напомнить о начале</button>
											</fieldset>
										</form>
									</div>
								<?endif?>
							</div>
						</div>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
<?endif?>
