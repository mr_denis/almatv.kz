var closeRemindsTimeout;
$(function() {
	$(".translation-remind").each(function(){
		$(this).validate({
			rules: {
				email: {
					required: true,
					email: true
				},
			},
			submitHandler: function(form) {
			    $.ajax({
					dataType: "json",
					url: "/ajax/reminder.php",
					method: "post",
					data: $(form).serialize(),
					success: function (response) {
						if (!response.success) {
							alert("Извините, произошла внутренняя ошибка сервера, попробуйте записаться позже.\n\n" + response.errors.join("\n"));
							return;
						}
						$(form).html("<span class=\"success-message\">За 15 минут до начала телепередачи вам напомнят по электронной почте</span>");
						var parent = $(form).closest(".telecast__nearest_channel")
						parent.find(".telecast__remind_box").remove()
						parent.append("Вы записаны");
						closeRemindsTimeout = setTimeout(function(){
							closeAllRemindPopups();
						}, 3000);
					},
					error: function () {
						alert("Извините, произошла внутренняя ошибка сервера, попробуйте записаться позже.");
					}
				});
				return false;
			}
		});
	});

	$.validator.addMethod("email", function(value){
		return isValidEmailAddress(value);
	}, "Пожалуйста, введите корректный адрес электронной почты.");

	$(".telecast__remind_form_cap").on("click",  function() {
		closeAllRemindPopups();
		$(this).closest(".telecast__nearest_channel").find(".telecast__remind_form").addClass("active");
	});

	$(this).on("click", function(e){
		if (!$(e.target).closest(".telecast__remind_form_cap, .telecast__remind_form").length) {
			closeAllRemindPopups();
		}
	})
});

function closeAllRemindPopups () {
	if (closeRemindsTimeout) {
		clearTimeout(closeRemindsTimeout);
	}
	$(".telecast__nearest").find(".telecast__remind_form").removeClass("active");
}