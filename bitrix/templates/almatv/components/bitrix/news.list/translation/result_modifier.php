<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('highloadblock');
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity as Entity;

$obHLIB = HL\HighloadBlockTable::getById(HLIB_TV_REMINDER)->Fetch();
$obEntity = HL\HighloadBlockTable::compileEntity($obHLIB);
$obDataClass = $obEntity->getDataClass();

$arBroadcastingsIds = array();
foreach($arResult["ITEMS"] as $arItem) {
	$arBroadcastingsIds[] = $arItem["ID"];
}

$arRemindsBroadcastings = array();

$arRemindsFilter = array(
	"UF_BROADCASTING" => $arBroadcastingsIds,
	"UF_SESSID" => bitrix_sessid_get()
);

$obQuery = new Entity\Query($obEntity);
$obQuery->setSelect(array("ID", "UF_BROADCASTING"));
$obQuery->SetFilter($arRemindsFilter);
$rsReminds = new CDBResult($obQuery->exec());
while ($arRemind = $rsReminds->Fetch()) {
    $arRemindsBroadcastings[] = $arRemind["UF_BROADCASTING"];
}

foreach($arResult["ITEMS"] as &$arItem) {
	if (in_array($arItem["ID"], $arRemindsBroadcastings)) {
		$arItem["REMIND"] = "Y";
	}
	else {
		$arItem["REMIND"] = "N";
	}
}
unset($arItem);
?> 