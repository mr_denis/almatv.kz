<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if (count($arResult['ITEMS'])) {?>
<style>
.full_banner {
	background: white !important;
}
</style>
<div class="full_banner">
	<div class="container">
	   <h1 class="full_banner__title full_banner__title--tv">Телевидение</h1>
	</div>
	<div class="container">
		<div class="full_banner-box">
			<?foreach ($arResult['ITEMS'] as $arItem) {
				$prop = $arItem['PROPERTIES'];

				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div class="full_banner-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<div class="full_banner-bg"></div>
					<div class="full_banner-active">
						<div class="full_banner-active_close">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="12px" height="12px" viewBox="0 0 348.333 348.334" style="enable-background:new 0 0 348.333 348.334;" xml:space="preserve">
								<g>
									<path fill="#fff" d="M336.559,68.611L231.016,174.165l105.543,105.549c15.699,15.705,15.699,41.145,0,56.85 c-7.844,7.844-18.128,11.769-28.407,11.769c-10.296,0-20.581-3.919-28.419-11.769L174.167,231.003L68.609,336.563 c-7.843,7.844-18.128,11.769-28.416,11.769c-10.285,0-20.563-3.919-28.413-11.769c-15.699-15.698-15.699-41.139,0-56.85 l105.54-105.549L11.774,68.611c-15.699-15.699-15.699-41.145,0-56.844c15.696-15.687,41.127-15.687,56.829,0l105.563,105.554 L279.721,11.767c15.705-15.687,41.139-15.687,56.832,0C352.258,27.466,352.258,52.912,336.559,68.611z"/>
								</g>
							</svg>
						</div>
						<div class="full_banner-active_head"><?=$arItem['NAME']?></div>
						<div class="full_banner-active_content"><?=$arItem['DETAIL_TEXT']?></div>
					</div>
					<img src="<?=$arItem['PICTURE']?>" alt="<?=$arItem['NAME']?>" class="full_banner-pic">
					<img src="<?=$arItem['PICTURE_DETAIL']?>" alt="<?=$arItem['NAME']?>" class="full_banner-pic-mini">
					<div class="full_banner-info">
						<span class="full_banner-head"><?=$arItem['NAME']?></span>
						<br><span class="full_banner-text"><?=$arItem['PREVIEW_TEXT']?></span>
						<?if (!empty($prop['BUTTON']['VALUE'])) {?>
							<br><span class="full_banner-btn"><?=$prop['BUTTON']['VALUE']?></span>
						<?}?>
					</div>
				</div>
			<?}?>
		</div>
	</div>
</div>
<?} else {?>
<style>
	.full_banner{
		height: 443px;
	}	
</style>
<div class="full_banner" style="background-image: url(/upload/iblock/fae/fae1d2a17d901ba8f569e2733aef8301.jpg);">
	<div class="container">
	   <h1 class="full_banner__title full_banner__title--tv">Телевидение</h1>
	</div>
</div>
<?}?>