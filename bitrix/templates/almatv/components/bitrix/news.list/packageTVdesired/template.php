<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if (count($arResult["ITEMS"]) > 0) {?>
<div class="info_row">
<label class="user_data__label user_data__label--req">Желаемый пакет программ цифрового ТВ</label>	
<?if (isInetCity($_COOKIE["City"])) {?>
<label style = "margin-left: 50px; margin-right: 10px" class ="">Желаю комбинированный пакет (Интернет+ТВ) <input name="form_checkbox_COMBINED[]" value="36" id = "tv_it" type="checkbox"> </label>	
<?}?>
<ul class="get_pack j-get_pack--flat">
<?foreach($arResult["ITEMS"] as $key => $arItem):?>
	<?
	$prop = $arItem['PROPERTIES']; 
	$recommend = $prop['RECOMMEND'];
	$class_reccomend = '';
	if ($recommend['VALUE_ENUM_ID'][0] == 12)
	{
		$class_reccomend = 'get_pack__title--cool';
	}
	//PR($prop['CHANNEL']);
	//PR($prop['TYPE']['VALUE_ENUM_ID']);
	if (in_array(15, $prop['TYPE']['VALUE_ENUM_ID'])) { //в квартиру//
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		
		<li class="get_pack__item <?if ($_REQUEST['package'] == $arItem['ID']) {?> active <?}?>" data-id="<?=$arItem['ID']?>" data-pack="<?=$arItem['NAME']?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>"> <!-- active data-pack="<?//=$arItem['ID']?>"  -->
			<?if (count($arItem['CHANNELS']) > 0) {?>
			<div data-id = "<?=$arItem['ID']?>" class = "channel_button get_pack__add"></div>
			<?}?>
			<h4 class="get_pack__title <?=$class_reccomend?>"><?=$arItem['NAME']?><?//=$arItem['ID']?></h4>
			<div class="clear">
				<span class="get_pack__info"><span><?=count($arItem['CHANNELS'])//count($prop['CHANNEL']['VALUE'])?></span>  каналов</span>
				<span class="get_pack__info get_pack__info--right"><span><?=$prop['PRICE']['VALUE']?></span>  тг./мес.</span>
			</div>
		</li>
	<?}?>
<?endforeach;?>
</ul>					
						
<ul class="get_pack j-get_pack--home get_pack--hidden">
	<?foreach($arResult["ITEMS"] as $key => $arItem):?>
	<?
	$prop = $arItem['PROPERTIES']; 
	$recommend = $prop['RECOMMEND'];
	$class_reccomend = '';
	if ($recommend['VALUE_ENUM_ID'][0] == 12)
	{
		$class_reccomend = 'get_pack__title--cool';
	}
	if (in_array(16, $prop['TYPE']['VALUE_ENUM_ID']))  {  //в частный дом
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		
		<li class="get_pack__item <?if ($_REQUEST['package'] == $arItem['ID']) {?> active <?}?>" data-id="<?=$arItem['ID']?>" data-pack="<?=$arItem['NAME']?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<?//if (count($arItem['CHANNELS']) > 0) {?>
			<div data-id = "<?=$arItem['ID']?>" class = "channel_button get_pack__add">
				<?/*<div class="get_pack__add_hidden">
					<div class="add_packs_box__hidden add_packs_box__hidden--getpack">
						<div class="add_packs_box__hidden_scroll">
							<?foreach ($arItem['CHANNELS'] as $channelItem) {?>		
							<span class="add_packs_box__hidden_item">
									<?if (!empty($channelItem['PREVIEW_PICTURE'])) {?>
										<span class="add_packs_box__hidden_pic">
											<img src="<?=$channelItem['PREVIEW_PICTURE']?>" alt="<?=$channelItem['NAME']?>">
										</span>
									<?} else {?>
										<span class="add_packs_box__hidden_pic add_packs_box__hidden_pic--noimg">
											<span><?=substr($channelItem['NAME'], 0, 1)?></span>
										</span>
									<?}?>
								<a href="/channel/<?=$channelItem['ID']?>/" class="add_packs_box__hidden_link"><?=$channelItem['NAME']?></a>
							</span>
							<?}?>
						</div>
					</div>
				</div>*/?>
			</div>
			
			<?//}?>
			<h4 class="get_pack__title <?=$class_reccomend?>"><?=$arItem['NAME']?></h4>
			<div class="clear">
				<span class="get_pack__info"><span><?=count($arItem['CHANNELS'])//count($prop['CHANNEL']['VALUE'])?></span>  каналов</span>
				<span class="get_pack__info get_pack__info--right"><span><?=$prop['PRICE']['VALUE']?></span>  тг./мес.</span>
			</div>
		</li>
	<?}?>
<?endforeach;?>
</ul>
<input type="hidden" value="0" class="get_pack_result">
</div>

<?}?> 