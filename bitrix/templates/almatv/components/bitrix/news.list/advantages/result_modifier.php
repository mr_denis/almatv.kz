<?foreach($arResult["ITEMS"] as &$arItem) {
	$image = CFile::ResizeImageGet(
		$arItem['PREVIEW_PICTURE'],
		array("width" => 100, "height" =>100),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		true,
		false
	);
	 
	$arItem['PICTURE'] = $image;
}?>
