<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<div class="why_we__list">
<?foreach ($arResult["ITEMS"] as $key => $arItem) {?>
	
<?
$prop = $arItem['PROPERTIES'];
//PR($prop['MAIN']['VALUE']);
?>
	
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<div class="why_we__list_item <?if ($key == 3) { echo 'why_we__list_item--show1028';} elseif ($key == 4) { echo 'why_we__list_item--show1600'; } ?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<?if ($prop['MAIN']['VALUE'] == "Y") {?>
		<a href = "#" data-id = "<?=$arItem['ID']?>" class = "year_awarded">
		<div class="why_we__list_img why_we__list_img--time">
			<img src="<?=$arItem['PICTURE']['src']?>" alt="<?=$arItem['NAME']?>">
		</div> 
		<span class="why_we__list_link"><?=ckopStr($arItem['NAME'], 0, 30)?></span>
		</a>
		<?} else {?>
		<div class="why_we__list_img why_we__list_img--time">
			<img src="<?=$arItem['PICTURE']['src']?>" alt="<?=$arItem['NAME']?>">
		</div> 
		<span class="why_we__list_link"><?=ckopStr($arItem['NAME'], 0, 30)?></span>
		<?}?>
	</div>

<?}?>
</div>