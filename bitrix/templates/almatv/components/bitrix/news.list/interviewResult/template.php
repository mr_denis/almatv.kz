<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?
if (count($arResult["ITEMS"]) > 0) {
foreach($arResult["ITEMS"] as $arItem) {
	$propVoices = $arItem["PROPERTIES"]["VOICES"];
	//PR();
$count = count($arResult["ANSWERS"]);
//PR($arResult["ANSWERS"]);
//PR($propVoices);
if ($count <= 7) {?>
	<div class="ususal_box">
		<div class="box box--rating">
			<div class="rating">
				<div class="rating__title_box clear">
					<h4 class="rating__title"><?=$arParams['ANSWER']?></h4>
					<span class="rating__desc"><?=$arItem['DETAIL_TEXT']?></span>
				</div>
				<ul class="rating_result">
					<?foreach ($arResult["ANSWERS"] as $key => $answersItem) {?>
						<?
						if (empty($propVoices['VALUE'][$key]))
							$propVoices['VALUE'][$key] = 0;
						?>
						<li class="rating_result__item">
							<div class="rating_result__item_col" style="height: <?=$propVoices['VALUE'][$key]?>%">
								<div class="rating_result__item_head">
									<div class="rating_result__item_img">
										<img class="rating__list_item_img" src="<?=$answersItem['PICTURE']?>" alt="<?=$answersItem['NAME']?>">
									</div>
									<div class="rating_result__item_num">
										<span><?=$propVoices['VALUE'][$key]?>%</span>
									</div>
									<span class="rating_result__name"><?=$answersItem['NAME']?></span>
								</div>
							</div>
						</li>
					<?}?>
				</ul>
			</div>
		</div>
	</div>
<?} else {?>
	<div class="ususal_box">
		<div class="box box--rating">
			<div class="rating">
				<div class="rating__title_box clear">
					<h4 class="rating__title"><?=$arParams['ANSWER']?></h4>
					<span class="rating__desc"><?=$arItem['DETAIL_TEXT']?></span>
				</div>
				<ul class="rating_result rating_result--wide">
					<?foreach ($arResult["ANSWERS"] as $key => $answersItem) {?>
						<?
						if (empty($propVoices['VALUE'][$key]))
							$propVoices['VALUE'][$key] = 0;
						?>
					<li class="rating_result__item">
						<div class="rating_result__item_col">
							<div class="rating_result__item_head">
								<div class="rating_result__item_img">
									<img class="rating__list_item_img" src="<?=$answersItem['PICTURE']?>" alt="<?=$answersItem['NAME']?>">
								</div>
								<div class="rating_result__item_num">
									<span><?=$propVoices['VALUE'][$key] ?>%</span>
								</div>
								<span class="rating_result__name"><?=$answersItem['NAME']?></span>
							</div>
						</div>
					</li>
					<?}?>
				</ul>
			</div>
		</div>
	</div>
	<?}

}
}?>	
