<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$phone = $arItem['PROPERTIES']['PHONE_DEV'];
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	//PR($phone);
	?>
	<div class="support_phone" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	<span class="support_phone__tel"><span><?=$phone['VALUE'][0]?></span></span>
	<span class="support_phone__link">Служба поддержки</span>
</div>
<?endforeach;?>

