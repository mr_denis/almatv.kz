<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<div class="vacancy_holder">
<?
	$class = '';
	$count = count($arResult["ITEMS"])-1;
	foreach($arResult["ITEMS"] as $key => $arItem) {?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<?
		if ($count == $key)
			$class = 'active';
	?>
	<div class="vacancy" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<span class="j-accordion__trigger vacancy__trigger <?=$class?>"><?=$arItem['NAME']?></span>
		<div class="j-accordion__slide vacancy__content cms_content <?=$class?>">
			<h2 class="vacancy__title"><?=$arItem['NAME']?></h2>
			<?=$arItem['DETAIL_TEXT']?>
		</div>
	</div>
	<?
	$key++;
	}?>
</div>
