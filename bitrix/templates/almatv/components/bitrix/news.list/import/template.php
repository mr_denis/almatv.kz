<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<script>
	$(function()
	{
		$('a.add').click(function(e)
		{
			$('#j-popup_import').html('<img src = "/images/loading.gif" />');
			$('#j-popup_import').arcticmodal();
			//$('#j-popup_dev').arcticmodal();
			e.preventDefault();
			$.ajax({
				type: 'POST',
				url: '/import/parser.php',
				data: ({id: $(this).data('id'), filename: $(this).data('filename')}),
				success: function(ob)
				{
					//console.log(ob);
					$('#j-popup_import').html('<div class = "popup_import-box">' + ob + '</div>');
					//alert('TV программа загружена.');
				}
			});
			
			//alert('Добавление');
		});
		
		$('a.del').click(function(e)
		{
			$('#j-popup_import').html('<img src = "/images/loading.gif" />');
			$('#j-popup_import').arcticmodal();
			//$('#j-popup_dev').arcticmodal();
			e.preventDefault();
			$.ajax({
				type: 'POST',
				url: '/import/delete.php',
				data: ({id: $(this).data('id')}),
				success: function(ob)
				{
					//console.log(ob);
					$('#j-popup_import').html('<div class = "popup_import-box">' + ob + '</div>');
					//alert('TV программа удалена.');
				}
			});
		});
	});
</script>

<style>
	.news-list .name-channel
	{
		width: 400px; 
		float: left;
	}
	
	.content-line
	{
		width: 100%;
		height: 40px;
	}
	
	.name-channel span
	{
		font-weight: bold;
	}
	.green
	{
		color: green;
	}
</style>

<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$prop = $arItem['PROPERTIES'];
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class = "content-line">
		<div class = "name-channel"> <span> <a href = "/channel/<?=$arItem['ID']?>/"> <?=$arItem['NAME']?> </a> </span>
		<?
		$xmlName = $prop['XML']['VALUE'];
		if (!empty($xmlName)) {?>
			<span class = "green"> Файл для выгрузки /import/<?=$prop['XML']['VALUE']?> </span>
		<?} else {?>
			<span style = "color: #aaa"> Не прикреплен </span>
		<?}?>
		</div>
		<?if (!empty($xmlName)) {?>
		<div> 
			<a data-id = "<?=$arItem['ID']?>" data-filename = <?=$prop['XML']['VALUE']?>  class = 'add' href = "#"> Добавить </a> &nbsp;&nbsp;&nbsp; 
			<a data-id = "<?=$arItem['ID']?>" class = "del" href = "#"> Очистить телепрограммы для данного канала </a>
		</div>
		<?}?>
	</div>
	 
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
