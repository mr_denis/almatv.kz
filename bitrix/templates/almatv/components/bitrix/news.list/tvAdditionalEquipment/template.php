<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?if (count($arResult["ITEMS"]) > 0) {?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$prop = $arItem['PROPERTIES'];
	//PR($prop);
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>			
	<div class="add_eq" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<h5 class="add_eq__title">Дополнительное оборудование</h5>
		<div class="j-add_eq__tabs_img">
			<?/*<img class="active" src="/bitrix/templates/almatv/images/eq-in-flat-1.png">
			<img src="/bitrix/templates/almatv/images/eq-in-hose.png">*/?>
			<img class="active"src="/images/2.png">
			<img  src="/images/1.png">
			
		</div>
		<ul class="tarif_cat__trigger tarif_cat__trigger--add_eq j-add_eq__tabs_head">
			<?if ($arParams['TYPE_1'] > 1) {?><li class="tarif_cat__trigger_item active">в квартиру</li><?}?>
			<?if ($arParams['TYPE_2'] > 1) {?><li class="tarif_cat__trigger_item">в частный дом</li><?}?>
		</ul>
		<ul class="add_eq__tabs_cont">
			<li class="add_eq__tabs_cont_item">
				<span class="add_eq__tabs_cont_desc"><?=$prop['K_PREVIEW_RU']['VALUE']['TEXT']?></span>
				<?if (!empty($prop['K_HREF']['VALUE'])) {?><a href="<?=$prop['K_HREF']['VALUE']?>" class="add_eq__more"><!--Узнать продробнее--></a><?}?>
				<span class="add_eq__tabs_cont_rows">
					<?//PR($prop['K_HARDWARE']);?>
					<?=$prop['K_HARDWARE']['~VALUE']['TEXT']?>
					<?/*<span class="add_eq__tabs_cont_row">
						<span class="add_eq__tabs_cont_col">САМ-модуль:</span>
						<span class="add_eq__tabs_cont_res">999 тг.</span>
					</span>
					<span class="add_eq__tabs_cont_row">
						<span class="add_eq__tabs_cont_col">Приставка:</span>
						<span class="add_eq__tabs_cont_res">999 тг.</span>
					</span>*/?>
					<!-- <span class="pack_cost pack_cost--add_eq"><?=$prop['K_PRICE']['VALUE']?> <span>тг.</span></span> -->
				</span>
			</li>
			<li class="add_eq__tabs_cont_item">
				<span class="add_eq__tabs_cont_desc"><?=$prop['D_PREVIEW_RU']['VALUE']['TEXT']?></span>
				<?if (!empty($prop['D_HREF']['VALUE'])) {?><a href="<?=$prop['D_HREF']['VALUE']?>" class="add_eq__more"><!--Узнать продробнее--></a><?}?>
				<span class="add_eq__tabs_cont_rows">
					<?=$prop['D_HARDWARE']['~VALUE']['TEXT']?>
					<?/*<span class="add_eq__tabs_cont_row">
						<span class="add_eq__tabs_cont_col">Антенна:</span>
						<span class="add_eq__tabs_cont_res">999 тг.</span>
					</span>
					<span class="add_eq__tabs_cont_row">
						<span class="add_eq__tabs_cont_col">САМ-модуль:</span>
						<span class="add_eq__tabs_cont_res">999 тг.</span>
					</span>
					<span class="add_eq__tabs_cont_row">
						<span class="add_eq__tabs_cont_col">Приставка:</span>
						<span class="add_eq__tabs_cont_res">999 тг.</span>
					</span>*/?>
					<!-- <span class="pack_cost pack_cost--add_eq"><?=$prop['K_PRICE']['VALUE']?> <span>тг.</span></span> -->
				</span>
			</li>
		</ul>
	</div>
<?endforeach;?>
<? } else {?>
	<div class="add_eq">
		<h5 class="add_eq__title">Дополнительное оборудование</h5>
		Отсутстувует в данном городе.
	</div>
<? }?>
