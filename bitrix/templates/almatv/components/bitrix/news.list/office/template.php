<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<aside class="contact_sidebar">
	<nav>
		<ul class="side_nav">
		<?foreach($arResult["ITEMS"] as $key => $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<li data-id = "<?=$arItem['ID']?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="side_nav__item <?if ($arParams["SELECTED_OFFICE"] == $arItem['ID']) {?> active <? } ?> "><a href="#" class="side_nav__link"><?=$arItem['NAME']?></a></li>
		<?endforeach;?>
		</ul>
	</nav>
</aside>