<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<div class="tv_super_holder">
	<div class="tv_pluses_holder">
		<ul class="tv_plus">	
		<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<li class="tv_plus__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="tv_plus__img">
				<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
			</div>
			<div class="tv_plus__content">
				<span class="tv_plus__title"><?=ckopStr($arItem['NAME'], 0, 20)?></span>
				<?/*<span class="tv_plus__text"><?=$arItem['PREVIEW_TEXT']?></span>*/?>
			</div>
		</li>
		<?endforeach;?>
		</ul>
	</div>
</div>

