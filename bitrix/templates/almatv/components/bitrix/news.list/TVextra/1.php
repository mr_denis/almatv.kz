<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if (count($arResult["ITEMS"]) > 0) {?>
<h2 class="dotted_title">?????????????? ?????? ????????</h2>
				<div class="add_package_list_holder">
					<ul class="add_package_list add_package_list--only3">
					
	<?foreach($arResult["ITEMS"] as $key => $arItem):?>
		<?
		
		$prop = $arItem['PROPERTIES']; 
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
			
		<li class="add_package_list__item add_package_list__item--mini" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<?if (!empty($arItem['DETAIL_PICTURE'])) { ?>
			<div class="add_package_list__img">
				<img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
			</div>
			<?}?>
			<h6 class="add_package_list__title">
				<?if (!empty($arItem['PREVIEW_PICTURE'])) { ?>
				<span class="add_package_list__title_ico">
					<img src="<?=$arItem['PREVIEW_PICTURE']['src']?>" alt="<?=$arItem['NAME']?>">
				</span>
				<?}?>
				<span class="add_package_list__title_name"><?=$arItem['NAME']?></span>
			</h6>
			<ul class="tarif_add_pack__list tarif_add_pack__list--add_package">
				<?$isExt = false?>
				<?for ($i = 0; $i < count($arItem['CHANNELS']); $i++) {
					$channel = $arItem['CHANNELS'][$i];
					if ($i == 5)
					{
						$isExt = true;
						break;
					}
					?>
					<li class="tarif_add_pack__list_li"><a href="/channel/<?=$channel['ID']?>/"><?=$channel['NAME']?></a></li>
				<?}?>
				
				<?if ($isExt) {
					$count = count($arItem['CHANNELS']) - 5;?>
					<li class="tarif_add_pack__list_li tarif_add_pack__list_li--more_packs">
						<span class="add_packs_box__count add_packs_box__count--black">
							??? <?=$count?> ???????
							<span class="add_packs_box__hidden <?if ($key == 0) echo 'add_packs_box__hidden--rigth';?>">
							<div class="add_packs_box__hidden_scroll">
								<?for ($i = 5; $i < count($arItem['CHANNELS']); $i++) {
									$channel = $arItem['CHANNELS'][$i];?>
								<span class="add_packs_box__hidden_item">
									<?if (!empty($channel['PREVIEW_PICTURE']['src'])) { ?>
									<span class="add_packs_box__hidden_pic">
										<img src="<?=$channel['PREVIEW_PICTURE']['src']?>" alt="<?=$channel['NAME']?>">
									</span>
									<?} else {?>
									<span class="add_packs_box__hidden_pic add_packs_box__hidden_pic--noimg">
										<span><?=substr($channel['NAME'], 0, 1)?></span>
									</span>
									<?}?>
									<a href="/channel/<?=$channel['ID']?>/" class="add_packs_box__hidden_link"><?=$channel['NAME']?></a>
								</span>
								<? } ?>
							</div>
							</span>
						</span>
					</li>
				<?} ?>
			</ul>
		</li>
	<?endforeach;?>
	</ul>
</div>
<?}?>

	