<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if (count($arResult["ITEMS"])) {?>
<div class="info_row">
	<label class="user_data__label user_data__label--req">Тарифы</label>
	<label style = "margin-left: 50px; margin-right: 10px" class ="">Желаю комбинированный пакет (Интернет+ТВ) <input name="form_checkbox_COMBINED[]" value="37" id = "tv_it" type="checkbox"> </label>	
	<div class="tarif_holder">
		<ul class="tarif_cols">
			<li class="tarif_cols__item">Название тарифа</li>
			<li class="tarif_cols__item">
				<span class="tarif_cols__item1024">Скорость</span>
				<span class="tarif_cols__item1280">Скорость на внешние ресурсы</span>
			</li>
			<li class="tarif_cols__item">Абонентская плата</li>
		</ul>

	<?foreach($arResult["ITEMS"] as $key => $arItem):?>
		<?
		$prop = $arItem['PROPERTIES'];
		//PR($prop);
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<?
		$class = '';
		switch ($key)
		{
			case 0:
				break;
			case 1:
				$class = 'tarif__name--light_blue';
				break;
			case 2:
				$class = 'tarif__name--blue';
				break;
			case 3:
				$class = 'tarif__name--purple';
				break;
			default:
				$class = '';
		}
		$class_reccomend = '';
		$recommend = $prop['RECOMMEND'];
		if ($recommend['VALUE_ENUM_ID'] == 13)
		{
			$class_reccomend = 'tarif__name_holder--best';
		}
		
		?>
		<div data-value = "<?=$arItem['ID']?>" data-name = "<?=$arItem['NAME']?>" class="tarif <?if ($_REQUEST['tarif'] == $arItem['ID']) {?> active <?}?>"" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<ul class="tarif__table">
				<li class="tarif__table_item">
					<div class="tarif__name_holder  <?=$active?> <?=$class_reccomend?>">
						<span class="tarif__name <?=$class?>"><?=$prop['NAME_RU']['VALUE']//=$arItem['NAME']?></span>
					</div>
					<?if (!empty($prop['DESCRIPTION']['VALUE'])) {?>
					<div class="tarif__name_tip">
						<em><?=$prop['DESCRIPTION']['VALUE']?></em>
					</div>
					<?}?>
				</li>
				<li class="tarif__table_item">
					<div class="tarif__speed_img">
						<img src="<?=$arItem['PICTURE']['src']?>" alt="<?=$arItem['NAME']?>">
					</div>
					<span class="tarif__speed"><?=$prop['V']['VALUE']?> <span>Мбит/с</span></span>
				</li>
				<li class="tarif__table_item">
					<span class="tarif__cost"><?=$prop["PRICE"]["VALUE"]?> <span>тг./мес.</span></span>
				</li>
			</ul>
		</div>
	<?endforeach;?>
	</div>
</div>

<div class="info_row">
<label class="user_data__label">Дополнительные опции</label>
<table class="ip_table">
	<thead>
		<tr>
			<td>Количество IP-адресов</td>
			<td>Единовременная плата</td>
			<td>Абонентская плата</td>
		</tr>
	</thead>
	<tbody>
		<tr data-value = "4">
			<td>
				<span class="ip_table__count"><span>4</span> IP Адреса</span>
			</td>
			<td>
				<span class="ip_table__cost"><span>4 500</span> тг./мес.</span>
			</td>
			<td>
				<span class="ip_table__cost"><span>2 000</span> тг./мес.</span>
			</td>
		</tr>
		<tr data-value = "8">
			<td>
				<span class="ip_table__count"><span>8</span> IP Адресов</span>
			</td>
			<td>
				<span class="ip_table__cost"><span>6 500</span> тг./мес.</span>
			</td>
			<td>
				<span class="ip_table__cost"><span>3 200</span> тг./мес.</span>
			</td>
		</tr>
		<tr data-value = "16">
			<td>
				<span class="ip_table__count"><span>16</span> IP Адресов</span>
			</td>
			<td>
				<span class="ip_table__cost"><span>4 500</span> тг./мес.</span>
			</td>
			<td>
				<span class="ip_table__cost"><span>2 000</span> тг./мес.</span>
			</td>
		</tr>
		<tr data-value = "32">
			<td>
				<span class="ip_table__count"><span>32</span> IP Адреса</span>
			</td>
			<td>
				<span class="ip_table__cost"><span>9 000</span> тг./мес.</span>
			</td>
			<td>
				<span class="ip_table__cost"><span>4 500</span> тг./мес.</span>
			</td>
		</tr>
		<tr data-value = "64">
			<td>
				<span class="ip_table__count"><span>64</span> IP Адреса</span>
			</td>
			<td>
				<span class="ip_table__cost"><span>15 000</span> тг./мес.</span>
			</td>
			<td>
				<span class="ip_table__cost"><span>7 000</span> тг./мес.</span>
			</td>
		</tr>
	</tbody>
</table>
</div>
			
<div class="info_row info_row--no_dots">
	<div class="user_data">
		<div class="user_data__row user_data__row--full">
			<label class="user_data__label">Прикрепить файл</label>
			<input name = "form_file_40" type="file" class="custom-file custom-file__repair">
			<span> Размер файла не должен превышать 5 мб </span>
		</div>
	</div>
</div>
			
<div class="submit_box">
	<input type="submit" value="Отправить заявку" name="web_form_submit" class="submit_box__btn button">
</div>
		
<?} else {?>
	<div class="info_row">
		<label class="user_data__label">Тарифы отсутствуют в данном городе</label> 
	</div>
<?}?>