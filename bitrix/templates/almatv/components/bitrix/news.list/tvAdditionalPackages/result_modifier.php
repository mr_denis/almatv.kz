<?
if (count($arResult["ITEMS"]) > 0) {
	foreach($arResult["ITEMS"] as &$arItem) {
		$prop = $arItem['PROPERTIES'];
		
		$id_paskages = intval($prop['ELEMENT']['VALUE']);
		$ID_PACKAGE = 10;
		$res = CIBlockElement::GetList(Array(), Array("ID" => $id_paskages), false, false, Array("PROPERTY_PRICE", "NAME", "PROPERTY_TYPE"));
		//$arPaskages = array();
		if($ob = $res->GetNext())
		{
			$arItem['PACKAGE'] = $ob;
		}
		//PR($arItem['PACKAGE']);
		$ID_CHANNELS = 9;

		$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE");   //PRICE
		$arFilter = Array("IBLOCK_ID"=>$ID_CHANNELS, "PROPERTY_PACKAGES" => $id_paskages, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");  //������ �� ������
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		$arChannels = array();
		while($ob = $res->GetNext())
		{
			$image = CFile::ResizeImageGet(
				$ob['PREVIEW_PICTURE'],
				array("width" => 20, "height" =>20),
				BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
				true,
				false
			);
			
			$arChannel = array(
				"ID" => $ob['ID'],
				"NAME" => $ob['NAME'],
				"PREVIEW_PICTURE" => $image,
			);
			$arChannels[] = $arChannel;
		}
		$arItem['CHANNELS'] = $arChannels;
	}
}