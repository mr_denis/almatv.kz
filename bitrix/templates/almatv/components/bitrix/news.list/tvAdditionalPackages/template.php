<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<style>
	.content-pack.active
	{
		display: block;
	}
	.content-pack
	{
		display: none;
	}
</style>

<?if (count($arResult["ITEMS"]) > 0) {?>
<?//PR($arResult["ITEMS"]);?>
<div class="tarif_add_pack">

<?
$isTypePackage = false;
foreach($arResult["ITEMS"] as $key => $arItem):?>
	<?
	$prop = $arItem['PROPERTIES'];
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<?
	if ($arItem['PACKAGE']['PROPERTY_TYPE_ENUM_ID'] == 15) {
		$isTypePackage = true;
		?>
	<div class = "content-pack active">
		<h5 class="tarif_add_pack__title" id="<?=$this->GetEditAreaId($arItem['ID']);?>"><?=$arItem['PACKAGE']['NAME']?></h5>
		<span class="tarif_add_pack__title_desc"><?=$prop['DESCRIPTION_RU']['VALUE']['TEXT']?></span>
		<div class="tarif_add_pack__cont">
			<ul class="tarif_add_pack__list">
				<?
				$count = count($arItem['CHANNELS']);
				for ($i = 0; $i < 9; $i++) {?> 
				<?
					if ($i == $count)
						break;
					$item = $arItem['CHANNELS'][$i];
					?>
					<li class="tarif_add_pack__list_li"><a href="/channel/<?=$item['ID']?>/"><?=$item['NAME']?></a></li>
				<?}?>
				<?
				
				if (count($arItem['CHANNELS']) > 9) {?>
				<li class="tarif_add_pack__list_li tarif_add_pack__list_li--more_packs">
					<span class="add_packs_box__count add_packs_box__count--tarif_add">
						<?
						$count = 9;
						?>		
						Ещё <?=(count($arItem['CHANNELS']) - 9)?> <?=DeclensionNameChannel(count($arItem['CHANNELS']) - 9)?>
						<span class="add_packs_box__hidden">
						<div class="add_packs_box__hidden_scroll">
							<?for ($i = $count; $i < count($arItem['CHANNELS']); $i++) {?>
							<?
							$item = $arItem['CHANNELS'][$i];
							?>
							
							<span class="add_packs_box__hidden_item">
								<?if (!empty($item['PREVIEW_PICTURE']['src'])) {?>
								<span class="add_packs_box__hidden_pic">
									<img src="<?=$item['PREVIEW_PICTURE']['src']?>" alt="<?=$item['NAME']?>">
								</span>
								<?}else
								{?>
								<span class="add_packs_box__hidden_pic add_packs_box__hidden_pic--noimg">
									<span><?=substr($item['NAME'], 0, 1)?></span>
								</span>
								<?}?>
								<a href="/channel/<?=$item['ID']?>/" class="add_packs_box__hidden_link"><?=$item['NAME']?></a>
							</span>
							<?}?>
						</div>
						</span>
					</span>
				</li>
				<?}?>
			</ul>
		</div>
		<div class="clear pack_cost__box_bottom">
			<span class="pack_cost pack_cost--tarif_add_pack"><?=$arItem['PACKAGE']['PROPERTY_PRICE_VALUE']?> <span>тг./мес.</span></span>
			<a href="/request/" class="button button-no_width button-tarif_add_pack">Заказать</a>
		</div>
	</div>
	<?}?>

<?endforeach;?>

<?if (!$isTypePackage) {?>
<div class = "content-pack active">
	<h5 class="tarif_add_pack__title">Дополнительные пакеты</h5>
	<span class="tarif_add_pack__title_desc">Нет пакетов в квартиру для данного города</span>
</div>
<?}?>
	
<?
$isTypePackageExp = false;
foreach($arResult["ITEMS"] as $key => $arItem) {?>
	<?
	$prop = $arItem['PROPERTIES'];
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<?if ($arItem['PACKAGE']['PROPERTY_TYPE_ENUM_ID'] == 16) {
		//PR($arItem['PACKAGE']['PROPERTY_TYPE_ENUM_ID']);
		$isTypePackageExp = true;
		?>
	<?//PR($arItem['PACKAGE']['PROPERTY_TYPE_ENUM_ID']);?>
	<div class = "content-pack">
		<h5 class="tarif_add_pack__title" id="<?=$this->GetEditAreaId($arItem['ID']);?>"><?=$arItem['PACKAGE']['NAME']?></h5>
		<span class="tarif_add_pack__title_desc"><?=$prop['DESCRIPTION_RU']['VALUE']['TEXT']?></span>
		<div class="tarif_add_pack__cont">
			<ul class="tarif_add_pack__list">
				<?
				$count = count($arItem['CHANNELS']);
				for ($i = 0; $i < 9; $i++) {?> 
				<?
					if ($i == $count)
						break;
					$item = $arItem['CHANNELS'][$i];
					?>
					<li class="tarif_add_pack__list_li"><a href="/channel/<?=$item['ID']?>/"><?=$item['NAME']?></a></li>
				<?}?>
				<?
				
				if (count($arItem['CHANNELS']) > 9) {?>
				<li class="tarif_add_pack__list_li tarif_add_pack__list_li--more_packs">
					<span class="add_packs_box__count add_packs_box__count--tarif_add">
						<?
						$count = 9;
						?>		
						Ещё <?=(count($arItem['CHANNELS']) - 9)?> <?=DeclensionNameChannel(count($arItem['CHANNELS']) - 9)?>
						<span class="add_packs_box__hidden">
						<div class="add_packs_box__hidden_scroll">
							<?for ($i = $count; $i < count($arItem['CHANNELS']); $i++) {?>
							<?
							$item = $arItem['CHANNELS'][$i];
							?>
							
							<span class="add_packs_box__hidden_item">
								<?if (!empty($item['PREVIEW_PICTURE']['src'])) {?>
								<span class="add_packs_box__hidden_pic">
									<img src="<?=$item['PREVIEW_PICTURE']['src']?>" alt="<?=$item['NAME']?>">
								</span>
								<?}else
								{?>
								<span class="add_packs_box__hidden_pic add_packs_box__hidden_pic--noimg">
									<span><?=substr($item['NAME'], 0, 1)?></span>
								</span>
								<?}?>
								<a href="/channel/<?=$item['ID']?>/" class="add_packs_box__hidden_link"><?=$item['NAME']?></a>
							</span>
							<?}?>
						</div>
						</span>
					</span>
				</li>
				<?}?>
			</ul>
		</div>
		<div class="clear pack_cost__box_bottom">
			<span class="pack_cost pack_cost--tarif_add_pack"><?=$arItem['PACKAGE']['PROPERTY_PRICE_VALUE']?> <span>тг./мес.</span></span>
			<a href="/request/" class="button button-no_width button-tarif_add_pack">Заказать</a>
		</div>
	</div>
	<?}?>
<?}?>

<?if (!$isTypePackageExp) {?>
<div class = "content-pack">
	<h5 class="tarif_add_pack__title">Дополнительные пакеты</h5>
	<span class="tarif_add_pack__title_desc">Нет пакетов в частный дом для данного города</span>
</div>
<?}?>

</div>	
<?} else {?>
	<div class="tarif_add_pack">
		<h5 class="tarif_add_pack__title">Дополнительные пакеты</h5>
		<span class="tarif_add_pack__title_desc">Отсутствуют в данном городе</span>
		<!--<div class = "tarif_add_pack__no_pack"> <img src = "/upload/nemo_tv.png"> </div>-->
	</div>
<?}?>

