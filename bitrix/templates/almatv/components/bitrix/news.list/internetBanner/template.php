<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<h1 class="home_inet">Домашний интернет</h1>
		<div class="inet_list_box">
			<ul class="inet_list">

	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$prop = $arItem['PROPERTIES'];
		
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<?//PR($prop);?>
		<li class="inet_list__item <?=$arItem['PROPERTIES']['CLASS']['VALUE']?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<!--<img src = "<?//=$arItem['arImage']['src']?>"/>-->
			
			<div class="inet_list__item_img">
				<img class="inet_list__item_img1024" src="<?=$arItem['arImage1024']['src']?>" alt="<?=$arItem['NAME']?>1024">
				<img class="inet_list__item_img1600" src="<?=$arItem['arImage1600']['src']?>" alt="<?=$arItem['NAME']?>1600">
			</div>
			
			<h2 class="inet_list__title head"><?=ckopStr($arItem['NAME'], 0, 10)?></h2>
			<span class="inet_list__desc"><?=$arItem['PREVIEW_TEXT']?></span>
			<?if (!empty($prop['HREF']['VALUE'])) {?>
			<a href="<?=$prop['HREF']['VALUE']?>" class="inet_list__btn button"><?=ckopStr($prop['NAME_HREF']['VALUE'], 0, 10)?></a>
			<?}?>
		</li>

	<?endforeach;?>
	</ul>
</div>
