<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<script>
$(document).on('click', '.answer', function(e) {
	e.preventDefault();
	_this = this;
	var $id = $(this).data('questions'); //.parents('.content-container').data('material-id'),
		cookie_date = new Date ( 4000, 01, 01),
		questions = getCookie("Questions");
	//var $name = $(this).data('name');
	//document.cookie = "AnswerName=" + $name + "; path=/; expires=" + cookie_date.toGMTString();
	
	//console.log(questions);
	if (questions !== undefined || questions == '')
	{
		var arr_questions_num = questions.toString().split(','),
		f_l = false;

		for (var i = 0; i < arr_questions_num.length; i++) {
			if (arr_questions_num[i] == $id)
			{
			  f_l = true;
			  break;
			}
		}
		if (!f_l)
		{
			document.cookie = "Questions=" + questions + $id + ',' + "; path=/; expires=" + cookie_date.toGMTString();
			click_answer(_this);
		}
	}
	else
	{
		document.cookie = "Questions=" + $id + ',' + "; path=/; expires=" + cookie_date.toGMTString();
		click_answer(_this);
	}
return false;
});

	function click_answer(_this)
	{
		$answer = $(_this).data('answer');
		$questions = $(_this).data('questions');
		$.ajax({
			type: 'POST',
			url: '/ajax/add_voices.php',
			data: ({answer: $answer, questions: $questions}),
			success: function(ob)
			{
				$('.interview').html(ob);
			}
		});
	}
</script>

<?foreach($arResult["ITEMS"] as $arItem):?>

<?
$count = count($arResult["ANSWERS"]);
if ($count <= 7) {?>

<div class="ususal_box">
	<div class="box box--rating">
		<div class="rating">
			<div class="rating__title_box clear">
				<h4 class="rating__title"><?=$arItem['NAME']?></h4>
				<span class="rating__desc"><?=$arItem['PREVIEW_TEXT']?></span>
			</div>
			<div class="rating__list_holder">
				<ul class="rating__list">
					<?foreach ($arResult["ANSWERS"] as $key => $answersItem) {?>
						<li class="rating__list_item">
							<a class = "answer" data-name = "<?=$answersItem['NAME']?>" data-questions = "<?=$arItem['ID']?>" data-answer = "<?=$key?>" class="rating__list_item_link" href="#"><img class="rating__list_item_img" src="<?=$answersItem['PICTURE']?>" alt="<?=$answersItem['NAME']?>"></a>
							<a style = "color: black" class = "answer" data-questions = "<?=$arItem['ID']?>" data-answer = "<?=$key?>"> <span class="rating__list_title"><?=$answersItem['NAME']?></span></a>
						</li>
					<?}?>
				</ul>
			</div>
		</div>
	</div>
</div>
<?} else {?>
<div class="ususal_box">
	<div class="box box--rating">
		<div class="rating">
			<div class="rating__title_box clear">
				<h4 class="rating__title"><?=$arItem['NAME']?></h4>
				<span class="rating__desc"><?=$arItem['PREVIEW_TEXT']?></span>
			</div>
			<div class="rating__list_holder">
				<ul class="rating__list rating__list--wide">
					<?foreach ($arResult["ANSWERS"] as $key => $answersItem) {?>
						<li class="rating__list_item">
							<a data-questions = "<?=$arItem['ID']?>" data-answer = "<?=$key?>" class="rating__list_item_link answer" href="#"><img class="rating__list_item_img" src="<?=$answersItem['PICTURE']?>" alt="<?=$answersItem['NAME']?>"></a>
							<a style = "color: black" data-questions = "<?=$arItem['ID']?>" data-answer = "<?=$key?>" class="answer" href="#"><span class="rating__list_title"><?=$answersItem['NAME']?></span></a>
						</li>
					<?}?>
				</ul>
			</div>
		</div>
	</div>
</div>
<?}?>		

<?endforeach;?>
