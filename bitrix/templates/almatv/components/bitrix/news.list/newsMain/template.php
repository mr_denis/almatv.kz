<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="ususal_box">
    <div class="box box--no_pad box--no_border">
        <div class="news">
            <a class = "news_href" href = "/presscenter/"><h3 class="news__title"><span class="news__title_inner">Новости</span></h3></a>
            <ul class="related_news">
			<?foreach($arResult["ITEMS"] as $key => $arItem) {?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<?/*<li id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="news__list_item <?if ($key >= 3) {?>news__list_item--show1280 <?}?>">
					<h5 class="news__list_item_title"><a class="news__title_link" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></h5>
					<span class="news__list_item_date"><?=$arItem['DISPLAY_ACTIVE_FROM']?></span>
					<span class="news__list_item_link"><?=$arItem['PREVIEW_TEXT']?></span>
				</li>*/?>
				
				<li id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="related_news__item">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="related_news__link">
						<img class="related_news__img" src="<?=$arItem['arImage']['src']?>" alt="<?=$arItem['NAME']?>">
						<span class="related_news_info">
							<h6 class="related_news__title"><?=ckopStr($arItem['NAME'], 0, 40)?></h6>
							<span class="related_news__date"><?=$arItem['ACTIVE_FROM']?></span>
						</span>
					</a>
				</li>	
			<?}?>
			</ul>
		</div>
	</div>
</div>


<?/*<ul class="related_news">
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$prop = $arItem['PROPERTIES'];
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<?
	//PR($arItem['arImage']);
	?>
	<!-- если ссылка не пустая вешаем-->
	<li class="related_news__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="related_news__link">
			<img class="related_news__img" src="<?=$arItem['arImage']['src']?>" alt="<?=$arItem['NAME']?>">
			<span class="related_news_info">
				<h6 class="related_news__title"><?=$arItem['NAME']?></h6>
				<span class="related_news__date"><?=$arItem['ACTIVE_FROM']?></span>
			</span>
		</a>
	</li>		
	<?endforeach;?>
</ul>*/?>
