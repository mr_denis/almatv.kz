<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if (count($arResult["ITEMS"]) > 0) {?>
<div class="footer_tels">
	<?foreach($arResult["ITEMS"] as $arItem) {?>
		<?
		//PR($arItem['PROPERTIES']);
		$phone_quality = $arItem['PROPERTIES']['PHONE_QUALITY'];
		$phone_dev = $arItem['PROPERTIES']['PHONE_DEV'];
		
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<?if (count($phone_quality['VALUE']) > 0) {?>
			<div class="footer_tels__left">
				<?foreach ($phone_quality['VALUE'] as $itemPhone) {?>
				<span class="footer_tels__num"><?=$itemPhone?></span>
				<?}?>
				<span class="footer_tels__link">Служба качества</span>
			</div>
			<?}?>
			
			<?if (count($phone_dev['VALUE']) > 0) {?> 
			<div class="footer_tels__right">
				<?foreach ($phone_dev['VALUE'] as $itemPhone) {?>
				<span class="footer_tels__num"><span><?=$itemPhone?></span></span>
				<?}?>
				<span class="footer_tels__link">Служба поддержки</span>
			</div>
			<?}?>
		</div>
	</div>
	<?}?>
</div>
<?}?>
