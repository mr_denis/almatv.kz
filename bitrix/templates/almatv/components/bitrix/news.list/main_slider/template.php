<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//PR($arResult["ITEMS"]);?>
<div class="main_slider_holder">
	<div class="main_slider" id="j-main_slider">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$prop = $arItem['PROPERTIES'];
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		
		$isHref = empty($prop['HREF']['VALUE']);
		$type = $prop['TYPE']['VALUE_ENUM_ID'];
		?>
		<!-- если ссылка не пустая вешаем-->

		<div class="main_slider__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<?if (!$isHref && ($type == 6)) {?>
				<a href = "<?=$prop['HREF']['VALUE']?>">
			<?}?>
			<img class="main_slider__img" src="<?=$arItem['arImage']['src']?>" alt="<?=$arItem['NAME']?>">
			<?$prop = $arItem['PROPERTIES'];?>
			
			<div class="main_slider__text">
			
				<?if (!$isHref && ($type == 5)) {?>
					<a href = "<?=$prop['HREF']['VALUE']?>">
				<?}?>
				<h5 class="main_slider__title"><?=$arItem['PREVIEW_TEXT']?></h5>
				<?if (!$isHref && ($type == 5)) {?>
					</a>
				<?}?>
				
				<?=$arItem['DETAIL_TEXT']?>

				<?/*if (!empty($arItem["ACTIVE_TO"])) {?>
				<?$deltaUnix = strtotime($arItem["ACTIVE_TO"]) - strtotime(date("d.m.Y H:i:s"));
					$delta['days'] = (int)($deltaUnix / (3600 * 24));
					$delta['hours'] = (int)(($deltaUnix - $delta['days'] * 3600 * 24) / 3600);
					$delta['minutes'] = (int)(($deltaUnix - $delta['days'] * 3600 * 24 -  $delta['hours'] * 3600) / 60);
					?>
					<div class="timer">
						<div class="timer__item">
							<div class="timer__num">
								<span><?=(int)$delta['days']?></span>
							</div>
							<span class="timer__deep">дн.</span>
						</div>
						<div class="timer__item">
							<div class="timer__num">
								<span><?=$delta['hours']?></span>
							</div>
							<span class="timer__deep">час.</span>
						</div>
						<div class="timer__item">
							<div class="timer__num">
								<span><?=$delta['minutes']?></span>
							</div>
							<span class="timer__deep">мин.</span>
						</div>
					</div>
				<?}*/?>
			</div>
			<?if (!$isHref && ($type == 6)) {?>
				</a>
			<?}?>
		</div>
	<?endforeach;?>
	</div>
	<div class="main_slider_left" id="j-main_slider_left">
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="17.53px" height="33.09px" viewBox="0 0 17.53 33.09" enable-background="new 0 0 17.53 33.09" xml:space="preserve">
			<g><g><path class="slider_arrow" fill="#FFFFFF" d="M2.401,16.545L17.25,1.696c0.391-0.391,0.391-1.023,0-1.414s-1.023-0.391-1.414,0L0.28,15.838 c-0.391,0.39-0.391,1.023,0,1.414l15.556,15.556c0.391,0.391,1.023,0.391,1.414,0s0.391-1.023,0-1.414L2.401,16.545z"/></g></g>
		</svg>
	</div>
	<div class="main_slider_right" id="j-main_slider_right">
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="17.53px" height="33.09px" viewBox="0 0 17.53 33.09" enable-background="new 0 0 17.53 33.09" xml:space="preserve">
			<g><g><path class="slider_arrow" fill="#FFFFFF" d="M15.128,16.545L0.279,1.696c-0.391-0.391-0.391-1.023,0-1.414s1.023-0.391,1.414,0L17.25,15.838 c0.391,0.39,0.391,1.023,0,1.414L1.693,32.809c-0.391,0.391-1.023,0.391-1.414,0s-0.391-1.023,0-1.414L15.128,16.545z"/></g></g>
		</svg>
	</div>
	<div class="main_slider__paging" id="j-main_slider__paging"></div>
</div>
