<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<div class="popup popup_city" id="j-popup_city">
	<!--<div class="close_popup"></div>-->
	<div class="popup_city-content">
		<div class="popup_city-head">Выберите свой город</div>
		<form>
			<ul class="head_location__list">
				<?
				$count = ceil((int)count($arResult["ITEMS"]) / 3);
				for($j = 0; $j < 3; $j++) {?>
				<li class="head_location__list_item">
					<?for ($i = $j * $count; $i < ($j + 1) * $count; $i++) {
						$arItem = $arResult["ITEMS"][$i];
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
						?>
						<?if (!empty($arItem['NAME'])) {?>
						<div class="head_location__radio_box">
							<a data-name="<?=$arItem['NAME']?>" data-id="<?=$arItem['ID']?>" href="?city=<?=$arItem['ID']?>" class="head_location__link"><?=$arItem['NAME']?></a>
						</div>
						<?}?>
					<?}?>
				</li>
				<?}?>
			</ul>
		</form>
	</div>
</div>
