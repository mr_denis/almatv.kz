<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="press_list_holder">
	<ul class="press_list">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

		$arrDate = explode(' ', $arItem['DISPLAY_ACTIVE_FROM']);
		$d = $arrDate[0];
		$m = $arrDate[1];
		?>
		<li class="press_list__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="press_list__img_box">
				<?if (!empty($arItem['arImage']['src']))?><img src="<?=$arItem['arImage']['src']?>" class="press_list__img">
				<span class="press_list__date"><span><?=$d?></span><?=$m?></span>
			</div>
			<div class="press_list__cont">
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="press_list__link"><?=$arItem["NAME"]?></a>
				<span class="press_list__text"><?=$arItem["PREVIEW_TEXT"]?></span>
			</div>
		</li>
	<?endforeach;?>
	</ul>
</div>
