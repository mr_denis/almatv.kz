<?
foreach ($arResult["ITEMS"] as &$arItem)
{	 
	$arItem['arImage'] = CFile::ResizeImageGet(
		$arItem["PREVIEW_PICTURE"]['ID'],
		array("width" => 168, "height" =>168),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		true,
		false
	);
	
	$prop = $arItem['PROPERTIES'];
	$path = CFile::GetPath($prop['FILE']['VALUE']);
	$filename = $_SERVER["DOCUMENT_ROOT"].CFile::GetPath($prop['FILE']['VALUE']);
	
	$arItem['_FILE_'] = array(
		"PATH" => $path,
		"EXP" => substr(strrchr($filename, '.'), 1),
		"FILESIZE" => round(filesize($filename)/1024, 2),
	);
}
?> 