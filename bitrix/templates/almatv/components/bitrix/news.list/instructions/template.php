<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<h3>Инструкции</h3>
<div class="clear">
	<?$count = count($arResult["ITEMS"]);
	$part = ceil($count / 2);
	?>
	<ul class="docs docs--left">
	<?for ($i = 0; $i < $part; $i++) {?>
		<?
		$arItem = $arResult["ITEMS"][$i];
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		
		switch ($arItem['_FILE_']['EXP'])
		{
			case 'doc':
				$class = "docs__list--doc";
				break;
			case 'pdf':
				$class = "docs__list--pdf";
				break;
		}
		
		?>
		<li class="docs__list <?=$class?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<strong class="docs__name">
			<?if (!empty($arItem['PROPERTIES']['FILE']['VALUE'])) {?>
				<a target = "_blank" href="<?=$arItem['_FILE_']['PATH']?>" class="docs__name_inner"><?=$arItem['NAME']?></a>
			<?} else {?>
				<?=$arItem['NAME']?>
			<?}?>
			</strong>
			<?if (!empty($arItem['PROPERTIES']['FILE']['VALUE'])) {?>
			<a href="<?=$arItem['_FILE_']['PATH']?>" class="docs__zip">.<?=$arItem['_FILE_']['EXP']?>, <?=$arItem['_FILE_']['FILESIZE']?> Кб</a>
			<?}?>
		</li>
	<?}?>
	</ul>
	<?if ($part >= 1) {?>
	<ul class="docs docs--right">
		<?for ($j = $i; $j < $count; $j++) {?>
			<?
			$arItem = $arResult["ITEMS"][$j];
			
			switch ($arItem['_FILE_']['EXP'])
			{
				case 'doc':
					$class = "docs__list--doc";
					break;
				case 'pdf':
					$class = "docs__list--pdf";
					break;
			}
			
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<li class="docs__list <?=$class?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<strong class="docs__name">
					<?if (!empty($arItem['PROPERTIES']['FILE']['VALUE'])) {?>
						<a target = "_blank" href="<?=$arItem['_FILE_']['PATH']?>" class="docs__name_inner"><?=$arItem['NAME']?></a>
					<?} else {?>
						<?=$arItem['NAME']?>
					<?}?>
				</strong>
				<?if (!empty($arItem['PROPERTIES']['FILE']['VALUE'])) {?>
				<a target = "_blank" href="<?=$arItem['_FILE_']['PATH']?>" class="docs__zip">.<?=$arItem['_FILE_']['EXP']?>, <?=$arItem['_FILE_']['FILESIZE']?> Кб</a>
				<?}?>
			</li>
		<?}?>
	<?}?>
	</ul>
</div>
	
	
	
