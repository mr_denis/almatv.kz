<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (count($arResult['ITEMS']) > 0) {?>
<ul class="today_programm">
	<?
	$dateTime = time();
	//PR(date('m.d.Y H:i:s'));
	foreach ($arResult['ITEMS'] as $arItem) {
		$prop = $arItem['PROPERTIES'];
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

		$dateFromUnix = strtotime($prop['DATE_FROM']['VALUE']);
		$dateToUnix = strtotime($prop['DATE_TO']['VALUE']);
		//PR(date("d.m.Y H:i:s", $dateFromUnix));

		$class = 'today_programm__item--is_past';
		if (($dateTime >= $dateFromUnix) && ($dateTime <= $dateToUnix))
		{
			$class = 'today_programm__item--is_current';
		}
		elseif ($dateTime > $dateToUnix)
		{
			$class = 'today_programm__item--is_next';
		}
	?>
	<li class="today_programm__item <?=$class?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<span class="today_programm__time"><?=date("H:i", strtotime($arItem['PROPERTIES']['DATE_FROM']['VALUE']))?></span>
		<div class="today_programm__name" data-id = "<?=$arItem['ID']?>">
			<span><?=$arItem['NAME']?></span>
		</div>
	</li>
	<?}?>
</ul>
<?} else {?>
	Список пуст
<?}?>