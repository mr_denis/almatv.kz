<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if (count($arResult["ITEMS"])) {?>
<div class="box box--package_tv_home">
<div class="package_intro">
	<a class = "decoration-none" href = "/tv/"> <h3 class="package_intro__title package_intro__title--tv">ТВ в частный дом</h3> </a>
	<table class="package_intro__tab">

<?foreach($arResult["ITEMS"] as $key => $arItem):?>
	<?
	$prop = $arItem['PROPERTIES'];
	$recommend = $prop['RECOMMEND'];

	$class_reccomend = '';
	if ($recommend['VALUE_ENUM_ID'][0] == 12)
	{
		$class_reccomend = 'package_intro__pack_name--recomm';
	}

	switch ($key)
	{
		case 0:
			$style = '--yel';
			break;
		case 1:
			$style = '--orange_l';
			break;
		case 2:
			$style = '--orange';
			break;
	}
	
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<tr id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<td>
		<?
			$name = $arItem['NAME'];
		?>
			<a class = "channel_button" data-id="<?=$arItem['ID']?>" style = "text-decoration: none" href = "#"> <h5 class="package_intro__pack_name package_intro__pack_name<?=$style?> <?=$class_reccomend?>"><?=$name;?> <div class="tarif_row__list_all_channels_tip">Посмотреть наполнение</div></h5><a>
		</td>
		<td>
			<span class="package_intro__pack_info"><span><?=count($arItem['CHANNELS'])?></span> <?=DeclensionNameChannel(count($arItem['CHANNELS']))?></span>
		</td>
		<td>
			<span class="package_intro__pack_info"><span><?=$prop['PRICE']['VALUE']?></span> тг./мес.</span>
		</td>
	</tr>
<?endforeach;?>
	</table>
</div>
</div>
<?} else {
	if ($_SESSION["ONE_TV"])
	{?>
		<script>
			$(function()
			{
				$('#one-tv').addClass('m-ususal_box--one');
			});
		</script>
	<?}else {?>
	<div class="box box--package_int box--image"><!-- 518x454 -->
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH."/include/centerMain.php",
				"EDIT_TEMPLATE" => ""
			)
		);?>	
	</div>
	<?}?>
<?}?>
