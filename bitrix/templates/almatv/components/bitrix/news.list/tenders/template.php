<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?//PR($arResult["ITEMS"]);?>

<?
global $USER;
if ($USER->IsAdmin()) 
{?>
	<a id = "a-tenders" href = "#"> Запустить скрипт обновления</a> <br /><br /><br />
<?}?>
	
<?if (count($arResult["ITEMS"])) {?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include", 
		".default", 
		array(
			"AREA_FILE_SHOW" => "file",
			"PATH" => SITE_TEMPLATE_PATH."/include/tenders_y.php",
			"EDIT_TEMPLATE" => ""
		),
		false
	);?>
	
	<ul class="tender_list">
		<?foreach($arResult["ITEMS"] as $key => $arItem) {?>
		<?
		$prop = $arItem['PROPERTIES'];
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<li class="tender_list__item">
			<span class="tender_list__id">Тендер #<?=$prop['NUMBER']['VALUE']?></span>
			<div class="tender_list__content">
				<ul class="tender_list__chapter">
					<li class="tender_list__chapter_item">
						<h2 class="tender_list__title"><?=$arItem['NAME']?></h2>
						<span class="tender_list__title_desc"><?=$prop['CATEGORY']['VALUE']?></span>
						<span class="tender_list__title_desc"><?=$prop['TYPE']['VALUE']?></span>
					</li>
					<li class="tender_list__chapter_item">
						<span><?=$arItem['PREVIEW_TEXT']?></span>
					</li>
					<li class="tender_list__chapter_item">
						<?if (!empty($arItem['ACTIVE_FROM'])) {?>
						<div class="tender_list__dates">
							<h6 class="tender_list__dates_title">Дата начала</h6>
							<span class="tender_list__dates_calendar"><?=date("d.m.Y", strtotime($arItem['ACTIVE_FROM']))?></span>
							<span class="tender_list__dates_time"><?=date("h:s", strtotime($arItem['ACTIVE_FROM']))?></span>
						</div>
						<?}?>
						
						<?if (!empty($arItem['DATE_ACTIVE_TO'])) {?>
						<div class="tender_list__dates">
							<h6 class="tender_list__dates_title tender_list__dates_title--end">Дата окончания</h6>
							<span class="tender_list__dates_calendar"><?=date("d.m.Y", strtotime($arItem['DATE_ACTIVE_TO']))?></span>
							<span class="tender_list__dates_time"><?=date("h:s", strtotime($arItem['DATE_ACTIVE_TO']))?></span>
						</div>
						<?}?>
					</li>
				</ul>
			</div>
			<div class="tender_list__price_box">
				<div class="tender_list__price">
					<span class="tender_list__price_num"><?=$prop['SUM']['VALUE']?></span>
					<span class="tender_list__price_cur">Тенге</span>
				</div>
				<?/*<a href="#" class="button button--tender_list dev">Отправить заявку</a>*/?>
			</div>
		</li>
		<?}?>
	</ul>
<?} else {
	$APPLICATION->IncludeComponent(
		"bitrix:main.include", 
		".default", 
		array(
			"AREA_FILE_SHOW" => "file",
			"PATH" => SITE_TEMPLATE_PATH."/include/tenders_n.php",
			"EDIT_TEMPLATE" => ""
		),
		false
	);
}?>

