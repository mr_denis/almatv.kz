<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<div class="inet_pluses_sliderbox">
	<div class="inet_pluses_slider" id="j-inet_pluses_slider">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		
		<div class="inet_pluses_slider__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="inet_pluses_slider__item_img">
				<img src="<?=$arItem['arImage']['src']?>" alt="image_description">
			</div>
			<h5 class="inet_pluses_slider__item_title"><?=ckopStr($arItem['NAME'], 0, 20)?></h5>
			<span class="inet_pluses_slider__item_desc"><?=$arItem['PREVIEW_TEXT']?></span>
		</div>
	<?endforeach;?>
	</div>
	<div class="pluses_slider_left" id="j-inet_pluses_slider_left"></div>
	<div class="pluses_slider_right" id="j-inet_pluses_slider_right"></div>
</div>