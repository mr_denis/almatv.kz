<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if (count($arResult["ITEMS"]) > 0) {?>
<div class="info_row">
<label class="user_data__label">Дополнительные пакеты</label>
				
				
<ul class="add_packs_box">
	<li class="add_packs_box__item">					
	<?foreach($arResult["ITEMS"] as $key => $arItem):?>
		<?
		//PR($arItem);
		$prop = $arItem['PROPERTIES']; 
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		
		<div class="add_packs_box__tv">
			<label class="add_packs_box__label" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<input type="checkbox" data-id = "<?=$arItem['ID']?>" data-name = "<?=$arItem['NAME']?>">
				<?if (!empty($arItem['PREVIEW_PICTURE']['SRC'])) {?>
				<span class="add_packs_box__pic">
					<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" height="14" width="22"  alt="<?=$arItem['NAME']?>">
				</span>
				<?}?>
				<span class="add_packs_box__name"><?=$arItem['NAME']?> <?//=$arItem['ID']?></span>
			</label>
			<span class="add_packs_box__count" data-id = "<?=$arItem['ID']?>">
				(<?=count($arItem['CHANNELS'])?> канал.)
				<?if (count($arItem['CHANNELS']) > 0) {?>
				<span class="add_packs_box__hidden">
				<div class="add_packs_box__hidden_scroll">
					<?foreach ($arItem['CHANNELS'] as $channelItem) {?>	
					<span class="add_packs_box__hidden_item">
							<?if (!empty($channelItem['PREVIEW_PICTURE'])) {?>
							<span class="add_packs_box__hidden_pic">
								<img src="<?=$channelItem['PREVIEW_PICTURE']?>" alt="<?=$channelItem['NAME']?>">
							</span>
							<?}else{?>
							<span class="add_packs_box__hidden_pic add_packs_box__hidden_pic--noimg">
								<span><?=substr($channelItem['NAME'], 0, 1)?></span>
							</span>
							<?}?>
						<a href="/channel/<?=$channelItem['ID']?>/" class="add_packs_box__hidden_link"><?=$channelItem['NAME']?></a>
					</span>
					<?}?>
				</div>
				</span>
				<?}?>
			</span>
		</div>

	<?endforeach;?>

	</li>
</ul>
</div>
<?}?>