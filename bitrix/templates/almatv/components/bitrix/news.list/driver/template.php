<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<h3>Драйверы для модемов</h3>
<ul class="prod_list">
	<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?><br />
	<?endif;?>
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<li class="prod_list__item" id="<?=$this->GetEditAreaId($arItem['ID'])?>">
			<a href="<?=$arItem['_FILE_']['PATH']?>" target = "_blank" class="prod_list__img"><img src="<?=$arItem['arImage']['src']?>" alt="<?=$arItem['NAME']?>"></a>
			<div class="prod_list__cont">
				<h5 class="prod_list__name">
					<?if (!empty($arItem['PROPERTIES']['FILE']['VALUE'])) {?>
						<a target = "_blank" href="<?=$arItem['_FILE_']['PATH']?>"><?=$arItem['NAME']?></a>
					<?} else {?>
						<?=$arItem['NAME']?>
					<?}?>
				</h5>
				<?if (!empty($arItem['PROPERTIES']['OC']['VALUE'])) {?>
					<span class="prod_list__add"><?=$arItem['PROPERTIES']['OC']['VALUE']?></span>
				<?}?>
				<?if (!empty($arItem['PROPERTIES']['FILE']['VALUE'])) {?>
					<a target = "_blank" href="<?=$arItem['_FILE_']['PATH']?>" class="prod_list__zip">.<?=$arItem['_FILE_']['EXP']?>, <?=$arItem['_FILE_']['FILESIZE']?> Кб</a>
				<?}?>
			</div>
		</li>
	<?endforeach;?>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<br /><?=$arResult["NAV_STRING"]?>
	<?endif;?>
</ul>
