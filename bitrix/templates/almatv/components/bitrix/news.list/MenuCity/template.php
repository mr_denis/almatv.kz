<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<form>
<fieldset>
	<span class="head_location__title">Выберите свой город</span>
	<ul class="head_location__list">
<?
$count = ceil((int)count($arResult["ITEMS"]) / 3);
?>	
	
<?for($j = 0; $j < 3; $j++) {?>
	<li class="head_location__list_item">
	<?
	for ($i = $j * $count; $i < ($j + 1) * $count; $i++)
	{
		$arItem = $arResult["ITEMS"][$i];
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		$prop = $arItem['PROPERTIES'];
		
		//$phone_quality = $arItem['PROPERTIES']['PHONE_QUALITY'];
		//$phone_dev = $arItem['PROPERTIES']['PHONE_DEV'];
		//onClick = "document.location.href = '/?city=<?=$arItem['ID']
		?>
		
		<?if (!empty($arItem['NAME'])) {?>
		<a style = "text-decoration: none;" href = "?city=<?=$arItem['ID']?>" > 
				<span data-name = "<?=$arItem['NAME']?>" data-id = "<?=$arItem['ID']?>"  id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="head_location__radio <?if (intval($_COOKIE['City']) == $arItem['ID']) {?>active<?}?>">
					<?=$arItem['NAME']?>
				</span>
				<?php /* <label data-name = "<?=$arItem['NAME']?>" data-id = "<?=$arItem['ID']?>"  id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="head_location__radio <?if (intval($_COOKIE['City']) == $arItem['ID']) {?>active<?}?>">
					<input type="radio" <?if (intval($_COOKIE['City']) == $arItem['ID']) {?> checked <?}?>  name="location_city" class="location_city"><?=$arItem['NAME']?>
				</label> */?>
				<?/*<label class="head_location__radio">
					<input type="radio" <?if (intval($_COOKIE['City']) == $arItem['ID']) {?> checked <?}?>  name="location_city" class="location_city">
				<?=$arItem['NAME']?></label>*/?>
			</a>
		<?}	
	}
	?>
	</li>
<? } ?>
	</ul>
</fieldset>
</form>

<?
//исключаем лишние запросы(не выводим телефоны в компоненте напрямую)
$arFilter = Array("IBLOCK_ID"=>8, "ACTIVE"=>"Y", "ID" => $_COOKIE['City']);  //пакеты в квартиру
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, array('ID', 'NAME', 'PROPERTY_PHONE_DEV', 'PROPERTY_PHONE_QUALITY'));
while($ob = $res->GetNext())
{
	$arrPhoneQuality[] = $ob['PROPERTY_PHONE_QUALITY_VALUE'];
	$arrPhoneDev[] = $ob['PROPERTY_PHONE_DEV_VALUE'];
}
?>

<?
$this->SetViewTarget('phoneHeader');
$digitsItemPhone = preg_replace("/[^0-9+]/", "", $arrPhoneDev[0]);?>
<div class="support_phone">
	<a href="tel:<?=$digitsItemPhone?>"><span class="support_phone__tel"><span><?=$arrPhoneDev[0]?></span></span></a>
	<span class="support_phone__link">Служба поддержки</span>
</div>
<?$this->EndViewTarget();?> 

<?$this->SetViewTarget('phoneFooter');?>
<div class="footer_tels footer_tels--onwider">
	<?if (count($arrPhoneQuality) > 0) {?>
	<div class="footer_tels__left footer_tels__left--no_float">
		<?foreach ($arrPhoneQuality as $itemPhone) {
			$digitsItemPhone = preg_replace("/[^0-9+]/", "", $itemPhone);?> 
		<a href="tel:<?=$digitsItemPhone?>"> <span class="footer_tels__num"><?=$itemPhone?></span></a>
		<?}?>
		<span class="footer_tels__link">Служба качества</span>
	</div>
	<?}?>
	
	<?if (count($arrPhoneDev) > 0) {?> 
	<div class="footer_tels__right footer_tels__right--no_float">
		<?foreach ($arrPhoneDev as $itemPhone) {
			$digitsItemPhone = preg_replace("/[^0-9+]/", "", $itemPhone);?>
		<a href="tel:<?=$digitsItemPhone?>"><span class="footer_tels__num"><span><?=$itemPhone?></span></span></a>
		<?}?>
		<span class="footer_tels__link">Служба поддержки</span>
	</div>
	<?}?>
</div>
<?$this->EndViewTarget();?> 

<?$this->SetViewTarget('phoneFooter1280');?>
	<div class="footer_tels">
	<?if (count($arrPhoneQuality) > 0) {?>
	<div class="footer_tels__left">
		<?foreach ($arrPhoneQuality as $itemPhone) {
			$digitsItemPhone = preg_replace("/[^0-9+]/", "", $itemPhone);?>
		<a href="tel:<?=$digitsItemPhone?>"><span class="footer_tels__num"><?=$itemPhone?></span></a>
		<?}?>
		<span class="footer_tels__link">Служба качества</span>
	</div>
	<?}?>

	<?if (count($arrPhoneDev) > 0) {?> 
	<div class="footer_tels__right">
		<?foreach ($arrPhoneDev as $itemPhone) {
			$digitsItemPhone = preg_replace("/[^0-9+]/", "", $itemPhone);?>
		<a href="tel:<?=$digitsItemPhone?>"><span class="footer_tels__num"><span><?=$itemPhone?></span></span></a>
		<?}?>
		<span class="footer_tels__link">Служба поддержки</span>
	</div>
	<?}?>
	</div>
<?$this->EndViewTarget();?> 
