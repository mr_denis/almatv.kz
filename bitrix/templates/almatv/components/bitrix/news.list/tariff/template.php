<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if (count($arResult["ITEMS"])) {?>

<div class="tarif_holder">
	<ul class="tarif_cols">
		<li class="tarif_cols__item">Название тарифа</li>
		<li class="tarif_cols__item">
			<span class="tarif_cols__item1024">Скорость</span>
			<span class="tarif_cols__item1280">Скорость на внешние ресурсы</span>
		</li>
		<li class="tarif_cols__item">Абонентская плата</li>
		<li class="tarif_cols__item"></li>
	</ul>


<?foreach($arResult["ITEMS"] as $key => $arItem):?>
	<?
	$prop = $arItem['PROPERTIES'];
	//PR($prop);
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<?
	$class = '';
	switch ($key)
	{
		case 0:
			break;
		case 1:
			$class = 'tarif__name--light_blue';
			break;
		case 2:
			$class = 'tarif__name--blue';
			break;
		case 3:
			$class = 'tarif__name--purple';
			break;
		default:
			$class = '';
	}
	$class_reccomend = '';
	$recommend = $prop['RECOMMEND'];
	if ($recommend['VALUE_ENUM_ID'] == 13)
	{
		$class_reccomend = 'tarif__name_holder--best';
	}
	
	$active = '';
	if ($_REQUEST['ELEMENT_ID'] == $arItem['ID'])
	{
		$active = 'active';
	}
	
	?>
	<div class="tarif" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<ul class="tarif__table">
			<li class="tarif__table_item">
				<div class="tarif__name_holder <?=$class_reccomend?>">
					<span class="tarif__name <?=$active?> <?=$class?>"><?=$prop['NAME_RU']['VALUE']//=$arItem['NAME']?></span>
				</div>
				<?if (!empty($prop['DESCRIPTION']['VALUE'])) {?>
				<div class="tarif__name_tip">
					<em><?=$prop['DESCRIPTION']['VALUE']?></em>
				</div>
				<?}?>
			</li>
			<li class="tarif__table_item">
				<div class="tarif__speed_img">
					<img src="<?=$arItem['PICTURE']['src']?>" alt="<?=$arItem['NAME']?>">
				</div>
				<span class="tarif__speed"><?=$prop['V']['VALUE']?> <span>Мбит/с</span></span>
			</li>
			<li class="tarif__table_item">
				<span class="tarif__cost"><?=$prop["PRICE"]["VALUE"]?> <span>тг./мес.</span></span>
			</li>
			<li class="tarif__table_item">
				<a href="/request/?type=inet&tarif=<?=$arItem['ID']?>" class="button button--no_width">Оформить заявку</a>
			</li>
		</ul>
		<ul class="tarif__hidden_info <?=$active?>">
			<?if (!empty($prop['TECH']['VALUE'])) {?>
			<li class="tarif__hidden_info_item">
				<span class="tarif__hidden_info_left">Технология</span>
				<span class="tarif__hidden_info_right"><?=$prop['TECH']['VALUE']?></span>
			</li>
			<?}?>
			
			<?if (!empty($prop['V_EXT']['VALUE'])) {?>
			<li class="tarif__hidden_info_item">
				<span class="tarif__hidden_info_left">Скорость на внешние ресурсы</span>
				<span class="tarif__hidden_info_right"><?=$prop['V_EXT']['~VALUE']['TEXT']?></span>
			</li>
			<?}?>
			
			<?if (!empty($prop['V_INN']['VALUE'])) {?>
			<li class="tarif__hidden_info_item">
				<span class="tarif__hidden_info_left">Скорость на внутренние ресурсы</span>
				<span class="tarif__hidden_info_right"><?=$prop['V_INN']['VALUE']?></span>
			</li>
			<?}?>
			<?//PR($prop['PURCHASE']['VALUE'])?>
			<?if (!empty($prop['PURCHASE']['VALUE']['TEXT'])) {?>
			<li class="tarif__hidden_info_item">
				<span class="tarif__hidden_info_left">Выкуп оборудования</span>
				<span class="tarif__hidden_info_right"><?=$prop['PURCHASE']['~VALUE']['TEXT']?></span>
			</li>
			<?}?>
			
			<?if (!empty($prop['RENT']['VALUE'])) {?>
			<li class="tarif__hidden_info_item">
				<span class="tarif__hidden_info_left">Аренда оборудования</span>
				<span class="tarif__hidden_info_right"><?=$prop['RENT']['~VALUE']['TEXT']?></span>
			</li>
			<?}?>
			
			<?if (!empty($prop['ANSWER']['VALUE'])) {?>
			<li class="tarif__hidden_info_item">
				<span class="tarif__hidden_info_left">Ответ. хранение оборудования</span>
				<span class="tarif__hidden_info_right">ONT</span>
			</li>
			<?}?>
		</ul>
	</div>
<?endforeach;?>
</div>
<?}?>
