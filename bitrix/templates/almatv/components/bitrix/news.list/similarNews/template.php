<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (count($arResult["ITEMS"]) > 0) {?>
	<h3 class="related_news_title">Похожие новости</h3>
	<ul class="related_news">
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$prop = $arItem['PROPERTIES'];
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<?
	//PR($arItem['arImage']);
	?>
	<!-- если ссылка не пустая вешаем-->
	<li class="related_news__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="related_news__link">
			<img class="related_news__img" src="<?=$arItem['arImage']['src']?>" alt="<?=$arItem['NAME']?>">
			<span class="related_news_info">
				<h6 class="related_news__title"><?=$arItem['NAME']?></h6>
				<span class="related_news__date"><?=$arItem['ACTIVE_FROM']?></span>
			</span>
		</a>
	</li>		
	<?endforeach;?>
	</ul>
<?}?>
