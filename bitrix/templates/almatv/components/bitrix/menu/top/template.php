<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<ul class="main_nav">
	<?$size = 4;?>
	<?for ($i = 0; $i < $size; $i++) {
		$arItem = $arResult[$i];
		$arItem['TEXT'] = ckopStr($arItem['TEXT'], 0, 18);
		?>
		<li class="main_nav__item <?if($arItem["SELECTED"]) {?> selected <?}?>"><a class="main_nav__link punkt<?=$i?>" href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a></li>
	<?}
	?>

	<?if (count($arResult) > 4) {?>
	<li class="main_nav__item main_nav__item--hidden_drop">
	<a class="main_nav__link" href="#">Ещё</a>
		<ul class="main_nav__inner_ul">
			<?for ($j = $size; $j < count($arResult); $j++) {
				$arItem = $arResult[$j];?>	
				<li class="main_nav__inner_item <?if($arItem["SELECTED"]) {?> selected <?}?>"><a class="main_nav__inner_link punkt<?=$i?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<? $i++;} ?>
		</ul>
	</li>	
	<?for ($j = $size; $j < count($arResult); $j++) {?>	
		<?
			$arItem = $arResult[$j];	
			$arItem['TEXT'] = ckopStr($arItem['TEXT'], 0, 18);
		?>
			<li class="main_nav__item main_nav__item--hidden <?if($arItem["SELECTED"]) {?> selected <?}?>"><a class="main_nav__link punkt<?=$i?>" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<? $i++;} ?>
	<?}?>
</ul>
<div hidden class="dev head_search_trigger"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="16.01px" height="15.98px" viewBox="0 0 16.01 15.98" enable-background="new 0 0 16.01 15.98" xml:space="preserve">
<path class="head_search_trigger__svg" fill-rule="evenodd" clip-rule="evenodd" fill="#374448" d="M15.568,15.542c-0.587,0.587-1.539,0.587-2.126,0l-2.653-2.649
	c-1.091,0.699-2.381,1.115-3.772,1.115c-3.875,0-7.016-3.136-7.016-7.005s3.141-7.005,7.016-7.005c3.874,0,7.016,3.136,7.016,7.005
	c0,1.39-0.418,2.678-1.117,3.767l2.653,2.65C16.155,14.006,16.155,14.956,15.568,15.542z M7.017,1.999
	c-2.768,0-5.012,2.24-5.012,5.004s2.244,5.004,5.012,5.004s5.011-2.24,5.011-5.004S9.784,1.999,7.017,1.999z"/>
</svg></div>