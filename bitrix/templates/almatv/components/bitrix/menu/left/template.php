<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="special_menu">

<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>

<?
	$f = true;
	$class_item = "item";
	if (isBlockPayment())
	{
		$class_item = "";
		$f = false;
	}
	
	if (($arItem["LINK"] == '/') && ($f))
		continue;

	$class_fancybox = '';
	$id_selector = '';
	$target = '';
	switch ($arItem["LINK"])
	{
		case '/request/':
			$class = 'special_menu__link--connect';
			break;
		case '/repair/':
			$class = 'special_menu__link--fix';
			break;
		case '/programm/':
			$class = 'special_menu__link--tv_prog';
			break;
		case '/':
			$class = 'special_menu__link--balance j-special_menu__link--balance';
			//$class_fancybox = 'fancybox';
			$id_selector = '#j-popup_balance';
			break;
		case '/drivers/':
			$class = 'special_menu__link--instuction';
			break;
		default:
			$class = 'special_menu__link--update';
			$target = 'target = "_blank"';
	}
	$arItem['TEXT'] = ckopStr($arItem['TEXT'], 0, 15);
?>

	<?if (!$id_selector) {?>
		<?if($arItem["SELECTED"]):?>
			<li class="special_menu__item" <?=$class_item?>><a class="special_menu__link <?=$class?>" <?=$target?> href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a></li>
		<?else:?>
			<li class="special_menu__item <?=$class_item?>"><a class="special_menu__link <?=$class?>" <?=$target?> href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
		<?endif?>
	<?} else {?>
		<?if($arItem["SELECTED"]):?>
			<li class="special_menu__item <?=$class_item?>"><a class="special_menu__link <?=$class?>" <?=$target?> href="<?=$id_selector?>" class="selected"><?=$arItem["TEXT"]?></a></li>
		<?else:?>
			<li class="special_menu__item <?=$class_item?>"><a class="special_menu__link <?=$class?>" <?=$target?> href="<?=$id_selector?>"><?=$arItem["TEXT"]?></a></li>
		<?endif?>
	<?}?>
<?endforeach?>

</ul>
<?endif?>