<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?=$arResult["FORM_NOTE"]?>

<?
/*
name="form_text_21" имя
name="form_text_22" email
name="form_text_23" телефон
name="form_text_24" город
name="form_text_25" адрес
name="form_textarea_26" опишите проблему
*/?>

<?
if(!CModule::IncludeModule("iblock"))
	return;
$arFilter = Array("IBLOCK_ID"=>8);  //запрос по айди пакета вывод каналов в пакете(пакет может быть привязан к 1 категории!!!)
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, array('ID', 'NAME'));
while($ob = $res->GetNext())
{
	$arrItem = array(
		"ID" => $ob['ID'],
		"NAME" => $ob['NAME'],
	);
	$arrCity[] = $arrItem;
}
?>

<?//PR($arResult["QUESTIONS"])?>

<?if ($arResult["isFormNote"] != "Y")
{	
?>
<?=str_replace('name="SIMPLE_FORM_1"','name="SIMPLE_FORM_1" id = "fix_order"', $arResult["FORM_HEADER"]);?>
<fieldset>
	<div class="form_outer">
		<div class="form_inner">
			<span class="req_info">Поля, отмеченные звёздочкой должны быть обязательно заполнены</span>
			<div class="info_row info_row--no_dots">
				<div class="user_data">
					<div class="user_data__row">
						<label class="user_data__label user_data__label--req">Статус</label>
						<select name="form_dropdown_SIMPLE_QUESTION_451" class="user_data__select width-auto">
							<?foreach ($arResult["QUESTIONS"]['SIMPLE_QUESTION_451']['STRUCTURE'] as $item) { ?>
								<option value="<?=$item['ID']?>"><?=$item['MESSAGE']?></option>
							<? } ?>
						</select>			
					</div>
					<div class="user_data__row">
						<label class="user_data__label user_data__label--req">Имя</label>
						<input type="text" name="form_text_21" class="user_data__field user_data__field--valid" value = "<?=$_REQUEST['form_text_21']?>">
					</div>
					<div class="user_data__row">
						<label class="user_data__label user_data__label--req">Ваша эл. почта</label>
						<input type="email" name="form_text_22" class="user_data__field" placeholder="example@mail.ru" value = "<?=$_REQUEST['form_text_22']?>">
					</div>
					<div class="user_data__row">
						<label class="user_data__label user_data__label--req">Контактный номер телефона</label>
						<input type="text" name="form_text_23" class="user_data__field j-user_data__field--phone" placeholder="+7 (     )     –    –" value = "<?=$_REQUEST['form_text_23']?>">
					</div>
					<div class="user_data__row">
						<input type="hidden" name="form_text_24" value = "<?=$arrCity[0]['ID']?>">
						<label class="user_data__label user_data__label--req">Город</label>
						<select id = "requestCity" class="user_data__select width-auto">
							<?foreach ($arrCity as $itemCity) {?>
								<option value="<?=$itemCity['ID']?>"><?=$itemCity['NAME']?></option>
							<?}?>
						</select>
					</div>
					<div class="user_data__row user_data__row--last">
						<label class="user_data__label user_data__label--req">Предполагаемый адрес установки</label>
						<input type="text" name="form_text_25" class="user_data__field" placeholder="ул. Чайковского, д. 37, кв. 82" value = "<?=$_REQUEST['form_text_25']?>">
					</div>
					<div class="user_data__row user_data__row--full">
						<label class="user_data__label user_data__label--req">Опишите проблему</label>
						<textarea class="user_data__textarea" name="form_textarea_26" value = "<?=$_REQUEST['form_textarea_26']?>"></textarea>
					</div>
				</div>
			</div>
			<div class="submit_box">
				<input type="submit" value="Отправить заявку" name="web_form_submit" class="submit_box__btn button">
			</div>
		</div>
	</div>
</fieldset>
<?=$arResult["FORM_FOOTER"]?>
<?
} //endif (isFormNote)
?>