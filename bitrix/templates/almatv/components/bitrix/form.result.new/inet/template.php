<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?=$arResult["FORM_NOTE"]?>

<?
/*
form_dropdown_SIMPLE_QUESTION_117



name="form_text_29" имя
name="form_text_30" email
name="form_text_31" телефон
name="form_text_32" город
name="form_text_33" адрес
name="form_text_34" тарифы
*/?>

<?
if(!CModule::IncludeModule("iblock"))
	return;
$arFilter = Array("IBLOCK_ID"=>8);  //запрос по айди пакета вывод каналов в пакете(пакет может быть привязан к 1 категории!!!)
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, array('ID', 'NAME'));
while($ob = $res->GetNext())
{
	$arrItem = array(
		"ID" => $ob['ID'],
		"NAME" => $ob['NAME'],
	);
	$arrCity[] = $arrItem;
}
?>

<?//PR($arResult["QUESTIONS"])?>

<?if ($arResult["isFormNote"] != "Y")
{	
?>

<script>
	$(document).ready(function() {
		$("#fix_order_internet").validate({
			rules: {
				form_text_29: "required",
				form_text_30 : {
					required: true,
					form_text_30: true
				},
				form_text_31: {
					required: true
				},
				form_text_33: {
					required: true
				}
			}
		});
		$('.j-user_data__field--phone').mask('+7-000-000-000');
		
		$('#requestCityInet').change(function()
		{
			$('input[name=form_text_32]').val($(this).val());
		});
	});
</script>

<?=str_replace('name="SIMPLE_FORM_3"','name="SIMPLE_FORM_3" id = "fix_order_internet"', $arResult["FORM_HEADER"]);?>
<fieldset>
	<div class="form_outer">
		<div class="form_inner">
			<span class="req_info">Поля, отмеченные звёздочкой должны быть обязательно заполнены</span>
			<div class="info_row">
				<div class="user_data">
					<div class="user_data__row">
						<label class="user_data__label user_data__label--req">Статус</label>
						<select name="form_dropdown_SIMPLE_QUESTION_117" class="user_data__select width-auto">
							<?foreach ($arResult["QUESTIONS"]['SIMPLE_QUESTION_117']['STRUCTURE'] as $item) { ?>
								<option value="<?=$item['ID']?>"><?=$item['MESSAGE']?></option>
							<? } ?>
						</select>			
					</div>
					<div class="user_data__row">
						<label class="user_data__label user_data__label--req">Имя</label>
						<input type="text" name="form_text_29" class="user_data__field user_data__field--valid" value = "<?=$_REQUEST['form_text_29']?>">
					</div>
					<div class="user_data__row">
						<label class="user_data__label user_data__label--req">Эл. почта</label>
						<input type="email" name="form_text_30" class="user_data__field" placeholder="example@mail.ru" value = "<?=$_REQUEST['form_text_30']?>">
					</div>
					<div class="user_data__row">
						<label class="user_data__label user_data__label--req">Контактный номер телефона</label>
						<input type="text" name="form_text_31" class="user_data__field j-user_data__field--phone" placeholder="+7 (     )     –    –" value = "<?=$_REQUEST['form_text_31']?>">
					</div>
					<div class="user_data__row">
						<input type="hidden" name="form_text_32" value = "<?=$arrCity[0]['ID']?>">
						<label class="user_data__label user_data__label--req">Город</label>
						<select id = "requestCityInet" class="user_data__select width-auto">
							<?foreach ($arrCity as $itemCity) {?>
								<option value="<?=$itemCity['ID']?>"><?=$itemCity['NAME']?></option>
							<?}?>
						</select>
					</div>
					<div class="user_data__row user_data__row--last">
						<label class="user_data__label user_data__label--req">Предполагаемый адрес установки</label>
						<input type="text" name="form_text_33" class="user_data__field" placeholder="ул. Чайковского, д. 37, кв. 82" value = "<?=$_REQUEST['form_text_33']?>">
					</div>

				</div>
			</div>		

				<style>
					.ip_table tbody tr:hover, .ip_table tbody tr.active, .tarif:hover, .tarif.active{
						background: #e1eef3;
						border-color: transparent;
					}
					.tarif__table_item, .ip_table tbody tr
					{
						cursor: pointer;
					}
					.tarif__table > li:last-child {
						text-align: left;
					}
					
					.tarif__name:after {
						background: none;
					}
					.tarif__name {
						cursor: default;
					}
				</style>
				<script>

					$(document).on('click', '.ip_table tr', function()
					{
						$('.ip_table tr').removeClass('active');
						$(this).addClass('active');
						$('input[name=form_text_35]').val($(this).data('value'));
					});
					
					$(document).on('click', '.tarif', function()
					{
						$('.tarif').removeClass('active');
						$(this).addClass('active');
						$('input[name=form_text_34]').val($(this).data('name') + '[' + $(this).data('value') + ']');
					});

					
					$(document).on('change', '#requestCityInet', function()
					{
						event.preventDefault();
						$.ajax({
							type: 'POST',
							url: '/ajax/tariffRequest.php',  //пакеты
							data: ({id: $(this).val()}),
							success: function(ob)
							{
								$('#contentCity').html(ob);
							}
						});
					});	
						
				</script>

				<input type="hidden" name="form_text_34" class="user_data__field">
			<div id = "contentCity">
			<?
				global $arrFilter;
				$arrFilter['PROPERTY_CITY'] = $arrCity[0]['ID'];
			?>
				
			<?$APPLICATION->IncludeComponent(
				"bitrix:news.list", 
				"tariffRequest", 
				array(
					"IBLOCK_ID" => "18",
					"NEWS_COUNT" => "20",
					"SORT_BY1" => "SORT",
					"SORT_ORDER1" => "DESC",
					"FILTER_NAME" => "arrFilter",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "TYPE",
						1 => "",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "N",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_BROWSER_TITLE" => "Y",
					"SET_META_KEYWORDS" => "Y",
					"SET_META_DESCRIPTION" => "Y",
					"SET_STATUS_404" => "Y",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
					"ADD_SECTIONS_CHAIN" => "Y",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "Y",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"PARENT_SECTION" => "",
					"SORT_BY2" => "NAME",
					"SORT_ORDER2" => "ASC"
				),
				false
			);?>
			</div>
		<input type="hidden" name="form_text_35" class="user_data__field">
		<div class="info_row">
		<label class="user_data__label">Дополнительные опции</label>
		<table class="ip_table">
			<thead>
				<tr>
					<td>Количество IP-адресов</td>
					<td>Единовременная плата</td>
					<td>Абонентская плата</td>
				</tr>
			</thead>
			<tbody>
				<tr data-value = "4">
					<td>
						<span class="ip_table__count"><span>4</span> IP Адреса</span>
					</td>
					<td>
						<span class="ip_table__cost"><span>4 500</span> тг./мес.</span>
					</td>
					<td>
						<span class="ip_table__cost"><span>2 000</span> тг./мес.</span>
					</td>
				</tr>
				<tr data-value = "8">
					<td>
						<span class="ip_table__count"><span>8</span> IP Адресов</span>
					</td>
					<td>
						<span class="ip_table__cost"><span>6 500</span> тг./мес.</span>
					</td>
					<td>
						<span class="ip_table__cost"><span>3 200</span> тг./мес.</span>
					</td>
				</tr>
				<tr data-value = "16">
					<td>
						<span class="ip_table__count"><span>16</span> IP Адресов</span>
					</td>
					<td>
						<span class="ip_table__cost"><span>4 500</span> тг./мес.</span>
					</td>
					<td>
						<span class="ip_table__cost"><span>2 000</span> тг./мес.</span>
					</td>
				</tr>
				<tr data-value = "32">
					<td>
						<span class="ip_table__count"><span>32</span> IP Адреса</span>
					</td>
					<td>
						<span class="ip_table__cost"><span>9 000</span> тг./мес.</span>
					</td>
					<td>
						<span class="ip_table__cost"><span>4 500</span> тг./мес.</span>
					</td>
				</tr>
				<tr data-value = "64">
					<td>
						<span class="ip_table__count"><span>64</span> IP Адреса</span>
					</td>
					<td>
						<span class="ip_table__cost"><span>15 000</span> тг./мес.</span>
					</td>
					<td>
						<span class="ip_table__cost"><span>7 000</span> тг./мес.</span>
					</td>
				</tr>
			</tbody>
		</table>
		</div>
			<div class="submit_box">
				<input type="submit" value="Отправить заявку" name="web_form_submit" class="submit_box__btn button">
			</div>
		</div>
	</div>
</fieldset>
<?=$arResult["FORM_FOOTER"]?>
<?
} //endif (isFormNote)
?>