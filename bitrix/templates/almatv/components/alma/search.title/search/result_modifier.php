<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

//You may customize user card fields to display
$arResult['USER_PROPERTY'] = array(
	"UF_DEPARTMENT",
);

$arIBlocks = array();

$arResult["SEARCH"] = array();
foreach($arResult["CATEGORIES"] as $category_id => $arCategory)
{
	foreach($arCategory["ITEMS"] as $i => $arItem)
	{
		if(isset($arItem["ITEM_ID"]))
			$arResult["SEARCH"][] = &$arResult["CATEGORIES"][$category_id]["ITEMS"][$i];
	}
}

foreach($arResult["SEARCH"] as $i=>$arItem)
{
	$file = false;
	switch($arItem["MODULE_ID"])
	{
		case "socialnetwork":
		case "iblock":
			if(CModule::IncludeModule('iblock'))
			{
				if(!array_key_exists($arItem["PARAM2"], $arIBlocks))
					$arIBlocks[$arItem["PARAM2"]] = CIBlock::GetArrayByID($arItem["PARAM2"]);

				//section /element
				if(substr($arItem["ITEM_ID"], 0, 1) !== "S")
				{
					//Try to find gif by element proprety value xml id
					$rsElement = CIBlockElement::GetList(array(), array(
							"=ID" => $arItem["ITEM_ID"],
							"IBLOCK_ID" => $arItem["PARAM2"],
						),
						false, false, array(
							"ID",
							"IBLOCK_ID",
							"CODE",
							"XML_ID",
							"PROPERTY_DOC_TYPE",
							"PREVIEW_PICTURE"
						)
					);
					$arElement = $rsElement->Fetch();//var_dump($arElement);
					if ($arElement["PREVIEW_PICTURE"]) {
						$image_path =""; 
						//$file = CFile::GetPath($arElement["PREVIEW_PICTURE"]);
						
						$picture = CFile::ResizeImageGet(
							$arElement["PREVIEW_PICTURE"],
							array("width" => 30, "height" =>30),
							BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
							true,
							false
						);
						
						$file = $picture['src'];
					}
					
					//We failed. next try should be element section
					if(!$file)
					{
						$rsSection = CIBlockElement::GetElementGroups($arItem["ITEM_ID"], true);
						$arSection = $rsSection->Fetch();
						if($arSection)
							$SECTION_ID = $arSection["ID"];
					}
					else
					{
						$SECTION_ID = false;
					}
				}
				else
				{
					$SECTION_ID = $arItem["ITEM_ID"];
				}

			}
			break;
	}

	$arResult["SEARCH"][$i]["ICON"] = $image_path.$file;
}

?>