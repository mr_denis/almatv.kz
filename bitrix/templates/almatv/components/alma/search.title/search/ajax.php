<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(!empty($arResult["CATEGORIES"])) {?>
	<?foreach($arResult["CATEGORIES"] as $category_id => $arCategory) {?>
		<?
		$nameCategory = $arCategory['TITLE'];
		?>
		<?//if (!empty($nameCategory) || ($category_id == 'all')) {?>
			<div class="head_search_res_title"><?=$nameCategory?></div>
		<?//}?>
		
		<ul class="head_search_res_list">
		<?foreach($arCategory["ITEMS"] as $i => $arItem){
			?>
			<li class="head_search_res_item">
				<a class="head_search_res_link" href="<?echo $arItem["URL"]?>">
					<?if (!empty($arItem["ICON"])) {?>
					<span class="head_search_res_pic">
						<img src = "<?=$arItem["ICON"]?>" alt = "<?=$arItem['NAME']?>" />
					</span>
					<?}?>
					<span class = "head_search_res_name"><?=$arItem["NAME"]?></span>
				</a>
			</li>
		<?}?>
		</ul>
	<?}?>
<?}?>