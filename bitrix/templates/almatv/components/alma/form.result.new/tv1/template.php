<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<?=$arResult["FORM_NOTE"]?>

<?
/*
name="form_text_4" имя
name="form_text_5" email
name="form_text_6" телефон
name="form_text_7" город
name="form_text_8" у вас частный дом или квартира
name="form_text_9" предпол адрес установки
name="form_text_10" кол-во точек
name="form_text_11" желаемый пакет
name="form_text_12" доп. пакеты
*/?>
<?
$arrCity = $arResult['CITY'];
?>

<script>
	$(document).ready(function() {
		$("#connect_order").validate({
			rules: {
				form_text_4: "required",
				form_text_5 : {
					required: true,
					form_text_5: true
				},
				form_text_6: {
					required: true,
					minlength: 8,
				},
				form_text_9: {
					required: true
				}
			}
		});
		
		//$('.j-user_data__field--phone').mask('+7-000-000-0000');
		$('#requestCity').change(function()
		{
			$('input[name=form_text_7]').val($(this).val());
		});
	});
</script>


<script>
	$(function()
	{
		$('input[name="user_home"]').change(function()
		{
			var val = 'Квартира';
			if ($(this).val() == 'flat')
			{
				var val = 'Квартира';
			}
			if ($(this).val() == 'home')
			{
				var val = 'Частный дом';
			}
			$('input[name=form_text_8]').val(val);
		});
		
		$('#packageTVextra input').change(function()
		{
			var $extraPackages = '';
			$('#packageTVextra input:checkbox:checked').each(function(k, v){
				//console.log(v);
				$extraPackages += $(v).data('name') + '[' + $(v).data('id') + '];';
			});
			$('input[name=form_text_12]').val($extraPackages);
			
			return false;
		});
	});

	$(document).on('change', '#requestCity', function()
	{
		event.preventDefault();
		$.ajax({
			type: 'POST',
			url: '/ajax/packageTVdesired.php',  //пакеты
			data: ({id: $(this).val()}),
			success: function(ob)
			{
				$('#packageTVdesired').html(ob);
			}
		});
		
		
		$.ajax({
			type: 'POST',
			url: '/ajax/packageTVextra.php', //доп пакеты
			data: ({id: $(this).val()}),
			success: function(ob)
			{
				$('#packageTVextra').html(ob);
				$('.cat_drop__list--category,.cat_drop__list_inner--channel, .add_packs_box__hidden_scroll, .chat__dialog_scroll, .popup_channels-box').mCustomScrollbar();
				$('select, input[type="checkbox"], input[type="radio"]').styler({selectSearch: true});
			}
		});
	});

	$(document).on('click', '.get_pack__item', function(event)
	{
		$('input[name=form_text_11]').val(/*$(this).data('pack') + '[' + */$(this).data('id')/* + ']'*/);
		$('.get_pack__item').removeClass('active');
		$(this).addClass('active');
		$('.get_pack_result').val($(this).data('pack'));
	});
</script>

<?//PR($arResult["QUESTIONS"])?>

<?if ($arResult["isFormNote"] != "Y")
{	
$cityId = $arParams['CITY'];
?>
<?=str_replace('name="SIMPLE_FORM_2"','name="SIMPLE_FORM_2" id = "connect_order"', $arResult["FORM_HEADER"]);?>
	<fieldset>
		<div class="form_outer">
			<div class="form_inner">
				<span class="req_info">Поля, отмеченные звёздочкой должны быть обязательно заполнены</span>
				<div class="info_row">
					<div class="user_data">
						<div class="user_data__row">
							<label class="user_data__label user_data__label--req">Источник</label>
							<select name="form_dropdown_STATUS" class="user_data__select width-auto">
									<option value="" disabled >Выберите вариант</option>
									<?foreach ($arResult["QUESTIONS"]['STATUS']['STRUCTURE'] as $item) { ?>
									<option value="<?=$item['ID']?>"><?=$item['MESSAGE']?></option>
								<? } ?>
							</select>
						</div>
						<div class="user_data__row">
							<label class="user_data__label user_data__label--req">Имя</label>
							<input type="text" name="form_text_4" maxlength="40" autocorrect="off" autocapitalize="words" class="user_data__field user_data__field--valid" value = "<?=$_REQUEST['form_text_4']?>">
						</div>
						<div class="user_data__row">
							<label class="user_data__label user_data__label--req">Эл. почта</label>
							<input type="email" name="form_text_5" maxlength="40" class="user_data__field" placeholder="example@mail.ru" value = "<?=$_REQUEST['form_text_5']?>">
						</div>
						<div class="user_data__row">
							<label class="user_data__label user_data__label--req">Контактный номер телефона</label>
							<input type="text" name="form_text_6" autocorrect="off" autocapitalize="words" placeholder="+7 " class="user_data__field j-user_data__field--phone" value = "<?=$_REQUEST['form_text_6']?>">
						</div>
						<div class="user_data__row">
							<label class="user_data__label user_data__label--req">Город</label>
							<select id = "requestCity" class="user_data__select width-auto">
								<?foreach ($arrCity as $itemCity) {?>
									<option <?if ($cityId == $itemCity['ID']) {?> selected <?}?> value="<?=$itemCity['ID']?>"><?=$itemCity['NAME']?></option>
								<?}?>
							</select>
							<input type = "hidden" name = "form_text_7" value = "<?=$cityId?>">
						</div>
						<div class="user_data__row">
							<label class="user_data__label">У вас частный дом или квартира</label>
							<input type = "hidden" name="form_text_8" value = "Квартира">
							<div>
								<label class="user_data__radio">
									<input type="radio" class="j-user_data__radio_input" name="user_home" value="flat" checked>
									Квартира
								</label>
								<label class="user_data__radio">
									<input type="radio" class="j-user_data__radio_input" name="user_home" value="home">
									Частный дом
								</label>
							</div>
						</div>
						<div class="user_data__row">
							<label class="user_data__label user_data__label--req">Предполагаемый адрес установки</label>
							<input type="text" name="form_text_9" maxlength="40" autocorrect="off" autocapitalize="words" class="user_data__field" placeholder="ул. Чайковского, д. 37, кв. 82" value = "<?=$_REQUEST['form_text_9']?>">
						</div>
						<div class="user_data__row">
							<label class="user_data__label user_data__label--req">Кол-во подключаемых точек</label>
							<input type = "hidden" name="form_text_10">
							<select name="form_dropdown_COUNT" class="user_data__select width-auto">
								<?foreach ($arResult["QUESTIONS"]['COUNT']['STRUCTURE'] as $item) { ?>
									<option value="<?=$item['ID']?>"><?=$item['MESSAGE']?></option>
								<? } ?>
							</select>
						</div>
					</div>
				</div>
				
				<input type="hidden" name="form_text_11" value = "<?=$_REQUEST['package']?>">
				<!-- желаемые пакеты --> 
				<div id = "packageTVdesired"> 
				<?
					global $arrFilter;
					$arrFilter['PROPERTY_EXT'] = 17;
					$arrFilter['PROPERTY_CITY'] = $cityId;
				?>
						<?$APPLICATION->IncludeComponent(
					"bitrix:news.list", 
					"packageTVdesired", 
					array(
						"IBLOCK_ID" => "10",
						"NEWS_COUNT" => "20",
						"SORT_BY1" => "SORT",
						"SORT_ORDER1" => "ASC",
						"FILTER_NAME" => "arrFilter",
						"FIELD_CODE" => array(
							0 => "",
							1 => "",
						),
						"PROPERTY_CODE" => array(
							0 => "TYPE",
							1 => "",
						),
						"CHECK_DATES" => "Y",
						"DETAIL_URL" => "",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "N",
						"PREVIEW_TRUNCATE_LEN" => "",
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"SET_TITLE" => "N",
						"SET_BROWSER_TITLE" => "Y",
						"SET_META_KEYWORDS" => "Y",
						"SET_META_DESCRIPTION" => "Y",
						"SET_STATUS_404" => "Y",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
						"ADD_SECTIONS_CHAIN" => "Y",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"PARENT_SECTION_CODE" => "",
						"INCLUDE_SUBSECTIONS" => "Y",
						"DISPLAY_DATE" => "Y",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"PAGER_TEMPLATE" => ".default",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"PAGER_TITLE" => "",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"PARENT_SECTION" => "",
						"SORT_BY2" => "NAME",
						"SORT_ORDER2" => "ASC"
					),
					false
				);?>
				</div>
				
				<input type="hidden" name="form_text_12">
				<!-- доп пакеты -->
				
				<div id = "packageTVextra"> 
				<?
					global $Filter;
					$Filter['PROPERTY_EXT'] = 18;
					if ($_COOKIE['City'])
					{
						$Filter['PROPERTY_CITY'] = $cityId;
					}
					else
					{
						$Filter['PROPERTY_CITY'] = $_REQUEST['city'];
					}
					
					//PR($Filter);
				?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:news.list", 
					"packageTVextra", 
					array(
						"IBLOCK_ID" => "10",
						"NEWS_COUNT" => "20",
						"SORT_BY1" => "SORT",
						"SORT_ORDER1" => "ASC",
						"FILTER_NAME" => "Filter",
						"FIELD_CODE" => array(
							0 => "",
							1 => "",
						),
						"PROPERTY_CODE" => array(
							0 => "TYPE",
							1 => "",
						),
						"CHECK_DATES" => "Y",
						"DETAIL_URL" => "",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "N",
						"PREVIEW_TRUNCATE_LEN" => "",
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"SET_TITLE" => "N",
						"SET_BROWSER_TITLE" => "Y",
						"SET_META_KEYWORDS" => "Y",
						"SET_META_DESCRIPTION" => "Y",
						"SET_STATUS_404" => "Y",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
						"ADD_SECTIONS_CHAIN" => "Y",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"PARENT_SECTION_CODE" => "",
						"INCLUDE_SUBSECTIONS" => "Y",
						"DISPLAY_DATE" => "Y",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"PAGER_TEMPLATE" => ".default",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"PAGER_TITLE" => "",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"PARENT_SECTION" => "",
						"SORT_BY2" => "NAME",
						"SORT_ORDER2" => "ASC"
					),
					false
				);?>
			</div>

			<div class="info_row info_row--no_dots">
				<div class="user_data">
					<div class="user_data__row user_data__row--full">
						<label class="user_data__label">Прикрепить файл</label>
						<input name = "form_file_39" type="file" class="custom-file custom-file__repair">
						<span> Размер файла не должен превышать 5 мб </span>
					</div>
				</div>
			</div>
				<div class="submit_box">
					<input type="submit" value="Отправить заявку" name="web_form_submit" class="submit_box__btn button">
				</div>
			</div>
		</div>
	</fieldset>
<?=$arResult["FORM_FOOTER"]?>
<?
} //endif (isFormNote)
?>