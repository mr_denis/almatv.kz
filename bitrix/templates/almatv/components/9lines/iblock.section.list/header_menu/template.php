<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul class="main_cats clear">
	<?foreach($arResult["SECTIONS"] as $arSection):?>
		<li class="main_cats__item">
			<a href="<?=$arSection["UF_LINK"]?>" class="main_cats__link <?=$arSection["UF_CLASS"]?>"><?=$arSection["NAME"]?></a>
			<?if (!empty($arSection["SECTIONS"])):?>
				<ul class="main_cats__drop">
					<?foreach($arSection["SECTIONS"] as $arSubSection):?>
						<li class="main_cats__drop_item"><a href="<?=$arSubSection["UF_LINK"]?>" class="main_cats__drop_link"><?=$arSubSection["NAME"]?></a></li>
					<?endforeach?>
				</ul>
			<?endif?>
		</li>
	<?endforeach?>
</ul>