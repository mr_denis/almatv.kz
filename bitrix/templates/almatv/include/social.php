<div class="big_social_box">
	<ul class="big_social">
		<li class="big_social__item"><a target="_blank" href="https://www.facebook.com/almatv.kz" class="big_social__link big_social__link--fb"></a></li>
		<li class="big_social__item"><a target="_blank" href="https://twitter.com/_ALMA_TV_" class="big_social__link big_social__link--tw"></a></li>
		<li class="big_social__item"><a target="_blank" href="https://vk.com/alma__tv" class="big_social__link big_social__link--vk"></a></li>
		<li class="big_social__item"><a target="_blank" href="http://my.mail.ru/community/alma_tv/" class="big_social__link big_social__link--mailru"></a></li>
		<li class="big_social__item"><a target="_blank" href="http://vse.kz/topic/669803-alma-tv-chast-3/" class="big_social__link big_social__link--somesoc"></a></li>
		<li class="big_social__item"><a target="_blank" href="https://instagram.com/alma__tv/" class="big_social__link big_social__link--inst"></a></li>
		<li class="big_social__item"><a target="_blank" href="http://ok.ru/group/54599056424968" class="big_social__link big_social__link--ok"></a></li>
		<li class="big_social__item"><a target="_blank" href="https://www.linkedin.com/company/alma-tv" class="big_social__link big_social__link--in"></a></li>
	</ul>
</div>
<br>