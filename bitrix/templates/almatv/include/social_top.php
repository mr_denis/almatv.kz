<ul class="social">
	<li class="social__item"><a target = "_blank" href="https://www.facebook.com/almatv.kz" class="social__link social__link--fb"></a></li>
	<li class="social__item"><a target = "_blank" href="https://twitter.com/_ALMA_TV_" class="social__link social__link--tw"></a></li>
	<li class="social__item"><a target = "_blank" href="https://vk.com/alma__tv" class="social__link social__link--vk"></a></li>
	<li class="social__item"><a target = "_blank" href="http://my.mail.ru/community/alma_tv/" class="social__link social__link--mailru"></a></li>
	<li class="social__item"><a target = "_blank" href="http://vse.kz/topic/669803-alma-tv-chast-3/" class="social__link social__link--some_net"></a></li>
	<li class="social__item"><a target = "_blank" href="https://instagram.com/alma__tv/" class="social__link social__link--inst"></a></li>
	<li class="social__item"><a target = "_blank" href="http://ok.ru/group/54599056424968" class="social__link social__link--ok"></a></li>
	<li class="social__item"><a target = "_blank" href="https://www.linkedin.com/company/alma-tv" class="social__link social__link--in"></a></li>
</ul>