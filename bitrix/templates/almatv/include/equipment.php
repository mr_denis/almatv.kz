<div class="vert_slide__item" id="vert_slide__item_obor">
	<div class="inet_list__item_img">
 <img src="/bitrix/templates/almatv/images/vert-pic-1.jpg" class="inet_list__item_img1024" alt=""> <img src="/bitrix/templates/almatv/images/vert-pic-1.jpg" class="inet_list__item_img1600" alt="">
	</div>
	<h2 class="inet_list__title head">Оборудование</h2>
 <span class="inet_list__desc">Для организации доступа к сети передачи данных, Вам потребуется взять в аренду или приобрести модем. В зависимости от района подключения и тарифа, устройство подготовлено для сетей.</span> <span class="inet_list__info_link"> <a href="#!" class="rotete-elem" data-rotete="rotete-docsis">DOCSIS</a>
	или <a href="#!" class="rotete-elem" data-rotete="rotete-gpon">GPON</a> </span>
</div>
<div class="vert_slide__item" id="vert_slide__item_docsis">
	<div class="inet_list__item_img">
 <img src="/bitrix/templates/almatv/images/docsis-pic-2.jpg" class="inet_list__item_img1024" alt=""> <img src="/bitrix/templates/almatv/images/docsis-pic-2.jpg" class="inet_list__item_img1600" alt="">
	</div>
 <span class="inet_list__info_link"> <a href="#!" class="rotete-elem" data-rotete="rotete-docsis">DOCSIS</a> <span class="rotete-elem--opacity">или <a href="#!" class="rotete-elem" data-rotete="rotete-gpon">GPON</a></span> </span> <span class="inet_list__desc">Модем <strong>Cisco EPC3208G</strong> с поддержкой EuroDOCSIS 3.0 <br>
	Стоимость модема: <strong>8 700 тг</strong>. <br>
	Модем <strong>Thomson TCM420</strong> с под. EuroDOCSIS 2.0 <br>
	Стоимость модема: <strong>5 100 тг</strong>.</span>
</div>
<div class="vert_slide__item" id="vert_slide__item_gpon">
	<div class="inet_list__item_img">
 <img src="/bitrix/templates/almatv/images/gpon-pic-3.jpg" class="inet_list__item_img1024" alt=""> <img src="/bitrix/templates/almatv/images/gpon-pic-3.jpg" class="inet_list__item_img1600" alt="">
	</div>
 <span class="inet_list__info_link"> <span class="rotete-elem--opacity"><a href="#!" class="rotete-elem" data-rotete="rotete-docsis">DOCSIS</a>
	или</span> <a href="#!" class="rotete-elem" data-rotete="rotete-gpon">GPON</a> </span> <span class="inet_list__desc">Модем <strong>Alcatel Lucent I-021G-P</strong> <br>
	Существует возможность аренды с послед. выкупом
	<ul class="inet_list__rotete_list">
		<li>* на 5 лет, оплачивая <strong>240 тг</strong>. в месяц,</li>
		<li>* на 3 года, оплачивая <strong>400 тг</strong>. в месяц,</li>
		<li>* на 1 год, оплачивая <strong>1 200 тг</strong>. в месяц.</li>
	</ul>
	 Стоимость модема: <strong>14 400 тг</strong>.</span>
</div>
<br>