<div class="static_ip">
<h3 class="static_ip__title head">Статический IP</h3>
<span class="static_ip__text">Статический IP-адрес может понадобиться Вам для организации на своем компьютере сервисов, доступных из публичных сетей Интернет. Например, для организации почтового сервера или web-ресурса.</span> <a href="/internet/static_ip/" class="static_ip__btn button button--no_width">Подробнее</a>
</div>