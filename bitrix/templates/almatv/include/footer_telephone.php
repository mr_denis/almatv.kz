<div class="footer_tels footer_tels--onwider">
	<div class="footer_tels__left footer_tels__left--no_float">
		<span class="footer_tels__num">8 800 080 51 51</span>
		<span class="footer_tels__link">Служба качества</span>
	</div>
	<div class="footer_tels__right footer_tels__right--no_float">
		<span class="footer_tels__num"><span>8 (727)</span> 3000 501</span>
		<span class="footer_tels__link">Служба поддержки</span>
	</div>
</div>