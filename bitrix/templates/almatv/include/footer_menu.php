<ul class="common_footer_nav">
	<li class="common_footer_nav__item">
	<h6 class="common_footer_nav__title"><span class="common_footer_nav__title_link">Компания</span></h6>
	<ul class="footer_nav">
		<li class="footer_nav__item"><a href="/company/contacts/" class="footer_nav__link">Контактные данные</a></li>
				<li class="footer_nav__item"><a href="/presscenter/" class="footer_nav__link">Пресс-центр</a></li>
		<li class="footer_nav__item"><a href="/vacancies/" class="footer_nav__link">Вакансии</a></li>
<li class="footer_nav__item"><a href="/company/" class="footer_nav__link">О нас</a></li>
	</ul>
 </li>
	<li class="common_footer_nav__item">
	<h6 class="common_footer_nav__title"><span class="common_footer_nav__title_link">Клиентам</span></h6>
	<ul class="footer_nav">
		<li class="footer_nav__item"><a href="/internet_tv/" class="footer_nav__link">Интернет + Телевидение</a></li>
		<li class="footer_nav__item"><a href="/tv/" class="footer_nav__link">Телевидение в квартиру</a></li>
		<li class="footer_nav__item"><a href="/tariffs/" class="footer_nav__link">Интернет в квартиру</a></li>
		<li class="footer_nav__item"><a href="#" class="footer_nav__link dev">Для бизнеса</a></li>
	</ul>
 </li>
	<li class="common_footer_nav__item">
	<h6 class="common_footer_nav__title"><span class="common_footer_nav__title_link">Поддержка</span></h6>
	<ul class="footer_nav">
		<li class="footer_nav__item"><a href="/drivers/" class="footer_nav__link">Инструкции и драйверы</a></li>
		<li class="footer_nav__item"><a href="/request/" class="footer_nav__link">Заявка на подключение</a></li>
		<li class="footer_nav__item"><a href="/repair/" class="footer_nav__link">Заявка на ремонт</a></li>
		<li class="footer_nav__item"><a href="/how_to_connect/" class="footer_nav__link">Как подключиться</a></li>
	</ul>
 </li>
	<li class="common_footer_nav__item">
	<h6 class="common_footer_nav__title"><span class="common_footer_nav__title_link">Реклама</span></h6>
	<ul class="footer_nav">
		<li class="footer_nav__item"><span class="footer_nav__link">По вопросам размещения:</span></li>		
<li class="footer_nav__item"><span class="footer_nav__link">Компания "RQ Holding"</span></li>
		<li class="footer_nav__item"><span class="footer_nav__link">+7 727 356 0 356</span></li>
	</ul>
 </li>
</ul>