<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>

<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=1024">
	<?$APPLICATION->ShowHead();?>
	<?CJSCore::Init(['jquery']);?>
	<title><?$APPLICATION->ShowTitle()?></title>
	
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/style.css");?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/media.css");?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/css.css");?>
	
	<?/*<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/style.css">
	<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/media.css">
	<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/css.css">
	*/?>
	
	<!--<link media="all" rel="stylesheet" href="<?//=SITE_TEMPLATE_PATH?>/css/media-markup.css">
	<link media="all" rel="stylesheet" href="<?//=SITE_TEMPLATE_PATH?>/css/style-markup.css">-->
	
	<link media="all" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
	
	<?/*
	<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.mCustomScrollbar.css">
	<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.formstyler.css">
	<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.arcticmodal-0.3.css">
	<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.fancybox.css">
	<link media="all" rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/pay.css"> */?>
	
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.mCustomScrollbar.css");?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.formstyler.css");?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.arcticmodal-0.3.css");?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.fancybox.css");?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/pay.css");?>
	
	<?/*
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.formstyler.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mCustomScrollbar.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mousewheel-3.0.6.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.simplr.smoothscroll.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.carouFredSel-6.2.1.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.placeholder.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.arcticmodal-0.3.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/messages_ru.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.mask.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/modernizr-touch.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/cookies.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.fancybox.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/js.js"></script>
	*/?>
	
	<?
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.formstyler.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.mCustomScrollbar.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.mousewheel-3.0.6.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.simplr.smoothscroll.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.carouFredSel-6.2.1.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery-ui.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.placeholder.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.arcticmodal-0.3.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.validate.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/messages_ru.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.mask.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/modernizr-touch.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/cookies.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.fancybox.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/main.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/js.js');
	?>
	<!--[if lt IE 9]>
		<script src="<?=SITE_TEMPLATE_PATH?>/js/html5shiv.js"></script>
	<![endif]-->

</head>
<?$page = $APPLICATION->GetCurPage();?>
<body <?if ($page == '/company/index.php') {?> class="cutted_x " <?}?>>
<?
	$isCity = empty($_COOKIE['CityName']);
?>
	<div id="panel"><?$APPLICATION->ShowPanel()?></div>
	<div class="wrapper">
		<?
			$APPLICATION->IncludeComponent(
	"alma:search.title", 
	"search", 
	array(
		"COMPONENT_TEMPLATE" => "search",
		"NUM_CATEGORIES" => "5",
		"TOP_COUNT" => "5",
		"ORDER" => "date",
		"USE_LANGUAGE_GUESS" => "Y",
		"CHECK_DATES" => "Y",
		"SHOW_OTHERS" => "N",
		"PAGE" => "#SITE_DIR#search/index.php",
		"SHOW_INPUT" => "Y",
		"INPUT_ID" => "title-search-input",
		"CONTAINER_ID" => "title-search",
		"CATEGORY_OTHERS_TITLE" => "Прочее",
		"CATEGORY_0_TITLE" => "Новости",
		"CATEGORY_0" => array(
			0 => "iblock_news",
		),
		"CATEGORY_0_iblock_news" => array(
			0 => "1",
		),
		"CATEGORY_1_TITLE" => "Телевидение",
		"CATEGORY_1" => array(
			0 => "iblock_video",
		),
		"CATEGORY_1_iblock_video" => array(
			0 => "9",
			1 => "25",
			2 => "26",
			3 => "all",
		),
		/*"CATEGORY_2_TITLE" => "Статические файлы",
		"CATEGORY_2" => array(
			0 => "main",
		),
		"CATEGORY_2_main" => array(
		),*/
		"CATEGORY_2_TITLE" => "Интернет",
		"CATEGORY_2" => array(
			0 => "iblock_internet",
		),
		"CATEGORY_2_iblock_internet" => array(
			0 => "18",
		),
		"CATEGORY_3_TITLE" => "Интернет+TV",
		"CATEGORY_3" => array(
			0 => "iblock_internet_tv",
		),
		"CATEGORY_3_iblock_internet_tv" => array(
			0 => "19",
		)
	),
	false
);
		?>

		<div class="head_location <?/*if ($isCity) {?>active <?}*/?>">
			<div class="head_location__box">
				<?$APPLICATION->IncludeComponent(
					"bitrix:news.list", 
					"MenuCity", 
					array(
						"CITY" => getCity(),
						"IBLOCK_TYPE" => "city",
						"IBLOCK_ID" => "8",
						"NEWS_COUNT" => "20",
						"SORT_BY1" => "NAME",
						"SORT_ORDER1" => "ASC",
						"SORT_BY2" => "SORT",
						"SORT_ORDER2" => "ASC",
						"FILTER_NAME" => "",
						"FIELD_CODE" => array(
							0 => "",
							1 => "",
						),
						"PROPERTY_CODE" => array(
							0 => "",
							1 => "",
						),
						"CHECK_DATES" => "Y",
						"DETAIL_URL" => "",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "N",
						"AJAX_OPTION_HISTORY" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_FILTER" => "Y",
						"CACHE_GROUPS" => "N",
						"PREVIEW_TRUNCATE_LEN" => "",
						"ACTIVE_DATE_FORMAT" => "j F Y",
						"SET_TITLE" => "N",
						"SET_BROWSER_TITLE" => "N",
						"SET_META_KEYWORDS" => "N",
						"SET_META_DESCRIPTION" => "N",
						"SET_STATUS_404" => "N",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"ADD_SECTIONS_CHAIN" => "N",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"PARENT_SECTION_CODE" => "",
						"INCLUDE_SUBSECTIONS" => "N",
						"DISPLAY_DATE" => "N",
						"DISPLAY_NAME" => "N",
						"DISPLAY_PICTURE" => "N",
						"DISPLAY_PREVIEW_TEXT" => "N",
						"PAGER_TEMPLATE" => ".default",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "N",
						"PAGER_TITLE" => "",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"PARENT_SECTION" => "",
						"COMPONENT_TEMPLATE" => "MenuCity",
						"SET_LAST_MODIFIED" => "N",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"SHOW_404" => "N",
						"MESSAGE_404" => ""
					),
					false
				);?>
				<div class="head_location__close">Закрыть</div>
			</div>
		</div>
		<header class="header">
			<div class="container">
				<div class="header_location_trigger_box">
					<span class="header_location_trigger"><span class="header_location_trigger__inner"><?if (!$isCity) { echo $_COOKIE['CityName']; } ?></span></span>
				</div>
				<div class="centerd_header_box">
					<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MAX_LEVEL" => "1",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_THEME" => "site",
		"COMPONENT_TEMPLATE" => "top",
		"CHILD_MENU_TYPE" => "left"
	),
	false
);?>
				</div>
				<ul class="lang_switcher">
					<li class="lang_switcher__item"><a class="lang_switcher__link dev" href="#">kz</a></li>
					<li class="lang_switcher__item"><a class="lang_switcher__link active" href="#">ru</a></li>
					<li class="lang_switcher__item"><a class="lang_switcher__link dev" href="#">en</a></li>
				</ul>
			</div>
		</header>

	<?
	$isOther = false;
	$isTV = false;
	//PR($page);
	if ($page == '/tv/index.php')
	{
		$isTV = true;
	}
	
	$arrPage = array('/tv/index.php', '/company/index.php', '/internet_tv/index.php');
	//PR($arrPage);
	?>	
	<?if(in_array($page, $arrPage))
	{
		$isOther = true;
	}
	?>
		<?if (!$isOther)
		{?>
			<div class="main container" role="main">
		<?} else {?>
			<div class="container">
		<?} ?>
			<div class="menu_trigger_holder"></div>
			<div class="secondary_menu_holder">
				<div class="secondary_menu_box clear">
					<a href="/" class="logo"></a>
					<?$APPLICATION->IncludeComponent(
						"9lines:iblock.section.list",
						"header_menu",
						array (
							"IBLOCK_CODE" => "header_menu",
							"TREE" => "Y",
							"MAX_DEPTH_LEVEL" => 2,
							"LIMIT" => 100,
							"SELECT" => array(
								"UF_LINK",
								"UF_CLASS"
							),
							"FILTER" => array(
								"UF_CITIES" => getCity()
							)
						)
					);?>
					<div class="secondary_additional clear">
					<!-- template MenuCity -->
					<?$APPLICATION->ShowViewContent('phoneHeader');?>		

						<? /*<a href="#" class="personal_cab_link dev">Личный кабинет</a>  
						<a href="#" class="personal_cab_link personal_cab_link--updates dev">Обновления Сайта</a>*/?>
					</div>
				</div>
		<?if ($isOther)
		{?>
			</div>
		</div>
		<?}?>