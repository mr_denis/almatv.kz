<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("iblock"))
	return;

$arComponentParameters = array(
	"PARAMETERS" => array(
		"IBLOCK_ID" => Array(
			"NAME"=>'ID инфоблока',
			"PARENT" => "BASE",
            "TYPE" => "STRING",
			"DEFAULT" => "", 
			"VALUES"=> '',
		),
		"ID_CHANNEL" => Array(
			"NAME"=>'ID канала',
			"PARENT" => "BASE",
            "TYPE" => "STRING",
			"DEFAULT" => "", 
			"VALUES"=> '',
		),
		"ID_TV" => Array(
			"NAME"=>'ID ТВ программы',
			"PARENT" => "BASE",
            "TYPE" => "STRING",
			"DEFAULT" => "", 
			"VALUES"=> '',
		),
		"FILTER" => Array(
			"NAME"=>'Фильтр',
			"PARENT" => "BASE",
            "TYPE" => "STRING",
			"DEFAULT" => "", 
			"VALUES"=> '',
		),
	),
);
?>
