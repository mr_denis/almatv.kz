<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => 'Показ highload инфоблока',
	"DESCRIPTION" => 'Показ highload инфоблока',
	"ICON" => "/images/banner.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
        "ID" => "hl",
        "CHILD" => array(
            "ID" => "hl",
            "NAME" => 'Показ highload инфоблока'
        )
	),
);
?>