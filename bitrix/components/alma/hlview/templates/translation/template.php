<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?
//PR($arResult["ITEMS"]);
/*
if (!$USER->IsAdmin()) {
if (count($arResult["ITEMS"]) > 0):?>
	<?if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'):?>
		<script><?=file_get_contents(__DIR__ . "/script.js");?></script>
	<?endif?>
	<div class="telecast__li">
		<h5 class="telecast__title_mini">Ближайшие трансляции</h5>
		<table class="telecast__nearest">
			<thead>
				<tr>
					<td>Дата</td>
					<td>Время</td>
					<td>Напоминание о трансляции</td>
				</tr>
			</thead>
			<tbody>
				<?foreach($arResult["ITEMS"] as $arItem) {
					//PR($arItem['UF_NAME']);
					$d = date('d', strtotime($arItem['UF_DATE_FROM']));
					$m = date('m', strtotime($arItem['UF_DATE_TO']));
					$Hi = date('H:i', strtotime($arItem['UF_DATE_FROM']));
					$ARR_MONTH_RU = array(1 => 'января', 2 => 'февраля', 3 => 'марта', 4 => 'апреля', 5 => 'мая', 6 => 'июня', 7 => 'июля', 8 => 'августа', 9 => 'сентября', 10 => 'октября', 11 => 'ноября', 12 => 'декабря');
					?>
				<tr>
					<td><?=$d.' '.$ARR_MONTH_RU[(int)$m]?></td>
					<td>
						<span class="telecast__nearest_time"><?=$Hi?></span>
					</td>
					<td>
						<div class="clear">
							<div class="telecast__nearest_channel">
								<?if ($arItem["REMIND"] == "Y"):?>
									Вы записаны
								<?else:?>
									<div class="telecast__remind_box">
										<div class = "telecast__remind_form_cap"></div>
									</div>
									<div class="telecast__remind_form">
										<form action="" class="translation-remind">
											<input type="hidden" name="broadcasting" value="<?=$arItem['ID']?>">
											<fieldset>
												<div class="telecast__row">
													<input type="text" class="telecast__field" name="email">
													<label class="telecast__label">Ваша эл. почта</label>
												</div>
												<button type="submit">Напомнить о начале</button>
											</fieldset>
										</form>
									</div>
								<?endif?>
							</div>
						</div>
					</td>
				</tr>
				<?}?>
			</tbody>
		</table>
	</div>
<?endif?>
<?} else {*/?>



<?if (count($arResult["ITEMS"]) > 0):?>
	<?if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'):?>
		<script><?=file_get_contents(__DIR__ . "/script.js");?></script>
	<?endif?>
	<div class="telecast__li">
		<h5 class="telecast__title_mini">Ближайшие трансляции</h5>
		<div class="translation telecast__nearest">
			<div class="translation_row">
				<div class="translation_col">
					<span class="translation_head">Дата</span>
				</div>
				<div class="translation_col">
					<span class="translation_head">Время</span>
				</div>
			</div>
			<?
			$ARR_MONTH_RU = array(1 => 'января', 2 => 'февраля', 3 => 'марта', 4 => 'апреля', 5 => 'мая', 6 => 'июня', 7 => 'июля', 8 => 'августа', 9 => 'сентября', 10 => 'октября', 11 => 'ноября', 12 => 'декабря',);
			$ARR_WEEK_RU = array(1 => 'пн', 2 => 'вт', 3 => 'ср', 4 => 'чт', 5 => 'пт', 6 => 'сб', 7 => 'вс');
			?>
			
			<?foreach($arResult["ITEMS"] as $key => $arGroup) {
			$d = date('d', strtotime($key));
			$m = date('m', strtotime($key));
			$week = date("N", strtotime($key));
			?>
			<div class="translation_row">
				<div class="translation_col">
					<span class="translation_day">
						<span class="translation_day_short"><?=$ARR_WEEK_RU[$week]?></span>
						<span class="translation_day_full"><?=$d.' '.$ARR_MONTH_RU[(int)$m]?></span>
					</span>
				</div>
				<div class="translation_col">
					<div class="translation_list">
						<?foreach ($arGroup as $arItem) {
						$Hi = date('H:i', strtotime($arItem['UF_DATE_FROM']));
						?>
						<div class="translation_item">
							<div class="translation_time"><?=$Hi?></div>
							<div class="translation_help">
								<div class="telecast__nearest_channel">
									<?if ($arItem["REMIND"] == "Y"):?>
										Вы записаны
									<?else:?>
										<div class="telecast__remind_box">
											<div class="telecast__remind_form_cap">Напомнить</div>
										</div>
										<div class="telecast__remind_form">
											<form action="" class="translation-remind" novalidate="novalidate">
												<input type="hidden" name="broadcasting" value="<?=$arItem['ID']?>">
												<fieldset>
													<div class="telecast__row">
														<input type="text" class="telecast__field" name="email">
														<label class="telecast__label">Ваша эл. почта</label>
													</div>
													<button type="submit">Напомнить о начале</button>
												</fieldset>
											</form>
										</div>
									<?endif;?>
								</div>
							</div>
						</div>
						<?}?>
					</div>
				</div>
			</div>
			<?}?>
		</div>
	</div>
<?endif?>

<?//}?>
