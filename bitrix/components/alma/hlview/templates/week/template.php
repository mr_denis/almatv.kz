<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (count($arResult['ITEMS']) > 0) {?>
	<?/*<div class="turn_all clear">
		<span class="turn_all__item turn_all__item--up">Свернуть все</span>
		<span class="turn_all__item turn_all__item--down">Развернуть все</span>
	</div>*/?>
</div>
	
<?
	$currentWeek = getDateWeek();
	$ARR_WEEK_RU = array(1 => 'пн', 2 => 'вт', 3 => 'ср', 4 => 'чт', 5 => 'пт', 6 => 'сб', 7 => 'вс');
	$ARR_MONTH_RU = array(1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля', 5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа', 9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря',);
	?>

	<style>
		.tv_accordion__slide
		{
			display: block;
		}
		.tv_accordion__table tr td.active{
			background: #f0f6f9;
		}
		.tv_accordion__table tr td.active .tv_accordion__broadcast_name{
			color: #99a2a6;
		}
		.tv_accordion__table tr td.active .tv_accordion__broadcast_time{
			color: #b7d5f4;
		}
	</style>

	<?
	$arRes = array(1=>array(), 2=>array(), 3=>array(), 4=>array(), 5=>array(), 6=>array(), 7=>array());

	$endDateWeekUnix = end($currentWeek)['UNIX'];
	$beginDateWeekUnix = current($currentWeek)['UNIX'];
	
	//PR(count($arResult['ITEMS']));
	foreach ($arResult['ITEMS'] as $arItem)
	{		
		$date = date("N", strtotime($arItem['UF_DATE_FROM']));

		$arResItem = array(
			"ID" => $arItem['ID'],
			"DATE_FROM" => $arItem['UF_DATE_FROM'],
			"DATE_TO" => $arItem['UF_DATE_TO'],
			"NAME" => $arItem['UF_NAME'],
		);
		//PR($item['PROPERTIES']['DATE']['VALUE']);
		$arRes[(int)$date][] = $arResItem;
	}
	
	//PR($arRes[1]);
	//die();
	?>
	
	<table class="programm_days">
		<tr>
			<?
			$d = date('d', time());
			foreach ($currentWeek as $key => $item) {
				$class = '';
				if ((int)$item['D'] == (int)$d) {
					$class = 'class="active"';
				}?>
				<td <?=$class?>>
					<span class="programm_days__day"><?=$ARR_WEEK_RU[$item['N']]?></span>
					<span class="programm_days__date"><?=$item['D']?> <?=$ARR_MONTH_RU[(int)$item['M']]?></span>
				</td>
			<?}?>
		</tr>
	</table>
	<div class="tv_accordion">
		<div class="tv_accordion__slide active">
			<table class="tv_accordion__table">
				<tr>
					<?
					$unixDate = time();
					//PR($arRes);
					foreach ($arRes as $itemDayWeek) {?>
					<td>
						<?foreach ($itemDayWeek as $itemTvProgramm) {
							$class = '';

							$dateFromUnix = strtotime($itemTvProgramm['DATE_FROM']);
							$dateToUnix = strtotime($itemTvProgramm['DATE_TO']);

							if (($unixDate >= $dateFromUnix) && ($unixDate <= $dateToUnix))
							{
								$class = ' active';
							}
						?>
						
						<a data-id = "<?=$itemTvProgramm['ID']?>" href="#" class="tv_accordion__broadcast<?=$class?>" data-v="1">
							<span class="tv_accordion__broadcast_time"><?=date("H:i" , strtotime($itemTvProgramm['DATE_FROM']))?></span>
							<span class="tv_accordion__broadcast_name"><?=$itemTvProgramm['NAME']?></span>
						</a>
						<?}?>
					</td>
					<?}?>
				</tr>
			</table>
		</div>
	</div>
<?} else {?>
	</div>
<?}?>