<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if (count($arResult['UF_ICONS']) > 0)
{
	foreach ($arResult['UF_ICONS'] as $key => &$arItem)
	{
		//проверим на удаленное существование, файлы могут быть недоступны
		if (remoteFileExists($arItem))
		{	
			$image = CFile::ResizeImageGet(
				$arItem,
				array("width" => 654, "height" =>360),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true,
				false
			);
			$arItem = $image['src'];
		}
	}
}
/*жанры*/
if (count($arResult['UF_GANRE']) > 0)
{
	$arrGanreFilter = $arResult['UF_GANRE'];
	$arrGanreFilter = array('ID' => $arrGanreFilter);
	$arGanre = getFields(HL_TV_GANRE, $arrGanreFilter, array('*'));
	foreach ($arResult['UF_GANRE'] as &$ganre)
	{
		$ganre = $arGanre[$ganre];
	}
	
	unset($arGanre);
}

$arrFullName = array(
	"д/с" => "документальный сериал",
	"д/ф" => "документальный фильм",
	"м/с" => "мультсериал",
	"м/ф" => "мультфильм",
	"т/с" => "телесериал",
	"х/ф" => "художественный фильм",
);

function fullName($name, $arrFullName)
{
	foreach ($arrFullName as $key => $item)
	{
		if ($key == $name)
		{
			return $item;
			break;
		}
	}
	
	return $name;
}
/*секции*/
if (count($arResult['UF_SECTION']) > 0)
{
	$arFilter = array('IBLOCK_ID' => IB_PROGRAMS, 'ID' => $arResult['UF_SECTION']);
	$rsSections = CIBlockSection::GetList(array(), $arFilter, false, array('ID', 'NAME'));
	$arSectionResult = array();
	while ($arSection = $rsSections->Fetch())
	{
		$arSectionResult[$arSection['ID']] = $arSection['NAME'];
	}

	foreach ($arResult['UF_SECTION'] as &$section)
	{
		
		$section = fullName($arSectionResult[$section], $arrFullName);
	}
	
	unset($arSectionResult);
}
