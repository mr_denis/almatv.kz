<!-- Ближайшие трансляции filter по идентичному названию -->
		<?	
		global $filterTranslation;
		$filterTranslation = array('UF_CHANNEL' => $arResult['UF_CHANNEL'],
			"UF_NAME" => $arResult['UF_NAME'],
			">=UF_DATE_FROM"=> date("d.m.Y H:i:s", time()),
		);

		$APPLICATION->IncludeComponent(
			"alma:hlview", 
			"translation", 
			array(
				"COMPONENT_TEMPLATE" => ".default",
				"IBLOCK_ID" => HL_TV_PROGRAMM,
				"FILTER" => $filterTranslation,
				"COUNT" => 10,
			),
			false
		);
	?>
</div>