<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?
//для мобильной версии programm_tv.php выводится данные закешированные
global $USER;
if ($USER->IsAdmin())
{
	//PR($arResult);
	//[UF_COUNTRY] => 1643
	//[UF_CHANNEL]
}
//PR($arResult);
$ARR_MONTH_RU = array(1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля', 5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа', 9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря',);
$ARR_WEEK_RU = array(1 => 'пн', 2 => 'вт', 3 => 'ср', 4 => 'чт', 5 => 'пт', 6 => 'сб', 7 => 'вс');
$dateUnix = strtotime($arResult['UF_DATE_FROM']);
$dateUnixTo = strtotime($arResult['UF_DATE_TO']);

$dataD = date("j", $dateUnix);
$dataM = date("n", $dateUnix);

$dataHi = date("H:i", $dateUnix);
$dataMin = date("i", $dateUnix);

$dataHiTo = date("H:i", $dateUnixTo);
$dataMinTo = date("i", $dateUnixTo);
$week = date("N", $dateUnixTo);
$f = (count($arResult['UF_ICONS']) > 0);
?>

<div class="telecast__slider_box <?if (!$f) {
	$i = rand(1,4);
	echo 'telecast__slider_box--noimg telecast__slider_box--noimg-'.$i;}?>">
	<?/*<div class="telecast__time_box">
		<span class="telecast__date"><?=$dataD.' '.$ARR_MONTH_RU[$dataM]?></span>
		<span class="telecast__date telecast__date--time"><?=$dataHi?> - <?=$dataHiTo?></span>
	</div>*/?>
	<div class="telecast__time_box">
		<span class="telecast__date">
			<span class="telecast__date--day"><?=$ARR_WEEK_RU[$week]?></span>
			<?=$dataD.' '.$ARR_MONTH_RU[$dataM]?>
		</span>
		<span class="telecast__date telecast__date--time"><?=$dataHi?> - <?=$dataHiTo?></span>
	</div>
	<?if ($f) {?>
		<div class="telecast__slider" id="j-telecast__slider" style = "background-color: aliceblue;">
			<?foreach ($arResult['UF_ICONS'] as $item) {?>
			<div class="telecast__slider_item">
				<img src="<?=$item?>" alt="image_description">
			</div>
			<?}?>
			<?/*<div class="telecast__slider_item">
				<img src="<?=SITE_TEMPLATE_PATH?>/pics/ironman.jpg" alt="image_description">
			</div>*/?>
		</div>
		<div class="telecast__slider_paging" id="j-telecast__slider_paging"></div>
		<div class="telecast__slider_right" id="j-telecast__slider_right">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="17.53px" height="33.09px" viewBox="0 0 17.53 33.09" enable-background="new 0 0 17.53 33.09" xml:space="preserve">
			<g><g><path class="slider_arrow" fill="#FFFFFF" d="M15.128,16.545L0.279,1.696c-0.391-0.391-0.391-1.023,0-1.414s1.023-0.391,1.414,0L17.25,15.838 c0.391,0.39,0.391,1.023,0,1.414L1.693,32.809c-0.391,0.391-1.023,0.391-1.414,0s-0.391-1.023,0-1.414L15.128,16.545z"/></g></g>
		</svg>
		</div>
		<div class="telecast__slider_left" id="j-telecast__slider_left">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="17.53px" height="33.09px" viewBox="0 0 17.53 33.09" enable-background="new 0 0 17.53 33.09" xml:space="preserve">
			<g><g><path class="slider_arrow" fill="#FFFFFF" d="M2.401,16.545L17.25,1.696c0.391-0.391,0.391-1.023,0-1.414s-1.023-0.391-1.414,0L0.28,15.838 c-0.391,0.39-0.391,1.023,0,1.414l15.556,15.556c0.391,0.391,1.023,0.391,1.414,0s0.391-1.023,0-1.414L2.401,16.545z"/></g></g>
		</svg>
		</div>
	<?}?>
</div>
<div class="telecast__content">
	<div class="telecast__li">
		<div class="telecast__title_holder">
			<h4 class="telecast__title"><?=$arResult['UF_NAME']?> <span class="telecast__age"><?=$arResult['UF_RATING']?>+</span> <?/*<span class="telecast__title_eng">Iron Man 3</span></h4>*/?>
			<?if (!empty($arResult['UF_YEAR'])) {?>
				<span class="telecast__title_date"><?=$arResult['UF_YEAR']?></span>
			<?}?>
		</div>
		<?
		//PR($arResult['UF_GANRE']);
		?>
		<?if (($arResult['UF_SECTION']) > 0) {?>
		<ul class="telecast__label">
			<?
			foreach ($arResult['UF_SECTION'] as $key => $ganre)
				{
					switch ($key % 4)
					{
						case 0:
							$class = 'telecast__label_item_link--blue';
							break;
						case 1:
							$class = 'telecast__label_item_link--purple';
							break;
						case 2:
							$class = 'telecast__label_item_link--yellow';
							break;
						case 3:
							$class = 'telecast__label_item_link--red';
							break;
					}
				{?>
					<li class="telecast__label_item">
						<a class="telecast__label_item_link <?=$class?>"><?=$ganre?></a>
					</li>
				<?}?>					
			<?}?>
		</ul>
		<?}?>
	</div>
	
	<?/*Категория: 
	<?
	foreach ($arResult['UF_SECTION'] as $section) {
		print_r($section);
	}
	?>
	*/?>
	
	<?if (!empty($arResult['UF_ACTOR'])) {?>
	<div class="telecast__li">
			<h5 class="telecast__title_mini">В ролях</h5>
			<?foreach ($arResult['UF_ACTOR'] as $key => $actor) { ?>
			<span><?=$actor?></span>
			<?}?>
	</div>
	<?}?>
	
	<?if (!empty($arResult['UF_TEXT_RU'])) {?>
	<div class="telecast__li">
		<?if (strlen($arResult['UF_TEXT_RU']) > 450):?>
			<div class="telecast__accordion">
				<span><?=$arResult['UF_TEXT_RU']?></span>
				<div class="text-fade"></div>
			</div>
			<span class="telecast__accordion_trigger">Показать описание полностью</span>
		<?else:?>
			<div class="telecast__accordion">
				<span><?=$arResult['UF_TEXT_RU']?></span>
			</div>
		<?endif?>
	</div>
	<?}
	//трансляции в эпилоге не кешируются
	?>
