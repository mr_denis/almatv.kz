<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
/*VIEW HL_BLOCK*/
if (!CModule::IncludeModule('highloadblock'))
	return;
	
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

/*выводим все жанры и айдишники к ним*/
/*недельный фильтр*/
$currentWeek = getDateWeek();
	
if (!empty($arParams['FILTER']))
{
	$arrFilter = $arParams['FILTER'];
	$ob = getFields(HL_TV_PROGRAMM, $arrFilter, array('*'), $arParams['COUNT']);
	$arResult['ITEMS'] = $ob;
}
else
{
	if ($arParams['ID_TV'])
	{
		$arrFilter['ID'] = $arParams['ID_TV'];
		$ob = getFields(HL_TV_PROGRAMM, $arrFilter, array('*'), $arParams['COUNT']);
		$arResult = current($ob);
	}
	else
		return 0;  //return component
}

$this->IncludeComponentTemplate();
?>