<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("iblock"))
    return;

function isHome($idCity)
{
	//IS_HOME
	$arFilter = Array("IBLOCK_ID"=>IBLOCK_PACKAGES, "ACTIVE"=>"Y", "PROPERTY_TYPE" => PACKAGE_TYPE_HOME, "PROPERTY_MAIN_VALUE" => 'Y', "PROPERTY_CITY" => $idCity);  //пакеты в квартиру
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, array('ID', 'NAME', 'IBLOCK_SECTION_ID'));
	$count = $res->SelectedRowsCount();
	return $count;
}

function result($type, $cityId)
{
	$arFilter = Array("IBLOCK_ID"=>IBLOCK_PACKAGES, "ACTIVE"=>"Y", "PROPERTY_TYPE" => $type, "PROPERTY_MAIN_VALUE" => 'Y', "PROPERTY_CITY" => $cityId);  //пакеты в квартиру

	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, array('ID', 'NAME', 'IBLOCK_SECTION_ID'));
	while($ob = $res->GetNext())
	{
		$arResult['ARR_ID'][] = $ob['ID'];
		$arResult['PACKAGES'][$ob['ID']] = $ob;
	}
	
	return $arResult;
}

/*находим число каналов для каждого пакета*/
function getArrNumPackages($arId, $cityId)
{
	if ((count($arId) > 0) && $cityId)
	{	
		$arFilter = Array("IBLOCK_ID"=> IBLOCK_CHANNELS, "PROPERTY_PACKAGES" => $arId, "ACTIVE" => "Y", "PROPERTY_CITY" => $cityId);  //общие пакеты
		$res = CIBlockElement::GetList(Array(), $arFilter, array("PROPERTY_PACKAGES"), false, array("ID"));

		$arChannelCount = array();
		while($ob = $res->GetNext()) 
		{
			$arChannelCount[$ob['PROPERTY_PACKAGES_VALUE']] = $ob['CNT']; 
		}

		return $arChannelCount;
	}
	
	return 0;
}

$arResult['ROOM'] = result($arParams['TYPE'], $arParams['CITY_ID']);

$arNum = getArrNumPackages($arResult['ROOM']['ARR_ID'], $arParams['CITY_ID']);

foreach ($arResult['ROOM']['PACKAGES'] as $key => &$item)
{
	foreach ($arNum as $key_packages => $num)
	{
		if ($key == $key_packages)
		{
			$item['NUM'] = $num;
			break;
		}
	}
}

$arResult['IS_HOME'] = isHome($arParams['CITY_ID']);

$this->IncludeComponentTemplate(); //Вызвали шаблон
?>