<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if ($arParams['AJAX_CONTENT'] == 'N') {?>
<div class="packages_title_holder">
	<h1 class="packages_title">Пакеты программ</h1>
	<ul class="tarif_cat__trigger tarif_cat__trigger--flr j-tarif_cat__trigger--flat_home">
		<?if (isset($arResult['ROOM'])) {?>
			<li data-id = "15" class="tarif_cat__trigger_item active not_load">в квартиру</li>
		<?}?>
		<?if ($arResult['IS_HOME'] > 0) {?>
			<li data-id = "16" class="tarif_cat__trigger_item">в частный дом</li>
		<?}?>
	</ul>
</div>
<?}?>

<ul class="flat_home_tabs">
	<li class="flat_home_tabs__item">
		<span class="show_channels dev">Архив каналов</span>
		<ul class="tabs_links tabs_links--<?=$arParams["TYPE_CLASS"]?>">
			<?
			$i = 0;
			foreach ($arResult['ROOM']['PACKAGES'] as $key => $item) {
				if ($i == 0)
				{
					$firstId = $key;
					$firstCount = $item['NUM'];
				}
				
				switch ($key % 4)
				{
					case 0:
						$classColor = '';
						break;
					case 1:
						$classColor = 'tabs_links__channel_count--pure_orange';
						break;
					case 2:
						$classColor = 'tabs_links__channel_count--orange';
						break;
					case 3:
						$classColor = 'tabs_links__channel_count--red';
						break;
				}
				
				?>
				<?$isFirst = false;?>
				<li class="tabs_links__item <?if ($i == 0) { echo 'not_load'; $isFirst = true;}?>" data-id = "<?=$item['ID']?>" <?if (!$isFirst) {?> data-prev = "<?=$id_prev?>" <?}?>>
					<span class="tabs_links__channel"><?=$item['NAME']?></span>
					<span class="tabs_links__channel_count <?=$classColor?>"><?=$item['NUM']?> <?=DeclensionNameChannel($item['NUM'])?></span>
				</li>
				<?
				$id_prev = $item['ID'];
				$i++;
			}?>
		</ul>
		<ul class="tabs_content tabs_content--<?=$arParams["TYPE_CLASS"]?>">
			<?
			$i = 0;
			foreach ($arResult['ROOM']['PACKAGES'] as $key => $item) {?>
				<?if ($i == 0) {?>
					<li class="tabs_content__item">
						<?$APPLICATION->IncludeComponent(
							"alma:package",
							"",
							Array(
								"ID_PACKAGE" => $firstId,
								"COUNT_CHANNELS" => $firstCount,
								"ID_PACKAGE_PARENT" => 0,
							)
						);?>
					</li>
				<?}
				else
				{?>
					<li class="tabs_content__item"></li>
				<?}
				$i++;
				?>
			<?}?>
		</ul>
	</li>
	
	<!-- В частный дом ajax load -->
	<li class="flat_home_tabs__item" id = "home"> </li>
</ul>