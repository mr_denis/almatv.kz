<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("iblock"))
	return;

$arComponentParameters = array(
	"PARAMETERS" => array(
		"IBLOCK_ID" => Array(
			"NAME"=>'Инфоблок пакетов',
			"PARENT" => "BASE",
            "TYPE" => "STRING",
			"DEFAULT" => IBLOCK_PACKAGES, 
		),
		"TYPE" => array(
            "NAME" => "ID Общие/доп пакеты",
            "PARENT" => "BASE",
            "TYPE" => "STRING",
			"DEFAULT" => "15", 
        ),
		"CITY_ID" => array(
            "NAME" => "Город",
            "PARENT" => "BASE",
            "TYPE" => "STRING",
			"DEFAULT" => "42", 
        ),
	)
);
?>
