<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?//PR($arResult);?>
<?if (count($arResult["ITEMS"]) > 0) {?>
<div class="info_row">
<label class="user_data__label user_data__label--req">Желаемый пакет программ цифрового ТВ</label>	

<ul class="get_pack j-get_pack--flat">
<?foreach($arResult["ITEMS"] as $key => $arItem):?>
	<?
	PR($arItem['ID']);
	$prop = $arItem['PROPERTIES']; 
	//PR($prop['TYPE']);
	//PR($prop['CHANNEL']);
	//PR($prop['TYPE']['VALUE_ENUM_ID']);
	if (in_array(15, $prop['TYPE']['VALUE_ENUM_ID'])) { //в квартиру//
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		
		<li class="get_pack__item" data-id="<?=$arItem['ID']?>" data-pack="<?=$arItem['NAME']?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>"> <!-- active data-pack="<?//=$arItem['ID']?>"  -->
			<?if (count($arItem['CHANNELS']) > 0) {?>
			<div class="get_pack__add">
				<div class="get_pack__add_hidden">
					<div class="add_packs_box__hidden add_packs_box__hidden--getpack">
						<div class="add_packs_box__hidden_scroll">
							<?foreach ($arItem['CHANNELS'] as $channelItem) {?>		
							<span class="add_packs_box__hidden_item">
								<span class="add_packs_box__hidden_pic">
									<?if (!empty($channelItem['PREVIEW_PICTURE'])) {?>
										<img src="<?=$channelItem['PREVIEW_PICTURE']?>" alt="<?=$channelItem['NAME']?>">
									<?}?>
								</span>
								<a href="#" class="add_packs_box__hidden_link"><?=$channelItem['NAME']?></a>
							</span>
							<?}?>
						</div>
					</div>
				</div>
			</div>
			<?}?>
			<h4 class="get_pack__title get_pack__title--cool"><?=$arItem['NAME']?></h4>
			<div class="clear">
				<span class="get_pack__info"><span><?=count($arItem['CHANNELS'])//count($prop['CHANNEL']['VALUE'])?></span>  каналов</span>
				<span class="get_pack__info get_pack__info--right"><span><?=$prop['PRICE']['VALUE']?></span>  тг./мес.</span>
			</div>
		</li>
	<?}?>
<?endforeach;?>
</ul>					
						
					
<ul class="get_pack j-get_pack--home get_pack--hidden">
	<?foreach($arResult["ITEMS"] as $key => $arItem):?>
	<?
	$prop = $arItem['PROPERTIES']; 
	if (in_array(16, $prop['TYPE']['VALUE_ENUM_ID']))  {  //в частный дом
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		
		<li class="get_pack__item" data-id="<?=$arItem['ID']?>" data-pack="<?=$arItem['NAME']?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<?if (count($arItem['CHANNELS']) > 0) {?>
			<div class="get_pack__add">
				<div class="get_pack__add_hidden">
					<div class="add_packs_box__hidden add_packs_box__hidden--getpack">
						<div class="add_packs_box__hidden_scroll">
							<?foreach ($arItem['CHANNELS'] as $channelItem) {?>		
							<span class="add_packs_box__hidden_item">
								<span class="add_packs_box__hidden_pic">
									<?if (!empty($channelItem['PREVIEW_PICTURE'])) {?>
										<img src="<?=$channelItem['PREVIEW_PICTURE']?>" alt="<?=$channelItem['NAME']?>">
									<?}?>
								</span>
								<a href="#" class="add_packs_box__hidden_link"><?=$channelItem['NAME']?></a>
							</span>
							<?}?>
						</div>
					</div>
				</div>
			</div>
			<?}?>
			<h4 class="get_pack__title get_pack__title--cool"><?=$arItem['NAME']?></h4>
			<div class="clear">
				<span class="get_pack__info"><span><?=count($arItem['CHANNELS'])//count($prop['CHANNEL']['VALUE'])?></span>  каналов</span>
				<span class="get_pack__info get_pack__info--right"><span><?=$prop['PRICE']['VALUE']?></span>  тг./мес.</span>
			</div>
		</li>
	<?}?>
<?endforeach;?>
</ul>
<input type="hidden" value="0" class="get_pack_result">
</div>

<?}?>