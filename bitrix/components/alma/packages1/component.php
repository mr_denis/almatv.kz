<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("iblock"))
    return;

$arFilter = array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE"=>"Y");

$arFilter['PROPERTY_EXT'] = intval($arParams['TYPE']);
if ($_COOKIE['City'])
{
	$arFilter['PROPERTY_CITY'] = $_COOKIE['City'];
}
else
{
	$arFilter['PROPERTY_CITY'] = $_REQUEST['city'];
}
					
//timestamp_x
$sort_str = "name";

if (!empty($arParams['SORT']))
{
    $sort_str = $arParams['SORT'];
    $arSort = array(
        $sort_str => "asc",
    );
}

$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, Array("ID", "IBLOCK_ID", "NAME"));
$count = 0;
while($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();  
	$arrTmp = $arFields;
	$arProps = $ob->GetProperties();
	$arrTmp["PROPERTIES"] = $arProps;
	
	$arResult['ITEMS'][] = $arrTmp;
}
//PR($arResult);
$this->IncludeComponentTemplate(); //Вызвали шаблон
?>