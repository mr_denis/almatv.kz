<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => 'Секции',
	"DESCRIPTION" => 'Секции',
	"ICON" => "/images/banner.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
        "ID" => "ALMA",
        "CHILD" => array(
            "ID" => "ALMA",
            "NAME" => 'Секции'
        )
	),
);
?>