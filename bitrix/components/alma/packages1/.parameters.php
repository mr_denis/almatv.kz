<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("iblock"))
	return;

$arComponentParameters = array(
	"PARAMETERS" => array(
		"IBLOCK_ID" => Array(
			"NAME"=>'ID',
			"PARENT" => "BASE",
            "TYPE" => "STRING",
			"DEFAULT" => "", 
			"VALUES"=> '',
		),
		"TYPE" => array(
            "NAME" => "ID Общие/доп пакеты",
            "PARENT" => "BASE",
            "TYPE" => "STRING",
			"DEFAULT" => "", 
			"VALUES"=> '',
        ),
        "SORT" => array(
            "NAME" => "Сортировка",
            "PARENT" => "BASE",
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
	)
);
?>
