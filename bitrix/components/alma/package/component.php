<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("iblock"))
    return;
 
function compare_by_area($a, $b) {
    $a = $a['ID'];
    $b = $b['ID'];
    
    if ($a < $b) {
        return -1;
    } elseif ($a > $b) {
        return 1;
    } else {
        return 0;
    }
}

function getSection($arrSectionID)
{	
	$arFilter = array(
		"IBLOCK_ID" => IBLOCK_CHANNELS,
		"ACTIVE"=>"Y",
		"ID" => $arrSectionID,
	);

	$rsSections = CIBlockSection::GetList(array(), $arFilter, true, array("ID", "NAME", "PICTURE"), false);
	$rsSections->SetUrlTemplates(); //Получили строку URL для каждого из разделов (по формату из настроек инфоблока)
	$arrSections = array();
	while($arSection = $rsSections->GetNext())
	{
		$arSection['PICTURE'] = CFile::GetPath($arSection["PICTURE"]);
		$arrSections[$arSection['ID']] = $arSection;
	}
	
	return $arrSections;
}

function groupChannels($arChannels)
{
	$sectionChannels = array();
	foreach ($arChannels as $itemCh)
	{
		$tmpChannel = array(
			"ID" => $itemCh['ID'],
			"NAME" => $itemCh['NAME'],
			"PREVIEW_PICTURE" => $itemCh['PREVIEW_PICTURE'],
		);
		
		$sectionChannels[$itemCh['IBLOCK_SECTION_ID']][] = $tmpChannel;
	}
	
	return $sectionChannels;
}

/*функция находим список каналов для данного пакета*/
function listChannels($idPackage)
{
	$arrFilter = Array("IBLOCK_ID"=>IBLOCK_CHANNELS, "ACTIVE"=>"Y", "PROPERTY_PACKAGES" => $idPackage);
	$res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arrFilter, false, false, array('ID', 'NAME', 'PREVIEW_PICTURE', 'IBLOCK_SECTION_ID'));
	$arChannels = array();
	while($ob = $res->GetNext())
	{
		$picture = CFile::ResizeImageGet(
			$ob["PREVIEW_PICTURE"],
			array("width" => 20, "height" =>20),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true,
			false
		);
		
		$ob["PREVIEW_PICTURE"] = $picture['src'];
		$arChannels[] = $ob;
	}
	
	return $arChannels;
}

$arError = array();
$idPackage = intval($arParams['ID_PACKAGE']);
$arResult = array();

if ($idPackage > 0)
{
	/*по Id пакета выводим его параметры  (1 запрос) */
	$arrFilter = Array("IBLOCK_ID" => IBLOCK_PACKAGES, "ACTIVE" => "Y", "ID" => $idPackage);
	$res = CIBlockElement::GetList(Array(), $arrFilter, false, false, array('ID', 'NAME', 'PROPERTY_PRICE'));
	if($ob = $res->GetNext())
	{
		$arResult['PACKAGE'] = $ob;
	}

	/*находим список каналов в данном пакете   (2 запрос)*/
	$arChannels = listChannels($idPackage);

	$arResult['COUNT'] = count($arChannels);

	/*если prev есть*/ 
	if ($arParams['ID_PACKAGE_PARENT'])
	{
		/*находим список каналов во 2 пакете [3 запрос]*/
		$arChannelsPrev = listChannels($arParams['ID_PACKAGE_PARENT']);

		/*находим разные каналы*/
		$arChannels = array_udiff($arChannels, $arChannelsPrev, 'compare_by_area');
		$countDiff = $arResult['COUNT'] - count($arChannels);
		
		/*находим общие число общих каналов*/
		$arResult['COUNT_DIFF'] = $countDiff;
		
		/*по Id пакета выводим его параметры  (1 запрос) */
		$arrFilter = Array("IBLOCK_ID" => IBLOCK_PACKAGES, "ACTIVE" => "Y", "ID" => $arParams['ID_PACKAGE_PARENT']);
		$res = CIBlockElement::GetList(Array(), $arrFilter, false, false, array('ID', 'NAME'));
		if($ob = $res->GetNext())
		{
			$arResult['PACKAGE_PREV'] = $ob;
		}
	}
	/*группируем по разделам массив каналов*/
	$arGroupChannels = groupChannels($arChannels);

	$arrSectionID = array_keys($arGroupChannels);
	/*находим разделы нужные (картинка, название) (3 запрос)[4 запрос]*/
	$arSections = getSection($arrSectionID);

	/*формируем массивы*/
	$arResult["CHANNELS"] = $arGroupChannels;
	$arResult["SECTIONS"] = $arSections;
}
else
{
	$arError[] = 'Не верный ID пакета';
}

$this->IncludeComponentTemplate(); //Вызвали шаблон
?>