<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?
$package = $arResult['PACKAGE'];
$arChannels = $arResult['CHANNELS'];
$arSections = $arResult['SECTIONS'];
?>
<?
$countChannels = isset($arParams['COUNT_CHANNELS']) ? $arParams['COUNT_CHANNELS'] : $arResult['COUNT'];
?>
<div class="pack_main_info">
	<h2 class="pack_main_info__title"><?=$package['NAME']?></h2>
	<div class="pack_main_info__right">
		<ul class="pack_main_info__list">
			<li class="pack_main_info__item">
				<span class="pack_main_info__num"><?=$countChannels?> <span><?=DeclensionNameChannel($countChannels)?></span></span>
				<span class="pack_main_info__cost"><?=number_format($package['PROPERTY_PRICE_VALUE'], 0, '.', ' ');?><span>тг/мес.</span></span>
			</li> 
			<li class="pack_main_info__item">
				<span class="pack_main_info__discount">Скидка на подключение дополнительной точки <span>50%</span></span>
			</li>
			<li class="pack_main_info__item">
				<span class="pack_main_info__show_all channel_button" data-id="<?=$arParams['ID_PACKAGE']?>">Список всех каналов</span>
			</li>
		</ul>
	</div>
</div>

<?if ($arParams['ID_PACKAGE_PARENT']) {
	$packagePrev = $arResult['PACKAGE_PREV'];
	?>
<div class="how_channels">
	<span class="how_channels__desc"><span class="how_channels__desc_num"><?=$arResult['COUNT_DIFF']?></span> канала из пакета <span class="how_channels__desc_name"><a style = "text-decoration: none;" class = "channel_button" href = "#" data-id="<?=$packagePrev['ID']?>"><?=$packagePrev['NAME']?></a></span></span>
</div>
<div class="channels_plus"></div>
<?}?>

<div class="add_package_list_holder">
	<ul class="add_package_list">
		<?foreach ($arChannels as $nameSection => $channels) {
			$itemSection = $arSections[$nameSection];
			?>
		<li class="add_package_list__item">
			<?if (!empty($itemSection['PICTURE'])) {?>
			<div class="add_package_list__img">
				<img src="<?=SITE_TEMPLATE_PATH?>/pics/package_pic_01.jpg" alt="<?=$itemSection['NAME']?>">
			</div>
			<?}?>
			<h6 class="add_package_list__title">
				<span class="add_package_list__title_ico">
					<img src="<?=$itemSection['PICTURE']?>" height="21" width="22" alt="<?=$itemSection['NAME']?>">
				</span>
				<span class="add_package_list__title_name"><?=$itemSection['NAME']?></span>
			</h6>
			<ul class="tarif_add_pack__list tarif_add_pack__list--add_package">
				<?$isExt = false;?>
				<?for ($i = 0; $i < count($channels); $i++) {
					$channel = $channels[$i];
					if ($i == 6 || ($i == 5 && count($channels) > 6))
					{
						$isExt = true;
						break;
					}
					?>
					<li class="tarif_add_pack__list_li"><a href="/channel/<?=$channel['ID']?>/"><?=$channel['NAME']?></a></li>
				<?}?>
				
				<?if ($isExt) {
					$count = count($channels) - 5;?>
					<li class="tarif_add_pack__list_li tarif_add_pack__list_li--more_packs">
						<span class="add_packs_box__count add_packs_box__count--black">
							Ещё <?=$count?> <?=DeclensionNameChannel($count)?>
							<span class="add_packs_box__hidden <?if ($key == 0) echo 'add_packs_box__hidden--rigth';?>">
							<div class="add_packs_box__hidden_scroll">
								<?for ($i = 5; $i < count($channels); $i++) {
									$channel = $channels[$i];
									?>
								<span class="add_packs_box__hidden_item">
									<?if (!empty($channel['PREVIEW_PICTURE'])) { ?>
									<span class="add_packs_box__hidden_pic">
										<img src="<?=$channel['PREVIEW_PICTURE']?>" alt="<?=$channel['NAME']?>">
									</span>
									<?} else {?>
									<span class="add_packs_box__hidden_pic add_packs_box__hidden_pic--noimg">
										<span><?=substr($channel['NAME'], 0, 1)?></span>
									</span>
									<?}?>
									<a href="/channel/<?=$channel['ID']?>/" class="add_packs_box__hidden_link"><?=$channel['NAME']?></a>
								</span>
								<? } ?>
							</div>
							</span>
						</span>
					</li>
				<?} ?>
			</ul>
		</li>
		<?}?>
	</ul>
</div>