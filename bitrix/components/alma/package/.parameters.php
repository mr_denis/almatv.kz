<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"ID_PACKAGE" => Array(
			"NAME"=>'ID пакета',
			"PARENT" => "BASE",
            "TYPE" => "STRING",
			"DEFAULT" => '', 
		),
		"ID_PACKAGE_PARENT" => Array(
			"NAME"=>'ID родителя',
			"PARENT" => "BASE",
            "TYPE" => "STRING",
			"DEFAULT" => 0, 
		),
		"COUNT_CHANNELS" => Array(
			"NAME"=>'Количество каналов',
			"PARENT" => "BASE",
            "TYPE" => "STRING",
			"DEFAULT" => '', 
		),
	)
);
?>
