<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Добавление результат в форму",
	"DESCRIPTION" => "Добавление результат в форму",
	"ICON" => "/images/comp_result_new.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "ALMA_FORM",
		"CHILD" => array(
			"ID" => "ALMA_FORM",
			"NAME" => "Добавление в форму",
		)
	),
);
?>