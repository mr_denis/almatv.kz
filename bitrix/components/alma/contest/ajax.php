<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

require_once(__DIR__ . '/core.php');

$messageSuccess = alma_contest_get_message('alma_contest_message_success');
if (!$messageSuccess) {
  $messageSuccess = 'Вы выиграли:';
}
$messageFailure = alma_contest_get_message('alma_contest_message_failure');
if (!$messageFailure) {
  $messageFailure = 'Вы ничего не выиграли';
}

$present = alma_contest_winner_get($_POST['city_id'], $_POST['contract']);

if ($present) {
  echo <<<HTML
<p class="alma_content_success">{$messageSuccess} <strong id="present_title">{$present}</strong></p>
HTML;
}
else {
  echo <<<HTML
<p class="alma_content_nothing">{$messageFailure}</p>
HTML;
}

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_after.php');
die;