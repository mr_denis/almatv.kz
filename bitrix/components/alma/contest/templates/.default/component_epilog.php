<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
   die();

global $APPLICATION;
$APPLICATION->SetAdditionalCSS($arResult['component_url']."css/style.css");
$APPLICATION->AddHeadScript($arResult['component_url']."js/jquery.cookie.min.js");
$APPLICATION->AddHeadScript($arResult['component_url']."js/jquery.numeric.min.js");
$APPLICATION->AddHeadScript($arResult['component_url']."js/func.js");