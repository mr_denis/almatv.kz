<script type="application/javascript"><!--
  var ajaxurl = '<?php echo $arResult['component_url']; ?>ajax.php';
</script>

<?php
$cOptions = '<option disabled="disabled" selected="selected">Выберите город</option>';
foreach($arResult['cities'] as $c) {
  $cOptions .= "<option value='{$c['ID']}'>{$c['NAME']}</option>";
}
?>
<form action="#" id="alma_contest_form">
  <div class="form-group">
    <select class="form-control" id="alma_contest_city_id"><?php echo $cOptions; ?></select>
    <span class="alma_contest_error" style="display: none">Пожалуйста, выберите город</span>
  </div>
  <div class="form-group">
    <input class="form-control" id="alma_contest_contract" type="text" maxlength="10" placeholder="Номер договора" autofocus="autofocus">
    <span class="alma_contest_error" style="display: none">Пожалуйста, введите номер договора</span>
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-success" id="alma_contest_check">
      <img style="display: none;" alt="..." src="<?php echo $arResult['component_url']; ?>css/ajax-loader.gif" id="alma_contest_loader">
      Проверить
    </button>
  </div>
  <div class="form-group">
    <div id="alma_contest_result" style="display: none;"></div>
  </div>
</form>