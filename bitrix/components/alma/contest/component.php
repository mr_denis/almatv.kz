<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

require_once(__DIR__ . '/core.php');

if ($this->StartResultCache(3600))
{
  $arResult['cities']        = alma_contest_city_list();
  $arResult['component_url'] = $this->GetPath() . DIRECTORY_SEPARATOR;

  CModule::IncludeModule('form');
  $this->IncludeComponentTemplate();
}