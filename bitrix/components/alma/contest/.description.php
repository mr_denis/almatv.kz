<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arComponentDescription = array(
  "NAME" => 'Конкурс АлмаТВ',
  "DESCRIPTION" => 'Розыгрыш призов для АлмаТВ',
  "ICON" => "/images/icon.gif",
  "PATH" => array(
    "ID" => "utility",
  ),
  "CACHE_PATH" => "Y"
);