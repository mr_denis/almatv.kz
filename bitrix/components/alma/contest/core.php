<?php

if (!CModule::IncludeModule('iblock'))
	return;

function alma_contest_get_message($type) {
  $message = '';
  $messages = CIBlockElement::GetList(
    array(
      'SORT' => 'ASC',
      'NAME' => 'ASC'
    ),
    array(
      'GLOBAL_ACTIVE' => 'Y',
      'CODE' => $type
    ),
    false,
    false,
    array(
      'PREVIEW_TEXT'
    )
  );
  if ($messages->SelectedRowsCount() > 0) {
    $message = $messages->GetNext()['PREVIEW_TEXT'];
  }
  return $message;
}

function alma_contest_city_list() {
  $cities = array();

  $iCitiesID = CIBlock::GetList(['id' => 'ASC'], ['CODE' => 'city'], false);
  if ($iCitiesID->SelectedRowsCount() > 0) {
    $iCitiesID = $iCitiesID->GetNext()['ID'];
    // get cities
    $sections = CIBlockElement::GetList(
      array(
        'SORT' => 'ASC',
        'NAME' => 'ASC'
      ),
      array(
        'GLOBAL_ACTIVE' => 'Y',
        'IBLOCK_ID' => $iCitiesID
      ),
      false,
      false,
      array(
        'ID', 'NAME'
      )
    );
    while($c = $sections->GetNext()) {
      $cities[] = $c;
    }
  }
  return $cities;
}

function alma_contest_winner_get($cityID, $contract) {
  $present = false;

  $iWinnersID = CIBlock::GetList(['id' => 'ASC'], ['CODE' => 'alma_contest_winners'], false);
  if ($iWinnersID->SelectedRowsCount() > 0) {
    $iWinnersID = $iWinnersID->GetNext()['ID'];
    // get winner
    $winners = CIBlockElement::GetList(
      array(
        'SORT' => 'ASC',
        'NAME' => 'ASC'
      ),
      array(
        'GLOBAL_ACTIVE' => 'Y',
        'IBLOCK_ID' => $iWinnersID,
        'PROPERTY_city_id' => $cityID,
        'PROPERTY_contract' => $contract
      ),
      false,
      false,
      array(
        'ID', 'PROPERTY_present_id'
      )
    );
    while($p = $winners->GetNext()) {
      $presentID = $p['PROPERTY_PRESENT_ID_VALUE'];
      // get present
      if ($presentItem = CIBlockElement::GetByID($presentID)) {
        $present = $presentItem->GetNext()['NAME'];
      }
    }
  }
  return $present;
}

function alma_contest_present_add($title) {
  $title = trim($title);

  $iPresentsID = CIBlock::GetList(['id' => 'ASC'], ['CODE' => 'alma_contest_presents'], false);
  if ($iPresentsID->SelectedRowsCount() > 0) {
    $iPresentsID = $iPresentsID->GetNext()['ID'];
    // get present
    $presents = CIBlockElement::GetList(
      array(
        'SORT' => 'ASC',
        'NAME' => 'ASC'
      ),
      array(
        'GLOBAL_ACTIVE' => 'Y',
        'IBLOCK_ID' => $iPresentsID,
        'NAME' => $title
      ),
      false,
      false,
      array(
        'ID'
      )
    );
    while($p = $presents->GetNext()) {
      return $p['ID'];
    }

    $iPresent = new CIBlockElement;
    return $iPresent->Add(array(
      'IBLOCK_ID' => $iPresentsID,
      'NAME' => $title
    ));
  }
  return false;
}

function alma_contest_city_find($title) {
  $title = trim($title);

  $iCitiesID = CIBlock::GetList(['id' => 'ASC'], ['CODE' => 'city'], false);
  if ($iCitiesID->SelectedRowsCount() > 0) {
    $iCitiesID = $iCitiesID->GetNext()['ID'];
    // get city
    $cities = CIBlockElement::GetList(
      array(
        'SORT' => 'ASC',
        'NAME' => 'ASC'
      ),
      array(
        'GLOBAL_ACTIVE' => 'Y',
        'IBLOCK_ID' => $iCitiesID,
        'NAME' => $title
      ),
      false,
      false,
      array(
        'ID'
      )
    );
    while($p = $cities->GetNext()) {
      return $p['ID'];
    }
  }
  return null;
}

function alma_contest_winner_add($cityID, $presetID, $contract) {
  $contract = trim($contract);

  $iWinnersID = CIBlock::GetList(['id' => 'ASC'], ['CODE' => 'alma_contest_winners'], false);

  if ($iWinnersID->SelectedRowsCount() > 0) {
    $iWinnersID = $iWinnersID->GetNext()['ID'];

    $iPresent = new CIBlockElement;

    return $iPresent->Add(array(
      'IBLOCK_ID' => $iWinnersID,
      'NAME' => $contract,
      'PROPERTY_VALUES' => array(
        'city_id' => $cityID,
        'present_id' => $presetID,
        'contract' => $contract
      )
    ));
  }

  return false;
}

function alma_contest_import() {
  $c = 0;

  if (file_exists(dirname(__FILE__) . '/winners.csv')) {
    $f = fopen(dirname(__FILE__) . '/winners.csv', 'r');
    $presentID = 0;
    while ($s = fgetcsv($f, null, ';', '"')) {
      if ($s[0] == '!!') {
        $presentID = alma_contest_present_add($s[1]);
      }
      elseif($presentID) {
        $cityID = alma_contest_city_find($s[0]);
        if ($cityID) {
          $id = alma_contest_winner_add($cityID, $presentID, $s[1]);
          $c++;
        }
        else {
          die($s[0]);
        }
      }
      else {
        die($s[1]);
      }
    }
    fclose($f);
  }
  echo "<p>Импортировано {$c} записей</p>";
}