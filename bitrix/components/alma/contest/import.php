<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

require_once(__DIR__ . '/core.php');

ini_set('max_execution_time', 60);
ini_set('memory_limit', '512M');

alma_contest_import();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
die;