var alma_contest_highlight_city_by_cookie = function(sel) {
  if (sel) {
    $('#alma_contest_city_id').val(parseInt(sel));
  }
};

$(document).ready(function(){
  // set city from selector
  alma_contest_highlight_city_by_cookie($.cookie('cookie_city'));

  $('#alma_contest_contract').numeric();

  $('#alma_contest_form').submit(function(e){
    e.preventDefault();
    var cityField = $('#alma_contest_city_id');
    var cityID   = cityField.val();
    var contractField = $('#alma_contest_contract');
    var contract = contractField.val().trim();
    if (!cityID) {
      cityField.addClass('alma_contest_has_error');
    }
    else {
      cityField.removeClass('alma_contest_has_error');
      cityField.next('.alma_contest_error').hide();
    }
    if (!contract) {
      contractField.addClass('alma_contest_has_error');
    }
    else {
      contractField.removeClass('alma_contest_has_error');
      contractField.next('.alma_contest_error').hide();
    }
    if (cityID && contract) {
      $('#alma_contest_loader').show();
      $.post(
        ajaxurl,
        {
          'city_id': cityID,
          'contract': contract
        },
        function(response){
          $('.alma_contest_has_error').removeClass('alma_contest_has_error');
          $('#alma_contest_loader').fadeOut();
          $('#alma_contest_result').fadeOut(400, function(){$(this).html(response).fadeIn()});
        }
      );
    }
  });
});