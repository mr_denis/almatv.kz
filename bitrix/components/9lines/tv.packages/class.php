<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class TV_Packages extends CBitrixComponent {
    //protected $DBResult;

	public function onPrepareComponentParams($arParams)
    {
		$arParams["CACHE"] = $arParams["CACHE"] == "N" ? "N" : "Y";
        $arParams["CACHE_TIME"] = intval($arParams["CACHE_TIME"]) > 0 ? intval($arParams["CACHE_TIME"]) : 36000;
		//PR($arParams);
        return $arParams;
    }
	
	public function resultPackages($type, $arrCategory)
	{
		//фильтр по городам
		$arFilter = Array("IBLOCK_ID"=>IBLOCK_PACKAGES, "ACTIVE"=>"Y", "PROPERTY_TYPE" => $type);  //пакеты в квартиру
		
		$arFilter['PROPERTY_CITY'] = $this->arParams['FILTER']['UF_CITIES'];
		
		/*global $USER;
		if ($USER->IsAdmin())
		{
			PR($this->arParams['FILTER']['UF_CITIES']);
		}
		
		if ($_COOKIE['City'])
		{
			$arFilter['PROPERTY_CITY'] = $_COOKIE['City'];
		}
		else
		{
			$arFilter['PROPERTY_CITY'] = $_REQUEST['city'];
		}*/
		
		//$arFilter['PROPERTY_EXT'] = 17;
		$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, array("nTopCount" => 6), array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'PROPERTY_PRICE', 'PROPERTY_NAME_RU', 'PROPERTY_EXT', 'PROPERTY_POINT_EXT'));
		while($ob = $res->GetNext())
		{
			$arResult['POINT_EXT'][$ob['ID']] = $ob['~PROPERTY_POINT_EXT_VALUE']['TEXT'];
			$arResult['PRICE'][$ob['ID']] = $ob['PROPERTY_PRICE_VALUE'];  //айдишники
			$name = $ob['NAME'];
			if (!empty($ob['PROPERTY_NAME_RU_VALUE']))
			{
				$name = $ob['PROPERTY_NAME_RU_VALUE'];
			}
			$arResult["SECTION_NAME"][$ob['ID']] = $name;
			
			if($ob['PROPERTY_EXT_ENUM_ID'] == 17) //убираем доп пакеты, но каналы HD остаются
				$arRes['PACKAGE'][] = $ob;
		}

		//инициализация
		foreach ($arRes['PACKAGE'] as $itemPackage)
		{
			foreach ($arrCategory as $itemSection)
			{
				$arResult['_PACKAGE_'][$itemPackage['ID']][$itemSection["ID"]] = array('ID' => $itemSection["NAME"]);
			}
		}

		//для каждого пакета находим каналы и записываем их в ячейки
		foreach ($arResult['_PACKAGE_'] as $idPackage => &$arrCategory)
		{
			$arFilter = Array("IBLOCK_ID"=>IBLOCK_CHANNELS, "PROPERTY_PACKAGES" => $idPackage, "ACTIVE"=>"Y", /*"PROPERTY_TYPE" => 15*/);
			//$arFilter = Array("IBLOCK_ID"=>10, "ID" => $idPackage, "ACTIVE"=>"Y", "PROPERTY_TYPE" => 15);  //запрос по айди пакета вывод каналов в пакете(пакет может быть привязан к 1 категории!!!)
			$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, array('ID', 'NAME', 'IBLOCK_SECTION_ID'));
			$count = 0;
			while($ob = $res->GetNext())
			{
				$arrTmp = array();
				foreach ($arrCategory as $key => $item)
				{
					//PR($ob);
					if ($ob['IBLOCK_SECTION_ID'] == $key)
					{
						$count++;
						$itemTmp = array(
							"ID" => $ob['ID'],
							"NAME" => $ob['NAME'],
						);

						$arrCategory[$key]['VALUE'][] = $itemTmp;
					}
				}
				$arrCategory['COUNT'] = $count;
			}
		}
		return $arResult;
	}

    public function cacheAvailable ($arNavParams) {
		//PR($this->arParams);
        if ($this->arParams["CACHE"] != "Y") {
            return false;
        }

        return !$this->StartResultCache(false);
    }

	public function getPackages()
	{
		if(!CModule::IncludeModule("iblock"))
			return;

		$arFilter = array(
			"IBLOCK_ID" => IBLOCK_CHANNELS,
			"ACTIVE"=>"Y",
		);

		//секции
		$rsSections = CIBlockSection::GetList(array(), $arFilter, true, array("ID", "NAME"), false); //Получили список разделов из инфоблока
		$rsSections->SetUrlTemplates(); //Получили строку URL для каждого из разделов (по формату из настроек инфоблока)
		while($arSection = $rsSections->GetNext())
		{
			$arRes["SECTIONS"][] = $arSection;   //содержатся все категории каналов
		}

		$arResult = array();
		$arResult['TYPE_1'] = $this->resultPackages(PACKAGE_TYPE_ROOM, $arRes["SECTIONS"]);
		
		/*вывести только для админов */
		/*global $USER;
		if ($USER->IsAdmin())
		{
			PR($arResult['TYPE_1']);
		}*/
		
		$arResult['TYPE_2'] = $this->resultPackages(PACKAGE_TYPE_HOME, $arRes["SECTIONS"]);
		$arResult['SECTIONS'] = $arRes["SECTIONS"];
		//PR($arResult);
		return $arResult;
	}
}