<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult = array();

try {
    if (!$this->cacheAvailable()) {
		$arResult = $this->getPackages();
		$this->IncludeComponentTemplate();
		
		//$templateCachedData = $this->GetTemplateCachedData();
		/*$cache->EndDataCache(
			array(
				"arResult" => $arResult,
				"templateCachedData" => $templateCachedData
			)
		);*/
    }
	else
	{
		//extract($cache->GetVars());
		//$this->SetTemplateCachedData($templateCachedData);
	}
} catch (Exception $e) {
    ShowError($e->GetMessage());
}

?>