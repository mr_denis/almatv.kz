<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

<div class="container">
	<div class="tv_super_holder">
		<div class="tarif_cat__trigger_holder">
			<span class="tarif_cat__trigger_title">Тарифы</span>
			<ul class="tarif_cat__trigger j-tarif_cat__tabs_head">
				<?if ($arResult['TYPE_1']["SECTION_NAME"]) {?>
					<li data-id = "15" class="tarif_cat__trigger_item active">в квартиру</li>
				<?}?>
				<?if ($arResult['TYPE_2']["SECTION_NAME"]) {?>
					<li data-id = "16" class="tarif_cat__trigger_item">в частный дом</li>
				<?}?>
			</ul>
		</div>
	</div>
</div>

<div class="tarif_table_holder">
	<ul class="tarif_cat__tabs_cont">
		<li class="tarif_cat__tabs_cont_item">
			<div class="tarif_row_holder">
				<div class="tarif_row">
					<div class="tarif_row__inner">
						<div class="container">
							<div class="tv_super_holder">
								<div class="tarif_row__item tarif_row__item--first">&nbsp;</div>
								<div class="tarif_row__table_holder">
									<div class="tarif_row__table">
										<?
										$i = 0;
										foreach ($arResult['TYPE_1']['_PACKAGE_'] as $idPackage => $item) {
											switch ($i)
											{
												case 0:
													$class_color = 'tarif_row__list_title--yel';
													break;
												case 1:
													$class_color = 'tarif_row__list_title--orange_l';
													break;
												case 2:
													$class_color = 'tarif_row__list_title--orange';
													break;
												case 3:
													$class_color = 'tarif_row__list_title--orange';
													break;
											}
											
											?>
										<div class="tarif_row__cell j-tarif_row__cell_channels tarif_row__cell--first">
											<h5 data-id = "<?=$idPackage?>" class="tarif_row__list_title channel_button <?=$class_color?>"><?=$arResult['TYPE_1']["SECTION_NAME"][$idPackage]?></h5>
											<div class="tarif_row__list_ch_box">
												<span class="tarif_row__list_ch"><?=$item['COUNT']//=$arResult['TYPE_1']["COUNT"][$idPackage]['VALUE']?> <span><?=DeclensionNameChannel($item['COUNT'])?></span></span>
												<div data-id = "<?=$idPackage?>" class="tarif_row__list_all_channels"></div>
											</div>
										</div>
										<?
										$i++;
										}?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?foreach ($arResult['SECTIONS'] as $Category) {?>
				<div class="tarif_row">
					<div class="tarif_row__inner">
						<div class="container">
							<div class="tv_super_holder">
								<div class="tarif_row__item">
									<span><?=$Category['NAME']?></span>
								</div>
								<div class="tarif_row__table_holder">
									
									<div class="tarif_row__table">
										<?foreach ($arResult['TYPE_1']['_PACKAGE_'] as $idPackage => $item) {?>
											<?$count = count($item[$Category['ID']]['VALUE']);
											//PR($item);
											
											?>
											<div class="tarif_row__cell j-tarif_row__cell_channels <?if (!$count) echo 'null'?>" data-category="<?=$Category['ID']?>" data-package = "<?=$idPackage?>" data-packages="<?=implode(',',array_keys($arResult['TYPE_1']['SECTION_NAME']))?>">
												<div class="tarif_row__num">
													<span><?=count($item[$Category['ID']]['VALUE'])?></span>
												</div>
											</div>
										<?}?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tarif_popup">
						<div class="container">
							<div class="tarif_popup__loader">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/loader.gif">
							</div>
							<div class="tarif_popup__close"></div>
							<div class="tarif_popup_scroll">
								<!-- Сюда приезжает слайдер аяксом -->
							</div>
						</div>
					</div>
				</div>
				<?}?>
				
				<div class="tarif_row">
					<div class="tarif_row__inner">
						<div class="container">
							<div class="tv_super_holder">
								<div class="tarif_row__item tarif_row__item--last">
									<span>&nbsp;</span>
								</div>
								<div class="tarif_row__table_holder">
									<div class="tarif_row__table">
									<?foreach ($arResult['TYPE_1']['_PACKAGE_'] as $idPackage => $item) {?>												
										<div class="tarif_row__cell j-tarif_row__cell_channels tarif_row__cell--last">
											<span class="tarif_row__list_speed"><?=$arResult['TYPE_1']["PRICE"][$idPackage]?> <span>тг./мес.</span></span><?/*$arResult['TYPE_1']['REF_PRICE'][$arResult['TYPE_1']["PRICE"][$idPackage]]*/?>
											<?if (!empty($arResult['TYPE_1']["POINT_EXT"][$idPackage])) {?>
											<span class="tarif_row__cell__sub_info">
												<span class="tarif_row__cell__sub_text">
													<?=$arResult['TYPE_1']["POINT_EXT"][$idPackage]?>
												</span>
											</span>
											<?}?>
											<a href="/request/?package=<?=$idPackage?>" class="button button-no_width tarif_row__list_btn"><span>Оформить заявку</span><em>Оформить заявку</em></a>
										</div>
									<?}?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li class="tarif_cat__tabs_cont_item"> <!-- в частный дом -->
			<div class="tarif_row_holder">
				<div class="tarif_row">
					<div class="tarif_row__inner">
						<div class="container">
							<div class="tv_super_holder">
								<div class="tarif_row__item tarif_row__item--first">&nbsp;</div>
								<div class="tarif_row__table_holder">
									<div class="tarif_row__table">
										<?
										$i = 0;
										foreach ($arResult['TYPE_2']['_PACKAGE_'] as $idPackage => $item) {
											switch ($i)
											{
												case 0:
													$class_color = 'tarif_row__list_title--yel';
													break;
												case 1:
													$class_color = 'tarif_row__list_title--orange_l';
													break;
												case 2:
													$class_color = 'tarif_row__list_title--orange';
													break;
												case 3:
													$class_color = 'tarif_row__list_title--orange';
													break;
											}
											?>
										<div class="tarif_row__cell j-tarif_row__cell_channels tarif_row__cell--first">
											<h5 data-id = "<?=$idPackage?>" class="tarif_row__list_title channel_button <?=$class_color?>"><?=$arResult['TYPE_2']["SECTION_NAME"][$idPackage]?></h5>
											<div class="tarif_row__list_ch_box">
												<span class="tarif_row__list_ch"><?=$item['COUNT']?> <span><?=DeclensionNameChannel($item['COUNT'])?></span></span>
												<div data-id = "<?=$idPackage?>" class="tarif_row__list_all_channels"></div>
											</div>
										</div>
										<?
										$i++;
										}?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<?foreach ($arResult['SECTIONS']as $Category) {?>
				<div class="tarif_row">
					<div class="tarif_row__inner">
						<div class="container">
							<div class="tv_super_holder">
								<div class="tarif_row__item">
									<span><?=$Category['NAME']?></span>
								</div>
								<div class="tarif_row__table_holder">
									
									<div class="tarif_row__table">
										<?foreach ($arResult['TYPE_2']['_PACKAGE_'] as $idPackage => $item) {?>
											<?$count = count($item[$Category['ID']]['VALUE'])?>
											<div class="tarif_row__cell j-tarif_row__cell_channels <?if (!$count) echo 'null'?>" data-category="<?=$Category['ID']?>" data-package = "<?=$idPackage?>" data-packages="<?=implode(',',array_keys($arResult['TYPE_2']['SECTION_NAME']))?>">
												<div class="tarif_row__num">
													<span><?=count($item[$Category['ID']]['VALUE'])?></span>
												</div>
											</div>
										<?}?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tarif_popup">
						<div class="container">
							<div class="tarif_popup__loader">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/loader.gif">
							</div>
							<div class="tarif_popup__close"></div>
							<div class="tarif_popup_scroll">
								<!-- Сюда приезжает слайдер аяксом -->
							</div>
						</div>
					</div>
				</div>
				<?}?>
				
				<div class="tarif_row">
					<div class="tarif_row__inner">
						<div class="container">
							<div class="tv_super_holder">
								<div class="tarif_row__item tarif_row__item--last">
									<span>&nbsp;</span>
								</div>
								<div class="tarif_row__table_holder">
									<div class="tarif_row__table">
									<?foreach ($arResult['TYPE_2']['_PACKAGE_'] as $idPackage => $item) {?>
										<div class="tarif_row__cell j-tarif_row__cell_channels tarif_row__cell--last">
											<span class="tarif_row__list_speed"><?=$arResult['TYPE_2']["PRICE"][$idPackage]//$arResult['TYPE_2']['REF_PRICE'][$arResult['TYPE_2']["PRICE"][$idPackage]]?> <span>тг./мес.</span></span>
											<?if (!empty($arResult['TYPE_2']["POINT_EXT"][$idPackage])) {?>
											<span class="tarif_row__cell__sub_info">
												<span class="tarif_row__cell__sub_text">
													<?=$arResult['TYPE_2']["POINT_EXT"][$idPackage]?>
												</span>
											</span>
											<?}?>
											<?/*if (!empty($arResult['TYPE_2']["POINT_EXT"][$idPackage])) {?>
											<span class="tarif_row__cell__sub_info">
												<span class="tarif_row__cell__sub_text">
													<?=$arResult['TYPE_2']["POINT_EXT"][$idPackage]?>
												</span>
											</span>
											<?}*/?>
											<a href="/request/?package=<?=$idPackage?>" class="button button-no_width tarif_row__list_btn"><span>Оформить заявку</span><em>Оформить заявку</em></a>
										</div>
									<?}?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</li>
	</ul>
</div>