<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

Class IBlockSectionList extends CBitrixComponent {
    protected $DBResult;
    protected $arDependedModules = array(
        "iblock"
    );

    public function IncludeModules() {
        foreach ($this->arDependedModules as $module) {
            if (!CModule::IncludeModule($module)) {
                throw new Exception("Для правильной работы компонента необходим модуль $module");
            }
        }
    }

    public function onPrepareComponentParams ($arParams) {
        $arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
        $arParams["IBLOCK_CODE"] = strval($arParams["IBLOCK_CODE"]);
        $arParams["LIMIT"] = intval($arParams["LIMIT"]) > 0 ? intval($arParams["LIMIT"]) : 20;
        $arParams["SORT"] = is_array($arParams["SORT"]) && !empty($arParams["SORT"]) ? $arParams["SORT"] : array();
        $arParams["SELECT"] = is_array($arParams["SELECT"]) && !empty($arParams["SELECT"]) ? $arParams["SELECT"] : array();
        $arParams["FILTER"] = is_array($arParams["FILTER"]) && !empty($arParams["FILTER"]) ? $arParams["FILTER"] : array();
        $arParams["PAGER_TITLE"] = strval($arParams["PAGER_TITLE"]) != "" ? strval($arParams["PAGER_TITLE"]) : "IBlock Section List";
        $arParams["PAGER_TEMPLATE"] = strval($arParams["PAGER_TEMPLATE"]) != "" ? strval($arParams["PAGER_TEMPLATE"]) : "";
        $arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"] == "Y";
        $arParams["TREE"] = $arParams["TREE"] == "Y" ? "Y" : "N";
        $arParams["MAX_DEPTH_LEVEL"] = intval($arParams["MAX_DEPTH_LEVEL"]);
        $arParams["CACHE"] = $arParams["CACHE"] == "N" ? "N" : "Y";
        $arParams["CACHE_TIME"] = intval($arParams["CACHE_TIME"]) > 0 ? intval($arParams["CACHE_TIME"]) : 36000;
        return $arParams;
    }

    public function checkParams() {
        if ($this->arParams["IBLOCK_ID"] <= 0 && $this->arParams["IBLOCK_CODE"] == "") {
            throw new Exception("должен присутствовать хотя бы один из параметров IBLOCK_ID или IBLOCK_ID");
        }
    }

    public function getSections ($arSort, $arFilter, $arNavParams, $arSelect) {
        $CIBlockSection = new CIBlockSection;
        $CFile = new CFile;
        $this->DBResult = $CIBlockSection->GetList($arSort, $arFilter, false, $arSelect, $arNavParams);
        $arSections = array();
        while ($arSection = $this->DBResult->GetNext()) {
            if (isset($arSection["PICTURE"])) {
                $arSection["PICTURE"] = $CFile->GetFileArray($arSection["PICTURE"]);
            }
            $arSections[] = $arSection;
        }
        if ($this->arParams["TREE"] == "Y") {
            $arTempSections = $arSections;
            $arSections = array();
            foreach ($arTempSections as $arSection) {
                if (intval($arSection["DEPTH_LEVEL"]) > 1) {
                    continue;
                }
                $arSection["SECTIONS"] = $this->getSubSections($arTempSections, $arSection["ID"]);
                $arSections[] = $arSection;
            }
        }
        return $arSections;
    }

    public function getSubSections ($arSections, $parentID) {
        $arSubSections = array();
        foreach ($arSections as $arSection) {
            if ($arSection["IBLOCK_SECTION_ID"] == $parentID) {
                $arSection["SECTIONS"] = $this->getSubSections($arSections, $arSection["ID"]);
                $arSubSections[] = $arSection;
            }
        }
        return $arSubSections;
    }

    public function getSort() {
        $arSort = array();
        if ($this->arParams["TREE"] == "Y") {
            $arSort["LEFT_MARGIN"] = "ASC";
        }
        if (!empty($this->arParams["SORT"])) {
            return array_merge($arSort, $this->arParams["SORT"], $arSort);
        }
        $arSort["SORT"] = "ASC";
        $arSort["ID"] = "DESC";
        return $arSort;
    }

    public function getSelect() {
        $arSelect = array();
        $arSelect[] = "ID";
        $arSelect[] = "NAME";
        if ($this->arParams["TREE"] == "Y") {
            $arSelect[] = "LEFT_MARGIN";
            $arSelect[] = "DEPTH_LEVEL";
            $arSelect[] = "IBLOCK_SECTION_ID";
        }
        $arSelect = array_merge($arSelect, $this->arParams["SELECT"]);
        return array_unique($arSelect);
    }

    public function getFilter() {
        $arFilter = array();
        if ($this->arParams["IBLOCK_ID"] > 0) {
            $arFilter["IBLOCK_ID"] = $this->arParams["IBLOCK_ID"];
        }
        if ($this->isNeedUserFields() && !isset($arFilter["IBLOCK_ID"])) {
            $arFilter["IBLOCK_ID"] = $this->getIBlockID($this->arParams["IBLOCK_CODE"]);
        }
        if (!isset($arFilter["IBLOCK_ID"]) && $this->arParams["IBLOCK_CODE"] != "") {
            $arFilter["IBLOCK_CODE"] = $this->arParams["IBLOCK_CODE"];
        }
        if ($this->arParams["MAX_DEPTH_LEVEL"] > 0) {
            $arFilter["<=DEPTH_LEVEL"] = $this->arParams["MAX_DEPTH_LEVEL"];
        }

        return array_merge($arFilter, $this->arParams["FILTER"], $arFilter);
    }

    public function getNavParams () {
        return array("nPageSize" => $this->arParams["COUNT"]);
    }

    public function cacheAvailable ($arNavParams) {
        if ($this->arParams["CACHE"] != "Y") {
            return false;
        }
        $CDBResult = new CDBResult;
        $arNavParams = $this->getNavParams();
        $arNavigation = $CDBResult->GetNavParams($arNavParams);
        return !$this->StartResultCache(false, array($arNavigation));
    }

    public function getNavString () {
        if ($this->DBResult instanceof CDBResult) {
            return $this->DBResult->GetPageNavStringEx(
                $navComponentObject,
                $this->arParams["PAGER_TITLE"],
                $this->arParams["PAGER_TEMPLATE"],
                $this->arParams["PAGER_SHOW_ALWAYS"],
                $this
            );
        }
        return false;
    }

    public function getIBlockID ($code) {
        $CIBlock = new CIBlock;
        $arSort = array("ID" => "DESC");
        $arFilter = array("CODE" => $code);
        $arSelect = array("ID");
        $rsIBlock = $CIBlock->GetList($arSort, $arFilter, $arSelect);
        $rsIBlock->NavStart(1);
        $arIBlock = $rsIBlock->GetNext();
        if (!$arIBlock) {
            throw new Exception("Инфоблока с кодом $code не существует");
        }
        return $arIBlock["ID"];
    }

    public function isNeedUserFields () {
        foreach ($this->arParams["SELECT"] as $field) {
            if (substr($field, 0, 3) == "UF_") {
                return true;
            }
        }
        return false;
    }
}