<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult = array();

try {
    $this->IncludeModules();
    $this->checkParams();
    if (!$this->cacheAvailable()) {
        $arSort = $this->getSort();
        $arFilter = $this->getFilter();
        $arSelect = $this->getSelect();
        $arResult["SECTIONS"] = $this->getSections($arSort, $arFilter, $arNavParams, $arSelect);
        $arResult["NAV_STRING"] = $this->getNavString();
        $this->IncludeComponentTemplate();
    }
} catch (Exception $e) {
    ShowError($e->GetMessage());
}