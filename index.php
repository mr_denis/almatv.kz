<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "телевидение, интернет");
$APPLICATION->SetPageProperty("description", "AlmaTV");
$APPLICATION->SetTitle("Alma TV");
?><div class="slider_and_menu_holder clear">
	<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"left", 
	array(
		"CITY" => getCity(),
		"ROOT_MENU_TYPE" => "left",
		"MAX_LEVEL" => "1",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_THEME" => "site",
		"CHILD_MENU_TYPE" => "left",
		"COMPONENT_TEMPLATE" => "left"
	),
	false
);?>

	<?
	$cityId = CITY_ID_ALMATA;
	if (!empty($_COOKIE['City']))
	{
		$cityId = $_COOKIE['City'];
	}
	$GLOBALS["arrFilterSlider"] = array("PROPERTY_CITY_TO_NEWS.ID"=>$cityId);
	?>
	<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"main_slider", 
	array(
		"CITY" => $cityId, //для правильной работы кеша по городам
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => "5",
		"NEWS_COUNT" => "10",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "DESC",
		"FILTER_NAME" => "arrFilterSlider",
		"FIELD_CODE" => array(
			0 => "SORT",
			1 => "DATE_ACTIVE_TO",
			2 => "ACTIVE_TO",
			3 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "HREF",
			1 => "TYPE",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "A",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "120",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PARENT_SECTION" => "",
		"SORT_BY2" => "NAME",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "main_slider",
		"SET_LAST_MODIFIED" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>

		<?if (isBlockPayment()) {?>
			<div class="box box--dotted box--pay_top">
				<div class="pay_online">
					<h2 class="pay_online__title">Оплачивайте услуги онлайн</h2>
					<form class = "pay_online_pr" action="#" id="j-pay_online_pr-bull">
						<script>
							$(document).ready(function() {
								$("#j-pay_online_pr-bull").validate({
									rules: {
										number: {
											required: true,
										},
										summ: {
											required: true
										}
									}
								});
								$('.pay_online__field--number').mask('0000000000');
								$('.pay_online__field--sum').mask("00000");
							});
						</script>
						<fieldset>
							<div class="pay_online__row clear">
								<div class="pay_online__col_left">
									<input type="tel" class="pay_online__field pay_online__field--number" name = "number">
									<span class="pay_online__label">Номер договора</span>
								</div>
								<div class="pay_online__col_right">
									<input type="tel" class="pay_online__field pay_online__field--sum" name = "summ">
									<span class="pay_online__label">Сумма к пополнению</span>
								</div>
							</div>
							<style>
								.no-active-pay_online
								{
									background: #888;
								}
							</style>
							
							<div class="pay_online__row pay_online__row--m0 clear">
								<div class="pay_online__col_left">
									<button class="pay_online__btn pay_online_button">Оплатить</button>
								</div>
								<div class="pay_online__col_right">
									<ul class="pay_online__way">
										<li class="pay_online__way_item"><a target = "_blank" href="http://www.visa.com.ru/ru/ru-ru/personal/security/onlineshopping.shtml"><img width="55" alt="Visa" src="/bitrix/templates/almatv/images/visa.png" height="23"></a></li>
										<li class="pay_online__way_item"><a target = "_blank" href="http://www.mastercard.com/ru/consumer/"><img width="66" alt="MasterCard" src="/bitrix/templates/almatv/images/master_card.png" height="23"></a></li>
										<li class="pay_online__way_item pay_online__way_item--kassa"><a target = "_blank" href="https://my.kassa24.kz/entry/pay"><img width="27" alt="PayKassa24" src="/bitrix/templates/almatv/images/pay_kassa24.png" height="21"></a></li>
										<li class="pay_online__way_item"><a target = "_blank" href="https://qiwi.ru/"><img width="27" alt="PayQiwi" src="/bitrix/templates/almatv/images/pay_qiwi.png" height="27"></a></li>
									</ul>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		<?} else {?>
			<div class="box box--dotted box--pay_top">
				<div class="pay_online">
					<h2 class="pay_online__title">Проверьте баланс своего счёта онлайн</h2>
					<form class = "check-balance" id="j-check-balance" action="#" method="POST">
						<script>
							$(document).ready(function() {
								$("#j-check-balance").validate({
									rules: {
										number: {
											required: true,
										},
										surname: {
											required: true,
											minlength: 1,
											maxlength: 30
										}
									}
								});
							});
						</script>
						<fieldset>
							<div class="pay_online__row clear">
								<div class="pay_online__col_left">
									<input name="number" type="tel" pattern="\d*" class="pay_online__field pay_online__field--mask" minlength="1" maxlength="10">
									<label class="pay_online__label">Номер договора</label>
								</div>
								<div class="pay_online__col_right">
									<input name="surname" type="text" class="pay_online__field pay_online__field--surname" minlength="1" maxlength="30" autocorrect="off" autocapitalize="words" >
									<label class="pay_online__label">Ваша фамилия</label>
								</div>
							</div>
							<div class="pay_online__row pay_online__row--m0 clear">
								<div class="pay_online__col_left">
									<button class="pay_online__btn pay_online__btn_filial">Проверить баланс</button>
								</div>
								<div class="pay_online__col_right">
									<span class="pay_online__desc">Результаты проверки актуальны на момент подачи заявки</span>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		<?}?>
		</div>
		
		<div class="ususal_box ususal_box--hide1280">
			<?if (isBlockPayment()) {?>
			<div class="box box--dotted box--pay">
				<div class="pay_online">
					<h2 class="pay_online__title">Оплачивайте услуги онлайн</h2>
					<form class = "pay_online_pr" action="#" id="j-check-balance_mini">
						<script>
							$(document).ready(function() {
								$("#j-check-balance_mini").validate({
									rules: {
										number: {
											required: true,
										},
										summ: {
											required: true
										}
									}
								});
								$('.pay_online__field--number2').mask('0000000000');
								$('.pay_online__field--sum2').mask("00000");
							});
						</script>
						<fieldset>
							<div class="pay_online__row clear">
								<div class="pay_online__col_left">
									<input type="tel" pattern="\d*" class="pay_online__field pay_online__field--number2" name = "number">
									<span class="pay_online__label">Номер договора</span>
								</div>
								<div class="pay_online__col_right">
									<input type="tel" class="pay_online__field pay_online__field--sum2" name = "summ">
									<span class="pay_online__label">Сумма к пополнению</span>
								</div>
							</div>
							<div class="pay_online__row pay_online__row--m0 clear">
								<div class="pay_online__col_left">
									<button class = "pay_online_button">Оплатить</button>
								</div>
								<div class="pay_online__col_right">
									<ul class="pay_online__way">
										<li class="pay_online__way_item"><a href="#"><img width="55" alt="image_description" src="/bitrix/templates/almatv/images/visa.png" height="23"></a></li>
										<li class="pay_online__way_item"><a href="#"><img width="66" alt="image_description" src="/bitrix/templates/almatv/images/master_card.png" height="23"></a></li>
										<li class="pay_online__way_item"><a href="#"><img width="27" alt="image_description" src="/bitrix/templates/almatv/images/pay_kassa24.png" height="21"></a></li>
										<li class="pay_online__way_item"><a href="#"><img width="27" alt="image_description" src="/bitrix/templates/almatv/images/pay_qiwi.png" height="27"></a></li>
									</ul>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
			<?} else {?>
			<div class="box box--dotted box--pay">
				<div class="pay_online">
					<h2 class="pay_online__title">Проверьте баланс своего счёта онлайн</h2>
					<form class = "check-balance" id="j-check-balance_mini" action="#" method="POST">
						<script>
							$(document).ready(function() {
								$("#j-check-balance_mini").validate({
									rules: {
										number: {
											required: true
										},
										surname: {
											required: true,
											minlength: 1,
											maxlength: 30
										}
									},
									errorPlacement: function(error, element) {
										error.insertAfter(element);
									}
								});
								$('.pay_online__field--mask').mask('0000000000');

								var options =  { 
									'translation': {A: {pattern: /[А-Я а-яёЁ]/}},
								  onKeyPress: function(cep, event, currentField, options){
								    cep = cep.slice(0, 1).toUpperCase() + cep.slice(1);
								    $(currentField).val(cep);
								  }
								};

								$('.pay_online__field--surname').mask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', options);
							});
						</script>
						<fieldset>
							<div class="pay_online__row clear">
								<div class="pay_online__col_left">
									<input name="number" type="tel" pattern="\d*" class="pay_online__field pay_online__field--mask" minlength="1" maxlength="10">
									<label class="pay_online__label">Номер договора</label>
								</div>
								<div class="pay_online__col_right">
									<input name="surname" type="text" class="pay_online__field pay_online__field--surname" minlength="1" maxlength="30" autocorrect="off" autocapitalize="words">
									<label class="pay_online__label">Ваша фамилия</label>
								</div>
							</div>
							<div class="pay_online__row pay_online__row--m0 clear">
								<div class="pay_online__col_left">
									<button class="pay_online__btn pay_online__btn_filial">Проверить баланс</button>
								</div>
								<div class="pay_online__col_right">
									<span class="pay_online__desc">Результаты проверки актуальны на момент подачи заявки</span>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
			<?}?>

			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				".default",
				Array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_TEMPLATE_PATH."/include/banner1280.php",
					"EDIT_TEMPLATE" => ""
				)
			);?>
		</div>

		<div class="ususal_box">

			<div class="box">
				<div class="why_we clear">
				
				<h2 class="why_we__title">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_TEMPLATE_PATH."/include/banner.php",
						"EDIT_TEMPLATE" => ""
					)
				);?>

				</h2>
				<?$APPLICATION->IncludeComponent(
					"bitrix:news.list", 
					"advantages", 
					array(
						"IBLOCK_ID" => "22",
						"NEWS_COUNT" => "5",
						"SORT_BY1" => "SORT",
						"SORT_ORDER1" => "ASC",
						"SORT_BY2" => "ACTIVE_FROM",
						"SORT_ORDER2" => "DESC",
						"FILTER_NAME" => "",
						"CHECK_DATES" => "Y",
						"DETAIL_URL" => "",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_FILTER" => "Y",
						"CACHE_GROUPS" => "N",
						"PREVIEW_TRUNCATE_LEN" => "100",
						"ACTIVE_DATE_FORMAT" => "j F Y",
						"SET_TITLE" => "N",
						"SET_BROWSER_TITLE" => "Y",
						"SET_META_KEYWORDS" => "Y",
						"SET_META_DESCRIPTION" => "Y",
						"SET_STATUS_404" => "Y",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
						"ADD_SECTIONS_CHAIN" => "Y",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"PARENT_SECTION_CODE" => "",
						"INCLUDE_SUBSECTIONS" => "Y",
						"DISPLAY_DATE" => "Y",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"PAGER_TEMPLATE" => ".default",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"PAGER_TITLE" => "",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"PARENT_SECTION" => "",
						"IBLOCK_TYPE" => "news",
						"FIELD_CODE" => array(
							0 => "",
							1 => "",
						),
						"PROPERTY_CODE" => array(
							0 => "MAIN",
							1 => "",
						)
					),
					false
				);?>

				</div>
			</div>
		</div>

		<div id = "one-tv" class="ususal_box">
			<!-- TV -->
			<!-- фильтр по городу -->	
			<?
			
			$arrFilter = Array(
				"PROPERTY_MAIN_VALUE" => 'Y',//фильтр вывод на главной
				"PROPERTY_TYPE" => PACKAGE_TYPE_ROOM,
			);

			if ($_COOKIE['City'])
			{
				$arrFilter['PROPERTY_CITY'] = $_COOKIE['City'];
			}
			else
			{
				$arrFilter['PROPERTY_CITY'] = $_REQUEST['city'];
			}
			?>  
						
			<?$APPLICATION->IncludeComponent(
			"bitrix:news.list", 
			"tvMain", 
			array(
				"CITY_ID" => $cityId, //для правильной работы кеша по городам
				"IBLOCK_ID" => "10",
				"NEWS_COUNT" => "4",
				"SORT_BY2" => "ACTIVE_FROM",
				"SORT_ORDER2" => "DESC",
				"SORT_BY1" => "SORT",
				"SORT_ORDER1" => "ASC",
				"FILTER_NAME" => "arrFilter",
				"FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"PROPERTY_CODE" => array(
					0 => "NAME_RU",
					1 => "PRICE",
					2 => "PACKAGES",
					3 => "",
				),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"PREVIEW_TRUNCATE_LEN" => "100",
				"ACTIVE_DATE_FORMAT" => "j F Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "Y",
				"SET_META_KEYWORDS" => "Y",
				"SET_META_DESCRIPTION" => "Y",
				"SET_STATUS_404" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
				"ADD_SECTIONS_CHAIN" => "Y",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"PARENT_SECTION" => "",
				"IBLOCK_TYPE" => "news"
			),
			false
		);?>
			 
		<!-- INET -->
		<?$APPLICATION->IncludeComponent(
		"bitrix:news.list", 
		"internetMain", 
		array(
			"CITY_ID" => $cityId, //для правильной работы кеша по городам
			"IBLOCK_ID" => "18",
			"NEWS_COUNT" => "4",
			"SORT_BY2" => "NAME",
			"SORT_ORDER2" => "ASC",
			"SORT_BY1" => "SORT",
			"SORT_ORDER1" => "ASC",
			"FILTER_NAME" => "arrFilter",
			"FIELD_CODE" => array(
				0 => "",
				1 => "",
			),
			"PROPERTY_CODE" => array(
				0 => "MAIN",
				1 => "",
			),
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			"PREVIEW_TRUNCATE_LEN" => "100",
			"ACTIVE_DATE_FORMAT" => "j F Y",
			"SET_TITLE" => "N",
			"SET_BROWSER_TITLE" => "Y",
			"SET_META_KEYWORDS" => "Y",
			"SET_META_DESCRIPTION" => "Y",
			"SET_STATUS_404" => "Y",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
			"ADD_SECTIONS_CHAIN" => "Y",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"PARENT_SECTION_CODE" => "",
			"INCLUDE_SUBSECTIONS" => "Y",
			"DISPLAY_DATE" => "Y",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"PAGER_TEMPLATE" => ".default",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"PAGER_TITLE" => "",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"PARENT_SECTION" => "",
			"IBLOCK_TYPE" => "internet"
		),
		false
	);?>
				
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		".default",
		Array(
			"AREA_FILE_SHOW" => "file",
			"PATH" => SITE_TEMPLATE_PATH."/include/banners.php",
			"EDIT_TEMPLATE" => ""
		)
	);?>

		</div>
		<?
		$GLOBALS["arrFilterNews"] = array("PROPERTY_CITY_TO_NEWS.ID"=>$cityId, "IBLOCK_ID" => IB_NEWS);
		?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:news.list", 
			"newsMain", 
			array(
				"CITY_ID" => $cityId, //для правильной работы кеша по городам
				"IBLOCK_ID" => IB_NEWS,
				"NEWS_COUNT" => "4",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "arrFilterNews",
				"FIELD_CODE" => array(
					0 => "PREVIEW_PICTURE",
					1 => "DETAIL_PICTURE",
					2 => "",
				),
				"PROPERTY_CODE" => array(
					0 => "",
					1 => "",
				),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"PREVIEW_TRUNCATE_LEN" => "100",
				"ACTIVE_DATE_FORMAT" => "j F Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "Y",
				"SET_META_KEYWORDS" => "Y",
				"SET_META_DESCRIPTION" => "Y",
				"SET_STATUS_404" => "Y",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
				"ADD_SECTIONS_CHAIN" => "Y",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"PARENT_SECTION" => "",
				"IBLOCK_TYPE" => "news"
			),
			false
		);?>
		
		<div class="interview">
		<!-- Опрос interview -->
		<?//фильтруем по уже отвеченным
		$filter = explode(",", $_COOKIE['Questions']);
		global $arFilter;
		$arFilter = Array(
			"!ID" => $filter,//фильтр вывод на главной
		);
		?>
		
		<?$APPLICATION->IncludeComponent(
			"bitrix:news.list",
			"interview",
			Array(
				"IBLOCK_ID" => "23",
				"NEWS_COUNT" => "1",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "SORT",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "arFilter",
				"FIELD_CODE" => array(0=>"",1=>"",),
				"PROPERTY_CODE" => array(0=>"VOICES",1=>"",2=>"",),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"PREVIEW_TRUNCATE_LEN" => "100",
				"ACTIVE_DATE_FORMAT" => "j F Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "N",
				"DISPLAY_DATE" => "N",
				"DISPLAY_NAME" => "N",
				"DISPLAY_PICTURE" => "N",
				"DISPLAY_PREVIEW_TEXT" => "N",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"PARENT_SECTION" => "",
				"IBLOCK_TYPE" => "interview"
			)
		);
/*$filter = explode(",", $_COOKIE['Questions']);
global $arFilter;
$Filter = Array(
	"ID" => $filter,//фильтр вывод на главной
);*/
if (!empty($_COOKIE['Questions'])) {

$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"interviewResult", 
	array(
		"ANSWER" => "Благодарим за Ваш голос!",
		"IBLOCK_ID" => "23",
		"NEWS_COUNT" => "1",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "Filter",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "VOICES",
			1 => "SECTION_ANSWERS",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "100",
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PARENT_SECTION" => "",
		"IBLOCK_TYPE" => "interview"
	),
	false
);
}
?>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>