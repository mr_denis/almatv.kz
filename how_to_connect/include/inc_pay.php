<div class="connect_content__item">
	<h3 class="connect_content__title">Оплату можно внести через</h3>
	<ul>
		<li>Кассу торгового офиса</li>
		<li>Представителя Бригады</li>
		<li>Терминалы моментальной оплаты</li>
	</ul>
	 При новом подключении, первую оплату можно внести через торгового представителя компании.<br>
</div>