<ul class="connect_way__list">
	<li class="connect_way__list_item">
		<div class="connect_way__list_img connect_way__list_img--connect_app">
			<img alt="image_description" src="/bitrix/templates/almatv/images/connect_app.jpg">
		</div>
		<a href="/request/" class="button button-no_width connect_way__list_btn">Оставить заявку на сайте</a> 
	</li>
	<li class="connect_way__list_item">
		<div class="connect_way__list_img">
			<img alt="image_description" src="/bitrix/templates/almatv/images/connect_office.jpg">
		</div>
		<span class="connect_way__list_desc">Обратиться в ближайший<br>
			<a class="connect_way__list_desc_link" href="/company/contacts/">торговый офис</a>
		</span> 
	</li>
	<li class="connect_way__list_item">
		<div class="connect_way__list_img connect_way__list_img--connect_call">
			<img alt="image_description" src="/bitrix/templates/almatv/images/connect_call.jpg">
		</div>
		Обратиться в Колл-центр или к&nbsp;Онлайн-консультанту 
	</li>
	<li class="connect_way__list_item">
		<div class="connect_way__list_img connect_way__list_img--connect_tp">
			<img alt="image_description" src="/bitrix/templates/almatv/images/connect_tp.jpg">
		</div>
		<span class="connect_way__list_desc">Заключить договор через&nbsp;торгового&nbsp;представителя<!-- <a href="#" title="Обратитесь к торговому представителю вашего района" class="connect_way__list_faq"></a> --></span> 
	</li>
 </ul>