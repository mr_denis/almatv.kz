<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как подключиться?");
?>
<h1>
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/how_to_connect/include/inc_title.php"
		)
	);?>
</h1>
<div class="connect_way">
	<span class="connect_way__mini_desc">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			Array(
			"COMPONENT_TEMPLATE" => ".default",
			"AREA_FILE_SHOW" => "file",
			"AREA_FILE_SUFFIX" => "inc",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/how_to_connect/include/inc_top.php"
			)
		);?>
	</span>
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/how_to_connect/include/inc_image_block.php"
		)
	);?>
</div>
<div class="corporative">
	<div class="corporative__img">
		<img width="88" alt="image_description" src="/bitrix/templates/almatv/images/connect_letter.jpg" height="88">
	</div>
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/how_to_connect/include/inc_corporative_text.php"
		)
	);?>	
</div>
<div class="connect_content">
	<div class="connect_content__item">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			Array(
			"COMPONENT_TEMPLATE" => ".default",
			"AREA_FILE_SHOW" => "file",
			"AREA_FILE_SUFFIX" => "inc",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/how_to_connect/include/inc_agreement.php"
			)
		);?>	
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			Array(
			"COMPONENT_TEMPLATE" => ".default",
			"AREA_FILE_SHOW" => "file",
			"AREA_FILE_SUFFIX" => "inc",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/how_to_connect/include/inc_filling_contract.php"
			)
		);?>	
	</div>
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/how_to_connect/include/inc_pay.php"
		)
	);?>	
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/how_to_connect/include/inc_bottom_text.php"
		)
	);?>	
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>