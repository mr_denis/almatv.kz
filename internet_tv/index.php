<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Интернет+TV");
?>

<script src="<?=SITE_TEMPLATE_PATH?>/js/tv.js"></script>

<?
//вынести в компонент, город пернести в сессию
if (isInetCity($_COOKIE["City"])) {
$arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "PROPERTY_TYPE" => $type);  //тарифы

if ($_COOKIE['City'])
{
	$arFilter['PROPERTY_CITY'] = $_COOKIE['City'];
}
else
{
	$arFilter['PROPERTY_CITY'] = $_REQUEST['city'];
}

$arFilter['PROPERTY_EXT'] = 17;
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, array("nTopCount" => 4), array('ID', 'NAME', 'PROPERTY_PRICE'));

while($ob = $res->GetNext())
{
	$ress = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>9, "PROPERTY_PACKAGES" => $ob['ID'], "ACTIVE"=>"Y"), false, false, array('ID', 'NAME'));
	$countChannels = $ress->SelectedRowsCount();
	
	$itemPaskage = array(
		"ID" => $ob['ID'],
		"NAME" => $ob['NAME'],
		"COUNT_CHANNELS" => $countChannels,
		"PRICE" => $ob['PROPERTY_PRICE_VALUE'],
	);
	
	$arResult['PACKAGES'][$ob['ID']] = $itemPaskage;
}

$arFilter = Array("IBLOCK_ID"=>18, "ACTIVE"=>"Y");  //пакеты в квартиру
if ($_COOKIE['City'])
{
	$arFilter['PROPERTY_CITY'] = $_COOKIE['City'];
}
else
{
	$arFilter['PROPERTY_CITY'] = $_REQUEST['city'];
}

$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, array("nTopCount" => 5), array('ID', 'NAME', 'PROPERTY_V', 'PREVIEW_PICTURE', 'PROPERTY_PRICE'));
while($ob = $res->GetNext())
{
	$picture = CFile::ResizeImageGet(
		$ob["PREVIEW_PICTURE"],
		array("width" => 23, "height" =>20),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		true,
		false
	);
	$ob['PREVIEW_PICTURE'] = $picture['src'];
	$arResult['INET'][$ob['ID']] = $ob;
}

$arFilter = Array("IBLOCK_ID"=>19, "ACTIVE"=>"Y");  //тарифы
if ($_COOKIE['City'])
{
	$arFilter['PROPERTY_CITY'] = $_COOKIE['City'];
}
else
{
	$arFilter['PROPERTY_CITY'] = $_REQUEST['city'];
}

/*инициализация*/
$arInternetTV = array();

foreach ($arResult['INET'] as $keyInet => $inetItem)
{
	foreach ($arResult['PACKAGES'] as $keyTv => $tvItem)
	{
		$arInternetTV[$inetItem['ID']][$tvItem['ID']] = array(
			"INTERNET_NAME" => $arResult['INET'][$keyInet]['NAME'],
			"TV_NAME" => $arResult['PACKAGES'][$keyTv]['NAME'],
			"EXIST" => false,
		);
	}
}

/*заполнение*/
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, array('ID', 'NAME', 'PROPERTY_TV', 'PROPERTY_INTERNET', 'PROPERTY_PRICE'));
while($ob = $res->GetNext())
{
	$inetId = $ob['PROPERTY_INTERNET_VALUE'];
	$tvId = $ob['PROPERTY_TV_VALUE'];
	
	$priceTv = $arResult['PACKAGES'][$tvId]['PRICE'];
	$priceInet = $arResult['INET'][$inetId]['PROPERTY_PRICE_VALUE'];
	$discountPrice = ($priceTv + $priceInet) - $ob['PROPERTY_PRICE_VALUE'];
	$arInternetTV[$inetId][$tvId] = array(
		"TV_NAME" => $arResult['PACKAGES'][$tvId]['NAME'],
		"TV_PRICE" => $priceTv,
		"INTERNET_NAME" => $arResult['INET'][$inetId]['NAME'],
		"INTERNET_PRICE" => $priceInet ,
		"TOTAL_PRICE" => $ob['PROPERTY_PRICE_VALUE'],
		"DISCOUNT" => round($discountPrice / $ob['PROPERTY_PRICE_VALUE'] * 100), 
		"DISCOUNT_PRICE" => $discountPrice,
		"EXIST" => true,
	);
}

?>
<div class="container">
	<div class="tv_super_holder">
		<h1>Интернет + Телевидение</h1>
		<span class="inettv_main_desc">Если вы подключаете одновременно интернет и ТВ, мы готовы предложить вам наиболее выгодные цены.<br /><br />
		<span>Нажмите на ячейку для дополнительной информации по услуге передачи данных. Для отображения детальной информации по ТВ-пакету нажмите </span><img src = "/images/list.png" /></span> 
	</div>
</div>

<div class="tarif_row_holder tarif_row_holder--internettv">
	<div class="tarif_row">
		<div class="tarif_row__inner">
			<div class="container">
				<div class="tv_super_holder">
					<div class="tarif_row__item tarif_row__item--first">&nbsp;</div>
					<div class="tarif_row__table_holder">
						<div class="tarif_row__table">
							
							<?
							$i = 0;
							foreach ($arResult['PACKAGES'] as $packageItem) {
								switch ($i++ % 4)
								{
									case 0:
										$class_color = 'tarif_row__list_title--yel';
										break;
									case 1:
										$class_color = 'tarif_row__list_title--orange_l';
										break;
									case 2:
										$class_color = 'tarif_row__list_title--orange';
										break;
									case 3:
										$class_color = 'tarif_row__list_title--orange';
										break;
								}
							?>
							<div class="tarif_row__cell j-tarif_row__cell_channels tarif_row__cell--first">
								<h5 data-id="<?=$packageItem['ID']?>"  class="tarif_row__list_title channel_button <?=$class_color?>"><?=$packageItem['NAME']?></h5>
								<div class="tarif_row__list_ch_box">
									<span class="tarif_row__list_ch"><?=$packageItem['COUNT_CHANNELS']?> <span><?=DeclensionNameChannel($packageItem['COUNT_CHANNELS'])?></span></span>
									<div data-id="<?=$packageItem['ID']?>" class="tarif_row__list_all_channels"></div>
								</div>
							</div>
							<?}?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?
	$j = 0;
	foreach ($arInternetTV as $key => $arrInet) {
		
		$inet = $arResult['INET'][$key];
		$classInet = '';
		switch ($j++ % 4)
		{
			case 1:
				$classInet = 'tarif_row__item--packname_strongblue';
				break;
			case 2:
				$classInet = 'tarif_row__item--packname_blue';
				break;
			case 3:
				$classInet = 'tarif_row__item--packname_lightblue';
				break;
			default:
				$classInet = '';
		}
	?>
	<div class="tarif_row">
		<div class="tarif_row__inner">
			<div class="container">
				<div class="tv_super_holder">
					<div class="tarif_row__item">
						<span class="tarif_row__item--packname <?=$classInet?>"><?=$inet['NAME']?></span>
						<div class="tarif_row__speed">
							<?if (!empty($inet['PREVIEW_PICTURE'])) {?><img class="tarif_row__speed_img" src="<?=$inet['PREVIEW_PICTURE']?>" alt="<?=$inet['NAME']?>"><?}?>
							<span class="tarif_row__speed_text"><?=$inet['PROPERTY_V_VALUE']?> <span>Мбит/с</span></span>
						</div>
					</div>
					<div class="tarif_row__table_holder">
						<div data-id-inet = "<?=$arResult['INET'][$key]['ID']?>" class="tarif_row__table" >
							<?foreach ($arrInet as $item) {?>
								<?if ($item['EXIST']) {?>
									<div class="tarif_row__cell tarif_row__cell--isset j-tarif_row__cell_tarif">
										<div class="tarif_row__cell_cont">
											<span class="tarif_row__cell_cost"><?=$item['TOTAL_PRICE']?> <span>тг./мес.</span></span>
											<span class="tarif_row__cell_economy">Экономия <?=$item['DISCOUNT']?> %</span>
											<span class="tarif_row__cell_economy_count">(<?=$item['DISCOUNT_PRICE']?> тг./мес)</span>
										</div>
									</div>
								<? } else {?>
									<div class="tarif_row__cell">
										<div class="tarif_row__cell_cont"></div>
									</div>
								<?}?>
							<?}?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tarif_popup">
			<div class="container">
				<div class="tarif_popup__loader">
					<img src="<?=SITE_TEMPLATE_PATH?>/images/loader.gif">
				</div>
				<div class="tarif_popup__close"></div>
				<div class="ajax_tarif_scroll">
					<div class="tarif_popup_scroll">
						<!-- Сюда приезжает слайдер аяксом -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<? }?>
</div>
<?} else {?>
	<div class="container">
		<div class="tv_super_holder">
			<h1>Интернет + Телевидение</h1>
			<span class="inettv_main_no">На данный момент в вашем филиале отсутствует возможность предоставления услуги передачи данных. Следите за новостями!</span>
		</div>
	</div>
<?}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>