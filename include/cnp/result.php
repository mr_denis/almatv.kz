﻿<?php
session_start ();

require_once ("CNPMerchantWebServiceClient.php");

if (is_null ( $_REQUEST ["customerReference"] ) === false) {
	/*
	 * если мы в конце returnURL передали '&customerReference=' (смотрите checkout.php c 37-40 строки, подробнее в тех спец.), то этот параметр customerReference будет дополняться в конце URL адреса в вашем returnURL, по этому берем данные customerReference с url
	 */
	$rrn = $_REQUEST ["customerReference"];
} else {
	/* если все таки, customerReference не передался в конце url, берем данные по нему с сессии */
	$rrn = $_SESSION ["customerReference"];
}

$errors = "";

$client = new CNPMerchantWebServiceClient ();

$param = new completeTransaction ();
$param->merchantId = "555000000000023";
$param->referenceNr = $rrn;
$param->overrideAmount = NULL;

if (isset ( $_REQUEST ["btn_RequestPayment"] ) && is_null ( $_REQUEST ["btn_RequestPayment"] ) === false) {
	$param->transactionSuccess = true;
	if ($client->completeTransaction ( $param ) == false) {
		$errors = "Settlement request failed.";
	}
}
if (isset($_REQUEST["btn_RequestReversal"]) && is_null($_REQUEST["btn_RequestReversal"]) === false) {
	$param->transactionSuccess = false;
	if ($client->completeTransaction ( $param ) == false) {
		$errors = "Reversal request failed.";
	}
}
$params = new getTransactionStatus ();
$params->merchantId = "555000000000023";
$params->referenceNr = $rrn;
$tranResult = $client->getTransactionStatus ( $params );

$params = new getExtendedTransactionStatus ();
$params->merchantId = "555000000000023";
$params->referenceNr = $rrn;
$extendedTranResult = $client->getExtendedTransactionStatus ( $params );
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Transaction Result</title>
</head>
<body>
	<h1>The status of the transaction was ...</h1>
        <?php if ($tranResult == null) { ?>
            <p>Transaction could not be located.</p>
        <?php } else { ?>
            <p>
            <?php echo $tranResult->return->transactionStatus."<br />";
			
			// getExtendedTransactionStatus result!!!
			echo "<h2>ExtendedTransactionStatus:</h2>".
			"<br /> maskedCardNumber   = ".$extendedTranResult->return->maskedCardNumber.
			"<br /> cardIssuerCountry  = ".$extendedTranResult->return->cardIssuerCountry.
			"<br /> purchaserIpAddress = ".$extendedTranResult->return->purchaserIpAddress.
			"<br /> verified3D         = ".$extendedTranResult->return->verified3D; 
            ?>
            </p>
        <?php } ?>

        <?php if ($errors != "") { ?>
            <p>
		<font color='red'><b><?php echo $errors; ?></b></font>
	</p>
        <?php } ?>

        &nbsp;<br />
	<a href="result.php?btn_RequestPayment=now">Request Payment</a>
	<br />
	<a href="result.php?btn_RequestReversal=now">Request Cancellation</a>
	<br />
</body>
</html>