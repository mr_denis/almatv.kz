﻿<?php
session_start ();

require_once ("CNPMerchantWebServiceClient.php");
date_default_timezone_set('Asia/Almaty');
$basket = unserialize ( $_SESSION ["session_basket"] );

if (!isset($_REQUEST['number']))
{
	die('Не передан номер');
}

$errors = "";
$total = 0;

if (is_array ( $basket ) && count ( $basket ) > 0) {
	foreach ( $basket as $i => $item ) {
		$total += $item->amount;
	}
}

if (isset ( $_REQUEST ["btn_StartCardPayment"] ) && is_null ( $_REQUEST ["btn_StartCardPayment"] ) === false) {
	$pageURL = 'http';
	if (isset ( $_SERVER ["HTTPS"] ) && $_SERVER ["HTTPS"] == "on") {
		$pageURL .= "s";
	}
	$pageURL .= "://";
	if ($_SERVER ["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER ["SERVER_NAME"] . ":" . $_SERVER ["SERVER_PORT"] . $_SERVER ["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER ["SERVER_NAME"] . $_SERVER ["REQUEST_URI"];
	}
	
	$client = new CNPMerchantWebServiceClient ();
	$transactionDetails = new TransactionDetails ();
	$transactionDetails->merchantId = "555000000000023";
	$transactionDetails->totalAmount = $total;
	$transactionDetails->currencyCode = $basket[0]->currencyCode;
	$transactionDetails->description = "Transaction";
	$transactionDetails->merchantAdditionalInformationList = array("number" => $_REQUEST['number']);
	/*
	 * в конце url-а передаем '&customerReference=', для того чтоб использовать автоматическую генерацию RRN, автоматически сгенерированный customerReference будет дополняться в конце URL адреса в вашем returnURL.
	 */
	$transactionDetails->returnURL = str_replace ( "checkout.php", "result.php", $pageURL ) . "&customerReference=";
	$transactionDetails->goodsList = $basket;
	$transactionDetails->languageCode = "ru";
	$transactionDetails->merchantLocalDateTime = date ( "d.m.Y H:i:s" );
	$transactionDetails->orderId = rand ( 1, 10000 );
	$transactionDetails->purchaserName = $_REQUEST['name'];
	$transactionDetails->purchaserEmail = "purchaser@processing.kz";

	$st = new startTransaction ();
	$st->transaction = $transactionDetails;
	$startTransactionResult = $client->startTransaction ( $st );
	
		
		
	/*echo '<pre>';
	print_r($transactionDetails);
	echo '</pre>';
	
	echo '<pre>';
	print_r($startTransactionResult);
	echo '</pre>';
	
	echo '<pre>';
	print_r($client);
	echo '</pre>';
	
	
	die();*/
	
	
	
	
	if ($startTransactionResult->return->success == true) {
		// на всякой случай сохраняем данные customerReference в сессию
		$_SESSION ["customerReference"] = $startTransactionResult->return->customerReference;
		header ( "Location: " . $startTransactionResult->return->redirectURL );
	} else {
		$errors = 'Error: ' . $startTransactionResult->return->errorDescription;
	}
}