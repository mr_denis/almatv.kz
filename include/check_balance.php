<?include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
//ini_set("display_errors",1);
//error_reporting(E_ALL);
?>
<div class="check_balance__cont">
	<h4 class="check_balance__title">Проверка баланса</h4>

<?
function isEmpty($str)
{
	return (($str == '') && ($str == NULL));
}

if ( isset($_REQUEST['surname'])) {
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/xml.php");

	$query = "date_req=".$DB->FormatDate($RATE_DAY_SHOW, CLang::GetDateFormat("SHORT", SITE_ID), "D.M.Y");

	$userName = strtolower($_REQUEST['surname']); 
	$userNumber = (int)$_REQUEST['number'];
	$flAll = false;
	
	$cityId = intval($_COOKIE['City']);
	if ($cityId) {
		CModule::IncludeModule('iblock');
		$res = CIBlockElement::GetList(array(),array('IBLOCK_ID' => 8, 'ID' => (int)$_COOKIE['City']), false, false, array('ID', 'IBLOCK_ID', 'PROPERTY_CHECKBALANCE_ID', 'PROPERTY_IS_CHECKBALANCE'));
		if($ar_res = $res->GetNext())
		{
			if ($ar_res['PROPERTY_IS_CHECKBALANCE_VALUE'] == 'Y')
			{?>
				В данном городе сервис не доступен.
			<?
			die();
			}
			else
			{
				$query = QueryGetData("81.88.148.168:6064", 80, "/balance/app?command=check&surname={$userName}&number={$userNumber}&region={$ar_res['PROPERTY_CHECKBALANCE_ID_VALUE']}", $query, $errno, $errstr);

				if (empty($query))
				{
					echo '<div class = "check_balance_eror"><span>Абонент не найден</span></div>';
					die();
				}
			}
			
		}
		$flAll = true;
	}
	else
	{?>
		<h5 class="check_balance__user_name">Не выбран город</h5>	
	<?}

	if($errstr)
	{?>
		<h5 class="check_balance__user_name"><?=$errstr?></h5>	
	<?
		die();
	} 
	$query = eregi_replace("<!DOCTYPE[^>]{1,}>", "", $query);
	$query = eregi_replace("<"."\?XML[^>]{1,}\?".">", "", $query);

	$objXML = new CDataXML();
	$objXML->LoadString($query);
	$arData   = $objXML->GetArray();
	//PR($arData); 
	$arFields = $arData['response']['#'];
	$code = $arFields['code'][0]['#'];
//PR($code);
	switch ($code) {
		case 0:  //абонент не найден
			$error = 'Договор с абонентом расторжен';
			break;
		case 1:  //информация по абоненту
			break;
		case 2:
		case "-1":  //абонент не найден
			$error='Абонент не найден';
			break;
		case "300":  //внутренняя ошибка поставщика
			$error='Внутренняя ошибка поставщика';
			break;
		case "301":  //неизвестный тип запроса
			$error='Неизвестный тип запроса';
			break;
	}

	if ( $code==1 ) {
		
		if ( !$flAll ) {

			$number  = $arFields['number'][0]['#'];
			$abonent = $arFields['name'][0]['#'];
			$balance = $arFields['balance'][0]['#'];
			$status  = $arFields['status'][0]['#'];
			$usluga  = $arFields['service'][0]['#'];
			$tarif   = implode('<br>',explode(';',$arFields['tariff'][0]['#']));

			?>
			<h5 class="check_balance__user_name"><?=$abonent?></h5>
			<span class="check_balance__user_id">Номер договора: <span><?=$number?></span></span>
			<div class="check_balance__row check_balance__row--dotted">
				<table class="table_balance">
					<thead>
						<tr>
							<td>Услуга</td>
							<td>Статус</td>
							<td>Тариф</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><?=$usluga?></td>
							<td>
								<span class="table_balance__status"><?=$status?></span>
							</td>
							<td>
								<span class="table_balance__tarif"><?=$tarif?></span>
								<?/*<span class="table_balance__cost table_balance__cost--ib">2 600 тг./мес</span>
								<span class="table_balance__spot">(1 точка)</span>*/?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		<?
		} else {
			//PR($arFields);
			$number  = $arFields['ndoc'][0]['#'];
			$abonent = $arFields['abonent'][0]['#'];
			$balance = $arFields['tcosns'][0]['#'];
			$status  = $arFields['stat'][0]['#'];
			$usluga  = $arFields['usluga'][0]['#'];
			$tarif = $arFields['tcosns'][0]['#'].' в количестве 1 шт.'; //implode('<br>',explode(';',$arFields['tariff'][0]['#']));
			$tposns = $arFields['tposns'][0]['#'];
			$osnk = $arFields['osnk'][0]['#'];
			$dtpay = $arFields['dtpay'][0]['#'];
			$dolg = $arFields['dolg'][0]['#'];
			$dtfk = $arFields['dtfk'][0]['#'];
			$summfk = $arFields['summfk'][0]['#'];
			$doc = $arFields['doc'][0]['#'];
			$tpdops = $arFields['tpdops'][0]['#'];
			$date = $arFields['date'][0]['#'];
			$tp = $arFields['tp'][0]['#'];
			$balans = $arFields['balans'][0]['#'];
			$tr = $arFields['tarif'][0]['#'];
			?>
			
				<h5 class="check_balance__user_name"><?=$abonent?></h5>
				<span class="check_balance__user_id">Номер договора: <span><?=$number?></span></span>
				<div class="check_balance__row check_balance__row--dotted">
					<table class="table_balance">
						<thead>
							<tr>
								<?if (!isEmpty($usluga)) {?><td>Услуга</td><?}?>
								<?if (!isEmpty($status)) {?><td>Статус</td><?}?>
								<?//if (!isEmpty($tarif)) {?><td>Тариф</td><?//}?>
								<td>Баланс</td> 
						</thead>
						<tbody>
							<tr>
								<?if (!isEmpty($usluga)) {?><td><?=$usluga?></td><?}?>
								<?if (!isEmpty($status)) {?>
								<td>
									<span class="table_balance__status"><?=$status?></span>
								</td>
								<?}?>
								<?//if (!isEmpty($tarif)) {?>
								<td>
								 <?if ($cityId == 42) {?><a href = "/tv/"> <span class="table_balance__tarif"><?=$tp?> </span></a><?}
								 else {?>
									<span class="table_balance__tarif"><?=$tr?></span>
								 <?}?>
									<!--<span class="table_balance__cost table_balance__cost--ib"><?//=$tposns?> тг./мес</span>-->
									<span class="table_balance__spot">(<?=$osnk?> точка)</span>
								</td>
								<?//}?>
								<td>
									<?=$balans?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="check_balance__row">
					<table class="table_balance table_balance--justify">
						<tbody>
							<?if (!isEmpty($tposns)) {?>
							<tr>
								<td>Тарифный план основной точки</td>
								<td>
									<span class="table_balance__cost"><?=$tposns?> <span>тг./мес</span></span>
								</td>
							</tr>
							<?}?>
							<?if (!isEmpty($balance)) {?>
							<tr>
								<td>Общая сумма тарифного плана</td>
								<td>
									<span class="table_balance__cost"><?=$balance?> <span>тг./мес</span></span>
								</td>
							</tr>
							<?}?>
							<?if (!isEmpty($dtpay)) {?>
							<tr>
								<td>Оплачено по дату</td>
								<td><?=$dtpay?></td>
							</tr>
							<?}?>
							<?if (!isEmpty($dolg)) {?>
							<tr>
								<td>Долг</td>
								<td><?=$dolg?></td>
							</tr>
							<?}?>
							<?if (!isEmpty($dtfk)) {?>
							<tr>
								<td>Дата последней оплаты</td>
								<td><?=$dtfk?></td>
							</tr>
							<?}?>
							<?if (!isEmpty($summfk)) {?>
							<tr>
								<td>Сумма последней оплаты</td>
								<td>
									<span class="table_balance__cost"><?=$summfk?> <span>тг</span></span>
								</td>
							</tr>
							<?}?>
							<?if (!isEmpty($doc)) {?>
							<tr>
								<td>Место совершения оплаты</td>
								<td><?=$doc?></td>
							</tr>
							<?}?>
							<?if (!isEmpty($tpdops) && !isEmpty($dopk)) {?>
							<tr>
								<td>Тарифный план дополнительных точек</td>
								<td>
									<span class="table_balance__cost"><?=$tpdops?> <span>тг./мес</span> (<?=$dopk?> точки)</span>
								</td>
							</tr>
							<?}?>
						</tbody>
					</table>
				</div>
			<?/*
			<p style = "color: #62a3ed;"><?=$arFields['info1'][0]['#']?></p>
			<p style = "color: #62a3ed;"><?=$arFields['info2'][0]['#']?></p>
			<p style = "color: #62a3ed;"><?=$arFields['footer'][0]['#']?></p> */?>
			<?if (!empty($date)) {?>
			<span class="check_balance__info_status">Информация действительна на <?=$date?></span>
			<?}?>
			
			<?}?>
	<? } else { ?>
		<div class = "check_balance_eror"><span><?=$error?></span></div>
	<? } ?>
<? }?>
</div>