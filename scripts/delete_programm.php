<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>

<?
//Запустить на cron чистка старых телепередач
//Подчищаем телепередачу и автоматически файлы телепередач
//удалять телепрограммы кроме этой недели по 20 штук.
if(!CModule::IncludeModule("iblock"))
	return;

global $DB;

$currentWeek = getDateWeek();
/*echo '<pre>';
print_r($currentWeek);
echo '</pre>';*/

$ID_TV = 17;
$arSelect = Array("ID"); 
//удаляем текущую неделю
$arFilter = Array(
	"IBLOCK_ID" => $ID_TV, 
	"<PROPERTY_DATE_FROM" => date("Y-m-d H:i:s", mktime(0,0,0,(int)$currentWeek[0]['M'],(int)$currentWeek[0]['D'], $currentWeek[0]['Y'])),
);

//var_dump($arFilter);
$res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, array('nTopCount' => 20), $arSelect);

while($ob = $res->GetNext())
{
	if (!CIBlockElement::Delete($ob['ID']))
	{
		PR('Ошибка удаление элемента: '.$ob['ID']);
	}
}

?>