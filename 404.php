<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 такой страницы не существует!");?>

<?//PR($_SERVER);?>

<?
$url = '/';
if (!empty($_SERVER['HTTP_REFERER']))
{
	$url = $_SERVER['HTTP_REFERER'];
}
?>

<div class="page-404">
	<img class="page-404-img" src="/bitrix/templates/almatv/images/alma-404.png">
	<div class="page-404-title">Упс, такой страницы не существует!</div>
	<a href="javascript:history.back()" class="page-404-info">Вернуться назад</a>
</div>
<?/*php $APPLICATION->IncludeComponent("bitrix:main.map", ".default", Array(
	"LEVEL"	=>	"3",
	"COL_NUM"	=>	"2",
	"SHOW_DESCRIPTION"	=>	"Y",
	"SET_TITLE"	=>	"Y",
	"CACHE_TIME"	=>	"36000000"
	)
);*/

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>