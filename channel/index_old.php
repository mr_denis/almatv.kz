<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Канал");
?>

<script src="<?=SITE_TEMPLATE_PATH?>/js/tv.js"></script>

<?$idChannel = $APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"channel", 
	array(
		"IBLOCK_TYPE" => "video",
		"IBLOCK_ID" => "9",
		"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
		"ELEMENT_CODE" => "",
		"CHECK_DATES" => "Y",
		"FIELD_CODE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "DETAIL_PICTURE",
			2 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "REGION",
			2 => "SOUND",
			3 => "PACKAGES",
			4 => "",
		),
		"IBLOCK_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "N",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"USE_PERMISSIONS" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>

<?
//global $USER;
//if ($USER->IsAdmin()) {?>

<div class="single_programm_head clear">
	<div class="single_programm_head__left">
		<h3 class="single_programm_head__title"><a href="/programm/">Телепрограмма</a></h3>
		<?$ARR_MONTH_RU = array(1 => 'января', 2 => 'февраля', 3 => 'марта', 4 => 'апреля', 5 => 'мая', 6 => 'июня', 7 => 'июля', 8 => 'августа', 9 => 'сентября', 10 => 'октября', 11 => 'ноября', 12 => 'декабря',);?>
		<span class="single_programm_head__date"><?=date("d").' '.$ARR_MONTH_RU[date("n")].', '.date("Y")?></span>
	</div>
	<?$ARR_WEEK_RU = array(1 => 'Пн', 2 => 'Вт', 3 => 'Ср', 4 => 'Чт', 5 => 'Пт', 6 => 'Сб', 7 => 'Вс');?>
	<?$currentWeek = getDateWeek();?>
	
	<div class="single_programm_head__right">
		<ul class="tarif_cat__trigger tarif_cat__trigger--single_programm" id="j-single_programm_tabs_head" data-id="<?=$idChannel?>">
			<?
			$d = (int)date('d', time());
			foreach ($currentWeek as $key => $item) {
				$class = '';
				$toDay = '';
				if ((int)$item['D'] == $d)
				{
					$class = 'active full';
					$toDay = 'сегодня, ';
				}
				?>
				<li data-num = "<?=$key?>" data-date = "<?=$item['UNIX']?>" data-datef = "<?=date("d", $item['UNIX']).' '.$ARR_MONTH_RU[date("n", $item['UNIX'])].', '.date("Y",$item['UNIX'])?>" class="tarif_cat__trigger_item <?=$class?>"><?=$toDay?> <?=$ARR_WEEK_RU[$item['N']]?></li>
			<?}?>
		</ul>
	</div>
</div><!-- Выведем программу на текущую даты, остальное будем подгружать аяксом, если блок <ul class="today_programm"></ul> не пуст-->
<ul class="single_programm_tabs_body" id="j-single_programm_tabs_body">
	<?foreach ($currentWeek as $key => $item) { ?>
		<li class="single_programm_tabs_body__item">
			<?if ((int)$item['D'] == $d) {
				$m = (int)date('m', time());
				$y = date('Y', time());
				global $Filter;
				$Filter = Array(
					"PROPERTY_CHANNEL" => $idChannel,
					">=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", mktime(0,0,0,$m,$d,$y)),
					"<=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", mktime(23,59,59,$m,$d,$y)),
				);

				$APPLICATION->IncludeComponent(
					"bitrix:news.list", 
					"channelProgram", 
					array(
						"IBLOCK_ID" => "17",
						"NEWS_COUNT" => "2000",
						"SORT_BY1" => "PROPERTY_DATE_FROM",
						"SORT_ORDER1" => "ASC",
						"SORT_BY2" => "PROPERTY_DATE_FROM",
						"SORT_ORDER2" => "ASC",
						"FILTER_NAME" => "Filter",
						"CHECK_DATES" => "Y",
						"DETAIL_URL" => "",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"CACHE_TYPE" => "N",
						"CACHE_TIME" => "36000000",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "N",
						"PREVIEW_TRUNCATE_LEN" => "100",
						"ACTIVE_DATE_FORMAT" => "j F Y",
						"SET_TITLE" => "N",
						"SET_BROWSER_TITLE" => "Y",
						"SET_META_KEYWORDS" => "Y",
						"SET_META_DESCRIPTION" => "Y",
						"SET_STATUS_404" => "Y",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
						"ADD_SECTIONS_CHAIN" => "Y",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"PARENT_SECTION_CODE" => "",
						"INCLUDE_SUBSECTIONS" => "Y",
						"DISPLAY_DATE" => "Y",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"PAGER_TEMPLATE" => ".default",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"PAGER_TITLE" => "",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"PARENT_SECTION" => "",
						"IBLOCK_TYPE" => "news",
						"FIELD_CODE" => array(
							0 => "",
							1 => "",
						),
						"PROPERTY_CODE" => array(
							0 => "DATE_FROM",
							1 => "MAIN",
							2 => "",
						),
						"COMPONENT_TEMPLATE" => "channelProgram",
						"SET_LAST_MODIFIED" => "N",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"SHOW_404" => "N",
						"MESSAGE_404" => ""
					),
					false
				);
			}?>
		</li>
	<?}?>
</ul>

<?
//}
?>
<div class="popup__loader" style="text-align:center; display:none;opacity: 0.6;background-color: rgb(0, 0, 0);position: fixed;left: 0;top: 0;right: 0;bottom: 0;z-index: 1000;">
	<img style="margin-top:25%" src="<?=SITE_TEMPLATE_PATH?>/images/loader.gif">
</div>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
