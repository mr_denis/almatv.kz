<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Канал");
?>

<?/*<script src="<?=SITE_TEMPLATE_PATH?>/js/tv_hl.js"></script>*/?>

<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/tv_hl.js');?>

<?$idChannel = $APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"channel", 
	array(
		"IBLOCK_TYPE" => "video",
		"IBLOCK_ID" => "9",
		"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
		"ELEMENT_CODE" => "",
		"CHECK_DATES" => "Y",
		"FIELD_CODE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "DETAIL_PICTURE",
			2 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "REGION",
			2 => "SOUND",
			3 => "PACKAGES",
			4 => "",
		),
		"IBLOCK_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "N",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"USE_PERMISSIONS" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "N",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>

<?
//global $USER;
//if ($USER->IsAdmin()) {?>

<div class="single_programm_head clear">
	<div class="single_programm_head__left">
		<h3 class="single_programm_head__title"><a href="/programm/">Телепрограмма</a></h3>
		<?$ARR_MONTH_RU = array(1 => 'января', 2 => 'февраля', 3 => 'марта', 4 => 'апреля', 5 => 'мая', 6 => 'июня', 7 => 'июля', 8 => 'августа', 9 => 'сентября', 10 => 'октября', 11 => 'ноября', 12 => 'декабря',);?>
		<span class="single_programm_head__date"><?=date("d").' '.$ARR_MONTH_RU[date("n")].', '.date("Y")?></span>
	</div>
	<?$ARR_WEEK_RU = array(1 => 'Пн', 2 => 'Вт', 3 => 'Ср', 4 => 'Чт', 5 => 'Пт', 6 => 'Сб', 7 => 'Вс');?>
	<?$currentWeek = getDateWeek();?>
	
	<div class="single_programm_head__right">
		<ul class="tarif_cat__trigger tarif_cat__trigger--single_programm" id="j-single_programm_tabs_head" data-id="<?=$idChannel?>" data-v ="1">
			<?
			$d = (int)date('d', time());
			foreach ($currentWeek as $key => $item) {
				$class = '';
				$toDay = '';
				if ((int)$item['D'] == $d)
				{
					$class = 'active full';
					$toDay = 'сегодня, ';
				}
				?>
				<li data-num = "<?=$key?>" data-date = "<?=$item['UNIX']?>" data-datef = "<?=date("d", $item['UNIX']).' '.$ARR_MONTH_RU[date("n", $item['UNIX'])].', '.date("Y",$item['UNIX'])?>" class="tarif_cat__trigger_item <?=$class?>"><?=$toDay?> <?=$ARR_WEEK_RU[$item['N']]?></li>
			<?}?>
		</ul>
	</div>
</div><!-- Выведем программу на текущую даты, остальное будем подгружать аяксом, если блок <ul class="today_programm"></ul> не пуст-->

<ul class="single_programm_tabs_body" id="j-single_programm_tabs_body">
	<?foreach ($currentWeek as $key => $item) { ?>
		<li class="single_programm_tabs_body__item">
			<?if ((int)$item['D'] == $d) {
				$m = (int)date('m', time());
				$y = date('Y', time());

				$Filter = Array(
					"UF_CHANNEL" => $idChannel,
					">=UF_DATE_FROM"=> date("d.m.Y H:i:s", mktime(0,0,0,$m,$d,$y)),
					"<=UF_DATE_FROM"=> date("d.m.Y H:i:s", mktime(23,59,59,$m,$d,$y)),
				);

				$APPLICATION->IncludeComponent(
					"alma:hlview", 
					".default", 
					array(
						"COMPONENT_TEMPLATE" => ".default",
						"IBLOCK_ID" => HL_TV_PROGRAMM,
						//"ID_CHANNEL" => "223",
						"FILTER" => $Filter,
					),
					false
				);				
			}?>
		</li>
	<?}?>
</ul>

<?
//}
?>
<div class="popup__loader" style="text-align:center; display:none;opacity: 0.6;background-color: rgb(0, 0, 0);position: fixed;left: 0;top: 0;right: 0;bottom: 0;z-index: 1000;">
	<img style="margin-top:25%" src="<?=SITE_TEMPLATE_PATH?>/images/loader.gif">
</div>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>
