<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("alma tv");
?>

<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/messages_ru.js"></script>

<script>
	$(document).ready(function() {
		$("#connect_order").validate({
			rules: {
				name: "required",
				email : {
					required: true,
					email: true
				},
				phone: {
					required: true
				},
				address: {
					required: true
				}
			}
		});
		$('.j-user_data__field--phone').mask('+7-000-000-000');
	});
</script>
	

<?
/*
name="form_text_4" имя
name="form_text_5" email
name="form_text_6" телефон
name="form_text_7" город
name="form_text_8" у вас частный дом или квартира
name="form_text_9" предпол адрес установки
name="form_text_10" кол-во точек
name="form_text_11" желаемый пакет
name="form_text_12" доп. пакеты
*/?>
	
<?$APPLICATION->IncludeComponent("bitrix:form.result.new", "tv", Array(
	"AJAX_MODE" => "Y",
		"AJAX_OPTION_SHADOW" => "N",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "Y",
		"WEB_FORM_ID" => "2",	// ID веб-формы
		"IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
		"USE_EXTENDED_ERRORS" => "N",	// Использовать расширенный вывод сообщений об ошибках
		"SEF_MODE" => "N",	// Включить поддержку ЧПУ
		"SEF_FOLDER" => "/",	// Каталог ЧПУ (относительно корня сайта)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"LIST_URL" => "",	// Страница со списком результатов
		"EDIT_URL" => "",	// Страница редактирования результата
		"SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
		"CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
		"CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		)
	),
	false
);?>	
	
	
<h1>Заявка на подключение</h1>
				<ul class="tabs_links tabs_links--podkl">
					<li class="tabs_links__item">Телевидение</li>
					<li class="tabs_links__item">Интернет</li>
					<li class="tabs_links__item">Интернет + Телевидение</li>
				</ul>
				<ul class="tabs_content tabs_content--podkl">
					<li class="tabs_content__item">
						<form action="#" id="connect_order">
							<fieldset>
								<span class="req_info">Поля, отмеченные звёздочкой должны быть обязательно заполнены</span>
								<div class="info_row">
									<div class="user_data">
										<div class="user_data__row">
											<label class="user_data__label user_data__label--req">Статус</label>
											<select class="user_data__select width-auto">
												<option value="0">Домашний пользователь</option>
												<option value="1">пользователь</option>
												<option value="2">Домашний пользователь</option>
											</select>
										</div>
										<div class="user_data__row">
											<label class="user_data__label user_data__label--req">Имя</label>
											<input type="text" name="name" class="user_data__field user_data__field--valid">
										</div>
										<div class="user_data__row user_data__row--no_mar">
											<label class="user_data__label user_data__label--req">Эл. почта</label>
											<input type="email" name="email" class="user_data__field" placeholder="example@mail.ru">
										</div>
										<div class="user_data__row">
											<label class="user_data__label user_data__label--req">Контактный номер телефона</label>
											<input type="text" name="phone" class="user_data__field j-user_data__field--phone" placeholder="+7 (     )     –    –">
										</div>
										<div class="user_data__row">
											<label class="user_data__label user_data__label--req">Город</label>
											<select class="user_data__select width-auto">
												<option value="0">Алматы</option>
												<option value="1">Астана</option>
											</select>
										</div>
										<div class="user_data__row user_data__row--no_mar">
											<label class="user_data__label">У вас частный дом или квартира</label>
											<div>
												<label class="user_data__radio">
													<input type="radio" class="j-user_data__radio_input" name="user_home" value="flat" checked>
													Квартира
												</label>
												<label class="user_data__radio">
													<input type="radio" class="j-user_data__radio_input" name="user_home" value="home">
													Частный дом
												</label>
											</div>
										</div>
										<div class="user_data__row">
											<label class="user_data__label user_data__label--req">Предполагаемый адрес установки</label>
											<input type="text" name="address" class="user_data__field" placeholder="ул. Чайковского, д. 37, кв. 82">
										</div>
										<div class="user_data__row">
											<label class="user_data__label user_data__label--req">Кол-во подключаемых точек</label>
											<select class="user_data__select width-auto" name="spots">
												<option value="0">1</option>
												<option value="1">2</option>
												<option value="2">3</option>
												<option value="2">4</option>
												<option value="2">5</option>
											</select>
										</div>
									</div>
								</div>
								<div class="info_row">
									<label class="user_data__label user_data__label--req">Желаемый пакет программ цифрового ТВ</label>
									<ul class="get_pack j-get_pack--flat">
										<li class="get_pack__item active" data-pack="0">
											<div class="get_pack__add">
												<div class="get_pack__add_hidden">
													<div class="add_packs_box__hidden add_packs_box__hidden--getpack">
														<div class="add_packs_box__hidden_scroll">
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Боец</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">KazSport</a>
															</span>
														</div>
													</div>
												</div>
											</div>
											<h4 class="get_pack__title get_pack__title--cool">TV100+</h4>
											<div class="clear">
												<span class="get_pack__info"><span>103</span>  каналов</span>
												<span class="get_pack__info get_pack__info--right"><span>1 250</span>  тг./мес.</span>
											</div>
										</li>
										<li class="get_pack__item" data-pack="1">
											<div class="get_pack__add">
												<div class="get_pack__add_hidden">
													<div class="add_packs_box__hidden add_packs_box__hidden--getpack">
														<div class="add_packs_box__hidden_scroll">
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Боец</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">KazSport</a>
															</span>
														</div>
													</div>
												</div>
											</div>
											<h4 class="get_pack__title get_pack__title--orange">TV70+</h4>
											<div class="clear">
												<span class="get_pack__info"><span>74</span>  каналов</span>
												<span class="get_pack__info get_pack__info--right"><span>1 050</span>  тг./мес.</span>
											</div>
										</li>
										<li class="get_pack__item" data-pack="2">
											<div class="get_pack__add">
												<div class="get_pack__add_hidden">
													<div class="add_packs_box__hidden add_packs_box__hidden--getpack">
														<div class="add_packs_box__hidden_scroll">
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Боец</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">KazSport</a>
															</span>
														</div>
													</div>
												</div>
											</div>
											<h4 class="get_pack__title get_pack__title--red">TV MAX</h4>
											<div class="clear">
												<span class="get_pack__info"><span>125</span>  канала</span>
												<span class="get_pack__info get_pack__info--right"><span>2 100</span>  тг./мес.</span>
											</div>
										</li>
									</ul>
									<ul class="get_pack j-get_pack--home">
										<li class="get_pack__item active" data-pack="0">
											<div class="get_pack__add">
												<div class="get_pack__add_hidden">
													<div class="add_packs_box__hidden add_packs_box__hidden--getpack">
														<div class="add_packs_box__hidden_scroll">
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Боец</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">KazSport</a>
															</span>
														</div>
													</div>
												</div>
											</div>
											<h4 class="get_pack__title get_pack__title--cool">Антенна60+</h4>
											<div class="clear">
												<span class="get_pack__info"><span>62</span>  канала</span>
												<span class="get_pack__info get_pack__info--right"><span>1 250</span>  тг./мес.</span>
											</div>
										</li>
										<li class="get_pack__item" data-pack="1">
											<div class="get_pack__add">
												<div class="get_pack__add_hidden">
													<div class="add_packs_box__hidden add_packs_box__hidden--getpack">
														<div class="add_packs_box__hidden_scroll">
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Боец</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">KazSport</a>
															</span>
														</div>
													</div>
												</div>
											</div>
											<h4 class="get_pack__title get_pack__title--orange">Антенна80+</h4>
											<div class="clear">
												<span class="get_pack__info"><span>82</span>  канала</span>
												<span class="get_pack__info get_pack__info--right"><span>1 050</span>  тг./мес.</span>
											</div>
										</li>
										<li class="get_pack__item" data-pack="2">
											<div class="get_pack__add">
												<div class="get_pack__add_hidden">
													<div class="add_packs_box__hidden add_packs_box__hidden--getpack">
														<div class="add_packs_box__hidden_scroll">
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Боец</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
															</span>
															<span class="add_packs_box__hidden_item">
																<span class="add_packs_box__hidden_pic">
																	<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
																</span>
																<a href="#" class="add_packs_box__hidden_link">KazSport</a>
															</span>
														</div>
													</div>
												</div>
											</div>
											<h4 class="get_pack__title get_pack__title--red">Антенна MAX</h4>
											<div class="clear">
												<span class="get_pack__info"><span>95</span>  каналов</span>
												<span class="get_pack__info get_pack__info--right"><span>2 100</span>  тг./мес.</span>
											</div>
										</li>
									</ul>
									<input type="hidden" value="0" class="get_pack_result">
								</div>
								<div class="info_row">
									<label class="user_data__label">Дополнительные пакеты</label>
									<ul class="add_packs_box">
										<li class="add_packs_box__item">
											<div class="add_packs_box__tv">
												<label class="add_packs_box__label">
													<input type="checkbox">
													<span class="add_packs_box__pic">
														<img src="<?=SITE_TEMPLATE_PATH?>/pics/pack_balapan.png" height="27" width="27" alt="image_description">
													</span>
													<span class="add_packs_box__name">Балапан</span>
												</label>
												<span class="add_packs_box__count">
													(1 канал)
													<span class="add_packs_box__hidden">
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Боец</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">KazSport</a>
														</span>
													</span>
												</span>
											</div>
											<div class="add_packs_box__tv">
												<label class="add_packs_box__label">
													<input type="checkbox">
													<span class="add_packs_box__pic">
														<img src="<?=SITE_TEMPLATE_PATH?>/pics/pack_religious.png" height="23" width="22"  alt="image_description">
													</span>
													<span class="add_packs_box__name">Религиозный</span>
												</label>
												<span class="add_packs_box__count">
													(1 канал)
													<span class="add_packs_box__hidden">
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Боец</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">KazSport</a>
														</span>
													</span>
												</span>
											</div>
											<div class="add_packs_box__tv">
												<label class="add_packs_box__label">
													<input type="checkbox">
													<span class="add_packs_box__pic">
														<img src="<?=SITE_TEMPLATE_PATH?>/pics/pack_hd.png" height="18" width="22" alt="image_description">
													</span>
													<span class="add_packs_box__name">HD TV</span>
												</label>
												<span class="add_packs_box__count">
													(5 каналов)
													<span class="add_packs_box__hidden">
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Боец</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">KazSport</a>
														</span>
													</span>
												</span>
											</div>
											<div class="add_packs_box__tv">
												<label class="add_packs_box__label">
													<input type="checkbox">
													<span class="add_packs_box__pic">
														<img src="<?=SITE_TEMPLATE_PATH?>/pics/pack_film.png" height="22" width="24" alt="image_description">
													</span>
													<span class="add_packs_box__name">Фильмовый</span>
												</label>
												<span class="add_packs_box__count">
													(3 канала)
													<span class="add_packs_box__hidden">
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Боец</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">KazSport</a>
														</span>
													</span>
												</span>
											</div>
											<div class="add_packs_box__tv">
												<label class="add_packs_box__label">
													<input type="checkbox">
													<span class="add_packs_box__pic">
														<img src="<?=SITE_TEMPLATE_PATH?>/pics/pack_diplomat.png" height="19" width="22" alt="image_description">
													</span>
													<span class="add_packs_box__name">Дипломат</span>
												</label>
												<span class="add_packs_box__count">
													(4 канала)
													<span class="add_packs_box__hidden">
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Боец</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">KazSport</a>
														</span>
													</span>
												</span>
											</div>
											<div class="add_packs_box__tv">
												<label class="add_packs_box__label">
													<input type="checkbox">
													<span class="add_packs_box__pic">
														<img src="<?=SITE_TEMPLATE_PATH?>/pics/pack_cognitive.png" height="24" width="19" alt="image_description">
													</span>
													<span class="add_packs_box__name">Познавательный</span>
												</label>
												<span class="add_packs_box__count">
													(3 каналов)
													<span class="add_packs_box__hidden">
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Боец</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">KazSport</a>
														</span>
													</span>
												</span>
											</div>
											<div class="add_packs_box__tv">
												<label class="add_packs_box__label">
													<input type="checkbox">
													<span class="add_packs_box__pic">
														<img src="<?=SITE_TEMPLATE_PATH?>/pics/pack_musical.png" height="20" width="19" alt="image_description">
													</span>
													<span class="add_packs_box__name">Музыкальный</span>
												</label>
												<span class="add_packs_box__count">
													(7 каналов)
													<span class="add_packs_box__hidden">
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Боец</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">KazSport</a>
														</span>
													</span>
												</span>
											</div>
											<div class="add_packs_box__tv">
												<label class="add_packs_box__label">
													<input type="checkbox">
													<span class="add_packs_box__pic">
														<img src="<?=SITE_TEMPLATE_PATH?>/pics/pack_sport.png" height="15" width="19" alt="image_description">
													</span>
													<span class="add_packs_box__name">Спортивная жизнь</span>
												</label>
												<span class="add_packs_box__count">
													(4 канала)
													<span class="add_packs_box__hidden">
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Боец</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">KazSport</a>
														</span>
													</span>
												</span>
											</div>
											<div class="add_packs_box__tv">
												<label class="add_packs_box__label">
													<input type="checkbox">
													<span class="add_packs_box__pic">
														<img src="<?=SITE_TEMPLATE_PATH?>/pics/pack_home.png" height="14" width="22"  alt="image_description">
													</span>
													<span class="add_packs_box__name">Домашний</span>
												</label>
												<span class="add_packs_box__count">
													(3 каналов)
													<span class="add_packs_box__hidden">
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_12.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Боец</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_07.png" height="13" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">EuroSport-2</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_11.png" height="17" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Наш футбол</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_10.png" height="7" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">Сетанта Спорт</a>
														</span>
														<span class="add_packs_box__hidden_item">
															<span class="add_packs_box__hidden_pic">
																<img src="<?=SITE_TEMPLATE_PATH?>/pics/select_pic_09.png" height="20" width="20" alt="image_description">
															</span>
															<a href="#" class="add_packs_box__hidden_link">KazSport</a>
														</span>
													</span>
												</span>
											</div>
										</li>
									</ul>
								</div>
								<div class="submit_box">
									<input type="submit" value="Отправить заявку" class="submit_box__btn button">
								</div>
							</fieldset>
						</form>
					</li>
					<li class="tabs_content__item">Телевидение</li>
					<li class="tabs_content__item">Интернет + Телевидение</li>
				</ul>
			</div>
		</div>
		
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>