<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заявка на подключение");
?><script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.min.js"></script> <script src="<?=SITE_TEMPLATE_PATH?>/js/messages_ru.js"></script> <?$cityId = cityId();?> 

<script src="<?=SITE_TEMPLATE_PATH?>/js/inputmask.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.inputmask.js" type="text/javascript"></script>
<script>
	$(function()
	{
		$(".j-user_data__field--phone").inputmask("+7 9{0,10}");
	});
</script>
<h1>Заявка на подключение</h1>
<p>
	Форма предназначена для новых клиентов,&nbsp;если Вы являетесь нашим абонентом и желаете оставить обращение -&nbsp;просим пройти по данной <a href="/repair/"> ссылке </a>
</p>
<p>
 <br>
</p>
<ul class="tabs_links tabs_links--podkl">
	<li data-type = "tv" class="tabs_links__item">Телевидение</li>
	<?$isInetCity = isInetCity($_COOKIE["City"])?> 
	<?if ($isInetCity) {?>
	<li data-type = "internet" class="tabs_links__item <?if (($_REQUEST['WEB_FORM_ID'] == 3) || ($_REQUEST['type'] == 'inet') && ($_REQUEST['WEB_FORM_ID'] != 2)) {?>active<?}?>">Интернет</li>
	<?}?>
	<?/*<li class="tabs_links__item dev">Интернет + Телевидение</li>*/?>
</ul>
<ul class="tabs_content tabs_content--podkl">
	<li class="tabs_content__item">
	<?$APPLICATION->IncludeComponent(
	"alma:form.result.new",
	"tv1",
	Array(
		"CITY" => $cityId,
		"WEB_FORM_ID" => "2",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"USE_EXTENDED_ERRORS" => "N",
		"SEF_MODE" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"LIST_URL" => "",
		"EDIT_URL" => "",
		"SUCCESS_URL" => "",
		"CHAIN_ITEM_TEXT" => "",
		"CHAIN_ITEM_LINK" => "",
		"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",)
	)
);?> </li>
	 <?if ($isInetCity) {?>
	<li class="tabs_content__item">
	<?$APPLICATION->IncludeComponent(
	"alma:form.result.new",
	"inet",
	Array(
		"CITY" => $cityId,
		"WEB_FORM_ID" => "3",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"USE_EXTENDED_ERRORS" => "N",
		"SEF_MODE" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"LIST_URL" => "",
		"EDIT_URL" => "",
		"SUCCESS_URL" => "",
		"CHAIN_ITEM_TEXT" => "",
		"CHAIN_ITEM_LINK" => "",
		"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",)
	)
);?> </li>
	 <?}?> <!--<li class="tabs_content__item">Находится в разработке</li>-->
</ul><?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>