<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Alma TV");
?>

<?
/*VIEW HL_BLOCK*/
if (!CModule::IncludeModule('highloadblock'))
	return;
	
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$arParams['ID_CHANNEL'] = 223;

/*выводим все жанры и айдишники к ним*/
/*недельный фильтр*/
$currentWeek = getDateWeek();
$arrFilter = array('UF_CHANNEL'=> $arParams['ID_CHANNEL'], /*"UF_GANRE" => 5, по жанру*/
	">=UF_DATE_FROM"=> date("d.m.Y H:i:s", mktime(0,0,0,(int)$currentWeek[0]['M'],(int)$currentWeek[0]['D'], $currentWeek[0]['Y'])),
	"<=UF_DATE_TO"=> date("d.m.Y H:i:s", mktime(23,59,59,(int)$currentWeek[6]['M'],(int)$currentWeek[6]['D'], $currentWeek[6]['Y'])),
);

function addProgramm($HL_Infoblock_ID)
{
	$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
	
	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}
	
	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$result = $entity_data_class::add(array(
		"UF_CHANNEL" => 223,
		"UF_DATE_FROM" => '26.10.2015 00:00:00',
		"UF_DATE_TO" => '26.10.2015 00:00:00',
		"UF_RATING" => 4,
		"UF_ICONS" => array(4, 6),
		"UF_YEAR" => 2015,
		"UF_SECTION" => 114,
		"UF_GANRE" => array(1 , 2),
		"UF_DIRECTOR" => array('ru' , 'ru'),
		"UF_DIRECTOR_KZ" => array('kz' , 'kz'),
		"UF_DIRECTOR_EU" => array('eu' , 'eu'),
		"UF_ACTOR" => array('ru' , 'ru'),
		"UF_ACTOR_EU" => array('eu' , 'eu'),
		"UF_ACTOR_KZ" => array('kz' , 'kz'),
		//"UF_TIME" => ConvertTimeStamp($time, "FULL", "ru"),
	));

	if ($result->isSuccess()) 
	{
		return $result->getId();
	}
	else
	{
		//$message = $result->getErrors();
		return 0;
	}

}

function getFields($HL_Infoblock_ID, $arrFilter = array(), $arrSelect = array('*'))
{
	//$HL_Infoblock_ID = 3;
	$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 
	$Query->setSelect($arrSelect);

	$Query->setFilter($arrFilter);

	//$Query->setOrder(array('UF_SORT' => 'ASC'));

	//Выполним запрос
	$result = $Query->exec();

	$result = new CDBResult($result);

	$arResult = array();
	while ($row = $result->Fetch())
	{
		foreach ($row as &$itemFields)
		{
			/*datetime*/
			if ($itemFields instanceof \Bitrix\Main\Type\DateTime)
			{
				$itemFields = $itemFields->toString();
			}
		}
		
		$arResult[$row['ID']] = $row;
	}
	
	return $arResult;
}

function bind($ob, $obGanre, $fields)
{
	if (!$fields)
		return false;
	
	foreach ($ob as &$item)
	{
		$arrGanre = &$item[$fields];
	
		foreach ($arrGanre as &$itemGanre)
		{
			foreach ($obGanre as &$item) 
			{
				if ($itemGanre == $item['ID'])
				{
					$itemGanre = $item;
				}
			}	
		}
			
	}
	
	return $ob;
}


$ob = getFields(HL_TV_PROGRAMM, $arrFilter);
$obGanre = getFields(HL_TV_GANRE);

$ob = bind($ob, $obGanre, "UF_GANRE");

//addProgramm(HL_TV_PROGRAMM);

?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>