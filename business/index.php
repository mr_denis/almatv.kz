<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Бизнес");
?>
<h1 class="bussines_title">Для бизнеса</h1>
		<ul class="tabs_links tabs_links--business">
			<li class="tabs_links__item">Интернет</li>
			<li class="tabs_links__item active">Телевидение</li>
		</ul>
		<ul class="tabs_content tabs_content--business">
			<li class="tabs_content__item">
				
			</li>
			<li class="tabs_content__item">
				<div class="business_text_box">
					<p>Цифровое телевидение (Digital TV) – технология передачи по цифровым каналам связи телевизионного изображения и звука, кодированного в соответствии со стандартом сжатия данных MPEG.</p>
				</div>
				<table class="ip_table ip_table--business">
					<thead>
						<tr>
							<td>Название тарифа</td>
							<td>Дополнительных точек</td>
							<td>Абонентская плата</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<h4 class="business_table_title">BusinessTV-2</h4>
							</td>
							<td>
								<span class="ip_table__cost"><span>2 </span>точки</span>
							</td>
							<td>
								<span class="ip_table__cost"><span>2 600 </span>тг./мес.</span>
							</td>
							<td>
								<a href="#" class="button dev">Оформить заявку</a>
							</td>
						</tr>
						<tr>
							<td>
								<h4 class="business_table_title business_table_title--darker">BusinessTV-3</h4>
							</td>
							<td>
								<span class="ip_table__cost"><span>3 </span>точки</span>
							</td>
							<td>
								<span class="ip_table__cost"><span>3 900 </span>тг./мес.</span>
							</td>
							<td>
								<a href="#" class="button dev">Оформить заявку</a>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="business_two_cols">
					<div class="business_left_col">
						<div class="business_left_col__img">
							<img src="<?=SITE_TEMPLATE_PATH?>/images/pult.jpg" alt="image_description">
						</div>
						<div class="business_left_col__cont">
							<div class="business_left_col__cont_row">
								<h3 class="business_left_col__title">Количество каналов</h3>
								<span class="business_left_col__desc">Количество каналов в каждом из пакетов зависит от технической возможности региона трансляции и варьируется от 42-х до 57-и.</span>
							</div>
							<div class="business_left_col__cont_row">
								<div class="business_left_col__cont_row_cell">
									<h5 class="business_left_col__cont_row_title">Выберите город</h5>
									<form action="#">
										<fieldset>
											<select class="business_left_col__select">
												<option value="1">Алматы</option>
												<option value="2">Аксу</option>
												<option value="3">Костанай</option>
												<option value="4">Астана</option>
											</select>
										</fieldset>
									</form>
								</div>
								<div class="business_left_col__cont_row_cell business_left_col__cont_row_cell--dot">
									<h5 class="business_left_col__cont_row_title">Каналов</h5>
									<div class="tarif_row__list_ch_box tarif_row__list_ch_box--business">
										<span class="tarif_row__list_ch">57 <span>каналов</span></span>
										<div class="tarif_row__list_all_channels"></div>
									</div> 
								</div>
							</div>
							<div class="business_left_col__cont_row">
								<span class="business_left_col__desc">Пакет «BusinessTV» включает <a href="#" class="business_left_col__link">13 обязательных</a> к распространению на территории Казахстана телеканалов. </span>
							</div>
						</div>
					</div>
					<div class="business_right_col">
						<div class="business_right_col__img">
							<img src="<?=SITE_TEMPLATE_PATH?>/images/inet_machine_business.jpg" alt="image_description">
						</div>
						<div class="business_right_col__cont">
							<h3 class="business_right_col__title">Оборудование</h3>
							<ul class="tarif_cat__trigger tarif_cat__trigger--business" id="j-business_tabs_head">
								<li class="tarif_cat__trigger_item">приставка</li>
								<li class="tarif_cat__trigger_item">сам-модуль</li>
							</ul>
							<ul class="business_tabs_cont" id="j-business_tabs_cont">
								<li class="business_tabs_cont__item">
									<span>Приставка Set-top Box (STB) предназначена для приёма и декодирования сигналов цифрового кабельного телевидения в стандарте DVB-C и последующего вывода видео- и аудиосигнала на бытовой телевизор. </span>
								</li>
								<li class="business_tabs_cont__item">
									<span>Приставка Set-top Box (STB) предназначена для приёма и декодирования сигналов цифрового кабельного телевидения в стандарте DVB-C и последующего вывода видео- и аудиосигнала на бытовой телевизор. Приставка Set-top Box (STB) предназначена для приёма и декодирования сигналов цифрового кабельного телевидения в стандарте DVB-C и последующего вывода видео- и аудиосигнала на бытовой телевизор. </span>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>