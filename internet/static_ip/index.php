<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Статический IP");
?>
<h1>
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
		"COMPONENT_TEMPLATE" => ".default",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/internet/static_ip/include/inc_title.php"
		)
	);?>
</h1>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
	"COMPONENT_TEMPLATE" => ".default",
	"AREA_FILE_SHOW" => "file",
	"AREA_FILE_SUFFIX" => "inc",
	"EDIT_TEMPLATE" => "",
	"PATH" => "/internet/static_ip/include/inc_description.php"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
	"COMPONENT_TEMPLATE" => ".default",
	"AREA_FILE_SHOW" => "file",
	"AREA_FILE_SUFFIX" => "inc",
	"EDIT_TEMPLATE" => "",
	"PATH" => "/internet/static_ip/include/inc_table.php"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
	"COMPONENT_TEMPLATE" => ".default",
	"AREA_FILE_SHOW" => "file",
	"AREA_FILE_SUFFIX" => "inc",
	"EDIT_TEMPLATE" => "",
	"PATH" => "/internet/static_ip/include/inc_bottom_text.php"
	)
);?>
<br>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>