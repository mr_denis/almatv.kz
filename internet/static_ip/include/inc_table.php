<table class="ip_table">
	<thead>
		<tr>
			<td>
				 Количество IP-адресов
			</td>
			<td>
				 Единовременная плата
			</td>
			<td>
				 Абонентская плата
			</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<span class="ip_table__count">4 IP Адреса</span>
			</td>
			<td>
				<span class="ip_table__cost">4 500 тг./мес.</span>
			</td>
			<td>
				<span class="ip_table__cost">2 000 тг./мес.</span>
			</td>
		</tr>
		<tr>
			<td>
				<span class="ip_table__count">8 IP Адресов</span>
			</td>
			<td>
				<span class="ip_table__cost">6 500 тг./мес.</span>
			</td>
			<td>
				<span class="ip_table__cost">3 200 тг./мес.</span>
			</td>
		</tr>
		<tr>
			<td>
				<span class="ip_table__count">16 IP Адресов</span>
			</td>
			<td>
				<span class="ip_table__cost">8 000 тг./мес.</span>
			</td>
			<td>
				<span class="ip_table__cost">4 000 тг./мес.</span>
			</td>
		</tr>
		<tr>
			<td>
				<span class="ip_table__count">32 IP Адреса</span>
			</td>
			<td>
				<span class="ip_table__cost">9 000 тг./мес.</span>
			</td>
			<td>
				<span class="ip_table__cost">4 500 тг./мес.</span>
			</td>
		</tr>
		<tr>
			<td>
				<span class="ip_table__count">64 IP Адреса</span>
			</td>
			<td>
				<span class="ip_table__cost">15 000 тг./мес.</span>
			</td>
			<td>
				<span class="ip_table__cost">7 000 тг./мес.</span>
			</td>
		</tr>
	</tbody>
</table>