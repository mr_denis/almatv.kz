<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Пресс-центр");
?>

<div class="main_title_holder">
	<h1 class="main_title_left">Пресс-центр</h1>
	<!--<a href="newsarhive/" class="news_archive_link">Архив новостей</a>-->
</div>

<?
$cityId = CITY_ID_ALMATA;
if (!empty($_COOKIE['City']))
{
	$cityId = $_COOKIE['City'];
}
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"sectionList", 
	array(
		"CITY_ID" => $cityId, //для правильной работы кеша по городам
		"VIEW_MODE" => "TEXT",
		"SHOW_PARENT_NAME" => "Y",
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => "1",
		"SECTION_CODE" => "",
		"SECTION_URL" => "",
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => "1",
		"SECTION_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "Y",
		"COMPONENT_TEMPLATE" => "sectionList",
		"SECTION_ID" => $_REQUEST["SECTION_ID"]
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>