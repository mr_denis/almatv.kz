<?php 
$_SERVER["DOCUMENT_ROOT"] = "/var/www/v-20173/data/www/almatv.kz";
//$_SERVER["DOCUMENT_ROOT"] = '/home/n/ninelines/alma-tv.9-lines.com/public_html';
define("LANG", "ru"); 

define("NO_KEEP_STATISTIC", true); 
define("NOT_CHECK_PERMISSIONS", true); 

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
set_time_limit(0); 

//Запустить на cron чистка старых телепередач
//Подчищаем телепередачу и автоматически файлы телепередач
//удалять телепрограммы кроме этой недели по 20 штук.
if(!CModule::IncludeModule("iblock"))
	return;

global $DB;

$start = microtime(true); 
$currentWeek = getDateWeek();

$interval = 10000000;//за 1 раз удалим все

$ID_TV = 17;
$arSelect = Array("ID"); 
//удаляем текущую неделю
$arFilter = Array(
	"IBLOCK_ID" => $ID_TV, 
	//"<PROPERTY_DATE_FROM" => date("Y-m-d H:i:s", mktime(0,0,0,(int)$currentWeek[0]['M'],(int)$currentWeek[0]['D'], $currentWeek[0]['Y'])),
);

$DB->StartTransaction();
try
{
	//var_dump($arFilter);
	$res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, array('nTopCount' => $interval), $arSelect);
	$i = 0;
	while($ob = $res->GetNext())
	{
		if (!CIBlockElement::Delete($ob['ID']))
		{
			echo 'Ошибка удаление элемента: '.$ob['ID'];
		}
		$i++;
	}
	
	$DB->Commit();
	
	echo "Удалено $i элементов";

}
catch( Exception $ex )
{
   // Откат изменений
   $DB->Rollback();
   echo 'An error occurred: ' . $ex->getMessage();
}
//24.9925630093 сек.
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.';

//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules­/main/include/epilog_after.php"); ?>