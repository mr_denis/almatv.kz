<?$_SERVER["DOCUMENT_ROOT"] = "/var/www/v-20173/data/www/almatv.kz";
//$_SERVER["DOCUMENT_ROOT"] = '/home/n/ninelines/alma-tv.9-lines.com/public_html';
define("LANG", "ru");

define("NO_KEEP_STATISTIC", true); 
define("NOT_CHECK_PERMISSIONS", true); 

require($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_admin_before.php');
set_time_limit(0); 

if(!CModule::IncludeModule("iblock"))
    return;

$start = microtime(true); 

$IBLOCK_ID = 9;
$arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "!PROPERTY_XML" => false);
$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false, array("ID", "NAME", "PROPERTY_XML"));
$i = 0;
while($ob = $res->GetNext())
{
	echo "$i ";
	parse($ob['PROPERTY_XML_VALUE'], $ob['ID'], $ob['NAME']);
	$i++;
	//die();
}

echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.';

function parse($filename, $idCh, $nameChannel)
{
	CModule::IncludeModule("iblock");
	$CIBE = new CIBlockElement;
	$CIBS = new CIBlockSection;

	if (($idCh <= 0) && isset($idCh)) 
	{
		echo $nameChannel.': Отсутствует id канала<br />';
		return;
	}

	$DATA_FILE_NAME = $_SERVER["DOCUMENT_ROOT"].'/import/xml/'.$filename;

	if (strval($DATA_FILE_NAME) == "" || !file_exists($DATA_FILE_NAME)) {
		echo $nameChannel.': Отсутствует id канала<br />';
		return;
	}

	$arErrors = array();

	$XML = simplexml_load_file($DATA_FILE_NAME);

	$currentWeek = getDateWeek();
	$arSelect = Array("ID"); 

	$arFilter = Array(
		"IBLOCK_ID"=> IB_PROGRAMS, 
		"PROPERTY_CHANNEL" => $idCh,
		">=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", mktime(0,0,0,(int)$currentWeek[0]['M'],(int)$currentWeek[0]['D'], $currentWeek[0]['Y'])),
		"<=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", mktime(23,59,59,(int)$currentWeek[6]['M'],(int)$currentWeek[6]['D'], $currentWeek[6]['Y'])),
	);

	$res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelect);
	$count = $res->SelectedRowsCount();

	if ($count)  
	{
		echo $nameChannel.': Элементы уже добавлены<br />';
		return;
	}

	$arPrograms = array();
	$i = 1;
	$arProgramsSections = array();

	foreach($XML->programme as $key => $obProgram) {
		$arLocalErrors = array();
		$obProgramAttributes = $obProgram->attributes();
		$channelID = strval($obProgramAttributes->channel);

		if (!isset($obProgramAttributes->channel) || $obProgramAttributes->channel == "") {
			$arLocalErrors[] = "Ошибка в передаче #$i: отсутствует id канала.";
		}
		if (!isset($obProgramAttributes->start) || $obProgramAttributes->stop == "") {
			if (strtotime($obProgramAttributes->start) <= mktime(21,59,59,(int)$currentWeek[6]['M'],(int)$currentWeek[6]['D'], $currentWeek[6]['Y']))
			{
				echo $nameChannel.': Выгрузка старого расписания<br />';
				return;
			}
			$arLocalErrors[] = "Ошибка в передаче #$i: отсутствует время начала трансляции.";
		}
		elseif (strtotime($obProgramAttributes->start) <= 0) {
			$arLocalErrors[] = "Ошибка в передаче #$i: время начала трансляции имеет неверный формат (" . $obProgramAttributes["start"] . ").";
		}
		if (!isset($obProgram->title) || $obProgram->title == "") {
			$arLocalErrors[] = "Ошибка в передаче #$i: отсутствует название.";
		}

		if (empty($arLocalErrors)) {

			$arCategories = array();
			foreach ($obProgram->category as $category) {
				$arCategories[] = mb_strtolower(strval($category));
			}
			$arProgramsSections = array_merge($arProgramsSections, $arCategories);

			$arFiles = array();
			foreach($obProgram->icon as $icon) {
			    $arIconAttributes = $icon->attributes();
			    if (isset($arIconAttributes->src) && $arIconAttributes->src != "" && $arIconAttributes->src != "empty") {
					$arFiles[] = $arIconAttributes->src;
			    }
			}
			
			$arGanre = array();
			foreach($obProgram->ganre as $ganre) {
			    $arGanre[] = $ganre;
			}

			if (!isset($arProgramsByChannels[$channelID]))
				$arProgramsByChannels[$channelID] = array();
			$arPrograms[] = array(
				"IBLOCK_ID" => IB_PROGRAMS,
				"NAME" => strval($obProgram->title),
				"PREVIEW_TEXT" => strval($obProgram->desc),
				"PROPERTY_VALUES" => array(
					"CHANNEL" => $idCh,
					"DATE_FROM" => date("d.m.Y H:i:s", strtotime($obProgramAttributes->start)),//strval($obProgram->date),
					"DATE_TO" => date("d.m.Y H:i:s", strtotime($obProgramAttributes->stop)), 
					"RATING" => strval($obProgram->rating->value),
					"ICONS" => $arFiles,
					"YEAR" => $obProgram->year,
					"GANRE" => $arGanre,
				),
				"CATEGORIES" => $arCategories,
			);
		}
		else {
			$arErrors = array_merge($arErrors, $arLocalErrors);
		}
	}

	$arProgramsSections = array_unique($arProgramsSections); //убираем повторы

	$arExistingProgramsSections = array();
	$rsProgramsSections = CIBlockSection::GetList(
		array(),
		array(
			"IBLOCK_ID" => IB_PROGRAMS,
			"NAME" => $arProgramsSections
		),
		false,
		array(
			"ID",
			"NAME"
		)
	);
	while ($arProgramsSection = $rsProgramsSections->GetNext()) {
		$arExistingProgramsSections[$arProgramsSection["NAME"]] = $arProgramsSection["ID"];
	}

	$arUnexistingProgramsSections = array_diff($arProgramsSections, array_keys($arExistingProgramsSections));

	foreach($arUnexistingProgramsSections as $section) {
		$id = $CIBS->add(array(
			"IBLOCK_ID" => IB_PROGRAMS,
			"NAME" => $section
		));
		if ($id) {
			$arExistingProgramsSections[$section] = $id;
		}
	}
	
	global $DB;
	$DB->StartTransaction();
	try
	{
		$successCnt = 0;
		foreach ($arPrograms as &$arProgram) {
			$arCategories = $arProgram["CATEGORIES"];
			$arNewCategories = array();
			foreach($arProgram["CATEGORIES"] as $category) {
				$arNewCategories[] = $arExistingProgramsSections[$category];
			}
			$arProgram["IBLOCK_SECTION"] = $arNewCategories;
			unset($arNewCategories);
			
			unset($arProgram['CATEGORIES']);

			$id = $CIBE->Add($arProgram);

			if (!$id) {
				echo "Error: ".$CIBE->LAST_ERROR; 
				$arErrors["Ошибка: не получилось сохранить передачу \"" . $arProgram["NAME"] . "\" на " . $arProgram["DATE_ACTIVE_FROM"]];
			}
			else {
				
				$successCnt++;
			}
		}
		unset($arProgram);
		
		$DB->Commit();
	}
	catch( Exception $ex )
	{
	   $DB->Rollback();
	   echo 'An error occurred: ' . $ex->getMessage();
	}

	$strImportOKMessage = $nameChannel.": Импорт закончен. добавлено " . $successCnt . " позиций.<br />";
	echo $strImportOKMessage;
}
