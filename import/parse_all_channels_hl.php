<?$_SERVER["DOCUMENT_ROOT"] = "/var/www/v-20173/data/www/almatv.kz";
//$_SERVER["DOCUMENT_ROOT"] = '/home/n/ninelines/alma-tv.9-lines.com/public_html';
define("LANG", "ru");

$fileNameUploadLog = $_SERVER["DOCUMENT_ROOT"].'/import/edit.txt';

/*для ежедневной проверки всех выгружаемых программ*/

/*добавить персон, типы персон, для тв множественно поле, страны пересоздать таблицу, множ. свойство., добавить спорт передачу*/

define("NO_KEEP_STATISTIC", true); 
define("NOT_CHECK_PERMISSIONS", true); 

require($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_admin_before.php');
set_time_limit(0); 

if(!CModule::IncludeModule("iblock"))
    return;

if (!CModule::IncludeModule('highloadblock'))
	return;

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$arrGanre = getGanre(HL_TV_GANRE);
$arrCountry = getCountry(HL_TV_COUNTRY);
$arrTypePerson = getTypePerson(HL_TV_TYPE_PERSON);
PR($arrTypePerson);
die();

/*Тип Персон*/

function getTypePerson($id)
{
	$hlblock = HL\HighloadBlockTable::getById($id)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 

	//$Query->setFilter(array(/*"UF_NAME_RU" => $name_ru*/));
	$Query->setSelect(array('*'));

	$result = $Query->exec();

	$result = new CDBResult($result);

	$arResult = array();
	
	while ($row = $result->Fetch())
	{
		$arResult[$row['UF_TYPE']] = $row['ID'];
	}
	
	return $arResult;
}

function addCountry($HL_Infoblock_ID, $arData)
{
	$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
	
	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}
	
	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();

	$result = $entity_data_class::add(array(
		"UF_NAME_RU" => $arData['UF_NAME_RU'],
		"UF_NAME_EU" => $arData['UF_NAME_EU'],
		"UF_NAME_KZ" => $arData['UF_NAME_KZ'],
	));

	if ($result->isSuccess()) 
	{
		$arData['ID'] = $result->getId();
		return $arData;
	}
	else
	{
		//$message = $result->getErrors();
		return 0;
	}
}

function getCountry($id/*, $name_ru*/)
{
	$hlblock = HL\HighloadBlockTable::getById($id)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 

	//$Query->setFilter(array(/*"UF_NAME_RU" => $name_ru*/));
	$Query->setSelect(array('*'));

	$result = $Query->exec();

	$result = new CDBResult($result);

	$arResult = array();
	
	while ($row = $result->Fetch())
	{
		$arResult[] = $row;
	}
	
	return $arResult;
}

function addGanre($HL_Infoblock_ID, $arData)
{
	$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
	
	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}
	
	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();

	$result = $entity_data_class::add(array(
		"UF_NAME_RU" => $arData['UF_NAME_RU'],
		"UF_NAME_EU" => $arData['UF_NAME_EU'],
		"UF_NAME_KZ" => $arData['UF_NAME_KZ'],
	));

	if ($result->isSuccess()) 
	{
		$arData['ID'] = $result->getId();
		return $arData;
	}
	else
	{
		//$message = $result->getErrors();
		return 0;
	}
}

function getGanre($id/*, $name_ru*/)
{
	$hlblock = HL\HighloadBlockTable::getById($id)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 

	//$Query->setFilter(array(/*"UF_NAME_RU" => $name_ru*/));
	$Query->setSelect(array('*'));

	$result = $Query->exec();

	$result = new CDBResult($result);

	$arResult = array();
	
	while ($row = $result->Fetch())
	{
		$arResult[] = $row;
	}
	
	return $arResult;
}


function deleteTV($HL_Infoblock_ID, $arrFilter = array(), $arrSelect = array('*'))
{
	$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 
	$Query->setSelect($arrSelect);
	$Query->setFilter($arrFilter);
	
	//Выполним запрос
	$result = $Query->exec();
	$result = new CDBResult($result);

	$arResult = array();
	$i = 0;
	while ($row = $result->Fetch())
	{
		$res = $entity_data_class::delete($row['ID']);
		if(!$res->isSuccess()){ //произошла ошибка
			echo $res->getErrorMessages(); //выведем ошибку
		} else {
			$i++;
		}
	}
	
	return $i;
}

function updateHash($HL_Infoblock_ID, $arData)
{
	$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();

	$id = $arData['ID'];
	
	$arBxData = array(
		'UF_ID_CHANNEL' => $arData['UF_ID_CHANNEL'],
		'UF_HASH' =>  $arData['UF_HASH'],
	);

	$result = $entity_data_class::update($id, $arBxData);
    if(!$result->isSuccess()) { //произошла ошибка
         echo $result->getErrorMessages(); //выведем ошибку
	}
}

function getHash($id, $idCh)
{
	$hlblock = HL\HighloadBlockTable::getById($id)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	if (!$idCh)
		return;
	
	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 

	$Query->setFilter(array("UF_ID_CHANNEL" => $idCh));
	$Query->setSelect(array('*'));

	$result = $Query->exec();

	$result = new CDBResult($result);

	$arResult = array();
	
	if ($row = $result->Fetch())
	{
		return $row;
	}
	
	return 0;
}

function addHash($HL_Infoblock_ID, $idCh, $hash)
{
	$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
	
	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}
	
	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();

	$result = $entity_data_class::add(array(
		"UF_ID_CHANNEL" => $idCh,
		"UF_HASH" => $hash,
	));

	if ($result->isSuccess()) 
	{
		return $result->getId();
	}
	else
	{
		//$message = $result->getErrors();
		return 0;
	}
}

function addProgramm($HL_Infoblock_ID, $arrFields)
{
	$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();
	
	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}
	
	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$result = $entity_data_class::add($arrFields);

	if ($result->isSuccess()) 
	{
		return $result->getId();
	}
	else
	{
		$message = $result->getErrors();
	}
}

$start = microtime(true); 

$IBLOCK_ID = IBLOCK_CHANNELS;
$arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y", "!PROPERTY_XML" => false);
$res = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false, array("ID", "NAME", "PROPERTY_XML"));

$pathEpgService = 'http://xmldata.epgservice.ru:8181/EPGService/hs/xmldata/almatv_extended/file/';
//$ii = 0;
while($ob = $res->GetNext())
{
	if (empty($ob['PROPERTY_XML_VALUE']))
		continue;
	
	$idXml = preg_replace("/(\D)/", "", $ob['PROPERTY_XML_VALUE']);
	$fileName = $pathEpgService.$idXml.'/';

	//    http://xmldata.epgservice.ru:8181/EPGService/hs/xmldata/almatv_extended/index   Изменение берем
	//echo $idXml.' ';
	
	if($idXml == '000000778')
	{
		parse($fileName, $ob['ID'], $ob['NAME']);
	}
	
	//break;
	
	//parse($fileName, $ob['ID'], $ob['NAME']);
	
	
	//if ($idXml == '000001380')
	//{
	//	echo $fileName;
	//	echo $ob['NAME'];
		
		/*$XML = simplexml_load_file($fileName);
		$hash = md5($XML);
		print_r($hash);
		$ob = getHash(HL_TV_HASH, $ob['ID'], $hash);
		print_r($ob);*/
		
	//	parse($fileName, $ob['ID'], $ob['NAME']);
		
	//die();
		//die();
	//}
	
	//echo $fileName;
	//echo $ob['NAME'];
	
	//$ii++;
	//parse($fileName, $ob['ID'], $ob['NAME']);
}

echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.';

function parse($fileName, $idCh, $nameChannel)
{
	$currentWeek = getDateWeek();
	$logText = array();
	
	CModule::IncludeModule("iblock");
	//$CIBE = new CIBlockElement;
	$CIBS = new CIBlockSection;

	if (($idCh <= 0) && isset($idCh)) 
	{
		echo $nameChannel.': Отсутствует id канала<br />';
		return;
	}

	if ($fileName == "") { // $idCh
		echo $nameChannel.': Отсутствует id канала<br />';
		return;
	}

	$arErrors = array();

	$XML = simplexml_load_file($fileName);
	$hash = md5($XML);

	/*
	375	24 KZ	10d5b8c1dbd41273c0a0c4d6ffbe1026   Проверить на обновление
	376	24-DOC	b59109cb602c6ddc0dd20802e8c129fb
	377	31 канал	62c9cc89c8bb57f5bd7591959fc0e718
	
	echo $idCh;
	echo '   ';
	print_r($hash);
	die();*/
	
	if ($ob = getHash(HL_TV_HASH, $idCh, $hash))
	{	
		if ($ob['UF_HASH'] == $hash)
		{
			$logText['ACTUAL'] .= $idCh.' ';
			echo '[actual]';
			return;
		}
		else
		{ //проверить!!!
			$arrFilterTV = array(
				"UF_CHANNEL" => $idCh,
				">=UF_DATE_FROM"=> date("d.m.Y H:i:s", mktime(0,0,0,(int)$currentWeek[0]['M'],(int)$currentWeek[0]['D'], $currentWeek[0]['Y'])),
				"<=UF_DATE_FROM"=> date("d.m.Y H:i:s", mktime(23,59,59,(int)$currentWeek[6]['M'],(int)$currentWeek[6]['D'], $currentWeek[6]['Y'])),
			);
			
			$logText['UPDATE'] .= $idCh.' ';
			
			echo '[update]';
			/* удаляем старое расписание по каналу недельное*/
			deleteTV(HL_TV_PROGRAMM, $arrFilterTV);
			//удаляем для данного канала TV программы
		}
	}
	else
	{
		addHash(HL_TV_HASH, $idCh, $hash);
	}

	$arPrograms = array();
	$i = 1;
	$arProgramsSections = array();

	foreach($XML->programme as $key => $obProgram) {

		$arLocalErrors = array();
		$obProgramAttributes = $obProgram->attributes();
		$channelID = strval($obProgramAttributes->channel);

		if (!isset($obProgramAttributes->channel) || $obProgramAttributes->channel == "") {
			$arLocalErrors[] = "Ошибка в передаче #$i: отсутствует id канала.";
		}
		if (!isset($obProgramAttributes->start) || $obProgramAttributes->stop == "") {
			if (strtotime($obProgramAttributes->start) <= mktime(21,59,59,(int)$currentWeek[6]['M'],(int)$currentWeek[6]['D'], $currentWeek[6]['Y']))
			{
				echo $nameChannel.': Выгрузка старого расписания<br />';
				return;
			}
			$arLocalErrors[] = "Ошибка в передаче #$i: отсутствует время начала трансляции.";
		}
		elseif (strtotime($obProgramAttributes->start) <= 0) {
			$arLocalErrors[] = "Ошибка в передаче #$i: время начала трансляции имеет неверный формат (" . $obProgramAttributes["start"] . ").";
		}
		if (!isset($obProgram->title) || $obProgram->title == "") {
			$arLocalErrors[] = "Ошибка в передаче #$i: отсутствует название.";
		}

		
		if (empty($arLocalErrors)) {

			$arCategories = array();
			foreach ($obProgram->category as $category) {
				$arCategories[] = mb_strtolower(strval($category));
			}
			$arProgramsSections = array_merge($arProgramsSections, $arCategories);
			/*Иконки*/
			$arFiles = array();
			foreach($obProgram->icon as $icon) {
			    $arIconAttributes = $icon->attributes();
			    if (isset($arIconAttributes->src) && $arIconAttributes->src != "" && $arIconAttributes->src != "empty") {
					//echo ($arIconAttributes->src);
					$arFiles[] = (string)$arIconAttributes->src;
			    }
			}
			/*Жанры*/
			$arGanre = array();
			foreach($obProgram->ganre as $ganre) {
			    $arGanre[] = (string)$ganre;  //Жанры русские нужно дописать
			}

			global $arrGanre;
			//die();
			$arGanreID = array();
			foreach ($arGanre as &$itemGanreXML) //новости, ж/р
			{
				$f = false;
				foreach ($arrGanre as $itemGanre_) //БД тест1, тест2
				{
					if ($itemGanre_['UF_NAME_RU'] == $itemGanreXML) // русские названия обязательные!
					{
						/*if ($itemGanre_['UF_NAME_KZ'])
						{
							//обновим
						}*/
						$arGanreID[] = $itemGanre_['ID'];
						
						$f = true;
						break;
					}
					
					
				}
				/*если не нашли добавляем*/
				if (!$f)
				{
					$dataGanre = addGanre(HL_TV_GANRE, array("UF_NAME_RU" => $itemGanreXML, "UF_NAME_EU" => $itemGanreXML, "UF_NAME_KZ" => $itemGanreXML));
					$arGanreID[] = $dataGanre['ID'];
					$arrGanre[] = $dataGanre; //добавляем в массив
				}
			}
			
			/*Страны*/
			global $arrCountry;
			$arCountryID = array();
			$arCountry = array();

			$arCountry[] = (string)$obProgram->country;
			foreach ($arCountry as &$country) //новости, ж/р
			{
				$f = false;
				foreach ($arrCountry as $arrCountry_) //БД тест1, тест2
				{
					if ($arrCountry_['UF_NAME_RU'] == $country) // русские названия обязательные!
					{
						/*if ($itemGanre_['UF_NAME_KZ'])
						{
							//обновим
						}*/
						$arCountryID[] = $arrCountry_['ID'];
						
						$f = true;
						break;
					}	
				}
				/*если не нашли добавляем*/
				if (!$f)
				{
					$dataCountry = addCountry(HL_TV_COUNTRY, array("UF_NAME_RU" => $country, "UF_NAME_EU" => $country, "UF_NAME_KZ" => $country));
					$arCountryID[] = $dataCountry['ID'];
					$arrCountry[] = $dataCountry; //добавляем в массив
				}
			}

			/*
			<person type="actor">
				<name lang="ru">Кейт Хадсон</name>
				<icon src="http://xmldata.epgservice.ru:8181/EPGService/hs/xmldata/almatv_extended/perspic/691"/>
			</person>
			*/

			//PR($obProgram->credits);
			//die();
			
			foreach ($obProgram->credits->person as $person)
			{
				//PR($person);
				//die();
				//die();
				$personAttributes = $person->attributes();
				echo(strval($personAttributes->type));
				echo '    ';
				echo(strval($person->name));
				echo '    ';
				echo(strval($person->icon));
				echo '    ';
				echo(strval($person->biography));
				
				//$type = strval($type->type);
				//PR($type);
				//PR($type);
				//die();
			}
			/*проверяем всех актеров, если нет добавляем иначе возвращаем ID*/
			die();
			
			//$obProgramAttributes = $obProgram->attributes();
			//$channelID = strval($obProgramAttributes->channel);
			
			if (!isset($arProgramsByChannels[$channelID]))
				$arProgramsByChannels[$channelID] = array();

				$arPrograms[] = array(
					"UF_NAME" => strval($obProgram->title),
					"UF_CHANNEL" => $idCh,
					"UF_DATE_FROM" => date("d.m.Y H:i:s", strtotime($obProgramAttributes->start)),//strval($obProgram->date),
					"UF_DATE_TO" => date("d.m.Y H:i:s", strtotime($obProgramAttributes->stop)), 
					"UF_RATING" => strval($obProgram->rating->value),
					"UF_ICONS" => $arFiles,
					"UF_YEAR" => (isset($obProgram->year)) ? $obProgram->year : 0,
					"UF_SECTION" => $arCategories,
					"UF_GANRE" => $arGanreID,
					"UF_COUNTRY" => $arCountryID[0], //пока 1 страна
					"UF_TEXT_RU" => strval($obProgram->desc),
				);
				
				//print_r($arPrograms);
				//die();
		}
		else {
			$arErrors = array_merge($arErrors, $arLocalErrors);
		}
	}


	$arProgramsSections = array_unique($arProgramsSections); //убираем повторы

	$arExistingProgramsSections = array();
	$rsProgramsSections = CIBlockSection::GetList(
		array(),
		array(
			"IBLOCK_ID" => IB_PROGRAMS,
			"NAME" => $arProgramsSections
		),
		false,
		array(
			"ID",
			"NAME"
		)
	);

	while ($arProgramsSection = $rsProgramsSections->GetNext()) {
		//PR($arProgramsSection);
		$arExistingProgramsSections[$arProgramsSection["NAME"]] = $arProgramsSection["ID"];
	}

	$arUnexistingProgramsSections = array_diff($arProgramsSections, array_keys($arExistingProgramsSections));

	foreach($arUnexistingProgramsSections as $section) {
		$id = $CIBS->add(array(
			"IBLOCK_ID" => IB_PROGRAMS,
			"NAME" => $section
		));
		if ($id) {
			$arExistingProgramsSections[$section] = $id;
		}
	}

	global $DB;
	$DB->StartTransaction();
	try
	{
		$successCnt = 0;
		foreach ($arPrograms as &$arProgram) {
			$arCategories = $arProgram["UF_SECTION"];
			$arNewCategories = array();
			foreach($arProgram["UF_SECTION"] as $category) {
				$arNewCategories[] = $arExistingProgramsSections[$category];
			}

			$arProgram["UF_SECTION"] = $arNewCategories;
		
			unset($arNewCategories);

			$id = addProgramm(HL_TV_PROGRAMM, $arProgram);	

			if (!$id) {
				//echo "Error: ".$CIBE->LAST_ERROR; 
				$arErrors["Ошибка: не получилось сохранить передачу \"" . $arProgram["NAME"] . "\" на " . $arProgram["DATE_ACTIVE_FROM"]];
			}
			else {
				$successCnt++;
			}
		}
		unset($arProgram);
		
		$DB->Commit();
	}
	catch( Exception $ex )
	{
	   $DB->Rollback();
	   echo 'An error occurred: ' . $ex->getMessage();
	}

	$strImportOKMessage = $nameChannel.": Импорт закончен. добавлено " . $successCnt . " позиций.<br />";
	echo $strImportOKMessage;
	
	/* обновляем хэш после выгрузки*/
	$ob['UF_HASH'] = $hash;
	updateHash(HL_TV_HASH, $ob);
	
	global $fileNameUploadLog;
	
	$current = file_get_contents($fileNameUploadLog);
	
	$current .= PHP_EOL;
	$current .= date("d.m.Y H:i:s");
	$current .= PHP_EOL;
	foreach ($logText as $type)
	{
		$current .= $type;
	}
	$current .= PHP_EOL;
	
	file_put_contents($fileNameUploadLog, $current);	
		
	unset($XML);
}
