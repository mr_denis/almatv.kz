<?
$_SERVER["DOCUMENT_ROOT"] = "/var/www/v-20173/data/www/almatv.kz";
$pathEpgService = 'http://xmldata.epgservice.ru:8181/EPGService/hs/xmldata/almatv_extended/index';
//require($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_admin_before.php');
$XML = simplexml_load_file($pathEpgService);

$interval = /*intval($_REQUEST['interval'])*/ 5 * 60; /*раз в 30 * 60 минут*/

//define(INTEVAL, 600);

$fileNameUploadLog = $_SERVER["DOCUMENT_ROOT"].'/import/upload.txt';

$arResult = array();
foreach($XML->channel as $channel) {
	
	$obProgramAttributes = $channel->attributes();
	
	$arItem = array(
		"id" => strval($obProgramAttributes->id),
		"name" => strval($channel->{'display-name'}),
		"week" => strval($channel->week),
		"update" => strtotime(strval((int)$channel->update)),
		"href" => strval($channel->href),
	);
	/*текущая дата + 30 минут < даты из xml*/
	//UNIX_TIMESTAMP(UF_DATE_FROM) - $date) <= 15 * 60) and ((UNIX_TIMESTAMP(UF_DATE_FROM) - $date) > 0)
	
	if ((time() + $interval - 2) < $arItem["update"]) /*задержка в 10 сек*/
	{
		$current = file_get_contents($fileNameUploadLog);
		$current .= $arItem['name'].' '.$arItem['href'].' date_xml'.$arItem['update'].' date:'.date("d.m.Y H:i:s").PHP_EOL;

		$arResult[] = $arItem;
	}
	else
	{
		$current = file_get_contents($fileNameUploadLog);
		$current .= $arItem['name'].' null date:'.date("d.m.Y H:i:s").PHP_EOL;
	}
	//PR($channel->update);
	//$channelID = PR($channel->display-name);
	//PR($channelID);
}

$current .= PHP_EOL;
file_put_contents($fileNameUploadLog, $current);	
	
/*echo '<pre>';
print_r($arResult);
echo '</pre>';*/
//PR($arResult);
?>
