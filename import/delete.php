<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>

<?
if(!CModule::IncludeModule("iblock"))
	return;

global $DB;

$idChannel = intval($_REQUEST['id']);

$ID_TV = 17;
$arSelect = Array("ID"); 
$currentWeek = getDateWeek();
/*удаляем текущую неделю*/
$arFilter = Array(
	"IBLOCK_ID"=> $ID_TV, 
	"PROPERTY_CHANNEL" => $idChannel,
	">=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", mktime(0,0,0,(int)$currentWeek[0]['M'],(int)$currentWeek[0]['D'], $currentWeek[0]['Y'])),
	"<=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", mktime(23,59,59,(int)$currentWeek[6]['M'],(int)$currentWeek[6]['D'], $currentWeek[6]['Y'])),
);

/*echo '<pre>';
print_r($arFilter);
echo '</pre>';*/

$res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelect);

flush();

//$count = 0;
while($ob = $res->GetNext())
{
	//print_r($ob);
	CIBlockElement::Delete($ob['ID']);
}

echo "Данные удалены";

?>