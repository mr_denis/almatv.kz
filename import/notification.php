<?$_SERVER["DOCUMENT_ROOT"] = "/var/www/v-20173/data/www/almatv.kz";
$file = $_SERVER["DOCUMENT_ROOT"].'/import/notification.txt';

define("LANG", "ru");
define("NO_KEEP_STATISTIC", true); 
define("NOT_CHECK_PERMISSIONS", true); 

require($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/main/include/prolog_admin_before.php');

//$startTimeScript = microtime(true);

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

if (!CModule::IncludeModule('highloadblock'))
	return;

function deleteReminder($id)
{
	$hlblock = HL\HighloadBlockTable::getById(HLIB_TV_REMINDER)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	$res = $entity_data_class::delete($id);
}

function getReminder($date)
{
	global $DB;
	$strSql ="SELECT r.ID, UF_NAME, UF_EMAIL, (UNIX_TIMESTAMP(UF_DATE_FROM) - $date), UF_DATE_FROM FROM tv_reminder as r, tv_programm WHERE tv_programm.ID = UF_BROADCASTING and ((UNIX_TIMESTAMP(UF_DATE_FROM) - $date) <= 15 * 60) and ((UNIX_TIMESTAMP(UF_DATE_FROM) - $date) > 0)"; //(UNIX_TIMESTAMP(UF_DATE_FROM) - $date)
	$db_res = $DB->Query($strSql);
	while ($res = $db_res->Fetch())
	{
		$arResult[] = $res;
	}
	return $arResult;
}

$arResult = getReminder(time());

foreach ($arResult as $item)
{
	if (mail($item['UF_EMAIL'], 'almatv.kz', "Вы записывались на tv-программу {$item[UF_NAME]}, которая начинается в {$item[UF_DATE_FROM]}"))
	{
		$current = file_get_contents($file);
		$current .= $item['UF_EMAIL'].' '.$item['UF_DATE_FROM'].' '.date('d.m.Y H:i:s').' '.$item['UF_NAME'].PHP_EOL;
		file_put_contents($file, $current);	
		deleteReminder($item['ID']);
	}
}	

	/*$date = time();
	$strSql ="SELECT r.ID, UF_NAME, UF_EMAIL, (UNIX_TIMESTAMP(UF_DATE_FROM) - $date), UF_DATE_FROM FROM tv_reminder as r, tv_programm WHERE tv_programm.ID = UF_BROADCASTING AND ((UNIX_TIMESTAMP(UF_DATE_FROM) - $date) > 0)"; //(UNIX_TIMESTAMP(UF_DATE_FROM) - $date)
	$db_res = $DB->Query($strSql);
	while ($res = $db_res->Fetch())
	{
		PR($res);
	}*/
	
//PR(date("d.m.Y H:i:s"));
//$timeScript = microtime(true) - $startTimeScript;
//PR($timeScript);
//Проходимся по всем данным select UF_NAME, UF_EMAIL, UF_DATE_FROM from tv_reminder, tv_programm WHERE tv_programm.ID = UF_BROADCASTING
?>