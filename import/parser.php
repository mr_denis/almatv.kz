<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
set_time_limit(0);
ignore_user_abort(true);

CModule::IncludeModule("iblock");
$CIBE = new CIBlockElement;
$CIBS = new CIBlockSection;

$filename = $_REQUEST['filename'];
$idCh = intval($_REQUEST['id']);
if (($idCh <= 0) && isset($_REQUEST['id']))
{
	echo 'Отсутствует id канала.';
	die();
}

$DATA_FILE_NAME = $_SERVER["DOCUMENT_ROOT"].'/import/xml/'.$filename;

if (strval($DATA_FILE_NAME) == "" || !file_exists($DATA_FILE_NAME)) {
    die("Файл не найден");
}

$arErrors = array();

$XML = simplexml_load_file($DATA_FILE_NAME);
$ID_TV = 17;

$currentWeek = getDateWeek();
$arSelect = Array("ID"); 

$arFilter = Array(
	"IBLOCK_ID"=> $ID_TV, "PROPERTY_CHANNEL" => $idCh,
	">=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", mktime(0,0,0,(int)$currentWeek[0]['M'],(int)$currentWeek[0]['D'], $currentWeek[0]['Y'])),
	"<=PROPERTY_DATE_FROM"=> date("Y-m-d H:i:s", mktime(23,59,59,(int)$currentWeek[6]['M'],(int)$currentWeek[6]['D'], $currentWeek[6]['Y'])),
);

$res = CIBlockElement::GetList(Array("NAME" => "ASC"), $arFilter, false, false, $arSelect);
$count = $res->SelectedRowsCount();

//session_start();

//define (INTERVAL, 50);

if ($count) 
{
	echo 'Элементы уже добавлены';
	die();
}

$arPrograms = array();
$i = 1;
$arProgramsSections = array();

//пошаговый с окончанием
//PR($_SESSION);

/*if ((!isset($_SESSION['ID_FILENAME'])))
{
	$_SESSION['ID_FILENAME'] = $filename.'['.$idCh.']';
}

if ($_SESSION['ID_FILENAME'] != ($filename.'['.$idCh.']'))
{
	$_SESSION['ID_FILENAME'] = $filename.'['.$idCh.']';
	unset($_SESSION['STEP']);
}*/

//!!! начинаем с начала разбора удалить $_SESSION['STEP']
//unset($_SESSION['STEP']);
/*if (!isset($_SESSION['STEP'])) {
	$_SESSION['STEP'] = 0;
	//
	foreach($XML->programme as $key => $obProgram) {
		$_COUNT_++;
	}
	$_SESSION['COUNT'] = $_COUNT_;
	
}

if (isset($_COUNT_))
{
	$_SESSION['COUNT'] = $_COUNT_;
}

PR($_SESSION['COUNT']);
PR($_SESSION['STEP']);*/


/*if ($_SESSION['STEP'] > $_SESSION['COUNT'])
{
	echo 'Импорт Завершен';
	die();
	unset($_SESSION['STEP']);
}*/

//die();
//PR($XML);
//разбор XML тоже нужно сделать пошаговым.

foreach($XML->programme as $key => $obProgram) {
	//PR($i.' '.$_SESSION['STEP']);
	/*пропустим пред элементы*/
	/*if ($i < $_SESSION['STEP'])
	{
		$i++;
		continue;
	}*/
	//PR($i);

    $arLocalErrors = array();
    $obProgramAttributes = $obProgram->attributes();
    $channelID = strval($obProgramAttributes->channel);

    if (!isset($obProgramAttributes->channel) || $obProgramAttributes->channel == "") {
        $arLocalErrors[] = "Ошибка в передаче #$i: отсутствует id канала.";
    }
    if (!isset($obProgramAttributes->start) || $obProgramAttributes->stop == "") {
        $arLocalErrors[] = "Ошибка в передаче #$i: отсутствует время начала трансляции.";
    }
    elseif (strtotime($obProgramAttributes->start) <= 0) {
        $arLocalErrors[] = "Ошибка в передаче #$i: время начала трансляции имеет неверный формат (" . $obProgramAttributes["start"] . ").";
    }
    if (!isset($obProgram->title) || $obProgram->title == "") {
        $arLocalErrors[] = "Ошибка в передаче #$i: отсутствует название.";
    }

    /*if (!isset($arChannels[$channelID])) {
        $arLocalErrors[] = "Ошибка в передаче #$i: отсутствует канал с названием " . $arChannelsNamesByID[$channelID] . ".";
    }*/

    if (empty($arLocalErrors)) {

        $arCategories = array();
        foreach ($obProgram->category as $category) {
            $arCategories[] = mb_strtolower(strval($category));
        }
        $arProgramsSections = array_merge($arProgramsSections, $arCategories);

        $arFiles = array();
        /*foreach($obProgram->icon as $icon) {
            $arIconAttributes = $icon->attributes();
            if (isset($arIconAttributes->src) && $arIconAttributes->src != "" && $arIconAttributes->src != "empty") {
                if ($arFile = CFile::MakeFileArray(strval($arIconAttributes->src))) {
                    if ($arFile["type"] != "unknown" && isset($arFile["tmp_name"]) && $arFile["tmp_name"] != "") {
                        $arFiles[] = $arFile;
                    }
                }
            }
        }*/

        if (!isset($arProgramsByChannels[$channelID]))
            $arProgramsByChannels[$channelID] = array();
        $arPrograms[] = array(
            "IBLOCK_ID" => IB_PROGRAMS,
            //"DATE_ACTIVE_FROM" => date("d.m.Y H:i:s", strtotime($obProgramAttributes->start)),
            //"DATE_ACTIVE_TO" => date("d.m.Y H:i:s", strtotime($obProgramAttributes->start)),
            "NAME" => strval($obProgram->title),
            "PREVIEW_TEXT" => strval($obProgram->desc),
            "PROPERTY_VALUES" => array(
                "CHANNEL" => $idCh,//$arChannels[$channelID],
                "DATE_FROM" => date("d.m.Y H:i:s", strtotime($obProgramAttributes->start)),//strval($obProgram->date),
				"DATE_TO" => date("d.m.Y H:i:s", strtotime($obProgramAttributes->stop)),
                "RATING" => strval($obProgram->rating->value),
                "ICONS" => $arFiles,
				"YEAR" => $obProgram->year,
            ),
            "CATEGORIES" => $arCategories,
        );
    }
    else {
        $arErrors = array_merge($arErrors, $arLocalErrors);
    }
	
	
		
	/*PR(($i - 1).' > '.$_SESSION['STEP'].'+'.INTERVAL);
	
	if (($_SESSION['STEP'] + INTERVAL) > $_SESSION['COUNT'])
	{
		$_SESSION['STEP'] += INTERVAL;
		break;
	}
	
	if ((($i) > ($_SESSION['STEP'] + INTERVAL)))
	{
		$_SESSION['STEP'] += INTERVAL;
		break;
	}
	
	
	
    $i++;*/
}

$arProgramsSections = array_unique($arProgramsSections); //убираем повторы

$arExistingProgramsSections = array();
$rsProgramsSections = CIBlockSection::GetList(
    array(),
    array(
        "IBLOCK_ID" => IB_PROGRAMS,
        "NAME" => $arProgramsSections
    ),
    false,
    array(
        "ID",
        "NAME"
    )
);
while ($arProgramsSection = $rsProgramsSections->GetNext()) {
    $arExistingProgramsSections[$arProgramsSection["NAME"]] = $arProgramsSection["ID"];
}

$arUnexistingProgramsSections = array_diff($arProgramsSections, array_keys($arExistingProgramsSections));

foreach($arUnexistingProgramsSections as $section) {
    $id = $CIBS->add(array(
        "IBLOCK_ID" => IB_PROGRAMS,
        "NAME" => $section
    ));
    if ($id) {
        $arExistingProgramsSections[$section] = $id;
    }
}

$successCnt = 0;
foreach ($arPrograms as &$arProgram) {
    $arCategories = $arProgram["CATEGORIES"];
    $arNewCategories = array();
    foreach($arProgram["CATEGORIES"] as $category) {
        $arNewCategories[] = $arExistingProgramsSections[$category];
    }
    $arProgram["IBLOCK_SECTION"] = $arNewCategories;
    unset($arNewCategories);
	
	unset($arProgram['CATEGORIES']);

	$id = $CIBE->Add($arProgram);

    if (!$id) {
        $arErrors["Ошибка: не получилось сохранить передачу \"" . $arProgram["NAME"] . "\" на " . $arProgram["DATE_ACTIVE_FROM"]];
    }
    else {
        $successCnt++;
    }
}
unset($arProgram);
 
$strImportErrorMessage = implode("\n", $arErrors);
$strImportOKMessage = "Импорт закончен. добавлено " . $successCnt . " позиций.";
echo $strImportOKMessage;