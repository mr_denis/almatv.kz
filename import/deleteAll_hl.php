<?php 
$_SERVER["DOCUMENT_ROOT"] = "/var/www/v-20173/data/www/almatv.kz";
//$_SERVER["DOCUMENT_ROOT"] = '/home/n/ninelines/alma-tv.9-lines.com/public_html';
define("LANG", "ru"); 
define("NO_KEEP_STATISTIC", true); 
define("NOT_CHECK_PERMISSIONS", true); 

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
set_time_limit(0); 

if (!CModule::IncludeModule('highloadblock'))
	return;
	
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

function deleteTV($HL_Infoblock_ID, $arrFilter = array(), $arrSelect = array('*'))
{
	$hlblock = HL\HighloadBlockTable::getById($HL_Infoblock_ID)->fetch();

	if (empty($hlblock))
	{
	   ShowError('404');
	   return;
	}

	$Entity = HL\HighloadBlockTable::compileEntity($hlblock);
	$entity_data_class = $Entity->getDataClass();
	
	$Query = new \Bitrix\Main\Entity\Query($Entity); 
	$Query->setSelect($arrSelect);
	$Query->setFilter($arrFilter);
	
	//Выполним запрос
	$result = $Query->exec();
	$result = new CDBResult($result);

	$arResult = array();
	$i = 0;
	while ($row = $result->Fetch())
	{
		$res = $entity_data_class::delete($row['ID']);
		if(!$res->isSuccess()){ //произошла ошибка
			echo $res->getErrorMessages(); //выведем ошибку
		} else {
			$i++;
		}
	}
	
	return $i;
}

global $DB;

$start = microtime(true); 
$currentWeek = getDateWeek();

$interval = 10000000;//за 1 раз удалим все

//удаляем текущую неделю
$arrFilter = array(
	"<UF_DATE_FROM"=> date("d.m.Y H:i:s", mktime(0,0,0,(int)$currentWeek[0]['M'],(int)$currentWeek[0]['D'], $currentWeek[0]['Y'])), //поменять знак! на <
);

$DB->StartTransaction();
try
{
	$count = deleteTV(HL_TV_PROGRAMM, $arrFilter, array('ID'));

	$DB->Commit();
	
	echo "Удалено $count элементов";

}
catch( Exception $ex )
{
   // Откат изменений
   $DB->Rollback();
   echo 'An error occurred: ' . $ex->getMessage();
}
//24.9925630093 сек.
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.';

//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules­/main/include/epilog_after.php"); ?>