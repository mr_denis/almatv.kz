<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

<h1>Контактная информация</h1>
		<div class="checked_city">
			<h3 class="checked_city__name"><?=$_COOKIE['CityName']?></h3>
			<div class="checked_city__another">
				<span class="checked_city__another_link">Выбрать другой город</span>
			</div>
		</div>

<?
/*
по городу найти первый город в списке*/
?>
	
<div class="contact_common">
<?
$arr_id = cityOffice();
//PR($arr_id);
if (count($arr_id)) {
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"office", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "Y",
		"SHARE_HIDE" => "N",
		"SHARE_TEMPLATE" => "",
		"SHARE_SHORTEN_URL_LOGIN" => "",
		"SHARE_SHORTEN_URL_KEY" => "",
		"AJAX_MODE" => "Y",
		"IBLOCK_ID" => "14",
		"ELEMENT_ID" => $arr_id[0],
		"CHECK_DATES" => "Y",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "ADDRESS",
			1 => "WEEK_DAYS",
			2 => "SUNDAY",
			3 => "SATURDAY",
			4 => "M_CENTER",
			5 => "M_ZOOM",
			6 => "M_PLACE_MARK",
		),
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "Y",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"USE_PERMISSIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_TEMPLATE" => "",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"IBLOCK_TYPE" => "vacancies",
		"ELEMENT_CODE" => "",
		"IBLOCK_URL" => "",
		"GROUP_PERMISSIONS" => array(
			0 => "2",
		),
		"SHARE_HANDLERS" => array(
			0 => "delicious",
			1 => "facebook",
			2 => "lj",
			3 => "mailru",
			4 => "twitter",
			5 => "vk",
		),
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
<?}?>
		</div>		

		
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>