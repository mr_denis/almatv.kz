<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");

//header('Location: contacts/');
//exit();
?>

<?php
function scan_Dir($dir) {
	$arrfiles = array();
	if (is_dir($dir)) {
		if ($handle = opendir($dir)) {
			chdir($dir);
			while (false !== ($file = readdir($handle))) { 
				if ($file != "." && $file != "..") { 
					if (is_dir($file)) { 
						$arr = scan_Dir($file);
						foreach ($arr as $value) {
							$arrfiles[] = $value;
						}
					} else {
						$arrfiles[] = $file;
					}
				}
			}
			chdir("../");
		}
		closedir($handle);
	}
	return $arrfiles;
}

function Gallery($dir)
{
	$dir = $_SERVER["DOCUMENT_ROOT"].$dir; 
	$arrFiles = scan_Dir($dir);
	$count = count($arrFiles);

	for ($i = 0; $i < $count; $i++)
	{
		if ($i > 2)
			break;
		$item = $arrFiles[$i];
		$item = '/upload/about/images/'.$item;
		echo "<a href='{$item}' rel='gallery' class='fancybox event__gallery_img'><img src='{$item}' width='80'></a>";
	}
	
	if ($count > 2)
	{
		$itemFirst = '/upload/about/images/'.$arrFiles[$i];
		for ($j = $i+1; $j < $count; $j++)
		{
			$item = $arrFiles[$j];
			$item = '/upload/about/images/'.$item;
			echo "<a style = 'display: none' href='{$item}' rel='gallery' class='fancybox event__gallery_img'></a>";
		}
		echo "<a href='{$itemFirst}' class='event__gallery_img_all fancybox' rel='gallery'>Ещё ".($count - 3)." фото </a>";
	}
}
?>
							
<script>
	$(document).ready(function() {
		$('.fancybox').fancybox();
	});
</script>
							
<div class="container history">
	<h1>О компании</h1>
</div>
<div class="events_nav_box">
	<div class="events_nav__plank">
		<div class="events_nav__plank-prev"></div>
		<div class="events_nav__plank-next"></div>
	</div>
	<ul class="events_nav">
		<li class="events_nav__item">
			<a href="#year_1994" class="events_nav__item_text">1994</a>
		</li>
		<li class="events_nav__item">
			<a href="#year_1995" class="events_nav__item_text">1995</a>
		</li>
		<li class="events_nav__item">
			<a href="#year_1998" class="events_nav__item_text">1998</a>
		</li>
		<li class="events_nav__item">
			<a href="#year_2002" class="events_nav__item_text">2002</a>
		</li>
		<li class="events_nav__item">
			<a href="#year_2006" class="events_nav__item_text">2006</a>
		</li>
		<li class="events_nav__item">
			<a href="#year_2007" class="events_nav__item_text">2007</a>
		</li>
		<li class="events_nav__item"> 
			<a href="#year_2008" class="events_nav__item_text">2008</a>
		</li>
		<li class="events_nav__item">
			<a href="#year_2010" class="events_nav__item_text">2010</a>
		</li>
		<li class="events_nav__item">
			<a href="#year_2012" class="events_nav__item_text">2012</a>
		</li>
		<li class="events_nav__item">
			<a href="#year_2015" class="events_nav__item_text">2015</a>
		</li>
		<li class="events_nav__item">
			<a href="#year_infinity" class="events_nav__item_text events_nav__item_text--infinity"></a>
		</li>
	</ul>
</div>
<div class="bg_section">
	<section class="bg_section__item bg_section__item--first" style="background: url(<?=SITE_TEMPLATE_PATH?>/images/about_section_01.jpg) no-repeat left bottom;">
		<div class="container history">
			<div class="event" id="year_1994">
				<div class="event__year_box">
					<h2 class="event__year">1994</h2>
				</div>
				<div class="event__scroll event__scroll--1994 is_scrolled">
					<div class="event__desc_box event__desc_box--first">
						<div class="event__desc">
							<span>
							История «Алма-ТВ» ведёт свой отсчёт с 1994 года, когда было создано ЗАО «Алма-ТВ» как совместное предприятие, в котором по 50% владели казахстанское ОАО «Cominvest» и американская International Telcell, Inc. (ITI), дочерняя компания Metromedia International Telecommunications Inc. (MITI)
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="bg_section__mountian"></div>
			<div class="bg_section__sun"></div>
			<div class="bg_section__flagstick">
				<div class="bg_section__flag"></div>
			</div>
			<div class="bg_section__clouds_first"></div>
			<div class="bg_section__clouds_second"></div>
			<div class="bg_section__clouds_third"></div>
		</div>
	</section>
	<section class="bg_section__item  bg_section__item--second" style="background: url(<?=SITE_TEMPLATE_PATH?>/images/about_section_02.jpg) no-repeat left bottom;">
		<div class="container history">
			<div class="event" id="year_1995">
				<div class="event__year_box">
					<h2 class="event__year">1995</h2>
					<span class="event__date">16 мая</span>
				</div>
				<div class="event__scroll event__scroll--1995 is_scrolled">
					<div class="event__desc_box event__desc_box--second">
						<div class="event__desc">
							<span>Дату 16 мая 1995 года, когда к системе телевизионного вещания «Алма-ТВ» по технологии MMDS был подключён первый клиент, можно считать началом эры коммерческого телевидения в нашей стране.</span>
						</div>
					</div>
				</div>
			</div>
			<div class="bg_section__home">
				<div class="bg_section__wave_left"></div>
				<div class="bg_section__wave_right"></div>
			</div>
			<div class="bg_section__clouds_blue_one"></div>
			<div class="bg_section__clouds_blue_two"></div>
			<div class="bg_section__smoke"></div>
		</div>
	</section>
	<section class="bg_section__item  bg_section__item--third" style="background: url(<?=SITE_TEMPLATE_PATH?>/images/about_section_03_no_map.jpg) repeat -150px 0;">
		<div class="container history">
			<div class="event" id="year_1998">
				<div class="event__year_box">
					<h2 class="event__year">1998</h2>
				</div>
				<div class="event__scroll event__scroll--1998 is_scrolled">
					<div class="event__desc_box event__desc_box--third">
						<div class="event__desc">
							<span>Начав с Алматы, компания «Алма-ТВ» постепенно расширила свою сеть телевещания с едиными принципами тарификации и унифицированным программным наполнением на все крупные города Казахстана. Первые филиалы были открыты в Актау, Караганде и Усть-Каменогорске.</span>
						</div>
					</div>
				</div>
			</div>
			<div class="event" id="year_2002">
				<div class="event__year_box">
					<h2 class="event__year">2002</h2>
				</div>
				<div class="event__scroll event__scroll--2002 is_scrolled">
					<div class="event__desc_box event__desc_box--fourth">
						<div class="event__desc">
							<img src="<?=SITE_TEMPLATE_PATH?>/images/flag.jpg" class="event__desc_flag" height="39" width="77" alt="image_description">
							<span class="event__desc_fl">Открылись филиалы в Атырау и Семее. <br>Летом 2002 года MITI продала свою долю, и с тех пор «Алма-ТВ» представляет собой чисто казахстанскую компанию.</span>
						</div>
						<div class="event__desc">
							<span>Открылись филиалы в Атырау и Семее.</span>
						</div>
					</div>
				</div>
			</div>
			<div class="event" id="year_2006">
				<div class="event__year_box">
					<h2 class="event__year">2006</h2>
				</div>
				<div class="event__scroll event__scroll--2006 is_scrolled">
					<div class="event__desc_box event__desc_box--fifth">
						<div class="event__desc">
							<span>Открылись филиалы в Астане, Павлодаре и Шымкенте.</span>
						</div>
					</div>
				</div>
			</div>
			<div class="event" id="year_2007">
				<div class="event__year_box">
					<h2 class="event__year">2007</h2>
				</div>
				<div class="event__scroll event__scroll--standart is_scrolled">
					<div class="event__desc_box event__desc_box--fifth">
						<div class="event__desc">
							<span>Открылись филиалы в Аксу, Талдыкоргане, Уральске и Экибастузе.</span>
						</div>
					</div>
				</div>
			</div>
			<div class="event" id="year_2008">
				<div class="event__year_box">
					<h2 class="event__year">2008</h2>
				</div>
				<div class="event__scroll event__scroll--standart is_scrolled">
					<div class="event__desc_box event__desc_box--fifth">
						<div class="event__desc">
							<span>Открылись филиалы в Актобе, Зыряновске и Таразе.</span>
						</div>
					</div>
				</div>
			</div>
			<div class="event" id="year_2010">
				<div class="event__year_box">
					<h2 class="event__year">2010</h2>
				</div>
				<div class="event__scroll event__scroll--standart is_scrolled">
					<div class="event__desc_box event__desc_box--fifth">
						<div class="event__desc">
							<span>Открылись филиалы в Кокшетау и Костанае.</span>
						</div>
					</div>
				</div>
			</div>
			<div class="event" id="year_2012">
				<div class="event__year_box">
					<h2 class="event__year">2012</h2>
				</div>
				<div class="event__scroll event__scroll--standart is_scrolled">
					<div class="event__desc_box event__desc_box--fifth">
						<div class="event__desc">
							<span>Осуществлено первое абонентское подключение с использованием технологии GPON.</span>
						</div>
					</div>
				</div>
			</div>
			<div class="bg_section__map">
				<div class="bg_section__map_dot bg_section__map_dot--01"></div>
				<div class="bg_section__map_dot bg_section__map_dot--02"></div>
				<div class="bg_section__map_dot bg_section__map_dot--03"></div>
				<div class="bg_section__map_dot bg_section__map_dot--04"></div>
				<div class="bg_section__map_dot bg_section__map_dot--05"></div>
				<div class="bg_section__map_dot bg_section__map_dot--06"></div>
				<div class="bg_section__map_dot bg_section__map_dot--07"></div>
				<div class="bg_section__map_dot bg_section__map_dot--08"></div>
				<div class="bg_section__map_dot bg_section__map_dot--09"></div>
				<div class="bg_section__map_dot bg_section__map_dot--11"></div>
				<div class="bg_section__map_dot bg_section__map_dot--12"></div>
				<div class="bg_section__map_dot bg_section__map_dot--13"></div>
				<div class="bg_section__map_dot bg_section__map_dot--14"></div>
				<div class="bg_section__map_dot bg_section__map_dot--15"></div>
				<div class="bg_section__map_dot bg_section__map_dot--16"></div>
				<div class="bg_section__map_dot bg_section__map_dot--17"></div>
				<div class="bg_section__map_dot bg_section__map_dot--18"></div>
				<div class="bg_section__map_dot bg_section__map_dot--19"></div>
			</div>
		</div>
	</section>
	<section class="bg_section__item bg_section__item--fourth" style="background: url(<?=SITE_TEMPLATE_PATH?>/images/about_section_04.jpg) no-repeat left bottom;">
		<div class="container history">
			<div class="event" id="year_2015">
				<div class="event__year_box">
					<h2 class="event__year event__year--white">2015</h2>
				</div>
				<div class="event__scroll event__scroll--2015 is_scrolled">
					<div class="event__desc_box">
						<div class="event__desc">
							<span>19-м городом присутствия «Алма-ТВ» стал Темиртау</span>
							<div class="event__gallery">
								<?//=Gallery('/upload/about/images/');?>
							</div>
						</div>
						<div class="event__desc">
							<span>
							На сегодняшний день «Алма-ТВ» — признанный лидер среди казахстанских операторов кабельного телевидения и передачи данных. Компания осуществляет ретрансляцию в цифровом формате более 130 каналов со всего мира – Европы, США, Японии, Южной Кореи, Китая, Турции, России и, конечно же, Казахстана. Пакеты программ формируются с учетом интересов самых разных слоёв населения, включая в себя фильмовые, детские, новостные, спортивные, научно-познавательные и музыкальные телевизионные каналы.
							</span>
						</div>
						<div class="event__desc">
							<span>
							Пользователи услуги нашего кабельного интернета могут оценить её удобства — возможность выбора скоростного режима, устойчивость соединения, доступные тарифы и отсутствие ограничения по трафику.
						</div>
					</div>
				</div>
			</div>
			<div class="bg_section__wave"></div>
			<div class="bg_section__wave_sun"></div>
			<div class="bg_section__lighthouse"></div>
			<div class="bg_section__gulls_left"></div>
			<div class="bg_section__gulls_right"></div>
		</div>
	</section>
	<section class="bg_section__item bg_section__item--fifth" style="background: url(<?=SITE_TEMPLATE_PATH?>/images/about_section_05.jpg) no-repeat left bottom;">
		<div class="container history">
			<div class="event" id="year_infinity">
				<div class="event__year_box">
					<h2 class="event__year event__year--blue">Перспективы</h2>
				</div>
				<div class="event__scroll event__scroll--infinity is_scrolled">
					<div class="event__desc_box event__desc_box--seven">
						<div class="event__desc">
							<span>
							Уже сейчас услуги цифрового телевещания «Алма-ТВ» доступны как абонентам кабельной сети, так и пользователям индивидуальных приёмных антенн в частном секторе. В наших ближайших планах не только дальнейшее расширение географии своего присутствия, но и, в первую очередь, техническое переоснащение компании с использованием последних достижений в сфере телекоммуникационных технологий. Также в перспективе возможность приобретение клиентами отдельных каналов и предоставление услуг интерактивного заказа – видео по предуведомлению, интересующие новости в удобное время, дистанционное обучение и т. п.
							</span>
						</div>
						<div class="event__desc">
							<span>
							Всё это в совокупности со сбалансированной ценовой политикой, высоким качеством услуг и уровнем сервисного обслуживания клиентов позволит нам в полной мере реализовать корпоративную стратегию «АЛМА-ТВ – в каждый дом!» и станет нашим главным преимуществом в дальнейшей борьбе за сохранение и упрочение ведущих позиций на казахстанском телекоммуникационном рынке.
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="bg_section__planet"></div>
			<div class="bg_section__stars_toleft"></div>
			<div class="bg_section__stars_toright"></div>
		</div>
	</section>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>