<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>
<?		
if(!CModule::IncludeModule("iblock"))
	return;

$id_paskage = intval($_REQUEST['package_id']); //142
$id_category = intval($_REQUEST['category_id']); //5

if (($id_paskage <= 0) && ($id_category <= 0))
{
	die();
}
flush();
	
$ID_CHANNELS = 9;
$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE");   //PRICE
$arFilter = Array("IBLOCK_ID"=>$ID_CHANNELS, /*"PROPERTY_PACKAGES" => $id_paskages, */"ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");  //фильтр по городу
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$arChannels = array();
//$i = 0;
while($ob = $res->GetNext())
{
	$image = CFile::ResizeImageGet(
		$ob['PREVIEW_PICTURE'],
		array("width" => 35, "height" =>35),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		true,
		false
	);

	$ob['PREVIEW_PICTURE'] = $image['src'];
	$ob['F'] = 0;
	$arrChannelAll[] = $ob;
	//PR($ob);
}

$arSelect = Array("ID", "NAME", /*"PREVIEW_PICTURE", */"PROPERTY_PACKAGES", "IBLOCK_SECTION_ID");   //PRICE
$arFilter = Array("IBLOCK_ID"=>$ID_CHANNELS, "PROPERTY_PACKAGES" => $id_paskage, "SECTION_ID" => $id_category, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");  //фильтр по городу
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$arChannels = array();
$count = 0;
while($ob = $res->GetNext())
{
	$count++;
	$arrChannelFilter[] = $ob;
	//PR($ob);
}

foreach ($arrChannelAll as &$channel)
{
	//PR($channel);
	foreach ($arrChannelFilter as $channelF)
	{
		if ($channel['ID'] == $channelF['ID'])
		{
			$channel['F'] = 1;
			break;
		}
	}
}

//PR(count($arrChannelAll));
/*
Выводим все каналы 1 запросом.
Вторым запросом выводим каналы в пакете для категории, сравниваем если не 
*/?>			

<div class="tarif_popup_holder j-tarif_popup_slider">
<?foreach ($arrChannelAll as $key => $channel) {?>
	<?if (($key % 3) == 0) { ?>
		<div class="tarif_popup__slider_item">
	<?}?>
		<?if ($channel['F']) {?>
			<div class="tarif_popup__list_item">
				<div class="tarif_popup__cont">
					<div class="tarif_popup__list_img">
						<?if (!empty($channel['PREVIEW_PICTURE'])) {?>
							<img src="<?=$channel['PREVIEW_PICTURE']?>" alt="<?=$channel['NAME']?>">
						<?} else {?>
							<div class="tarif_popup__list_no_img">
								<span><?=substr($channel['NAME'], 0, 1)?></span>
							</div>
						<?}?>
					</div>
					<a href="/channel/<?=$channel['ID']?>/" class="tarif_popup__list_title"><?=$channel['NAME']?></a>
				</div>
			</div>
		<?} else {?>
			<div class="tarif_popup__list_item tarif_popup__list_item--not_available">
				<div class="tarif_popup__cont">
					<div class="tarif_popup__list_img">
						<?if (!empty($channel['PREVIEW_PICTURE'])) {?>
							<img src="<?=$channel['PREVIEW_PICTURE']?>" alt="<?=$channel['NAME']?>">
						<?} else {?>
							<div class="tarif_popup__list_no_img">
								<span><?=substr($channel['NAME'], 0, 1)?></span>
							</div>
						<?}?>
					</div>
					<a href="/channel/<?=$channel['ID']?>/" class="tarif_popup__list_title"><?=$channel['NAME']?></a>
				</div>
				<div class="tarif_popup__list_available">
				
					<span>Доступен в пакетах <span class="tarif_popup__available_pack">TV100+</span> и <span class="tarif_popup__available_pack tarif_popup__available_pack--orange">TV MAX</span></span>
				
				</div>
			</div>
		<?}?>
	<?if (($key % 3) == 2) { ?>
	</div>
	<?}?>
<?}?>

<?if ((count($arrChannelAll) % 3) != 0) {?>
	</div>
<?}?>
</div>
<div class="tarif_popup_slider__paging"></div>
<div class="tarif_popup_slider__arrows tarif_popup_slider__left"></div>
<div class="tarif_popup_slider__arrows tarif_popup_slider__right"></div>
