<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Телевидение");
?>

<script src="<?=SITE_TEMPLATE_PATH?>/js/tv.js"></script>

<script>
	$(document).on('mouseenter', '.tarif_popup__list_item--not_available', function()
	{
		$content_packaged = $(this).find('.tarif_popup__list_available').text();
		if ($content_packaged == '')
		{
			$type_id = $('.tarif_cat__trigger_holder').find('.tarif_cat__trigger_item.active').data('id');
			$packages = $('.tarif_row__table .highlight').data('packages');
			$package_ = $('.tarif_row__table .highlight').data('package');
			
			$arr = $packages.split(',');
			var a = [];
			$.each($arr, function( i, val ) {
				if (val != $package_)
				{
					a.push(val);
				}
			});

			$(this).addClass('ajax_content');
			$_this = this;
			$.ajax({
				type: 'POST',
				url: '/ajax/packages.php',
				data: ({channel_id: $($_this).data('channel'), package_id: $package_, type: $type_id, packages_id: JSON.stringify(a)}),
				success: function(ob)
				{
					$($_this).find('.tarif_popup__list_available').html(ob);
				}
			});
		}
	});
</script>

<?
$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"bannerTV", 
	array(
		"IBLOCK_TYPE" => "-",
		"IBLOCK_ID" => "36",
		"NEWS_COUNT" => "2",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "DETAIL_PICTURE",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "BUTTON",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "500",
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PARENT_SECTION" => "",
		"COMPONENT_TEMPLATE" => "bannerTV",
		"SET_LAST_MODIFIED" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);
?>
<?/*$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"tv", 
	array(
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"USE_SHARE" => "Y",
		"SHARE_HIDE" => "N",
		"SHARE_TEMPLATE" => "",
		"SHARE_SHORTEN_URL_LOGIN" => "",
		"SHARE_SHORTEN_URL_KEY" => "",
		"IBLOCK_ID" => "20",
		"ELEMENT_ID" => "109",
		"CHECK_DATES" => "Y",
		"FIELD_CODE" => array(
			0 => "",
			1 => "PREVIEW_PICTURE",
			2 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "N",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"META_DESCRIPTION" => "-",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"USE_PERMISSIONS" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "N",
		"DISPLAY_TOP_PAGER" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Страница",
		"PAGER_TEMPLATE" => "",
		"PAGER_SHOW_ALL" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"IBLOCK_TYPE" => "news",
		"ELEMENT_CODE" => "",
		"IBLOCK_URL" => "",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_MODE" => "N",
		"GROUP_PERMISSIONS" => array(
			0 => "2",
		)
	),
	false
);*/?>

<div class="main container" role="main">
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"tvOpportunities", 
	array(
		"IBLOCK_TYPE" => "news",
		"IBLOCK_ID" => "21",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "500",
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PARENT_SECTION" => "",
		"COMPONENT_TEMPLATE" => "tvOpportunities",
		"SET_LAST_MODIFIED" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>
</div>

	<?$APPLICATION->IncludeComponent(
		"9lines:tv.packages", 
		"", 
		array(
			"FILTER" => array(
				"UF_CITIES" => getCity()
			)
		
		)
	  );
	?>

		<div class="container">
			<div class="tv_super_holder">
				<div class="clear add_eq_holder">
					<?global $arrFilter;
						$cityId = CITY_ID_ALMATA;
						if ($_COOKIE['City'])
						{
							$arrFilter['PROPERTY_CITY'] = $_COOKIE['City'];
							$cityId = $_COOKIE['City'];
						}
						else
						{
							$arrFilter['PROPERTY_CITY'] = $_REQUEST['city'];
							$cityId = $_REQUEST['city'];
						}
					?>
					<?$APPLICATION->IncludeComponent(
						"bitrix:news.list", 
						"tvAdditionalPackages", 
						array(
							"CITY_ID" => $cityId, //для правильной работы кеша по городам
							"IBLOCK_ID" => "25",
							"NEWS_COUNT" => "2",
							"SORT_BY1" => "SORT",
							"SORT_ORDER1" => "DESC",
							"FILTER_NAME" => "arrFilter",
							"FIELD_CODE" => array(
								0 => "DETAIL_PICTURE",
								1 => "",
							),
							"PROPERTY_CODE" => array(
								0 => "",
								1 => "TYPE",
								2 => "",
							),
							"CHECK_DATES" => "Y",
							"DETAIL_URL" => "",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"AJAX_OPTION_HISTORY" => "N",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "N",
							"PREVIEW_TRUNCATE_LEN" => "",
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							"SET_TITLE" => "N",
							"SET_BROWSER_TITLE" => "N",
							"SET_META_KEYWORDS" => "N",
							"SET_META_DESCRIPTION" => "N",
							"SET_STATUS_404" => "N",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"ADD_SECTIONS_CHAIN" => "N",
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",
							"PARENT_SECTION_CODE" => "",
							"INCLUDE_SUBSECTIONS" => "N",
							"DISPLAY_DATE" => "N",
							"DISPLAY_NAME" => "N",
							"DISPLAY_PICTURE" => "N",
							"DISPLAY_PREVIEW_TEXT" => "N",
							"PAGER_TEMPLATE" => ".default",
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "N",
							"PAGER_TITLE" => "",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"PARENT_SECTION" => "",
							"SORT_BY2" => "NAME",
							"SORT_ORDER2" => "ASC",
							"IBLOCK_TYPE" => "news"
						),
						false
					);?>
						
					<?$APPLICATION->IncludeComponent(
						"bitrix:news.list", 
						"tvAdditionalEquipment", 
						array(
							"TYPE_1" => count($arResult['TYPE_1']),
							"TYPE_2" => count($arResult['TYPE_2']),
							"IBLOCK_ID" => "26",
							"NEWS_COUNT" => "1",
							"SORT_BY1" => "SORT",
							"SORT_ORDER1" => "DESC",
							"FILTER_NAME" => "arrFilter",
							"FIELD_CODE" => array(
								0 => "",
								1 => "",
							),
							"PROPERTY_CODE" => array(
								0 => "K_PREVIEW_RU",
								1 => "K_PRICE",
								2 => "D_PREVIEW_RU",
								3 => "D_PRICE",
								4 => "D_HREF",
							),
							"CHECK_DATES" => "Y",
							"DETAIL_URL" => "",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "N",
							"AJAX_OPTION_HISTORY" => "N",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "N",
							"PREVIEW_TRUNCATE_LEN" => "",
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							"SET_TITLE" => "N",
							"SET_BROWSER_TITLE" => "N",
							"SET_META_KEYWORDS" => "N",
							"SET_META_DESCRIPTION" => "N",
							"SET_STATUS_404" => "N",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"ADD_SECTIONS_CHAIN" => "N",
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",
							"PARENT_SECTION_CODE" => "",
							"INCLUDE_SUBSECTIONS" => "N",
							"DISPLAY_DATE" => "N",
							"DISPLAY_NAME" => "N",
							"DISPLAY_PICTURE" => "N",
							"DISPLAY_PREVIEW_TEXT" => "N",
							"PAGER_TEMPLATE" => ".default",
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "N",
							"PAGER_TITLE" => "",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"PARENT_SECTION" => "",
							"SORT_BY2" => "NAME",
							"SORT_ORDER2" => "ASC"
						),
						false
					);?>
				</div>
				<?
				global $arrFilterExt;
				$arrFilterExt['PROPERTY_EXT'] = 18; //дополнительный
				
				if ($_COOKIE['City'])
				{
					$arrFilterExt['PROPERTY_CITY'] = $_COOKIE['City'];
				}
				else
				{
					$arrFilterExt['PROPERTY_CITY'] = $_REQUEST['city'];
				}
				?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:news.list", 
					"TVextra", 
					array(
						"CITY_ID" => $cityId, //для правильной работы кеша по городам
						"IBLOCK_TYPE" => "news",
						"IBLOCK_ID" => "10",
						"NEWS_COUNT" => "4",
						"SORT_BY1" => "SORT",
						"SORT_ORDER1" => "DESC",
						"FILTER_NAME" => "arrFilterExt",
						"FIELD_CODE" => array(
							0 => "",
							1 => "DETAIL_PICTURE",
							2 => "",
						),
						"PROPERTY_CODE" => array(
							0 => "TYPE",
							1 => "",
						),
						"CHECK_DATES" => "Y",
						"DETAIL_URL" => "",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "N",
						"PREVIEW_TRUNCATE_LEN" => "",
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"SET_TITLE" => "N",
						"SET_BROWSER_TITLE" => "Y",
						"SET_META_KEYWORDS" => "Y",
						"SET_META_DESCRIPTION" => "Y",
						"SET_STATUS_404" => "Y",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
						"ADD_SECTIONS_CHAIN" => "Y",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"PARENT_SECTION_CODE" => "",
						"INCLUDE_SUBSECTIONS" => "Y",
						"DISPLAY_DATE" => "Y",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"PAGER_TEMPLATE" => ".default",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"PAGER_TITLE" => "",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"PARENT_SECTION" => "",
						"SORT_BY2" => "NAME",
						"SORT_ORDER2" => "ASC"
					),
					false
				);?>
			
				<?if (isInetCity($_COOKIE["City"])) {
				$APPLICATION->IncludeComponent(
					"bitrix:main.include", 
					".default", 
					array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_TEMPLATE_PATH."/include/tv_inet.php",
						"EDIT_TEMPLATE" => ""
					),
					false
				);	
			}?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>