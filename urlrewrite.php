<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/presscenter/news/([a-z -_A-Z0-9]+)/#",
		"RULE" => "ID=\$1",
		"PATH" => "/presscenter/news/index.php",
	),
	array(
		"CONDITION" => "#^/vacancies/([0-9]+)/.*#",
		"RULE" => "ELEMENT_ID=\$1",
		"PATH" => "/vacancies/index.php",
	),
	array(
		"CONDITION" => "#^/tariffs/([0-9]+)/.*#",
		"RULE" => "ELEMENT_ID=\$1",
		"PATH" => "/tariffs/index.php",
	),
	array(
		"CONDITION" => "#^/channel/([0-9]+)/.*#",
		"RULE" => "ELEMENT_ID=\$1",
		"PATH" => "/channel/index.php",
	),
	array(
		"CONDITION" => "#^/vacancies/#",
		"RULE" => "",
		"ID" => "bitrix:news.list",
		"PATH" => "/vacancies/index.php",
	),
	array(
		"CONDITION" => "#^/internet/#",
		"RULE" => "",
		"ID" => "bitrix:news.list",
		"PATH" => "/internet/index.php",
	),
	array(
		"CONDITION" => "#^/services/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/services/index.php",
	),
	array(
		"CONDITION" => "#^/products/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/products/index.php",
	),
	array(
		"CONDITION" => "#^/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/news/index.php",
	),
);

?>