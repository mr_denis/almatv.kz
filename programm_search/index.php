<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "телевидение, интернет");
$APPLICATION->SetPageProperty("description", "AlmaTV");
$APPLICATION->SetTitle("Alma TV");
?>

<script src="<?=SITE_TEMPLATE_PATH?>/js/search.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/tv.js"></script>

<h1>Телепрограмма — Результаты поиска</h1>
<div class="main_search">
	<form action="#">
		<fieldset>
			<div class="main_search__row clear">
				<input type="text" class="main_search__field" maxlength="100" value="">
				<input type="submit" class="main_search__btn">
			</div>
		</fieldset>
	</form>
</div>

<div id = "result_content"></div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>