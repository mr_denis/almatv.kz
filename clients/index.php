<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Клиентам");
?>

		<h1>Клиентам</h1>
		<ul class="service_list">
			<li class="service_list__item">
				<div class="service_list__serv service_list__serv--tv">
					<div class="service_list__img">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/clients_tv.jpg" height="158" width="140" alt="image_description">
					</div>
					<h2 class="service_list__title">
						<a href="/tv/" class="service_list__title_link">Телевидение</a>
					</h2>
					<span class="service_list__desc">Мир кино и путешествий, спорт и наука, мода и музыка, новости и политика, все для детей и тематические каналы — все это и многое другое на экранах ваших телевизоров в любое время.</span>
				</div>
			</li> 
			<li class="service_list__item">
				<div class="service_list__serv service_list__serv--inet">
					<div class="service_list__img service_list__img--inet">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/clients_inet.jpg" height="124" width="177" alt="image_description">
					</div>
					<h2 class="service_list__title">
						<a href="/internet/" class="service_list__title_link">Интернет</a>
					</h2>
					<span class="service_list__desc">С Домашним Интернетом от Алма ТВ вы сможете воспользоваться всеми преимуществами выделенного интернет-канала.</span>
				</div>
			</li>
			<!--<li class="service_list__item">
				<div class="service_list__serv service_list__serv--bussines">
					<div class="service_list__img service_list__img--bussines">
						<img src="<?=SITE_TEMPLATE_PATH?>/images/clients_bussines.jpg" height="117" width="140" alt="image_description">
					</div>
					<h2 class="service_list__title">
						<a href="#" class="service_list__title_link">Для бизнеса</a>
					</h2>
					<span class="service_list__desc">Мы предлагаем выгодные решения для бизнеса по самым привлекательным ценам. Рассчет тарифов происходит индивидуально.</span>
				</div>
			</li>-->
		</ul>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>