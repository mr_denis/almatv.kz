<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Интернет");
?><h1>Интернет</h1>

<?$isInetCity = isInetCity($_COOKIE["City"])?>
<?if ($isInetCity) {?>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include/tarif.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
<?} else {
	$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include/no_tarif.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);
}?>
 
	<?
	global $arrFilter;
	/*if ($_COOKIE['City'])
	{
		$arrFilter['PROPERTY_CITY'] = $_COOKIE['City'];
	}
	else
	{
		$arrFilter['PROPERTY_CITY'] = $_REQUEST['City'];
	}*/
	$arrFilter['PROPERTY_CITY'] = cityId();
	$arrFilter['!PROPERTY_COMBO_VALUE'] = "Y";
	//PR($arrFilter);
	?> 
	<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"tariff", 
	array(
		"IBLOCK_ID" => "18",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"FILTER_NAME" => "arrFilter",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "V_EXT",
			1 => "MAIN",
			2 => "RECOMMEND",
			3 => "V_INN",
			4 => "PURCHASE",
			5 => "RENT",
			6 => "ANSWER",
			7 => "V",
			8 => "TECH",
			9 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_STATUS_404" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"PARENT_SECTION" => "",
		"SORT_BY2" => "NAME",
		"SORT_ORDER2" => "ASC",
		"IBLOCK_TYPE" => "news",
		"COMPONENT_TEMPLATE" => "tariff",
		"SET_LAST_MODIFIED" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_TEMPLATE_PATH."/include/tv_inet.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>

<?
/*<div class="vert_slide__item" id="vert_slide__item_obor">
	<div class="inet_list__item_img">
		<img class="inet_list__item_img1024" src="/bitrix/templates/almatv/images/vert-pic-1.jpg" alt="">
		<img class="inet_list__item_img1600" src="/bitrix/templates/almatv/images/vert-pic-1.jpg" alt="">
	</div>
	<h2 class="inet_list__title head">Оборудование</h2>
	<span class="inet_list__desc">Для организации доступа к сети передачи данных, Вам потребуется взять в аренду или приобрести модем. В зависимости от района подключения и тарифа, устройство подготовлено для сетей.</span>
	<span class="inet_list__info_link">
		<a href="#!" class="rotete-elem" data-rotete="rotete-docsis">DOCSIS</a>
		или
		<a href="#!" class="rotete-elem" data-rotete="rotete-gpon">GPON</a>
	</span>
</div>
<div class="vert_slide__item" id="vert_slide__item_docsis">
	<div class="inet_list__item_img">
		<img class="inet_list__item_img1024" src="/bitrix/templates/almatv/images/docsis-pic-2.jpg" alt="">
		<img class="inet_list__item_img1600" src="/bitrix/templates/almatv/images/docsis-pic-2.jpg" alt="">
	</div>
	<span class="inet_list__info_link">
		<a href="#!" class="rotete-elem" data-rotete="rotete-docsis">DOCSIS</a>
		<span class="rotete-elem--opacity">или
		<a href="#!" class="rotete-elem" data-rotete="rotete-gpon">GPON</a></span>
	</span>
	<span class="inet_list__desc">Модем <strong>Cisco EPC3208G</strong> с поддержкой EuroDOCSIS 3.0
	<br>Необходим при подключении <strong>NET I</strong>, <strong>NET II</strong> и <strong>NET III</strong>
	<br>Стоимость модема: <strong>8 700 тг</strong>.
	<br>Модем <strong>Thomson TCM420</strong> с под. EuroDOCSIS 2.0
	<br>Необходим при подключении <strong>NET</strong>
	<br>Стоимость модема: <strong>5 100 тг</strong>.</span>
</div>
<div class="vert_slide__item" id="vert_slide__item_gpon">
	<div class="inet_list__item_img">
		<img class="inet_list__item_img1024" src="/bitrix/templates/almatv/images/gpon-pic-3.jpg" alt="">
		<img class="inet_list__item_img1600" src="/bitrix/templates/almatv/images/gpon-pic-3.jpg" alt="">
	</div>
	<span class="inet_list__info_link">
		<span class="rotete-elem--opacity"><a href="#!" class="rotete-elem" data-rotete="rotete-docsis">DOCSIS</a>
		или</span>
		<a href="#!" class="rotete-elem" data-rotete="rotete-gpon">GPON</a>
	</span>
	<span class="inet_list__desc">Модем <strong>Alcatel Lucent I-021G-P</strong>
	<br>Используется в пакетах <strong>NET I</strong>, <strong>NET II</strong> и <strong>NET III</strong>
	<br>Существует возможность аренды с послед. выкупом
	<ul class="inet_list__rotete_list">
		<li>* на 5 лет, оплачивая <strong>240 тг</strong>. в месяц,</li>
		<li>* на 3 года, оплачивая <strong>400 тг</strong>. в месяц,</li>
		<li>* на 1 год, оплачивая <strong>1 200 тг</strong>. в месяц.</li>
	</ul>
	Стоимость модема: <strong>14 400 тг</strong>.</span>
</div>*/
?>
<div class="tarif_two_cols">
	<div class="inet_list__item inet_list__item--vert">
		<div class="inet_item__rotete rotete-start">
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include", 
				".default", 
				array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_TEMPLATE_PATH."/include/equipment.php",
					"EDIT_TEMPLATE" => ""
				),
				false
			);?>
		</div>
	</div>
	<?/*<div class="static_ip">
<h3 class="static_ip__title head">Статический IP</h3>
<span class="static_ip__text">Статический IP-адрес может понадобиться Вам для организации на своем компьютере сервисов, доступных из публичных сетей Интернет. Например, для организации почтового сервера или web-ресурса.</span> <a href="/internet/static_ip/" class="static_ip__btn button button--no_width">Подробнее</a>
</div>*/?>
	<div class="tarif_two_cols__right">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include", 
			".default", 
			array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH."/include/static_ip.php",
				"EDIT_TEMPLATE" => ""
			),
			false
		);?>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>